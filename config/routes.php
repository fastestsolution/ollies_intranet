<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass('DashedRoute');
Router::extensions(['json', 'xml']);

Router::scope('/api/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Apis','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Apis']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
});

Router::scope('/api/attendances/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Attendances','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Attendances']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
});
Router::scope('/api/orders/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Orders','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Orders']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});
Router::scope('/api/deadlines/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Deadlines','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Deadlines']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
});
Router::scope('/api/map_areas/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'MapAreas','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'MapAreas']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});

Router::scope('/summary_statistics/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'SummaryStatistics','action'=>'product_stats']);
    $routes->connect('/:action/*', ['controller' => 'SummaryStatistics']);
});

Router::scope('/api/stocks/', function (RouteBuilder $routes) {
    $routes->connect('/*', ['controller' => 'Stocks', 'action' => 'getDataOllies']);
    //$routes->connect('/:action/*', ['controller' => 'Stocks']);
});

Router::scope('/api/stocks/products/', function (RouteBuilder $routes) {
    $routes->connect('/*', ['controller' => 'Stocks', 'action' => 'getDataOlliesProducts']);
});

Router::scope('/statistics/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Statistics','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Statistics','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'Statistics']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
});

Router::scope('/users/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Users','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Users']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});


Router::scope('/products/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Products','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Products']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});

Router::scope('/product_groups/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'ProductGroups','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'ProductGroups']);
});

Router::scope('/product_group_printers/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'ProductGroupPrinters','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'ProductGroupPrinters']);
});

Router::scope('/product-addons/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'ProductAddons','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'ProductAddons']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});


Router::scope('/mapareas/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'MapAreas','action'=>'defineMap']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    //$routes->connect('/:action/*', ['controller' => 'Statistics']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});

Router::scope('/attendances/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Attendances','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    //$routes->connect('/:action/*', ['controller' => 'IntranetActuals']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/orders/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Orders','action' => 'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'Orders']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});
Router::scope('/orders-web/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Orders', 'action' => 'ordersWeb']);
    $routes->connect('/:action/*', ['controller' => 'Orders']);
});
Router::scope('/work-shop/', function (RouteBuilder $routes) {
    $routes->connect('/show/', ['controller' => 'WorkShop', 'action' => 'show']);
    $routes->connect('/:action/*', ['controller' => 'WorkShop']);
});
Router::scope('/deadlines/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Deadlines','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'Deadlines']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});
Router::scope('/sendSms/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'SendSms','action'=>'send']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'SendSms']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/intranet/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'IntranetActuals','action'=>'index2']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'IntranetActuals']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/stock-mirrors/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'StockMirrors','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'StockMirrors']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/gps/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Pages','action'=>'logGpsTemp']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'IntranetActuals']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});



Router::scope('/intranetEdit/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'IntranetActuals','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'IntranetActuals']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});


Router::scope('/stocks/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Stocks','action'=> 'index']);
    $routes->connect('/:action/*', ['controller' => 'Stocks']);
});

Router::scope('/fst_upload/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'IntranetActuals','action'=>'fst_upload']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'IntranetActuals']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/stocks-items/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'StocksItems','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'StocksItems']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});
Router::scope('/system-logs/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'SystemLogs','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'SystemLogs']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});
Router::scope('/stocks-list/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'StockList','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'StockList']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});
Router::scope('/global-items/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'StockGlobalItems','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'StockGlobalItems']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});
Router::scope('/settings/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Settings','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'Settings']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});
Router::scope('/statistics/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Statistics','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'Statistics']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});
Router::scope('/branches/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Branches','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'Branches']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});

Router::scope('/branches-stats/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'BranchesStats','action'=>'index']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    $routes->connect('/:action/*', ['controller' => 'BranchesStats']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);

});


Router::scope('/status-online/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Pages','action'=>'statusOnline']);
    $routes->connect('/switch/*', ['controller' => 'Pages','action'=>'switchOnline']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    //$routes->connect('/:action/*', ['controller' => 'Statistics']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/', function (RouteBuilder $routes) { 
    //$routes->connect('/', ['controller' => 'Pages','action'=>'homepage']);
    $routes->connect('/', ['controller' => 'Statistics','action'=>'index']);
	$routes->connect('/login/', ['controller' => 'Users','action'=>'login']);
    $routes->connect('/logout/', ['controller' => 'Users','action'=>'logout']);
    $routes->connect('/api/products', ['controller' => 'Products','action'=>'apiProducts']);
    $routes->connect('/api/product_groups', ['controller' => 'ProductGroups','action'=>'apiProductGroups']);
    $routes->connect('/trash/*', ['controller' => 'Stocks', 'action' => 'trash']);
    $routes->connect('/sql/:action/*', ['controller' => 'Schemas']);
    //$routes->connect('/:system_id/', ['controller' => 'Mapareas','action'=>'index'],[ 'pass' => ['system_id']]);
    //$routes->connect('/:action/*', ['controller' => 'Statistics']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    $routes->connect('/get_web_orders/*', ['controller' => 'Orders','action'=>'getOrdersFromWeb']);
    $routes->connect('/send_web_data/*', ['controller' => 'Stocks','action'=>'saveProductForWeb']);

});

Plugin::routes();
