<?php
/**
 * vytvori soubor db_config.php dle ENV promenych z pipeline, moznost vytvorit rucne s timto PHP
 * 
 * 
*<?php return array (
 * 'SQL_HOST' => '109.123.216.45',
 * 'SQL_USERNAME' => 'c1_default_test',
 * 'SQL_PASSWORD' => 'zJhxjsTY@5UB',
 * 'SQL_DATABASE' => 'c1_intr',
 * 'SQL_SECONDARY_HOST' => '109.123.216.45',
 * 'SQL_SECONDARY_USERNAME' => 'c1_ollies',
 * 'SQL_SECONDARY_PASSWORD' => 'qtYysYY@9Xn',
 * 'SQL_SECONDARY_DATABASE' => 'c1_ollies',
*); ?>
 */


$sql = [
    'SQL_HOST'=>'{{SQL_HOST}}',
    'SQL_USERNAME'=>'{{SQL_USERNAME}}',
    'SQL_PASSWORD'=>'{{SQL_PASSWORD}}',
    'SQL_DATABASE'=>'{{SQL_DATABASE}}',
];
$sql_secondary = [
    'SQL_SECONDARY_HOST'=>'{{SQL_SECONDARY_HOST}}',
    'SQL_SECONDARY_USERNAME'=>'{{SQL_SECONDARY_USERNAME}}',
    'SQL_SECONDARY_PASSWORD'=>'{{SQL_SECONDARY_PASSWORD}}',
    'SQL_SECONDARY_DATABASE'=>'{{SQL_SECONDARY_DATABASE}}',
];
$pathConfig = "config/db_config.php";

// DEV prostredi
if ($_ENV['CONFIG_ENV'] == 'DEV'){
    if (isset($_ENV['SQL_HOST_DEV'])){
        $sql['SQL_HOST'] = $_ENV['SQL_HOST_DEV'];
    }
    if (isset($_ENV['SQL_NAME_DEV'])){
        $sql['SQL_USERNAME'] = $_ENV['SQL_NAME_DEV'];
    }
    if (isset($_ENV['SQL_PASSWORD_DEV'])){
        $sql['SQL_PASSWORD'] = $_ENV['SQL_PASSWORD_DEV'];
    }
    if (isset($_ENV['SQL_DB_DEV'])){
        $sql['SQL_DATABASE'] = $_ENV['SQL_DB_DEV'];
    }    

    if (isset($_ENV['SQL_SECONDARY_HOST_DEV'])){
        if (isset($_ENV['SQL_SECONDARY_HOST_DEV'])){
            $sql2['SQL_SECONDARY_HOST'] = $_ENV['SQL_SECONDARY_HOST_DEV'];
        }
        if (isset($_ENV['SQL_SECONDARY_NAME_DEV'])){
            $sql2['SQL_SECONDARY_USERNAME'] = $_ENV['SQL_SECONDARY_NAME_DEV'];
        }
        if (isset($_ENV['SQL_SECONDARY_PASSWORD_DEV'])){
            $sql2['SQL_SECONDARY_PASSWORD'] = $_ENV['SQL_SECONDARY_PASSWORD_DEV'];
        }
        if (isset($_ENV['SQL_SECONDARY_DB_DEV'])){
            $sql2['SQL_SECONDARY_DATABASE'] = $_ENV['SQL_SECONDARY_DB_DEV'];
        }    
        $sql = array_merge($sql, $sql2);
    } 
} 
 
// PROD prostredi
else if ($_ENV['CONFIG_ENV'] == 'PROD'){
    if (isset($_ENV['SQL_HOST_PROD'])){
        $sql['SQL_HOST'] = $_ENV['SQL_HOST_PROD'];
    }
    if (isset($_ENV['SQL_NAME_DEV'])){
        $sql['SQL_USERNAME'] = $_ENV['SQL_NAME_PROD'];
    }
    if (isset($_ENV['SQL_PASSWORD_DEV'])){
        $sql['SQL_PASSWORD'] = $_ENV['SQL_PASSWORD_PROD'];
    }
    if (isset($_ENV['SQL_DB_DEV'])){
        $sql['SQL_DATABASE'] = $_ENV['SQL_DB_PROD'];
    } 

    if (isset($_ENV['SQL_SECONDARY_HOST_PROD'])){
        if (isset($_ENV['SQL_SECONDARY_HOST_PROD'])){
            $sql2['SQL_SECONDARY_HOST'] = $_ENV['SQL_SECONDARY_HOST_PROD'];
        }
        if (isset($_ENV['SQL_SECONDARY_NAME_PROD'])){
            $sql2['SQL_SECONDARY_USERNAME'] = $_ENV['SQL_SECONDARY_NAME_PROD'];
        }
        if (isset($_ENV['SQL_SECONDARY_PASSWORD_PROD'])){
            $sql2['SQL_SECONDARY_PASSWORD'] = $_ENV['SQL_SECONDARY_PASSWORD_PROD'];
        }
        if (isset($_ENV['SQL_SECONDARY_DB_PROD'])){
            $sql2['SQL_SECONDARY_DATABASE'] = $_ENV['SQL_SECONDARY_DB_PROD'];
        }    
        $sql = array_merge($sql, $sql2);
    }    
}


if (in_array($_ENV['CONFIG_ENV'], ['PROD','DEV'])){
        
    $fileData = "<?php return ".var_export($sql, true)."; ?>";
    print_r($fileData);

    $myfile = fopen($pathConfig, "w") or die("Unable to open file ".$pathConfig."!");
    fwrite($myfile, $fileData);
    echo 'Soubor vytvoren: '.$myfile;
} else {
    echo '!!!!!!! Neni definovano prostredi pro BUILD !!!!!!!!';
}