<?php
namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;

class WebBranchesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fastest__shop_dopravas');
    }

    public function getList()
    {
        $data = $this->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'            
        ])
        ->cache(function ($query) {
            return 'webBranchesList';
        })
        ->order('name ASC')
        ->toArray();

        return $data;
    }

    public static function defaultConnectionName()
    {
        return 'web';
    }
}