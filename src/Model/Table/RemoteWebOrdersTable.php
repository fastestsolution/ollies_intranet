<?php
namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;

class RemoteWebOrdersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('fastest__shop_orders');
        $this->HasMany('RemoteWebOrderItems', ['foreignKey' => 'shop_order_id']);
        $this->belongsTo('RemoteWebClients', ['foreignKey' => 'shop_client_id']);
        $this->belongsTo('WebBranches', ['foreignKey' => 'shop_doprava_id']);
    }
    
    public static function defaultConnectionName()
    {
        return 'web';
    }
}