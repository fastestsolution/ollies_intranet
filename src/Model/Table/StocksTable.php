<?php

namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;


class StocksTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
		$this->belongsTo("Products",['foreignKey'=>'stock_item_product_id']);
		$this->belongsTo("StockGlobalItems",['foreignKey'=>'stock_item_id']);

        //$this->table('users');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');
        //$this->addBehavior('Trash'); 
    }
  
	/**
	 * seznam pro zrcadlo
	 */
	public function stockMirrorList($system_id=null){ 
		
		//if (($dodavatel_list = Cache::read('dodavatel_list')) === false) {
        // $StocksItems = ClassRegistry::init('StocksItems');
        $Products = TableRegistry::get('Products');
        $productsIds = $this->Products->find()
        ->contain(['ProductRecipes'])
        ->select(['id'])
        ->where('is_mirror')->toArray();
        if ($productsIds){
            $listProductsGlobal = [];
            foreach($productsIds AS $pid){
                if ($pid->product_recipes){
                    foreach($pid->product_recipes AS $pr){
                        $listProductsGlobal[] = $pr->stock_item_global_id;
            
                    }
                    
                }
            }
        }
        // pr($listProductsGlobal);die();
        $StockGlobalItems = TableRegistry::get('StockGlobalItems');
        $this->stockItemsList = $StockGlobalItems->skladItems($listProductsGlobal);
        $ids = [];
        foreach($this->stockItemsList AS $k=>$sl){
            $ids[] = $k;
        }
        //pr($ids);
        $conditions = [
            'stock_item_id IN' =>$ids,

        ];
        if ($system_id){
            $conditions['system_id'] = $system_id;
        }
        // pr($conditions);die();
		$items_list_load = $this->find()
            ->where($conditions)
            ->contain('StockGlobalItems')
			->select([
                'id',
                'stock_item_id',
                'stock_type_id',
                'value',
                'StockGlobalItems.id',
                'StockGlobalItems.name',
                'StockGlobalItems.jednotka_id',
            ])
            ->order('StockGlobalItems.name ASC')
			// ->limit(50)
			// ->group('stock_global_item_id')
			->toArray();
			
			//Cache::write('dodavatel_list', $dodavatel_list);
        //}
        
        $items_list = [];
        foreach($items_list_load AS $l){
            if (!isset($items_list[$l->stock_item_id])){
                $items_list[$l->stock_item_id] = $l;
                $items_list[$l->stock_item_id]['sum'] = 0;
            }
            // 1=>'Příjem',
            // 2=>'Převodka',
            // 3=>'Odpis',
            // 4=>'Prodej',
            // 5=>'Zrcadlo plus',
            // 6=>'Zrcadlo minus',
            // 7=>'Příjem Makro',
            // 8=>'Příjem BidFood',
            
            // 1=>'Příjem',
            // 2=>'Převodka minus',
            // 11=>'Převodka plus',
            // 12=>'Storno objednavky',
            // 3=>'Odpis',
            // 4=>'Prodej',
            // 5=>'Zrcadlo plus',
            // 6=>'Zrcadlo minus',
            // 9=>'Zrcadlo plus - oprava',
            // 10=>'Zrcadlo minus - oprava',
            // 7=>'Příjem Makro',
            // 8=>'Příjem BidFood',
            $plus = [1,5,7,8,9,11,12];
            $minus = [2,4,6,10,3];
            
            $l->value = trim($l->value);
            $items_list[$l->stock_item_id]['sum'] = trim($items_list[$l->stock_item_id]['sum']);
                

            if (in_array($l->stock_type_id,$plus)){
                // if ($l->stock_item_id == 216 ) pr('value PLUS '.$l->value);
                $items_list[$l->stock_item_id]['sum'] += $l->value;
            }

            // $l->value = trim($l->value);
            // $items_list[$l->stock_item_id]['sum'] = trim($items_list[$l->stock_item_id]['sum']);
            
            if (in_array($l->stock_type_id,$minus)){
                // if ($l->stock_item_id == 216 ) {
                if ($l->stock_item_id == 388 ) {
                    // pr('value MINUS '.$l->value);
                    // var_dump($l->value);
                    // var_dump($items_list[$l->stock_item_id]['sum']);
                    // pr('before '.$items_list[$l->stock_item_id]['sum']);
                }
                
                // if (trim($l->value) == trim($items_list[$l->stock_item_id]['sum'])){
                //     $items_list[$l->stock_item_id]['sum'] = 0;
                // } else {
                    $items_list[$l->stock_item_id]['sum'] -= $l->value;

                // }
                
                // if ($l->stock_item_id == 216 ) {
                if ($l->stock_item_id == 388 ) {
                    // pr('after ');
                    // pr($items_list[388]['sum']);
                }
            }
            // if ($l->stock_item_id == 216){
                // if (isset($items_list[216])){
                
                // pr($items_list[216]['sum']);
                // }
            // }
            if($l->stock_item_id == 388){
                // pr($l);
                // pr($items_list[388]['sum']);
            }
        }
        // die('a');
        $result = [];
        foreach($items_list AS $item){
            $result[] = $item;
        }
        // pr($items_list);die();
		// pr($items_list_load);die();
		// pr($items_list);
		// sort($items_list);
		// pr($result);	
		return($result);	
	}
	
			
    
    public function validationDefault(Validator $validator){
        /*$validator
          ->add('id', 'valid', ['rule' => 'numeric'])
          ->allowEmpty('id', 'create');*/
/*
        $validator
          ->requirePresence('last_name', true,   __("Príjmení musí být vyplneno"))
          ->notEmpty('last_name');
        
        $validator
          ->requirePresence('first_name', true,   __("Jméno musí být vyplneno"))
          ->notEmpty('first_name');

        /*$validator
          ->requirePresence('email', true,   __("Email musí být vyplnen"))
          ->notEmpty('email');*/
        return $validator;
    }
}