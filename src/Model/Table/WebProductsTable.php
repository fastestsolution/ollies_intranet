<?php

namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;


class WebProductsTable extends Table
{
    
    public function initialize(array $config)
    {
        parent::initialize($config);
		$this->table('fastest__shop_products');
		
    }
    
    public static function defaultConnectionName() {
        return 'web';
    }
      
}