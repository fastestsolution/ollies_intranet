<?php

namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class UsersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->belongsTo('UserGroups');
        $this->addBehavior('Restful');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');
    }
  
    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];

        if ($entity->isNew()) {
            $hasher = new DefaultPasswordHasher();

            // Generate an API 'token'
            $entity->api_key_plain = sha1(Text::uuid());

            // Bcrypt the token so BasicAuthenticate can check
            // it during login.
            $entity->api_key = $hasher->hash($entity->api_key_plain);
        }
        return true;
    }
	
	public function phoneList(){
		$data_load = $this->find()
			->where([
				'phone >'=>0,
			])
			->select([
				'id',
				'name',
				'phone',
			])
			->order('name ASC')
			->toArray();
		$data = [];
		foreach($data_load AS $d){
			$data['420'.$d->phone] = $d->name.' ('.$d->phone.')';
		}
		//pr($data);
		
		return $data;
	}
	
	public function userList(){
		$data_load = $this->find()
			->where([
			])
			->select([
			])
			->cache(function ($query) {
				return 'usersList';
			})
			->order('name ASC')
			->toArray();
		$data = [];
		//pr($data_load);die();
		foreach($data_load AS $d){
			$data[$d->id] = [
				'name'=>$d->name,
				'url'=>$d->url,
				'email'=>$d->email,
			];
		}
		//pr($data);
		
		return $data;
	}
	
	public function usersList(){
		$data_load = $this->find()
			->where([
			])
			->select([
			])
			->order('name ASC')
			->toArray();
		$data = [];
		//pr($data_load);die();
		foreach($data_load AS $d){
			$data[$d->id] = $d->name;
		}
		//pr($data);
		
		return $data;
	}
	
	public function emailList(){
		$data_load = $this->find()
			->where([
			])
			->select([
			])
			->order('name ASC')
			->toArray();
		$data = [];
		//pr($data_load);die();
		foreach($data_load AS $d){
			$data[$d->id] = $d->email;
		}
		//pr($data);
		
		return $data;
	}
		
			
    
    public function validationDefault(Validator $validator){
        /*$validator
          ->add('id', 'valid', ['rule' => 'numeric'])
          ->allowEmpty('id', 'create');*/

        $validator
          ->requirePresence('username', true,   __("Uživatelské jméno být vyplněno"))
          ->notEmpty('username');
        
        // $validator
        //   ->requirePresence('name', true,   __("Provozovna být vyplněno"))
        //   ->notEmpty('name');

        /*$validator
          ->requirePresence('email', true,   __("Email musí být vyplněn"))
          ->notEmpty('email');*/
        return $validator;
    }
}