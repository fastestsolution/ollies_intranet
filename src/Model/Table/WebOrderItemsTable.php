<?php
namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;

class WebOrderItemsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('web_order_items');
        $this->addBehavior('Timestamp');
        $this->belongsTo('WebOrders', ['foreignKey' => 'shop_order_id']);
    }
}