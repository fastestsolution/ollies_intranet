<?php

namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class MapAreasTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
		
		//$this->belongsTo("Clients");

        //$this->table('users');
		
        $this->addBehavior('Timestamp');
        //$this->addBehavior('Trash');
    }
  
	
	public function getAreaList($system_id){
		$data = $this->find()
			->where([
				'id'=>$system_id,
			])
			->select([
				'id',
				'coords',
			])
			->hydrate(false)
			->first();
		$data['coords'] = json_decode($data['coords']);
		//pr($data);
		return $data;
	}		
    
    public function validationDefault(Validator $validator){
        /*$validator
          ->add('id', 'valid', ['rule' => 'numeric'])
          ->allowEmpty('id', 'create');*/
/*
        $validator
          ->requirePresence('last_name', true,   __("Príjmení musí být vyplneno"))
          ->notEmpty('last_name');
        
        $validator
          ->requirePresence('first_name', true,   __("Jméno musí být vyplneno"))
          ->notEmpty('first_name');

        /*$validator
          ->requirePresence('email', true,   __("Email musí být vyplnen"))
          ->notEmpty('email');*/
        return $validator;
    }
}