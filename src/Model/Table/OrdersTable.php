<?php

namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class OrdersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
		
		//$this->belongsTo("Clients");
        
        $this->hasMany("OrderItems", ['foreignKey'=>'order_id']);
        $this->belongsTo("Tables" /*, ['conditions'=> ['Orders.system_id = Tables.system_id']]*/ );

        //$this->table('users');
        $this->addBehavior('Timestamp');
        //$this->addBehavior('Trash');
    }
  
	public function attendancesList($system_id){
		$data = $this->find()
			->where([
				'system_id'=>$system_id,
				'date_to IS'=>null,
			])
			->select([
				'id',
				'delivery_id',
				'code',
				'name',
				'system_id',
			])
			->toArray();
		
		return $data;
	}
			
    
    public function validationDefault(Validator $validator){
        /*$validator
          ->add('id', 'valid', ['rule' => 'numeric'])
          ->allowEmpty('id', 'create');*/
/*
        $validator
          ->requirePresence('last_name', true,   __("Príjmení musí být vyplneno"))
          ->notEmpty('last_name');
        
        $validator
          ->requirePresence('first_name', true,   __("Jméno musí být vyplneno"))
          ->notEmpty('first_name');

        /*$validator
          ->requirePresence('email', true,   __("Email musí být vyplnen"))
          ->notEmpty('email');*/
        return $validator;
    }
}