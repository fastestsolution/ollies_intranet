<?php
namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;

class WebOrdersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('web_orders');
        $this->addBehavior('Timestamp');
        $this->hasMany('WebOrderItems', ['foreignKey' => 'shop_order_id']);
        $this->belongsTo('WebClients', ['foreignKey' => 'shop_client_id']);
        $this->belongsTo('WebPlatbas', ['foreignKey' => 'shop_platba_id']);
        $this->belongsTo('WebBranches', ['foreignKey' => 'shop_doprava_id']);
    }
}