<?php

namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class MobileDeliverysTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
		
		//$this->belongsTo("Clients");
        
        
        $this->table('rozvozces');
        $this->addBehavior('Timestamp');
        //$this->addBehavior('Trash');
    }
	
	public static function defaultConnectionName() {
        return 'central';
    }
  
	public function deliveryList($pokladna_id){
		$data_load = $this->find()
			->where([
				'pokladna_id'=>$pokladna_id,
			])
			->select([
				'rozvozce_id',
				'name',
				
			])
			->toArray();
		$data = [];
		foreach($data_load AS $d){
			$data[$d->rozvozce_id] = $d->name;
		}
		return $data;
	}
			
    
    public function validationDefault(Validator $validator){
        /*$validator
          ->add('id', 'valid', ['rule' => 'numeric'])
          ->allowEmpty('id', 'create');*/
/*
        $validator
          ->requirePresence('last_name', true,   __("Príjmení musí být vyplneno"))
          ->notEmpty('last_name');
        
        $validator
          ->requirePresence('first_name', true,   __("Jméno musí být vyplneno"))
          ->notEmpty('first_name');

        /*$validator
          ->requirePresence('email', true,   __("Email musí být vyplnen"))
          ->notEmpty('email');*/
        return $validator;
    }
}