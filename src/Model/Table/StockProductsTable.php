<?php

namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class StockProductsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
		
		//$this->belongsTo("Clients");

        //$this->table('users');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');
    
	}
	
	public function productList($all=false){
		$data_load = $this->find()
			->where([
			])
			->select([
				'id',
				'name',
				'code',
				'loss',
				'unit_id',
			])
			->order('name ASC')
			->toArray();
		$data = [];
		
		foreach($data_load AS $d){
			if ($all){
			$data[$d->id] = $d;
				
			} else {
			$data[$d->id] = $d->name.' ('.$d->code.')';
				
			}
		}
		//pr($data);
		return $data;
	}
	
			
    
    public function validationDefault(Validator $validator){
       /* $validator
          ->add('id', 'valid', ['rule' => 'numeric'])
          ->allowEmpty('id', 'create');*/

        $validator
          ->requirePresence('name', true,   __("Název musí být vyplněn"))
          ->notEmpty('name', ("Název musí být vyplněn"))  ;
        
       /* $validator
          ->requirePresence('first_name', true,   __("Jméno musí být vyplneno"))
          ->notEmpty('first_name');

        /*$validator
          ->requirePresence('email', true,   __("Email musí být vyplnen"))
          ->notEmpty('email');*/

        return $validator;
    }
}