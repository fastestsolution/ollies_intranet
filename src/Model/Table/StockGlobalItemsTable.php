<?php
namespace App\Model\Table;
use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use Cake\Cache\Cache;
use Cake\Utility\Hash;

use Cake\Network\Session;

class StockGlobalItemsTable extends Table
{

	public function initialize(array $config)
	{
		parent::initialize($config);
		$this->addBehavior('Timestamp');
        $this->addBehavior('Trash');
		$this->hasOne('ProductRecipes',array(
            'className' => 'ProductRecipes',
            'foreignKey' => 'stock_item_global_id'
        ));
		$this->hasOne('StocksItems');
		$this->hasOne('Stocks', ['foreignKey'=>'stock_item_id']);
		
	
	}
  
	public function beforeFind($event, $query, $options, $primary){
		
	}
	
	public function beforeSave($event){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		$event->data['entity']["system_id"] = $system_id;
		
		
		return $event;
	}
	
	
	public function findLastIdSkladItems(){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		
		$find = $this->find()
		->select(['id',])
		//->where(['system_id'=>$system_id])
		->order('id DESC')
		->first();
		//pr($find);
		return($find->id);	
	}
	
	public function skladItemData($id){
		
		$find = $this->find()
		->select(['id','tax_id','jednotka_id'])
		->where(['id'=>$id])
		->first();
		return($find);	
	}

    public function stockItemsList($stock_group_id=null){

        $conditions = [];
        if ($stock_group_id != null){
            $conditions['group_id']= $stock_group_id;
		}
		$items_list_load = $this->find()
            ->where($conditions)
            ->select([
                'id',
                'name',
                'jednotka_id',
            ])
			->toArray();
			
		//pr($items_list_load);
		$this->stock_unit_list = [
			1=>'ks',
			2=>'l',
			3=>'g',
			4=>'kg',
		];
		$items_list = [];
        foreach($items_list_load AS $i){
            //pr($i);
            $items_list[$i->id] = $i->name.(($i->jednotka_id>0)?' ('.$this->stock_unit_list[$i->jednotka_id].')':'');
        }
        return($items_list);
    }
	
	public function checkMakroSkladItem($data,$price_tax_list_makro){
		$find = $this->find()
			->where(['kos'=>0,'ean'=>$data[2]])
			->select([
				'id',
				'name',
				'jednotka_id',
			])
			//->combine('id','name')
			->first();
		if ($find){
			return $find;
		} else {
			//pr($data);
				
			$save_new = [
				'name'=>trim(strtr($data[13],['  '=>''])),
				'ean'=>$data[2],
				'dodavatel_id'=>1,
				'tax_id'=>$price_tax_list_makro[$data[12]],
			];
			
			$save_item = $this->newEntity($save_new);
			
			$find = $this->save($save_item);
			return $find;
			
			//pr($save_item);
			//die('neni');
		}
		
	}
	public function checkBidFoodSkladItem($data,$price_tax_list_makro){
		$find = $this->find()
			->where(['kos'=>0,'bidfood_id'=>$data[5]])
			->select([
				'id',
				'name',
				'jednotka_id',
			])
			//->combine('id','name')
			->first();
		if ($find){
			return $find;
		} else {
			//pr($data);
				
			$save_new = [
				'name'=>trim(strtr($data[6],['  '=>''])),
				'bidfood_id'=>$data[5],
				'dodavatel_id'=>2,
				'jednotka_id_dodavatel'=>$data[9],
				'tax_id'=>$price_tax_list_makro[$data[11]],
			];
			
			$save_item = $this->newEntity($save_new);
			//pr($save_item);die();
			
			//pr($save_item);die();
			$find = $this->save($save_item);
			//pr($find);die();
			return $find;
			
			//pr($save_item);
			//die('neni');
		}
		
	}
	
	public function stockItemsListFromGlobal($group_id=null){ 
		//$this->Table = TableRegistry::get('ProductRecepturas');
		//if (($dodavatel_list = Cache::read('dodavatel_list')) === false) {
		
		
		$conditions = [];
		if ($group_id != null){
			//$conditions['dodavatel_id']= $dodavatel_id;
			$conditions['StockGlobalItems.group_id']= $group_id;
		}
			$items_list_load = $this->find()
			->contain(
			[
				'ProductRecipes'=>function($q){
                    return $q->select(['product_id','stock_item_global_id']);
                },
				'StocksItems'=>function($q){
                    $q
					->select(['id','name','jednotka_id'])
					/*
					->contain([
						'Sklads'=>function($q){
							return $q->select(['id','nakup_price','nakup_price_vat','nakup_price_total','nakup_price_total_vat']);
						}
					])
					*/
					;
					
					
					return $q;
                },
			]
			)
			->where($conditions)
			->select([
				/*
				'id',
				'name',
				'jednotka_id',
				*/
			])
			->map(function($r){
				if (isset($r->sklad_item->sklad)){
					//$r->
				}
				return $r;
			})
			//->combine('id','name')
			->toArray();
			
			//Cache::write('dodavatel_list', $dodavatel_list);
		//}
		//pr($group_list);
		// pr($items_list_load);	
		
		$sklad_jednotka_list = [
			1=>'ks',
			2=>'l',
			3=>'g',
			4=>'kg',
		];
		pr($items_list_load);
		$items_list = [];
		foreach($items_list_load AS $i){
			//pr($i);
			//$items_list[$i->id] = $i->name.(($i->jednotka_id>0)?' ('.$sklad_jednotka_list[$i->jednotka_id].')':'');
			if (isset($i->stocks_item->id)){
				$items_list[$i->stocks_item->id] = $i->stocks_item->name.(($i->stocks_item->jednotka_id>0)?' ('.$sklad_jednotka_list[$i->stocks_item->jednotka_id].')':'');
				
			}
		}
		//pr($items_list);
		return($items_list);	
	}
	

	
	public function skladItems($ids=null){
		$conditions = [];
		if ($ids){
			$conditions['id IN'] = $ids;
		}
		//if (($dodavatel_list = Cache::read('dodavatel_list')) === false) {
		$items_list_load = $this->find()
			->where($conditions)
			->select([
				'id',
				'name',
				'min',
				'max',
				'tax_id',
				'jednotka_id',
			])
			->order('name ASC')
			 ->cache(function ($query) {
				return 'stockItems-list-sklad' . md5(serialize($query->clause('where')));
            })
			->toArray();
			
			//Cache::write('dodavatel_list', $dodavatel_list);
		//}
		$items_list = Hash::combine($items_list_load, '{n}.id','{n}');
		
		//pr($items_list);	
		return($items_list);	
	}



	public function skladItemsList($dodavatel_id=null){
		
		// if (($dodavatel_list = Cache::read('dodavatel_list')) === false) {
		$conditions = [];
		if ($dodavatel_id != null){
			//$conditions['dodavatel_id']= $dodavatel_id;
			$conditions['group_id']= $dodavatel_id;
		}
			$query = $this->find()
			->where($conditions)
			->select([
				'id',
				'name',
				'jednotka_id',
			])
			 ->cache(function ($query) {
				// pr($query);die();
				return 'stockItemsList-list' . md5(serialize($query->clause('where')));
            })
			->order('name ASC');
			//->combine('id','name')
			$items_list_load = $query->toArray();
			
			// Cache::write('dodavatel_list', $dodavatel_list);
		// }
		//pr($group_list);
		//pr($items_list);	
		
		$sklad_jednotka_list = [
			1=>'ks',
			2=>'l',
			3=>'g',
			4=>'kg',
		];
		$items_list = [];
		foreach($items_list_load AS $i){
			//pr($i);
			$items_list[$i->id] = $i->name.(($i->jednotka_id>0)?' ('.$sklad_jednotka_list[$i->jednotka_id].')':'');
		}
		return($items_list);	
	}
	
	public function stockGlobaUnitList(){
		
		//if (($dodavatel_list = Cache::read('dodavatel_list')) === false) {
		$conditions = [];
			$items_list_load = $this->find('list',[
				'keyField' => 'id',
				'valueField' => 'jednotka_id'
			])
			->where($conditions)
			->order('name ASC')
			->select([
				'id',
				'jednotka_id',
			])
			->cache(function ($query) {
				return 'stockGlobaUnitList';
            })
			//->combine('id','name')
			->toArray();
			
			//Cache::write('dodavatel_list', $dodavatel_list);
		//}
		return($items_list_load);	
	}
	
	
	
	public function validationDefault(Validator $validator){
    
		$validator
		  ->add('id', 'valid', ['rule' => 'numeric'])
		  ->allowEmpty('id', 'create');

		$validator
			//->requirePresence('name', 'create',   __("Mus�te vyplnit n�zev"))
			->notEmpty('name',__("Musíte vyplnit název"))
			
			->notEmpty('code',__("Musíte vyplnit kod"))
			->notEmpty('price',__("Musíte vyplnit cenu"))
			
		;
		return $validator;
	}

  
}
