<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class BranchesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        //$this->hasMany("ClientAddresses");
        
        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');
        

    }

    public function beforeSave(Event $event)
    {
        //$event->data['entity']->name = $event->data['entity']->last_name.' '.$event->data['entity']->first_name; 
        //pr($event);die();
        
        return $event;
    }

    public function branchesList(){
		$data = $this->find('list', [
                'keyField' => 'id',
                'valueField' => 'name'
            ])
			->where([
			])
			->select([
            ])
            ->cache(function ($query) {
                return 'branchesList';
            })
			->order('name ASC')
			->toArray();
		//pr($data);
		return $data;
	}
    
    public function branchesListAll(){
		$dataLoad = $this->find()
			->where([
			])
			->select([
                'id',
                'name',
                'eet_certificate',
                'eet_dic',
                'eet_dic_pover',
                'email',
            ])
            // ->cache(function ($query) {
            //     return 'branchesListAllData';
            // })
			->order('name ASC')
			->toArray();
        $data = [];
        foreach($dataLoad AS $d){
            $data[$d->id] = $d;
        }
            //pr($data);
		return $data;
	}

    

    public function validationDefault(Validator $validator){

        $validator
            ->requirePresence('name', true,   __("Musíte zadat název"))
            ->notEmpty('name',__("Musíte zadat název"))
        ;
            
        //     ->requirePresence('code', true,   __("Musíte zadat kód"))
        //     ->notEmpty('code',__("Musíte zadat kód"))
            
        //     ->requirePresence('product_group_id', true,   __("Musíte zadat skupinu produktu"))
        //     ->notEmpty('product_group_id',__("Musíte zadat skupinu produktu"))
            
        //     ->requirePresence('price', true,   __("Musíte zadat cenu"))
        //     ->notEmpty('price',__("Musíte zadat cenu"))
            
            
        // ;

        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplnen"))

          ->notEmpty('email');*/

        return $validator;

    }

}