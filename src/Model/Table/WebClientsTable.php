<?php
namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;

class WebClientsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('web_clients');
        $this->addBehavior('Timestamp');
    }  
}