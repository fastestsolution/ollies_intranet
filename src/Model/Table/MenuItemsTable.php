<?php
namespace App\Model\Table;
use App\Controller\AppController;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use Cake\Utility\Hash;

use Cake\Network\Session;

class MenuItemsTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);
	$this->addBehavior('Tree');
	$this->addBehavior('Timestamp');
	$this->table('product_groups');
  }
  
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('System.system_id');
		//$query->where(['CarTypes.system_id' => $system_id]);
		//pr($event->data);
	}
	
  public function beforeSave($event){
    $session = new Session();
	$system_id = $session->read('System.system_id');
	//$event->data['entity']["system_id"] = $system_id;
	
    
    return $event;
  }
	public function menuList($id=null){
		if ($id != null){
			$menu = $this->find()->where(['id'=>$id])->first();
		}
		
		$menus = $this->find('threaded')->order('lft ASC');
		if (isset($menu)){
			$menus->where(['lft >='=>$menu->lft,'rght <='=>$menu->rght]);
		}
		$menus = $menus->toArray();
		//pr($id);
		//pr($menus);
		$this->menu_list_data = [];
		$this->genMenu($menus);
		//pr($this->menu_list_data);
		return $this->menu_list_data;
	}
	
	private function genMenu($data){
		foreach($data AS $m){
			$this->menu_list_data[$m->id] = (($m->level>1)?str_repeat('-',$m->level).' ':'').$m->name;
			if (!empty($m->children)){
				$this->genMenu($m->children);
			}
		}
	}
		
	
  
 
  
}
