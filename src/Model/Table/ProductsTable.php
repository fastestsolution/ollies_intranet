<?php

namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class ProductsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('products');
        //$this->belongsTo('ProductGroups');
        $this->addBehavior('Restful');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');
		$this->hasOne('ProductConnect', ['className'=>'ProductConnects']);
		$this->hasMany('ProductConnects');
		$this->hasMany('ProductConnAddons');
		$this->hasMany('ProductRecipes');
		$this->hasMany('ProductPrices');
		$this->hasOne('OrderItems');
	}
	
	
	public function productList(){
		$query = $this->find('list', [
			'keyField' => 'id',
			'valueField' => 'name'
		])
		->where([])
		 ->cache(function ($query) {
			return 'productList-list' . md5(serialize($query->clause('where')));
        })
		->order('name ASC');
		$data = $query->toArray();
		//pr($data);
		return $data;
			
	}
  /*
    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];

        if ($entity->isNew()) {
            $hasher = new DefaultPasswordHasher();

            // Generate an API 'token'
            $entity->api_key_plain = sha1(Text::uuid());

            // Bcrypt the token so BasicAuthenticate can check
            // it during login.
            $entity->api_key = $hasher->hash($entity->api_key_plain);
        }
        return true;
    }
	
	public function phoneList(){
		$data_load = $this->find()
			->where([
				'phone >'=>0,
			])
			->select([
				'id',
				'name',
				'phone',
			])
			->order('name ASC')
			->toArray();
		$data = [];
		foreach($data_load AS $d){
			$data['420'.$d->phone] = $d->name.' ('.$d->phone.')';
		}
		//pr($data);
		
		return $data;
	}
	
	public function userList(){
		$data_load = $this->find()
			->where([
			])
			->select([
			])
			->order('name ASC')
			->toArray();
		$data = [];
		//pr($data_load);die();
		foreach($data_load AS $d){
			$data[$d->id] = [
				'name'=>$d->name,
				'url'=>$d->url,
				'email'=>$d->email,
			];
		}
		//pr($data);
		
		return $data;
	}
	
	public function emailList(){
		$data_load = $this->find()
			->where([
			])
			->select([
			])
			->order('name ASC')
			->toArray();
		$data = [];
		//pr($data_load);die();
		foreach($data_load AS $d){
			$data[$d->id] = $d->email;
		}
		//pr($data);
		
		return $data;
	}
		
			
    */
    public function validationDefault(Validator $validator){

        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            //->requirePresence('name', 'create',   __("Musíte vyplnit název"))
            ->notEmpty('name',__("Musíte vyplnit název"))

            ->notEmpty('code',__("Musíte vyplnit kód"))
        //    ->notEmpty('num',__("Musíte vyplnit číslo"))
           // ->notEmpty('price',__("Musíte vyplnit cenu"))

        ;
        return $validator;
    }
}