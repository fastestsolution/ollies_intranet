<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Cache\Cache;

class ProductGroupsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        //$this->hasMany("ClientAddresses");
        
        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');


        //this->hasMany('Products');
        $this->belongsTo('ParentGroup', ['className'=>'ProductGroups', 'foreignKey'=>'parent_id']);
        $this->hasMany('ProductConnects');

        //$this->addBehavior('Trash');

    }

    public function beforeSave(Event $event)
    {
        //$event->data['entity']->name = $event->data['entity']->last_name.' '.$event->data['entity']->first_name; 
        //pr($event);die();
        
        return $event;
    }
	
	
    public function groupList(){
		if (($posts = Cache::read('product_groups')) === false) {
		$query = $this->find('list',['keyField' => 'id','valueField' => 'name'])
		  //->contain(['ZakazkaConnects',])
		  ->where([
            //   'status'=>1
          ])
		  ->select([
			'id',
			'name',
          ])
          ->cache(function ($query) {
			return 'product_group_data-list' . md5(serialize($query->clause('where')));
            })
        ;

		  
		$data_list_load =   $query->toArray();
        $list = [];
        foreach($data_list_load AS $id=>$l){
            $crumbs = $this->find('path', ['for' => $id]);
            $path = [];
            foreach ($crumbs as $crumb) {
                $path[] = $crumb->name;
            }

            $list[$id] = implode( ' > ',$path);
           //die();
           Cache::write('product_groups', $list);
        }
        } else {
            $list = Cache::read('product_groups');
            // die('a');
        }


        return $list;  
    }	
    

    public function groupListLevel2(){
		if (($posts = Cache::read('product_groups_level_2')) === false) {
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->where([
            //   'status'=>1
          ])
		  ->select([
			'id',
            'name',
            'parent_id',
            'level',
          ])
         // ->cache(function ($query) {
		//	return 'product_group_data-list_level_2' . md5(serialize($query->clause('where')));
         //   })
        ;

		  
        $data_list_load =   $query->toArray();
        //pr($data_list_load);die();
        $list = [];
        foreach($data_list_load AS $id=>$l){
            if ($l->level == 3 ){
                $list[$l->id] = $l->parent_id ;
            }      
            if ($l->level == 2 ){
                $list[$l->id] = $l->id ;
            } 
            //$list[$id] = implode( ' > ',$path);

        }
        //pr($list);
        //die();
       Cache::write('product_groups_level_2', $list);
        } else {
            $list = Cache::read('product_groups_level_2');
            // die('a');
        }


        return $list;  
	}	

  

    

    public function validationDefault(Validator $validator){

        // $validator
        //     ->requirePresence('name', true,   __("Musíte zadat jméno"))
        //     ->notEmpty('name',__("Musíte zadat jméno"))
            
        //     ->requirePresence('code', true,   __("Musíte zadat kód"))
        //     ->notEmpty('code',__("Musíte zadat kód"))
            
        //     ->requirePresence('product_group_id', true,   __("Musíte zadat skupinu produktu"))
        //     ->notEmpty('product_group_id',__("Musíte zadat skupinu produktu"))
            
        //     ->requirePresence('price', true,   __("Musíte zadat cenu"))
        //     ->notEmpty('price',__("Musíte zadat cenu"))
            
            
        // ;

        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplnen"))

          ->notEmpty('email');*/

        return $validator;

    }

}