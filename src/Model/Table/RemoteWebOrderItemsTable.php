<?php
namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;

class RemoteWebOrderItemsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('fastest__shop_order_items');
        $this->belongsTo('RemoteWebOrders', ['foreignKey' => 'shop_order_id']);
    }

    public static function defaultConnectionName()
    {
        return 'web';
    }
}