<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

    class ProductPricesTable extends Table
    {

        public function initialize(array $config)
        {
            parent::initialize($config);
            $this->belongsTo("Products");
   
            $this->addBehavior('Timestamp');

        }
    }

?>