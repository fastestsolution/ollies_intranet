<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class BranchesStatsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        //$this->hasMany("ClientAddresses");
        
        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');
        

    }

    public function beforeSave(Event $event)
    {
        //$event->data['entity']->name = $event->data['entity']->last_name.' '.$event->data['entity']->first_name; 
        //pr($event);die();
        
        return $event;
    }

    public function branchesStatsList(){
		$data = $this->find()
			->where([
			])
			->select([
                'branch_id',
                'date',
                'value',
            ])
            ->toArray();

        $list = [
            'branches'=>[],
            'total'=>[],
        ];
        if ($data){
            foreach($data AS $d){
                // pr($d);
                $list['branches'][$d->branch_id][$d->date->format('Y-m-d')] = $d->value;
                if (!isset($list['total'][$d->date->format('Y-m-d')])){
                    $list['total'][$d->date->format('Y-m-d')] = 0;
                }
                $list['total'][$d->date->format('Y-m-d')] += $d->value;
            }    
            
        }
        // pr($list);
		return $list;
	}

    

    public function validationDefault(Validator $validator){

        $validator
            // ->requirePresence('name', true,   __("Musíte zadat název"))
            // ->notEmpty('name',__("Musíte zadat název"))
        ;
            
        //     ->requirePresence('code', true,   __("Musíte zadat kód"))
        //     ->notEmpty('code',__("Musíte zadat kód"))
            
        //     ->requirePresence('product_group_id', true,   __("Musíte zadat skupinu produktu"))
        //     ->notEmpty('product_group_id',__("Musíte zadat skupinu produktu"))
            
        //     ->requirePresence('price', true,   __("Musíte zadat cenu"))
        //     ->notEmpty('price',__("Musíte zadat cenu"))
            
            
        // ;

        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplnen"))

          ->notEmpty('email');*/

        return $validator;

    }

}