<?php

namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;


class WebPlatbasTable extends Table
{
    
    public function initialize(array $config)
    {
        parent::initialize($config);
        //$this->table('fastest__shop_platbas');
        $this->table('web_platbas');
    }
    
    /*public static function defaultConnectionName() {
        return 'web';
    }*/
      
}