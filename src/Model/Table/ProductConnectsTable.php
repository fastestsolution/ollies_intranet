<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Cache\Cache;

    class ProductConnectsTable extends Table
    {

        public function initialize(array $config)
        {
            parent::initialize($config);
            $this->belongsTo("Products");
            $this->belongsTo("ProductGroups");

            $this->addBehavior('Timestamp');

        }


        public function groupListSebou(){
            // if (($posts = Cache::read('product_groups_sebou')) === false) {
            $query = $this->find()
            //->contain(['ZakazkaConnects',])
            ->where([
                //   'status'=>1
            ])
            ->select([
                'product_group_id',
                'pref',
            ])
            // ->cache(function ($query) {
            //	return 'product_group_data-list_level_2' . md5(serialize($query->clause('where')));
            //   })
            ;

            
            $data_list_load =   $query->toArray();
            $list = [
                'tady'=>[],
                'sebou'=>[],

            ];
            //pr($data_list_load);die();
            foreach($data_list_load AS $id=>$l){
                if ($l->pref == 'S'){
                    $list['sebou'][$l->product_group_id] = $l->product_group_id;
                } else {
                    $list['tady'][$l->product_group_id] = $l->product_group_id;
                }
                //$list[$id] = implode( ' > ',$path);

            }
            //pr($list);
            //die();
            Cache::write('product_groups_sebou', $list);
            // } else {
            //     $list = Cache::read('product_groups_sebou');
            //     // die('a');
            // }
            // pr($list);

            return $list;  
        }	

	
        public function conList($product_group_id){
            //if (($posts = Cache::read('product_connects')) === false) {
            $list = $this->find('list',['keyField' => 'id','valueField' => 'product_id'])
            //->contain(['ZakazkaConnects',])
            ->where([
                  'product_group_id'=>$product_group_id
            ])
            ->select([
                'id',
                'product_id',
            ])
            ->cache(function ($query) {
                return 'product_connect_data-list' . md5(serialize($query->clause('where')));
                })
            ->toArray()
            ;

            // pr($list);die();
            //die();
            // Cache::write('product_groups', $list);
            // }
            
            // } else {
            //     $list = Cache::read('product_groups');
            //     // die('a');
            // }


            return $list;  
        }	
    }

?>