<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;


class RestfulBehavior extends Behavior{
    private $transFields = [];
    private $defaultLang = 'cz';
 
    public function initialize(array $config){
        if(isset($config['transFields'])){
            $this->transFields = $config['transFields'];
        }
        // Some initialization code here
    }
    /**
     *   We must add new associated items that was created after this item creation... 
         In param_lists are all associated items, in item we must add missing params that are in full list and are not in item list
     * @param type $listAllValues
     * @param type $listItem
     * @param type $AssociationName
     * @param type $sub_association
     * @return type
     */
    public function fillMissingParams($listAllValues, $listItem, $AssociationName, $sub_association){
        $assoc_name = Inflector::underscore($AssociationName);  //Translate to underscore form
        
        //We must prepare set of all associated params...
        $params = [];
        foreach($listAllValues as $par){
             $params[$par['id']] = $par;
        }
      
        if(isset($listItem->{$assoc_name}) && !empty($listItem->{$assoc_name})){
            foreach( $listItem->{$assoc_name} as $parItem ){
                if(isset($parItem->{$sub_association}->id) && isset($params[$parItem->{$sub_association}->id])){
                    unset( $params[$parItem->{$sub_association}->id] );
                }
            } 
        }
        
        foreach($params as $i=>$par){
            $listItem->{$assoc_name}[$i] = TableRegistry::get($AssociationName)->newEntity(TableRegistry::get($AssociationName)->getEmptyColumns(), ['validate'=>false]);
            $listItem->{$assoc_name}[$i]->{Inflector::singularize($sub_association) . '_id'} = $par['id'];
            $listItem->{$assoc_name}[$i]->{$sub_association} = $par;
        }
        
        return $listItem;
    }
    
    public function getEmptyColumns($contains = [], $preloadedLists = []){
        $columns = array_fill_keys($this->_table->schema()->columns(), '');
        unset($columns['created']);
        unset($columns['modified']);
        
        if(!empty($contains)){
            foreach($contains as $key => $c){
                if(!is_string($c)){
                    $c = $key;
                }
                    if(strpos($c, '.')){
                        $cParts = explode('.', $c);

                        switch(Count($cParts)){
                            case 2:

                                $underscore_name_first = Inflector::underscore($cParts[0]);
                                $underscore_name_second = Inflector::underscore($cParts[1]); 
                                if(isset($preloadedLists[$cParts[1]])){
                                    foreach($preloadedLists[$cParts[1]] as $i => $subitem){
                                         $columns[$underscore_name_first][$i] = TableRegistry::get($cParts[0])->newEntity( TableRegistry::get($cParts[0])->getEmptyColumns(), ['validate'=>false])->toArray();
                                         $columns[$underscore_name_first][$i][$underscore_name_second] = $subitem;
                                         $columns[$underscore_name_first][$i][ Inflector::singularize($underscore_name_second) . '_id' ] = $subitem['id'];

                                    }
                                }
                                if(!isset($columns[ $underscore_name_first ])){
                                     $columns[ $underscore_name_first ] = TableRegistry::get($cParts[0])->newEntity(  array_fill_keys( TableRegistry::get($cParts[0])->schema()->columns(), ""), ['validate'=>false] )->toArray(); 
                                }
                                $columns[ $underscore_name_first ][Inflector::underscore($cParts[1])] = [];
                            break;
                        }
                    }else{
                        $columns[Inflector::underscore($c)] = [];
                    }
                }
         
        }
        if(!empty($this->transFields)){
            $columns['_locale'] = $this->defaultLang;
            $columns['_translations'] = [];
        }
        return $columns;
    }
    
    public function getValidations($contain = [], $languages = [], $type = 'Default'){
        
        $valName = 'validation' . ucfirst($type);
        $validation = $this->_table->$valName(new \Cake\Validation\Validator());
        
        $result = $this->transformValidationRules( $validation->__debugInfo() );
        
        //If there are languages defined, we must check translated columns and if trans col has validation, we must prepare validation for all language versions of coluimn
        if(!empty($languages)){
            $lang_result = [];
            foreach($result as $fkey => $item){ 
                if(in_array($fkey, $this->transFields)){
                    foreach($languages as $lang){
                        $lang_result[$fkey .'_'. $lang ] = $item;
                    }
                }
            }
            $result = array_merge($result, $lang_result);
        }
        
        if(!empty($contain)){
            foreach($contain as $key=>$c){  
                if(is_callable($c)){
                    $c = $key;
                }
                $fkey = \Cake\Utility\Inflector::tableize($c);
                $result[$fkey] = $this->transformValidationRules( TableRegistry::get($c)->validationDefault(new \Cake\Validation\Validator())->__debugInfo());
            } 
        }
        return $result;
    }
    
    private function transformValidationRules($validation){
        $result = [];
        if($validation){
            foreach($validation['_fields'] as $key => $field){
                if($field['isPresenceRequired'] === true){
                    $result[$key] = [
                        'required'=> (isset($validation['_presenceMessages'][$key]) ? $validation['_presenceMessages'][$key] : null)];
                }
                if($field['isEmptyAllowed'] === true){
                    $result[$key] = [
                        'empty'=> (isset($validation['_allowEmptyMessages'][$key]) ? $validation['_allowEmptyMessages'][$key] : null)
                    ];
                }
            }
        }
        return $result;
    }
    
    public function fillTranslateFields($tanslations, $languages){
       
        foreach($languages as $lang){
            //if($lang != $this->defaultLang){
                if(!isset($tanslations[$lang])){
                     $tanslations[$lang] = $this->_table->newEntity( array_fill_keys($this->transFields, ''), ['validate'=>false]);
                }else{
                    foreach($this->transFields as $f){
                        if(!isset($tanslations[$lang]->{$f})){
                            $tanslations[$lang]->{$f} = '';
                        }
                    }
                }
                $tanslations[$lang]['locale'] = $lang;
           // }
        }
        return $tanslations;
    }
    
    public function patchTranslations($translations){
        foreach($translations as $lang => $trans){
            $translations[$lang] = $this->_table->newEntity($trans, ['validate'=>'Translation']);
        }
        return $translations;
    }
    
    public function mergeTranslationToMainData($data){
        $defaultTrans = $data['_translations'][$data['_locale']];
       
        if(isset($defaultTrans['locale'])){
            $defaultTrans['locale'] = null;
            unset($defaultTrans['locale']);
        }
        if(is_array($defaultTrans)){
             $data = array_merge($data, $defaultTrans);
        }
        if(isset($data['_translations'][$data['_locale']])){
            unset($data['_translations'][$data['_locale']]);
        }
        //Prices for products
        if(isset($data['shop_product_prices'][$data['_locale']])){
              $data['price'] = $data['shop_product_prices'][$data['_locale']]['price'];
              $data['price_vat'] = $data['shop_product_prices'][$data['_locale']]['price_vat'];
              $data['tax_id'] = $data['shop_product_prices'][$data['_locale']]['tax_id'];
        }
        return $data;
    }
    
}