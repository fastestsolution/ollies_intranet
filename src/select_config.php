<?php
$packageJson = json_decode(file_get_contents('../package.json'), true);
define('VERSION', $packageJson['version']);
$select_config = [

	'provozy' => [
		1=>'avion',
		2=>'vitkovice',
		3=>'poruba',
		4=>'olomouc',
		7=>'manifesto',
		12=>'balbin',
	],

    'price_tax_list' => [
        1=>'15%',
        2=>'21%',
        3=>'0%',
        4=>'10%',
    ],
    'price_tax_list_con'=>[
        1=>0.1304, // 15
        2=>0.1736, // 21
        3=>0, // 0
        4=>0.0909, // 10
    ],
	/*
	'system_list' => [
		1000=>['name'=>'Testovaci','url'=>'https://www.chachar.cz/','email'=>'test@fastest.cz'], 
		
	
			       
	],
	*/
	'menu'=>[
		['name'=>'Home','url'=>'/'],
        ['name'=>'Produkty','url'=>'/products/','child'=>[
            ['name'=>'Správa produktů','url'=>'/products/'],
			['name'=>'Skupiny produktů','url'=>'/product_groups/'],
			['name'=>'Tiskárny skupin','url'=>'/product_group_printers/'],
            ['name'=>'Přídavky produktů','url'=>'/product-addons/']
		]],
		['name' => 'Tržby', 'url' => '/orders/', 'child' => [
			['name' => 'Tržby pokladny', 'url' => '/orders/'],
			['name' => 'Tržby eshop', 'url' => '/orders-web/']
		]],
		['name' => 'Uzávěrky', 'url' => '/deadlines/'],
        //3=>['name'=>'Skupiny produktů','url'=>'/product_groups/'],
        ['name'=>'Sklady','url'=>'/stocks/','child'=>[
			['name'=>'Pohyby na skladě','url'=>'/stocks/'],
			// ['name'=>'Zrcadla skladů','url'=>'/stock-mirrors/'],
			['name'=>'Skladové položky','url'=>'/stocks-items/'],
			['name'=>'Skladové jednotky','url'=>'/stocks-list/'],
			['name'=>'Skladové globální položky','url'=>'/global-items/'],
		]],
		['name'=>'Statistiky','url'=>'/summary_statistics/','child'=>[
			['name'=>'Marže produktů','url'=>'/summary_statistics/'],
			['name'=>'Ceníkové položky','url'=>'/summary_statistics/product_item_stats'],
			['name'=>'Skladové položky','url'=>'/summary_statistics/stock_item_stats'],
		]],
        ['name'=>'Výrobna','url'=>'/work-shop/show/','child'=> [
				['name'=>'Objednané','url'=>'/work-shop/show/'],
				['name'=>'Výdej ze skladu','url'=>'/work-shop/dispatch/'],
				['name' => 'Plánovací modul', 'url' => '/work-shop/order/'],
			]
		], 
        ['name'=>'Zákazníci','url'=>'/customer/','disable'=>true],
    	['name'=>'Nastavení','url'=>'/settings/','child'=>[
			['name'=>'Uživatelé','url'=>'/users/'],
			['name'=>'Pobočky','url'=>'/branches/'],
			['name'=>'Pobočky statistiky','url'=>'/branches-stats/'],
			['name'=>'Logace','url'=>'/system-logs/'],
			// ['name'=>'Ceníky','url'=>'/price-list/'],
		]],
		// 8=>['name'=>'Statistiky','url'=>'/statistics/'],
		//7=>['name'=>'Docházka','url'=>'/attendances/'],
		//8=>['name'=>'Uzávěrky','url'=>'/deadlines/'],
		//10=>['name'=>'Sklady','url'=>'/stocks/'],
		//1=>['name'=>'Stav Online','url'=>'/status-online/'],
		//2=>['name'=>'Statistiky','url'=>'/statistics/'],
		//3=>['name'=>'Mapy rozvozu','url'=>'/mapareas/'],

		//5=>['name'=>'Správa intranet','url'=>'/intranetEdit/'],
		//9=>['name'=>'SMS','url'=>'/sendSms/'],
	],
	'controllerList'=>[
		'Settings'=>'Nastavení',
		'Stocks'=>'Sklady pohyby',
		'Branches'=>'Pobočky',
		'BranchesStats'=>'Pobočky statistiky',
		'Users'=>'Uživatelé',
		'Products'=>'Produkty',
		'ProductGroups'=>'Produkty skupiny',
		'ProductAddons'=>'Produkty přídavky',
		'Statistics'=>'Statistiky',
		'StockMirrors'=>'Zrcadlo',
		'StockProducts'=>'Sklad produkty',
		'StockList'=>'Sklad jednotky',
		'StockGlobalItems'=>'Sklad globalní produkty',
		'StockQueues'=>'Sklad fronta',
	],
	'users_group_list'=>[
		1=>'Admin',
		2=>'Provozní',
		3=>'Zaměstnanec',
	],
    'addon_list' => [
        1=>'Káva',
        2=>'Alkohol',
        3=>'Nealko',
        4=>'Steaky',
        5=>'Caje',
        6=>'Omácky',
    ],
    'group_stock_list' => [
        1=>'Rozbaleno',
        2=>'Seskupeno',
    ],
	'intranet_type_list'=>[
		0=>'Všechny informace',
		1=>'Aktuality/bleskovky – obecné aktuality',
		2=>'Pokladna OS',
		3=>'Normativní akty/směrnice',
		4=>'Grafické manuály',
		5=>'Receptury / videa',
		9=>'--- Špecle',
		10=>'--- Čína',
		11=>'--- Pizza',
		12=>'--- Pizza Records',
		6=>'Zápis z porad',
		7=>'Dodavatelé',
		8=>'Personalistika',
	],
	'stock_type_list'=>[
        1=>'Příjem',
        2=>'Převodka minus',
        3=>'Odpis',
        4=>'Prodej',
        5=>'Zrcadlo plus',
        6=>'Zrcadlo minus',
        7=>'Příjem Makro',
		8=>'Příjem BidFood',
		9=>'Zrcadlo plus - oprava',
		10=>'Zrcadlo minus - oprava',
        11=>'Převodka plus',
        12=>'Storno obj.',
      	13=>'E-rezervace plus',
        14=>'E-rezervace minus'
	],
    'sklad_group_list' => [
        1=>'Dorty',
        2=>'Zákusky',
        3=>'Nápoje',
    ],
    'group_trzba_list' => [
        1=>'Cukrárna Vlastní výrobky',
        2=>'Zboží',
        3=>'Bistro Vlastní výrobky',
    ],
	'stock_type_list2'=>[
		3=>'Sýry',
		2=>'Maso',
		1=>'Nápoje',
		5=>'Obaly',
		4=>'Koloniál',
		6=>'Zelenina',
		7=>'Vlastní',
	],
	'stock_unit_list' => [
        1=>'ks',
        2=>'l',
        3=>'g',
        4=>'kg',
    ],
	'yes_no' => [
		1=>'Ano',
		0=>'Ne',
	],
	'delivery_time_list' => [
		 1=>'60 min',
		 2=>'75 min',
		 3=>'90 min',
		 4=>'120 min',
		 'other'=>'Ostatní',
	],

];
?>