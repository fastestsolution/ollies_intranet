<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;

class ProductGroupPrintersController extends AppController{
    
    public function index(){
		$this->checkLogged();
		$this->set('title','Nastavení tiskáren pro kategorie');
		$this->set('sendUpdate',true);
		$this->loadModel('ProductGroups');

		if(!empty($this->request->data)){
			foreach($this->request->data as $item){
				if(isset($item['id'])){
					$this->ProductGroups->updateAll(['printer_id'=>$item['printer_id']], ['id'=> $item['id']]);
				}
			}
		}

		$conditions = ['ProductGroups.level' => 2];
		
		
		$data = $this->ProductGroups->find()
			->contain(['ParentGroup'])
			->where($conditions)
			->toArray();
			
		$this->loadModel('PrinterTypes');
		$printerTypes = $this->PrinterTypes->find('list')->toArray();

		$this->set('data', $data);
		$this->set('printerTypes', $printerTypes);
		
	}

	public function menusSave(){
	
		$connection = ConnectionManager::get('default');
		//$results = $connection->execute('TRUNCATE TABLE product_groups'); 
		
		$this->loadModel('ProductGroups');
		/*$menus = $this->ProductGroups->find('threaded')->hydrate(false)->toArray();
		$menus = $this->request->data;
		//pr($menus);
		//$menus[0]['name'] = 'test testa';
		foreach($menus as $menu){ 
			$this->saveNode($menu,1);
		}
		Cache::clear(false);
		die(json_encode(['r'=>true]));*/
	}

}
