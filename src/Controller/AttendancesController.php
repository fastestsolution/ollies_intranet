<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class AttendancesController extends AppController{
    
	
	
	
	public function index(){
		
		$this->checkLogged();
		$this->loadComponent('ViewIndex');
		
		$cols = [
			'id'=>['name'=>'ID'],
			'name'=>['name'=>'Rozvozce'],
			'code'=>['name'=>'Kod rozvozce'],
			'date_from'=>['name'=>'Datum od'],
			'date_to'=>['name'=>'Datum do'],
		];
		$filtrations = [
			//'name'=>['name'=>'Provoz','key'=>'name','type'=>'like'],
		];
		$posibility = [
			//'edit'=>['name'=>'Editovat','url'=>'/intranetEdit/edit/'],
			//'trash'=>['name'=>'Smazat','url'=>'/intranetEdit/trash/'],
		];
		
		$conditions = ['system_id'=>$this->loggedUser->system_id];
		$conditions = $this->ViewIndex->conditions($conditions);
		//pr($conditions);
		$data = $this->Attendances->find()
		->where($conditions)
		->order('id DESC')
		->select([
			'id',
			'name',
			'code',
			'date_from',
			'date_to',
		])
		;
		//->toArray();
		
		if (empty($data)){
			$data = null;
		}
		
		$params = [
		//	'filtrations'=>$filtrations,
			//'topActions'=>$topActions,
			'cols'=>$cols,
			//'posibility'=>$posibility,
			'data'=>$data,
		];
		//pr($data->toArray());
		$this->ViewIndex->load($params);
		
	
	}
	
	
	/**
	* save nova dochazka
	*/
	public function save(){
		//sleep(6);
		//print_r($_POST);
		
		//pr($connected_list);die('a');
		
		if ($_POST['type'] == 'enter'){
			$conditions = [
				'date_to IS'=>null,
				'code'=>$_POST['code'],
				'system_id'=>$_POST['system_id'],
			];
			
		}
		if ($_POST['type'] == 'exit'){
			$conditions = [
				'date_from IS NOT'=>null,
				'date_to IS'=>null,
				'code'=>$_POST['code'],
				'system_id'=>$_POST['system_id'],
			];
			
		}
		//pr($conditions);
		$find = $this->Attendances->find()
            ->select(['id','code','date_from','date_to'])
			->where($conditions)
			->order('id DESC')
			->first()
        ;
		//pr($find);
		if ($find && $_POST['type'] == 'enter'){
			die(json_encode(['result'=>false,'message'=>$_POST['name'].' ('.$_POST['code'].') nemá uzavřenu přechozí směnu']));
		} elseif (!$find && $_POST['type'] == 'exit'){
			die(json_encode(['result'=>false,'message'=>$_POST['name'].' ('.$_POST['code'].') nemá zaznamenán začátek směny']));
		} else {
			
			$saveData = [
				'code'=>$_POST['code'],
				'delivery_id'=>$_POST['delivery_id'],
				'name'=>$_POST['name'],
				'system_id'=>$_POST['system_id'],
			];
			if ($find){
				$saveData['id'] = $find->id;
			}
			if ($_POST['type'] == 'enter'){
				$saveData['date_from'] = new Time(date('Y-m-d h:i:s'));
				$titleResult = 'Začátek směny uložen';
			}
			if ($_POST['type'] == 'exit'){
				$saveData['date_to'] = new Time(date('Y-m-d h:i:s'));
				$titleResult = 'Konec směny uložen';
			}
			//pr($saveData);die();
			$this->saveAttendaces($saveData);
			
			
			if (isset($_POST['system_id'])){
				$data_load = $this->Attendances->attendancesList($_POST['system_id']);
				//pr($data_load);
				$connected_list = [];
				foreach($data_load AS $d){
					$hash = array(
						'pokladna_id'=>$_POST['system_id'],
						'rozvozce_id'=>$d->delivery_id,
						'code'=>$d->code,
					);
					$hash = $this->encode_long_url($hash);
					$url = $this->system_list[$_POST['system_id']]['url'].'/mobile/'.$hash;
					//echo '<img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='.$url.'&choe=UTF-8" /><br />';
					$d->qr = 'https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='.$url.'&choe=UTF-8';
					
					$connected_list[] = [
						'id'=>$d->delivery_id,
						'name'=>$d->name,
						'code'=>$d->code,
						'qr'=>$d->qr,
					];
				}
			
			}
		
			//pr($connected_list);
			die(json_encode(['result'=>true,'message'=>$titleResult,'connectedDeliverys'=>$connected_list]));	
		}
		
		
	}
	
	public function test2(){
		$_POST['system_id'] = 3;
		$url = $this->system_list[$_POST['system_id']]['url'].'mobile/'.$hash;
		pr($url);
		die('a');
	}
	
	/*** ENCODE LONG URL *
	private function encode_long_url($data){
         return $output = strtr(base64_encode(addslashes(gzcompress(json_encode($data),9))), '+/=', '-_,');    
    }
	*/
	
	private function saveAttendaces($saveData){
		$save_entity = $this->Attendances->newEntity($saveData);
		$this->Attendances->save($save_entity);
	}
	
	
	public function test(){
		$this->autoRender = false;
		$ch = curl_init();
		$post = [
			'type'=>'exit',
			//'type'=>'enter',
			'name'=>'Kuba',
			'code'=>'111',
			'system_id'=>1,
		];
		
		curl_setopt($ch, CURLOPT_URL, 'http://chacharint.fastest.cz/api/attendances/save/');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		curl_close ($ch);
		//pr('curl result');
		//pr($result);
		
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro uložení docházky, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
		//	pr($result);
			$result = json_decode($result,true);
		}
		pr($result);
		
		
	}
	
	
}