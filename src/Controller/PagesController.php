<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

class PagesController extends AppController
{
    public function homepage(){
        $this->checkLogged();
        $this->set('simpleLayout',true);
        
        //načtení skupin
        $this->loadModel('ProductGroups');
        $groupList = $this->ProductGroups->groupList();
        $this->set('groupList',$groupList);

        $this->loadModel('Stocks'); 
        $conditions=[
            'stock_item_product_id >'=>0,
        ];
        $data = $this->Stocks->find()
            ->select([

            ])
            ->order('Stocks.id ASC')
            ->where($conditions)
            ->contain(['Products'])
            ->toArray();
            //pr($data);
            
            //e('a');
        //return $this->render('../Pages/ng_layout');
    }
	
	public function logGpsTemp(){ 
		$message = date('Y-m-d H:i:s'). ': ' . strtr($_SERVER['REMOTE_ADDR'],['.'=>'']). ' - '. json_encode($_POST);
        file_put_contents( ROOT . '/tmp/logs/gps_log.txt', $message . PHP_EOL, FILE_APPEND);
		die('test pages');
    
	}
	
	public function switchOnline($json=false){
		$this->checkLogged();
		//$this->system_id = 24;
     	$curlUrl = $this->system_list[$this->system_id]['url'].'api/statusOnlineChange/'.$this->system_id;
		//pr($curlUrl);die();
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close ($ch);

		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro nahrání skupin produktů, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
        
        } else {
			//pr($result);
			$result = json_decode($result);
		}
		if ($json){
			die(json_encode($result));
		}
		$this->redirect('/status-online/');
		//die(json_encode($result));
	}
     public function statusOnline(){
		$this->checkLogged();
		
		$this->set('title','Stav zapnutí online objednávek');
		
		$ch = curl_init();
        $post = [];
        //pr($this->system_list);die();
		$curlUrl = $this->system_list[$this->system_id]['url'].'/api/statusOnline/'.$this->system_id;
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//pr('curl result');
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro uložení získání stavu online, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
            //pr($result);die();
			$result = json_decode($result,true);
            if (json_encode($result) == null){
                pr($result);
            }
			$this->set('onlineStatus',$result);
			//pr($result);
        }
		
	}
    
    public function login(){
        $this->viewBuilder()->layout("public");
        $this->set('title', 'Přihlášení');
        $domain = null;
        
        if(isset($_GET['a']) && $_GET['a'] == 'ab93439f0757f40c4fa7c45e4ab0229e'){
            $superadmin = true;
            $this->set('domain_list', $domain_list = $this->loadDomainList());
        } else{
            $superadmin = false;
            if(isset($_GET['d']) && is_base64_encoded($_GET['d'])){
                //Doména je nutná z důvodu načtení configu pro DB
                $domain = base64_decode($_GET['d']);
                /*  Ulozeni do Session, kvuli tomu že když proběhne odhlášení (uživatelem nebo vypršení session) tak aby 
                    po přesměrování na /prihlasit došlo k automatickému přidání Hashe do URL 
                */
                $this->request->session()->write('domain', $domain); 
            }else{
                if(($domainHash = getDomainHash())){
                    $this->request->session()->write('domain', null);
                    return $this->redirect(['controller'=>'Pages', 'action'=>'login','prefix'=>false, '?'=>['d'=>$domainHash]]);
                } 
                throw new NotFoundException();
            }
            $this->set('domain', $domain);
        }
 
        $this->set('superadmin', $superadmin);
        
        if ($this->request->is('post')) {
            //Z duvodu vypreseni session provadime ulozeni aktualni domeny do cookies, aby slo presmerovat na spravny Hash domeny
            setcookie("domain", $domain);
            
            $data = $this->request->data();


            if($superadmin && isset($data['domain_id']) && isset($domain_list[$data['domain_id']])){
                $domain = $domain_list[$data['domain_id']];
               
                $this->request->session()->write('domain', $domain);  
            }
            if(!$domain){
                $this->Flash->error('Není zvolena doména',['key' => 'auth']);
            }else{
                try{
                    $this->loadDomainSettings($domain);
                    
                    $domainOk = true;
                }catch(\Exception $e){
                    $this->Flash->error($e->getMessage(), [
                        'key' => 'auth'
                    ]);
                    $domainOk = false;
                }      
                if($domainOk){
                    try{
                        if(!($user = $this->superAdminLogin($data))){
                            $user = $this->Auth->identify();
                        }
                        if ($user) {
                           
                            $this->loadModel('States');
                            $user['languages'] = $this->States->find()->where(['translation_shortcut IS NOT NULL'])/*->cache('languages')*/->toArray();
                            
                            $this->Auth->setUser($user);
                            $this->request->session()->write('userMenu', $this->loadMenu());
                            return $this->redirect($this->Auth->redirectUrl());
                        } else {
                            $this->Flash->error(__('Uživatelské jméno nebo heslo je nesprávné'), [
                                'key' => 'auth'
                            ]);
                        }
                    }catch (\Exception $e){
                        $this->Flash->error(__('Nesprávné nastavení domény: '.$domain.'. ' . $e->getMessage()), [
                            'key' => 'auth'
                        ]);
                    }
                }
            }
        }
    }
    
    /**
     * Prihlaseni superadmina... globalni pro cely admin a vsechny domeny
     * @param type $data
     * @return boolean|string
     */
    private function superAdminLogin($data){
   
        if(isset($data['username']) && $data['username'] == 'superadmin' && isset($data['password']) && $data['password'] == 'program458'){
            $user = [
                'id' => -1,
                'user_group_id' => -1,
                'username' => 'superadmin',
                'first_name' => 'Super',
                'last_name' => 'Admin',
                'email' => 'kolenovsky@fastest.cz',
                'avatar' => 'fst_avatar.png'
            ];
            return $user;
        }else{
            return false;
        }
    }
    
    //Nahrati seznamu domen pro super admina
    private function loadDomainList(){
        $this->loadModel('AdminUsers');
        $list = $this->AdminUsers->find('list', [
            'keyField' => 'id',
            'valueField' => 'domain'
        ])
            ->cache('central_domain_list')
            ->toArray();
        return $list;
    }
    
    
    public function logout() {
        $this->request->session()->write('userMenu', null);
        $this->request->session()->write('domainSetting', null);
        //$domain = $this->request->session()->read('domain');
        return $this->redirect($this->Auth->logout() /*. '?d='.  base64_encode($domain)*/);
    }
    
    public function register()
    {
        $this->loadModel('Users');
        $user = $this->Users->newEntity($this->request->data);
        if ($this->Users->save($user)) {
            $this->Auth->setUser($user->toArray());
            $domain = $this->request->session()->read('domain');
            return $this->redirect([
                'controller' => 'Pages',
                'action' => 'login',
                '?'=> ['d'=> $domain]
            ]);
        }
    }
    
    public function getUser(){
        $user = $this->request->session()->read('Auth.User');
        if($user){
            unset($user['updated']);
            unset($user['created']);
            unset($user['trash']);
            $user['userMenu'] = $this->request->session()->read('userMenu');
            $user['lists'] = $this->loadGlobalLists();
            $domainSetting = $this->request->session()->read('domainSetting');
            $user['domain_upload'] = $domainSetting->domain_upload;
            $user['domain'] = $this->request->session()->read('domain');
            
            die(json_encode(['result'=>true, 'data'=>$user]));
        }else{
            die(json_encode(['result'=>false]));
        } 
    }
    
    private function loadGlobalLists(){
        $lists = [];
        
        $states_shortcuts = [];
        if($this->useTranslate){
            //pr($this->request->session()->read('Auth.User.languages'));
            foreach($this->request->session()->read('Auth.User.languages') as $state){ 
                $states_shortcuts[$state->id] = $state->translation_shortcut;
            }
        }
     
        $this->loadModel('Taxes');
        $tQuery = $this->Taxes->find()->select(['id','name'=>'name_short','state_id','value'])->order('id ASC')->hydrate(false);
        if(!$this->useTranslate){
            $tQuery->where(['state_id'=>1]);
        }
        $taxes = $tQuery->toArray();
        
        $lists['tax_values'] = $lists['taxes'] = [];
        foreach($taxes as $item){
            $lists['tax_values'][$item['id']] = $item['value'];
            if(!$this->useTranslate){
                $lists['taxes'][] = $item;
            }else{
                if(isset($states_shortcuts[$item['state_id']])){
                    $lists['taxes'][$states_shortcuts[$item['state_id']]][] = $item;
                }
            }
        } 

        $lists['param_types'] = [['id'=>1, 'name'=>'Select'],['id'=>2, 'name'=>'Text']];
        return $lists;
    }
    
    private function loadMenu(){
        $domain = $this->request->session()->read('domain');
        
        $name = 'default';
        
        $domainSetting = $this->request->Session()->read('domainSetting');
        if($domainSetting->modul_name && file_exists(__DIR__ . '/menu_settings/' . $domainSetting->modul_name . '.json')){
            $name = $domainSetting->modul_name;
        }
        $path = __DIR__ . '/menu_settings/'. $name .'.json';
        
        if(!file_exists($path)){
            $menu = [];
        }else{
            $menu = json_decode(file_get_contents($path), true);
            //pr($menu);
            $group_id =  $this->request->session()->read('Auth.User.user_group_id');
            
            if($group_id > 0){
                $this->loadModel('UserGroups');
                $userGroup = $this->UserGroups->find()->where(['id'=>$group_id])->first();
                if($userGroup){
                    $newMenu = [];
                    $userGroup->permissions = json_decode($userGroup->permissions, true);
                    foreach($menu as $i=>$menuItem){
                        if(isset($menuItem['modul'])){ 
                            //pr($userGroup->permissions[$menuItem['modul']]); 
                            if(( $menuItem['modul'] == '' || (isset($userGroup->permissions[$menuItem['modul']]) && $userGroup->permissions[$menuItem['modul']]['index'] != 'none'))){
                                $newMenu[] = $menuItem;
                            }
                        }else{
                            if(isset($menuItem['subitems'])){ 
                                $newSubmenu = ['subitems'=>[]];
                                foreach($menuItem['subitems'] as $i=>$subMenuItem){
                                   if(isset($subMenuItem['modul'])){ 
                                       if(( $subMenuItem['modul'] == '' || (isset($userGroup->permissions[$subMenuItem['modul']]) && $userGroup->permissions[$subMenuItem['modul']]['index'] != 'none'))){
                                           $newSubmenu['subitems'][] = $subMenuItem;
                                       }
                                   }
                                }
                                if(count($newSubmenu['subitems']) > 0){
                                    unset($menuItem['subitems']);
                                    $newSubmenu = array_merge($newSubmenu , $menuItem);
                                    $newMenu[] = $newSubmenu;
                                }
                           }
                        }
                        
                    }
                    $menu = $newMenu;
                }
            }else if(!$group_id && $group_id !== -1){
                $menu = [];
            }
        }

        /*$menu = [
            ['ico'=>'fa-dashboard','name'=>__('Nástěnka'), 'url'=>'/'],
            ['ico'=>'fa-shopping-cart','name'=>__('Produkty'), 'url'=>'/shop/products'],
            ['ico'=> 'fa-shopping-bag','name'=>__('Objednávky'), 'url'=>'/shop/orders'],
            ['ico'=> 'fa-male','name'=>__('Klienti'), 'url'=>'/shop/clients'],
            ['ico'=>'fa-wrench', 'name'=>__('Eshop - nastavení'), 'url'=>'#', 'subitems'=>[
                ['ico'=>'fa-tags','name'=>__('Kategorie produktů'), 'url'=>'/shop/categories'],
                ['ico'=>'fa-car','name'=>__('Dopravy'), 'url'=>'/shop/transports'],
                ['ico'=>'fa-money ','name'=>__('Platby'), 'url'=>'/shop/payments'],
                ['ico'=>'fa-lightbulb-o','name'=>__('Doplňkové služby'), 'url'=>'/shop/additional_services'],
                ['ico'=>'fa-gift','name'=>__('Slevové kupóny'), 'url'=>'/shop/discount_vouchers'],
                ['ico'=> 'fa-list','name'=>__('Číselníky parametrů'), 'url'=>'/shop/param_lists'],
                ['ico'=>'fa-truck','name'=>__('Číselník dodavatelů'), 'url'=>'/shop/suppliers'],
                ['ico'=>'fa-industry','name'=>__('Číselník výrobců'), 'url'=>'/shop/manufacturers']
            ]],
            ['ico'=>'fa-paragraph', 'name'=>__('Cms - Správa obsahu'), 'url'=>'#', 'subitems'=>[
                 ['ico'=>'fa-pencil','name'=>__('Články'), 'url'=>'/cms/articles'],
                 ['ico'=>'fa-newspaper-o','name'=>__('Aktuality'), 'url'=>'/cms/actuals'],
                 ['ico'=>'fa-th-large','name'=>__('Doplňkové boxy'), 'url'=>'/cms/smallboxes'],
                 ['ico'=>'fa-list','name'=>__('Kategorie menu'), 'url'=>'/cms/menu_items']
            ]],
            ['ico'=>'fa-cogs', 'name'=>__('Nastavení'), 'url'=>'#', 'subitems'=>[
                 ['ico'=>'fa-user','name'=>__('Uživatelé'), 'url'=>'/users'],
                 ['ico'=>'fa-users','name'=>__('Uživatelské skupiny'), 'url'=>'/user_groups'],
                 ['ico'=>'fa-envelope','name'=>__('Emailové šablony'), 'url'=>'/email_templates'],
                 ['ico'=>'fa-wrench','name'=>__('Nastavení stránek'), 'url'=>'/www_settings']
            ]]
        ]; 
        
        if($domain == 'prim-hodinky.cz'){
            $menu[1]['url'] = '/prim/products';
        }*/
        return $menu;
    }
}
