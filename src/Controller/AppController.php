<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Exception\Exception;
use Cake\I18n\I18n;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\Database\Type;
use Cake\Core\Configure;
date_default_timezone_set('Europe/Paris');
Time::$defaultLocale = 'cs-CZ';
//Type::build('date')->setLocaleFormat('yyyy-MM-dd');
// Type::build('datetime')->setLocaleFormat('yyyy-MM-dd HH:mm:ss'); 
Time::setToStringFormat('yyyy-MM-dd HH:mm:ss');
Date::setToStringFormat('dd.MM.yyyy');
FrozenDate::setToStringFormat('dd.MM.yyyy');
FrozenTime::setToStringFormat('dd.MM.yyyy HH:mm');

class AppController extends Controller
{

    public $return = [
        'data' => [],
        'lists' => [],
        'result' => false,
        'count' => null,
        'message' => null,
       // 'validations' => [],
       // 'validation_errors' => false // if validation error is thrown... then reutrn array with errors
    ];
   
    public $languages = ['cz'];
     
    public function initialize()
    {
        parent::initialize();
        //pr(date());

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator', [
            'limit' => 50
        ]);
        
        if(isset($_GET['per-page']) && $_GET['per-page'] > 0 && $_GET['per-page'] <= 50){
            $this->paginate['limit'] = $_GET['per-page'];
        }

        $loginAction = [ 'controller' => 'pages', 'action' => 'login', 'prefix'=>false];
      
        //Pri kazdem requestu se musi nastavit hodnota zda je ci neni povoleny preklad
        $this->set('version',$this->version = Configure::read('version'));
        $this->set('appName',$this->appName = Configure::read('appName'));
        $this->set('appPath',$this->appPath = Configure::read('appPath'));
		
		$this->set('here', $this->request->here);
		
    }

    /**
     * 
     */
    public function beforeFilter( \Cake\Event\Event $event){
        $this->initReporting();
        
        I18n::locale('cz');
        $this->checkLogged();
     
        $this->load_select_config();
        $this->loadModel('Users');
        $this->system_list = $this->Users->userList();
        
        $this->getBranchesList();
        //$this->switchBranch();

        $this->systemLogs();
        
   }

   /**
    * Before render callback.
    *
    * @param \Cake\Event\Event $event The beforeRender event.
    * @return void
    */
   public function beforeRender(Event $event)
   {
    
       if (!array_key_exists('_serialize', $this->viewVars) &&
           in_array($this->response->type(), ['application/json', 'application/xml'])
       ) {
           $this->set('_serialize', true);
       }
       //pr($this->system_list);
      
       $this->genSubmenu();
       $this->genBreadcums();
  
       
   }

   /**
    * MOnitoring a zasilani errors 
    */
   private function initReporting()
   {
       
       if (strpos($_SERVER['HTTP_HOST'],'localhost') > 0){
           $env = 'local';
       } else if (strpos($_SERVER['HTTP_HOST'],'fastestdev.cz') > 0){
           $env = 'development';
		   
			if ($_SERVER['REMOTE_ADDR'] != '78.45.122.114'){
				$this->redirect('http://intranet.ollies.cz');
			}
       } else {
           $env = 'production';
       }
       $_ENV['env'] = $env;
       $this->set('monitoringEnableLocal', Configure::read('monitoring')['enableLocal']);
       $this->set('monitoringAccessToken', Configure::read('monitoring')['access_token']);
       $this->set('env', $_ENV['env']);
   }

   /**
    * Zapsani datumu posledni upravy DB pro stazeni k pokladnam
    */
   protected function writeUpdateDate(){
        $myfile = fopen("uploaded/updated.txt", "w");
        if($myfile === false){
            return false;
        }
        $txt = date('Y-m-d H:i:s');
        fwrite($myfile, $txt);
        fclose($myfile);
        return true;
   }

    private function systemLogs(){
        if (isset($this->loggedUser->id)){
        $saveLog = [
            'user_id'=>$this->loggedUser->id,
            'controller'=>$this->request->controller,
            'action'=>$this->request->action,
            'url'=>$this->request->here,
            'data'=>(!empty($this->request->data)?json_encode($this->request->data):null),
        ];
        $this->loadModel('SystemLogs');
        $logEntity = $this->SystemLogs->newEntity($saveLog);
        // pr($logEntity);
        $disableList = [
            'SystemLogs',
        ];
        if (!in_array($this->request->controller,$disableList)){
            $this->SystemLogs->save($logEntity);
        
        }
        }
    }

    private function genBreadcums(){
        $currentUrl = $this->request->here;
        $action = $this->request->params['action'];
        if (isset($this->request->params['pass'][0])){
            $editId = $this->request->params['pass'][0];
        }
        // pr($this->request);
        $actions_list = [
            'edit'=>'Editace',
        ];
        $splitUrl = explode('/',$currentUrl);
        // pr($splitUrl);
        $firstUrl = $splitUrl[1];
        $path = [];

        if(is_array($this->menu)){
            foreach($this->menu AS $kk=>$mm){
                if ($mm['url'] == '/'.$firstUrl.'/'){
                    $path[] = ['url'=>$mm['url'],'name'=>$mm['name']];   
                }
                if (isset($mm['child'])){

                    foreach($mm['child'] AS $k=>$m){
                        //   pr($mm['child']);
                        if ($m['url'] == '/'.$firstUrl.'/') {
                            if (!isset($path[0])){
                                $path[] = ['url'=>$mm['url'],'name'=>$mm['name']];   
                                $path[] = ['url'=>$m['url'],'name'=>$m['name']];   
                            }
                        }
                    }
                }
            }
        }
        if (isset($this->request->query['type_id'])){
            if (isset($this->stock_type_list[$this->request->query['type_id']])){
                $specTitle = $this->stock_type_list[$this->request->query['type_id']];
            }
        }
        if (isset($actions_list[$action]))
        $path[] = ['name'=>(isset($editId)?$actions_list[$action].' ID: '.$editId:($action == 'edit'?(isset($specTitle)?$specTitle:'Nová položka'):$actions_list[$action]))];
        $last = end($path);
        unset($last['url']);
        $lastKey = key($path);
        // pr( $lastKey);
        unset($path[$lastKey]);
        $path[] = $last;
        $this->set('breadcums',$path);
    }

    private function getBranchesList(){
        $this->loadModel('Branches');
        $this->branches_list = $this->Branches->branchesList();
        $this->set('branches_list',$this->branches_list);
        // pr($this->branches_list);
    }


    public function genSubmenu(){
        $this->set('activeUrl', $this->request->here);
        $url = explode('/', $this->request->here);
        
        if(is_array($this->menu)){
            foreach($this->menu AS $m){
                if ($m['url'] == $this->request->here || (isset($url[1]) && $m['url'] == '/' . $url[1] . '/')) {
                    if (isset($m['child'])) {
                        $this->set('submenu', $m['child']);
                    }
                }
                if (isset($m['child']))
                foreach($m['child'] AS $mm){
                    if ($mm['url'] == $this->request->here){
                        if (isset($m['child'])) {
                            $this->set('here', $m['url']);
                            $this->set('submenu', $m['child']);
                        }
                    } elseif (isset($url[1]) && isset($url[2]) && $mm['url'] == '/' . $url[1] . '/' . $url[2] . '/') {
                        if (isset($m['child'])) {
                            $this->set('activeUrl', '/' . $url[1] . '/' . $url[2] . '/');
                            $this->set('here', $m['url']);
                            $this->set('submenu', $m['child']);
                        }
                    }
                }
            }
        }
    }
        
    public function isAuthorized($user = null)
    {
        return true;
        // Any registered user can access public functions
        if (empty($this->request->params['prefix'])) {
            return true;
        }

        // Only admins can access admin functions
        if ($this->request->params['prefix'] === 'admin') {
            return (bool)($user['role'] === 'admin');
        }

        // Default deny
        return false;
    }
	
	
	public function checkStatusOnline(){
		$ch = curl_init();
        $post = [];
		//$this->system_id = 24;
		$curlUrl = $this->system_list[$this->system_id]['url'].'/api/statusOnline/'.$this->system_id;
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//pr('curl result');
		
            //pr($result);die('a');
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro uložení získání stavu online, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
			$result = json_decode($result,true);
            if (json_encode($result) == null){
                pr($result);
            }
			$this->set('onlineStatus',$result);
			//pr($result);
        }
		
	}
	
	public function check_error($entity=null){
		if ($entity != null && $entity->errors()){
			$message = '';
			$invalid_list = array();
			//pr($entity->errors());
			$convert_valid = [
				'date_'=>'date-',
			];
			
			foreach($entity->errors() AS $k=>$mm){
				foreach($mm AS $mk=>$m){
					if (is_array($m)){
						$invalid_list[] = $k.'-'.strtr($mk,$convert_valid);
						foreach($m AS $m2){
						$message .= $m2.'<br />';
						
						}
						
					} else {
						$message .= $m.'<br />';
						
					}
					
				}
				$invalid_list[] = strtr($k,$convert_valid);
			}
			die(json_encode(['result'=>false,'message'=>$message,'invalid'=>$invalid_list]));
		}
    }
    
    /**
     * Checks if user is logged.
     * 
     * @return void
     * 
     * @access public
     */
    public function checkLogged()
    {
        $session = $this->request->session();
        $this->request->session()->write("requestedURL", $this->request->here);
        
		if ($session->read('loggedUser')){
			$this->loggedUser = $session->read('loggedUser');
			$this->set('loggedUser', $this->loggedUser);
			$this->set('system_id',$this->system_id = $this->loggedUser->system_id);
		}
		
		if (!$this->loggedUser) {
            $login_url = ['/login','/login/'];
            if (!in_array($this->request->here, $login_url)) {
                
                $controllersWithActionsAllowed = [
                    "Statistics" => [
                        "show"
                    ],
                    "Orders" => [
                        "poklWebOrderSave"
                    ],
                    "Stocks" => [
                        "saveProductForWeb"
                    ]
                ];

                $result = $this->in_array_get_position($this->request->params["controller"], $controllersWithActionsAllowed, true);

                if ($result >= 0) {
                    if (!in_array($this->request->params["action"], $controllersWithActionsAllowed[$result])) {
                        $this->redirect('/login/');
                    }
                } else {
                    $this->redirect('/login/');
                }
            }
		}
    }

    /**
     * Checks if a value exists in an array (-1 not found, 0-n found).
     * 
     * @param mixed $needle
     * @param array $haystack
     * @param boolean $key
     * 
     * @return int|string
     * 
     * @access private
     */
    private function in_array_get_position($needle, $haystack, $key)
    {
        $iterator = 0;

        if ($key) {
            foreach ($haystack as $key => $value) {
                if ($needle == $key) {
                    if (is_string($key)) {
                        return $key;
                    } else {
                        return $iterator;
                    }
                }

                $iterator++;
            }
        } else {
            foreach ($haystack as $stem) {
                if ($needle == $stem) {
                    return $iterator;
                }

                $iterator++;
            }
        }
        
        return -1;
    }
    
    /**
     * Checks if a value exists in an array of array.
     * 
     * @param mixed $needle
     * @param array $haystack
     * 
     * @return boolean
     * 
     * @access private
     */
    private function deep_in_array($needle, $haystack)
    {
        if (in_array($needle, $haystack)) {
            return true;
        }

        foreach ($haystack as $element) {
            if (is_array($element) && deep_in_array($needle, $element)) {
                return true;
            }
        }

        return false;
    }
	
	 /*** DECODE LONG URL **/	
	public function decode_long_url($data){
    	$data2 = explode('?',$data);
		$data = $data2[0];
		return json_decode(@gzuncompress(stripslashes(base64_decode(strtr($data, '-_,', '+/=')))),true);
    }

	/*** ENCODE LONG URL **/	
	public function encode_long_url($data){
         return $output = strtr(base64_encode(addslashes(gzcompress(json_encode($data),9))), '+/=', '-_,');    
    }
	
	
    private function load_select_config(){
		$sc = Configure::read('select_config');
		$this->select_config = $sc;
		
		foreach($sc AS $k=>$item){
			$this->$k = $item;
			$this->set($k,$item);
		}
		//pr($sc);
		$kurz_eu = Configure::read('kurz_eu');
		$this->set('kurz_eu',$kurz_eu);
		//pr($kurz_eu);
		//die();
	}
	
    
        
    public function getData($clearTimestamps = true){
         $data = json_decode(file_get_contents("php://input"), 1);
         if($clearTimestamps){
             if(isset($data['created'])){
                 unset($data['created']);
             }
             if(isset($data['modified'])){
                 unset($data['modified']);
             }
         }
         return $data;
    }
    
}
