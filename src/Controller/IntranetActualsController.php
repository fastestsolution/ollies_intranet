<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;

class IntranetActualsController extends AppController{
    
	
	public function index2(){
		$this->checkLogged();
		$conditions = [];
		
		$this->loadComponent('ViewIndex');
		$conditions = $this->ViewIndex->conditions($conditions);
		//pr($conditions);
		$data = $this->IntranetActuals->find()
		->where($conditions)
		->order('id DESC')
		->select([
			'id',
			'name',
			'created',
			'text',
			'type',
			'files',
		])
		/*
		->map(function($row){
			$row->files = json_decode($row->files);
			return $row;
		})
		*/
		;
		
		$filtrations = [
			'type'=>['name'=>'Typ','key'=>'type','type'=>'select','options'=>[''=>'Vyberte typ']+$this->intranet_type_list],
		];
		$this->set('filtrations',$filtrations);
		
		
		$this->set('data', $this->paginate($data)->toArray());
		
		if ($this->request->isAjax){
			$this->render('items2');
		} else {
			
			$this->render('index2');
		}
		//pr($data);
	}
	
    public function index(){
		$this->checkLogged();
		$this->loadComponent('ViewIndex');
		
		$cols = [
			'id'=>['name'=>'ID'],
			'name'=>['name'=>'Provoz'],
		];
		$topActions = [
			'edit'=>['name'=>'Nová položka','url'=>'/users/edit/'],
		];
		$filtrations = [
			'name'=>['name'=>'Provoz','key'=>'name','type'=>'like'],
		];
		$posibility = [
			'edit'=>['name'=>'Editovat','url'=>'/intranetEdit/edit/'],
			'trash'=>['name'=>'Smazat','url'=>'/intranetEdit/trash/'],
		];
		
		$conditions = [];
		$conditions = $this->ViewIndex->conditions($conditions);
		//pr($conditions);
		$data = $this->IntranetActuals->find()
		->where($conditions)
		->order('id DESC')
		->select([
			'id',
			'name',
		]);
		//->toArray();
		
		if (empty($data)){
			$data = null;
		}
		
		$params = [
			'filtrations'=>$filtrations,
			'topActions'=>$topActions,
			'cols'=>$cols,
			'posibility'=>$posibility,
			'data'=>$data,
		];
		
		$this->ViewIndex->load($params);
		
	
	}

    public function edit($id = null){
		
		$this->checkLogged();
        $data = $this->IntranetActuals->newEntity(); 
        
		if ($id != null){
			$conditions = ['id'=>$id];
			$data = $this->IntranetActuals->find()
            ->where($conditions)
            ->first()
            ;
		
        }
		$this->set(compact("data"));
        
		if (!empty($this->request->data)){   
			if (!empty($this->request->data['files_delete'])){
				$filesDelete = json_decode($this->request->data['files_delete']);
				foreach($filesDelete AS $f){
					$path = '.'.$f->path.$f->file;
					@unlink($path);
				}
			}
			//pr($this->request->data);die();
			
			$saveData = $this->IntranetActuals->patchEntity($data,$this->request->data);
			 $this->check_error($data);
	 
			//pr($saveData);die();
			$this->IntranetActuals->save($saveData);
           	die(json_encode(['result'=>true,'message'=>'Uloženo']));
			  
        }
    }
	
	public function convertFiles(){
		
		$data = $this->IntranetActuals->find()
		->contain(['IntranetFiles'])
		//->limit(1)
		->map(function($row){
			if ($row->intranet_files){
				$files = [];
				$uploadDirectory = "/uploaded/intranet/";
				foreach($row->intranet_files AS $f){
					//pr($f->file);
					$fileName = unserialize($f->file);
					$fileName = $fileName[0];
					$fileName = explode('|',$fileName);
					$files[] = [
						'file'=>$fileName[0],
						'fileName'=>(!empty($f->name)?$f->name:$fileName[0]),
						'path'=>$uploadDirectory,
					];
				}
				//pr($files);
				$row->files = json_encode($files);
				
				unset($row->intranet_files);
			}
			return $row;
		})
		->toArray();
		//pr($data);
		foreach($data AS $d){
		pr($d);
		//$saveData = $this->IntranetActuals->newEntity($d);
		//pr($saveData);	
		$this->IntranetActuals->save($d);
		}
		die();
	}
	
	public function fstUpload(){
		$file_list = [];
		//pr($_POST);
		//pr($this->request);die();
		//pr($_FILES);
		$currentDir = getcwd();
		$uploadDirectory = "/uploaded/intranet/";

		$errors = []; // Store all foreseen and unforseen errors here

		$fileExtensions = ['jpeg','jpg','png','gif','doc','pdf','xls','docx','avi']; // Get all the file extensions
		//pr($this->request->data);
		//pr($this->request->data);die();
		//pr($_FILES);die();
		$i = 0;
		foreach($this->request->data['files'] AS $file){
		//pr($file);
		$fileName = $file['name'];
		$fileSize = $file['size'];
		$fileTmpName  = $file['tmp_name'];
		$fileType = $file['type'];
		$fileExtension = explode('.',$fileName);
		$fileExtension = strtolower(end($fileExtension));
		
		$fileName = uniqid('file_').'.'.$fileExtension;
		$path = $currentDir . $uploadDirectory;
		$uploadPath = $path . basename($fileName); 

		//echo $uploadPath;

		if (isset($fileName)) {

			if (! in_array($fileExtension,$fileExtensions)) {
				
				die(json_encode(['result'=>false,'message'=>'Chyba nahrání špatný typ souboru']));
				//$errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
			}

			if ($fileSize > 10000000) {
				die(json_encode(['result'=>false,'message'=>'Chyba nahrání soubor je moc velký']));
	
				//$errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
			}

			if (empty($errors)) {
				$didUpload = move_uploaded_file($fileTmpName, $uploadPath);

				if ($didUpload) {
					//pr($fileName);
					$file_list[] = [
						'file'=>$fileName,
						'fileName'=>$this->request->data['filesName'][$i],
						'path'=>$uploadDirectory,
					];
					//echo "The file " . basename($fileName) . " has been uploaded";
				} else {
					//echo "An error occurred somewhere. Try again or contact the admin";
				}
			} else {
				foreach ($errors as $error) {
					//echo $error . "These are the errors" . "\n";
				}
			}
		}
		$i++;
		}
		
		die(json_encode(['result'=>true,'message'=>'Soubor nahrán','fileList'=>$file_list]));
	}
    
  

}
