<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class MapAreasController extends AppController{
    
	
	public function getMaps($system_id=null){
		if ($system_id == null)	
			die(json_encode(['result'=>false,'message'=>'Neni system id']));

		$load = $this->MapAreas->find()
		->select(['coords'])
		->where(['id'=>$system_id])
		->first();
		//pr($load);
		if (!$load || empty($load->coords)){
			die(json_encode(['result'=>false,'message'=>'Nenalezeny oblasti rozvozu']));
			
		}
		die(json_encode(['result'=>true,'message'=>'Mapove oblasti byly importovány','data'=>json_decode($load->coords)]));
	}
	
	
	/**
	* save map area
	*/
	public function defineMap(){
		//sleep(6);
		//print_r($_POST);die();
		//$this->system_id = 1000;
		$this->checkLogged();
		$this->set('title','Mapy rozvozu');
		
		//$save_entity = $this->Orders->newEntity($_POST,['associated' => ["Clients"=>['validate' => 'default','accessibleFields'=>['id' => true]], "Clients.ClientAddresses","OrderItems"]]);
		if ($this->request->is("ajax")){
			$this->request->data['coords'] = $this->request->data['area_coords'];
			$this->request->data['id'] = $this->system_id;
			$save_entity = $this->MapAreas->newEntity($this->request->data);
			if (!$resultDb = $this->MapAreas->save($save_entity)){
				die(json_encode(['result'=>false,'message'=>'Chyba uložení map']));	
			} else {
				die(json_encode(['result'=>true,'message'=>'Mapy uloženy']));	
			}
		} else {
			$load = $this->MapAreas->find()
			->where(['id'=>$this->system_id])
			->first();
			$this->set('load',$load);
		}
        
		
		
	}
	
	
	
}