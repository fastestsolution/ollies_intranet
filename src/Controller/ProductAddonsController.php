<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class ProductAddonsController extends AppController{

    public function beforeFilter(\Cake\Event\Event $event){

        parent::beforeFilter($event);
        $this->loadModel('SkladCiselniks');
        $this->set('sklad_group_list',$this->sklad_group_list = $this->SkladCiselniks->skladGrouplList());
    }
    public function index(){

        $this->set('title','Přídavky na produkty');
        $this->checkLogged();
        $this->loadComponent('ViewIndex');
        $this->genSubmenu();

        $cols = [
            'id'=>['name'=>'ID'],
            'name'=>['name'=>'Název'],
            'price'=>['name'=>'Cena'],
            'group_id'=>['name'=>'Skupina','list'=>$this->addon_list],
            'created'=>['name'=>'Vytvořeno'],
        ];
        $filtrations = [
            'name'=>['name'=>'Název','key'=>'name','type'=>'like'],
            //'code'=>['name'=>'Kod','key'=>'code','type'=>'like'],
           'group_id'=>['name'=>'Skupina','key'=>'group_id','type'=>'select','list'=>[''=>'Skupina']+$this->addon_list],
        ];
        $topActions = [
            'edit'=>['name'=>'Nová položka','url'=>'/product-addons/edit/'],
            //'products'=>['name'=>'Skladové produkty','url'=>'/stocks/products/'],
            //'import'=>['name'=>'Import produktů','url'=>'/stocks/importProducts/','type'=>'ajax'],
        ];
        $posibility = [
            'edit'=>['name'=>'Editovat','url'=>'/product-addons/edit/'],
            'trash'=>['name'=>'Smazat','url'=>'/product-addons/trash/'],
        ];

        //$conditions = ['system_id'=>$this->loggedUser->system_id];
        $conditions = [];
        $conditions = $this->ViewIndex->conditions($conditions);
        //pr($conditions);
        $mapper = function ($data, $key, $mapReduce) {
            $mapReduce->emit($data);
        };
        $data = $this->ProductAddons->find()
            ->where($conditions)
            ->order('id DESC')
            ->select([

                //'id',
                //'name',
                //'code',
                //'is_connected',
                //'full_name',
                'id',
                'name',
                'group_id',
                'price',
                'created',

            ])
            //->mapReduce($mapper)
        ;
        //->toArray();
        //pr($data->toArray());
        if (empty($data)){
            $data = null;
        }

        $params = [
            'filtrations'=>$filtrations,
            'topActions'=>$topActions,
            'cols'=>$cols,
            'posibility'=>$posibility,
            'data'=>$data,
        ];
        //pr($data->toArray());
        $this->ViewIndex->load($params);


    }

   

    public function edit($id = null){
        // $this->loadModel('StockProducts');
        // $this->set('stockProductsList',$this->stockProductsList = $this->StockProducts->productList());
        // $this->set('stockProducts',$this->stockProducts = $this->StockProducts->productList(true));
        $this->checkLogged();
        $data = $this->ProductAddons->newEntity();
        //pr($data);

        if ($id != null){
            $conditions = ['id'=>$id];
            $data = $this->ProductAddons->find()
                ->where($conditions)
                //->select(['id'])
                ->first()
            ;

        }

        $this->set(compact("data"));

        if (!empty($this->request->data)){

            //pr($this->request->data);die();
            $saveData = $this->ProductAddons->patchEntity($data,$this->request->data);

            $this->check_error($data);
            //pr($saveData);die();

            $this->ProductAddons->save($saveData);

            Cache::clear();
            die(json_encode(['result'=>true,'message'=>'Uloženo']));

        }
    }

   
    public function trash($id){
        $conditions = ['id'=>$id];
        $this->loadModel('ProductAddons');
        $this->ProductAddons->trashAll($conditions);
        $this->redirect('/product-addons/');


    }




}