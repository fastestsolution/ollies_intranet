<?php
namespace App\Controller;

use Cake\Routing\Router;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use Cake\I18n\Date;
use App\Component\vIComponent;
use Cake\Utility\Hash;
//use Cake\View\View;

/**
 * Celkove statistiky nakladu, prodeju, marze eventuelne vyvoj nakladu na skladove polozky v case
 */
class SummaryStatisticsController extends AppController
{   

	public function initialize(){
        parent::initialize();
	}

	/**
	 * Aktualni marze, naklady a prodeje jednotlivych produktu
	 * Je nutne spocitat naklady z receptur (posledni cena jednotlivych pouzitych skladovych polozek)
	 * Do budoucna se pravdepodobne bude upravovat zpusob vypoctu nakladovosti na prumerny naklad za obdobi
	 */
	public function productStats(){

		$this->set('title','Statistika marže');
		$this->loadComponent('ViewIndex');
		
		$conditions = [];
		$sumConditions = [];
		$view_type = 1;	//Zobrazovat vsechny sloupce, jen posledni nakupku nebo jen prumernou nakupku
		$from_date = null;
		$to_date = null;
		$system_id = null;
		$table_id = null;

		$this->loadModel('ProductGroups');
		$this->group_list = $this->ProductGroups->groupList();

		$this->loadModel('Branches');
		$this->branch_list = $this->Branches->branchesList();

		/*$this->loadModel('Tables');
		$this->table_list = $this->Tables->find('list')->toArray();*/
		

        $this->table_list = [1 => 'Klientsky', 2 => 'Zemestnanecky', 3 =>  'Firemni'];



		if(isset($this->request->query['view_type'])){
			$view_type = $this->request->query['view_type'] == '' ? 1 : $this->request->query['view_type'];
			unset($this->request->query['view_type']);
		}

		if(isset($this->request->query['system_id'])){
			$system_id = $this->request->query['system_id'] == '' ? null : $this->request->query['system_id'];
			unset($this->request->query['system_id']);
		}
	
		if(isset($this->request->query['created|fromDate|date']) && $this->request->query['created|fromDate|date'] != ''){
			$from_date = new Date($this->request->query['created|fromDate|date']);
			unset($this->request->query['created|fromDate|date']);
		}

		if(isset($this->request->query['created|toDate|date']) && $this->request->query['created|toDate|date'] != ''){
			$to_date = new Date($this->request->query['created|toDate|date']);
			unset($this->request->query['created|toDate|date']);
		}

		if(isset($this->request->query['table_id']) && $this->request->query['table_id'] != ''){
			$table_id = $this->request->query['table_id'];
			unset($this->request->query['table_id']);
		}

		$originalGroupId = null; //Podminka pro filtraci dle skupiny produktu
		$groupIds = null;
		if(isset($this->request->query['ProductConnect__product_group_id']) && $this->request->query['ProductConnect__product_group_id'] != ''){
			$originalGroupId = $this->request->query['ProductConnect__product_group_id'];
			$this->loadModel('ProductGroups');
			$group = $this->ProductGroups->find()->where(['id'=>$originalGroupId])->first();
			if($group && ($group->rght - $group->lft) > 1){
				$groupIds = $this->ProductGroups->find()->select(['id'])->where(['lft >='=> $group->lft, 'rght <='=>$group->rght])->toArray();
				if($groupIds){
					$groupIds = Hash::extract($groupIds, '{n}.id');
				}else{
					$groupIds = [];
				}
			}else{
				$groupIds = [$originalGroupId];
			}
			$this->loadModel('ProductConnects');
			$prodIds = $this->ProductConnects->find('list', ['keyField'=>'id', 'valueField'=>'product_id'])->where(['product_group_id IN'=>$groupIds])->group('product_id')->toArray();
			if(!empty($prodIds)){
				$conditions['Products.id IN'] = $prodIds;
			}else{
				$conditions['Products.id'] = 0;
			}
			unset($this->request->query['ProductConnect__product_group_id']);
		}

		$heading = $this->generateHeading($from_date, $to_date);
		
		$conditions = $this->ViewIndex->conditions($conditions);
		$appendix = '';
		if($table_id){
			$appendix = ' OR Orders.table_type_id != ' . $table_id;
		}
		
		$this->loadModel('Products');
		$query = $this->Products->find()
		 				 ->contain([ 
							  	'ProductRecipes' => function($q){ return $q->select(['product_id','stock_item_global_id','value']); },
							  	'OrderItems' => function($q) use ($from_date, $to_date, $system_id){ 
									$q->select(['OrderItems.product_id','OrderItems.price', 'OrderItems.price_without_tax','OrderItems.count']); 
									if($from_date){
										$q->where(['DATE(OrderItems.created) >='=> $from_date->format('Y-m-d')]);
									}
									if($to_date){
										$q->where(['DATE(OrderItems.created) <='=> $to_date->format('Y-m-d')]);
									}
									if($system_id){
										$q->where(['OrderItems.system_id'=> $system_id]);
									}
									//Nesmi se zapocitavat stornovane objednavky
									$q->where(['OR'=>['OrderItems.storno IS NULL','OrderItems.storno'=>0]]);
									return $q;
								},
						 ])
						  ->select(['Products.id', 'Products.name', 'Products.code',
									'sold_units'=>'SUM(IF(OrderItems.count IS NULL '.$appendix.', 0, OrderItems.count))', 
									'sold_price_item'=>'IF(OrderItems.price_without_tax IS NULL '.$appendix.', 0,  OrderItems.price_without_tax)', 
									'sold_price'=>'SUM(IF(OrderItems.price_without_tax IS NULL '.$appendix.', 0, OrderItems.price_without_tax * OrderItems.count))', 
									'sold_price_item_vat'=>'IF(OrderItems.price IS NULL '.$appendix.', OrderItems.price, 0)',
									'sold_price_vat'=>'SUM(IF(OrderItems.price IS NULL '.$appendix.', 0 ,OrderItems.price * OrderItems.count))',
						 ])
						 ->where($conditions)
						 ->group('Products.id');
						 //->map(function($data){ return $data; /* $this->loadExpensiveness($data);*/ }); //nefunguje callable [$this, 'loadExpensiveness']

		 //if($table_id){
			//$query->select(['bb'=>'GROUP_CONCAT(Orders.table_id SEPARATOR ",")']);
			//$query->select(['aa'=>'GROUP_CONCAT(Tables.id SEPARATOR ",")']);
			//$query->contain(['OrderItems.Orders']);
		 //}

		 $items = $this->Paginator->paginate($query, [
			'limit'=>30
		 ])->toArray();

		 $items = $this->loadExpensiveness($items, ['from_date'=> $from_date, 'to_date'=> $to_date]);

		 $filtrations = [
			'created_from'=>['name'=>'Datum od','key'=>'created|fromDate','type'=>'date'],
			'created_to'=>['name'=>'Datum do','key'=>'created|toDate','type'=>'date'],
			'name'=>['name'=>'Produkt','key'=>'Products__name','type'=>'like'],
			'product_group_id'=>['name'=>'Skupina','key'=>'ProductConnect__product_group_id','type'=>'select','list'=>[''=>'Skupina']+$this->group_list],
			'system_id'=>['name'=>'Pobočka','key'=>'system_id','type'=>'select','list'=>[''=>'Pobočka']+$this->branch_list],
			'table_id'=>['name'=>'Stůl','key'=>'table_id','type'=>'select','list'=>[''=>'Stůl']+$this->table_list],
			'view_type'=>['name'=>'Typ','key'=>'view_type','type'=>'select','list'=>[ ''=>'Zobrazit vše', 2 => 'Poslední nákup', 3 => 'Průměrný nákup']],
			//'code'=>['name'=>'Kod','key'=>'code','type'=>'like'],
			
			//'stock_item_id'=>['name'=>'Sklad položka','key'=>'stock_item_id','type'=>'select','list'=>[''=>'Skladová položka']+$this->stock_item_list],
		];

		$soldPriceSum = 0;
		$sumConditions = array_merge($sumConditions, $conditions);
		if($from_date){
			$sumConditions['DATE(OrderItems.created) >='] = $from_date->format('Y-m-d');
		}
		if($to_date){
			$sumConditions['DATE(OrderItems.created) <='] = $to_date->format('Y-m-d');
		}
		//TODO nejprve nacist ID produktu a az potom ziskat trzbu
		/*if($groupIds !== null){
			unset($sumConditions['ProductConnect.product_group_id IN']);
			unset($sumConditions['ProductConnect.product_group_id']);
			$this->loadModel('ProductConnects');
			$productIds = $this->ProductConnects->find('list', ['keyField'=>'product_id','valueField'=>'product_group_id'])->where(['ProductConnects.product_group_id IN' => $groupIds])->toArray();
			if(!empty($productIds)){
				$sumConditions['Products.id IN'] = array_keys($productIds);
			}else{
				$sumConditions['Products.id'] = 0;
			}
	}*/

		if($table_id){
			$sumContent = 'IF( Orders.table_type_id != ' . $table_id . ', 0 , --CONTENT--)';
		}else{
			$sumContent = '--CONTENT--';
		}
		$this->loadModel('OrderItems');
		$sumQuery = $this->OrderItems->find()
						->select([
							'sum_price'=> 'SUM(' . str_replace('--CONTENT--', 'ROUND(OrderItems.count * OrderItems.price_without_tax, 2)', $sumContent) . ')', 
							'sum_price_vat'=> 'SUM('. str_replace('--CONTENT--', 'ROUND(OrderItems.count * OrderItems.price, 2)', $sumContent) . ')'
						])
						->where($sumConditions)
						->contain([
							'Products'
						]);

		if($table_id){ 
			$sumQuery->contain(['Orders']);
		}

		if($system_id){
			$sumQuery->where(['OrderItems.system_id' => $system_id]);
		} 
		
		if($sumData = $sumQuery->first()){
			$soldPriceSum = $sumData->sum_price;
			$soldPriceSumVat = $sumData->sum_price_vat;
		}
		$lapelAppend = '<br/>bez dph';
		if($view_type == 1){
			$cols = [
				'id'=>['name'=>'ID'],
				'name'=>['name'=>'Produkt'],
				'sold_units'=>['name'=>'Prodáno ks'],
				'sold_price_vat'=>['name'=>'Tržba DPH'],
				'sold_price'=>['name'=>'Tržba'. $lapelAppend, 'col_class'=>'lined-left bg-blue', 'th_class'=>'lined-top', 'sum_colspan'=>9, 'sum_col_class'=>'lined-all'],
				'buy_price'=>['name'=>'Náklady'. $lapelAppend, 'col_class'=>'bg-blue', 'th_class'=>'lined-top'],
				'gain'=>['name'=>'Zisk'. $lapelAppend, 'col_class'=>'bg-blue', 'th_class'=>'lined-top'],
				'sold_price_item'=>['name'=>'Cena ks'. $lapelAppend, 'th_class'=>'lined-top'],
				'buy_price_item'=>['name'=>'Náklad ks'. $lapelAppend, 'th_class'=>'lined-top'],
				'buy_price_item_avg'=>['name'=>'Náklad ks &empty;'. $lapelAppend, 'th_class'=>'lined-top'],
				'buy_price_avg'=>['name'=>'Náklady &empty;'. $lapelAppend, 'th_class'=>'lined-top'],
				'gain_avg'=>['name'=>'Zisk &empty;'. $lapelAppend, 'th_class'=>'lined-top'],
				'gain_percent_avg'=>['name'=>'Zisk % &empty;'. $lapelAppend, 'th_class'=>'lined-top' ,'col_class'=>'lined-right'],
				'gain_percent'=>['name'=>'Zisk %', 'col_class'=>'bg-green']
			 ];
		 }else if($view_type == 2){
			$cols = [
				'id'=>['name'=>'ID'],
				'name'=>['name'=>'Produkt'],
				'sold_units'=>['name'=>'Prodáno ks'],
				'sold_price_vat'=>['name'=>'Tržba DPH'],
				'sold_price'=>['name'=>'Tržba'. $lapelAppend, 'col_class'=>'lined-left bg-blue', 'th_class'=>'lined-top', 'sum_colspan'=>5, 'sum_col_class'=>'lined-all'],
				'buy_price'=>['name'=>'Náklady'. $lapelAppend , 'col_class'=>'bg-blue', 'th_class'=>'lined-top'],
				'gain'=>['name'=>'Zisk'. $lapelAppend, 'col_class'=>'bg-blue', 'th_class'=>'lined-top'],
				'sold_price_item'=>['name'=>'Cena ks'. $lapelAppend, 'th_class'=>'lined-top'],
				'buy_price_item'=>['name'=>'Náklad ks'. $lapelAppend, 'th_class'=>'lined-top', 'col_class'=>'lined-right'],
			
				
				'gain_percent'=>['name'=>'Zisk %', 'col_class'=>'bg-green'], 
			 ];
		 }else if($view_type == 3){
			$cols = [
				'id'=>['name'=>'ID'],
				'name'=>['name'=>'Produkt'],
				'sold_units'=>['name'=>'Prodáno ks'],
				'sold_price_vat'=>['name'=>'Tržba DPH'],
				'sold_price'=>['name'=>'Tržba'. $lapelAppend, 'col_class'=>'lined-left bg-blue', 'th_class'=>'lined-top', 'sum_colspan'=>5, 'sum_col_class'=>'lined-all'],
				'buy_price_avg'=>['name'=>'Náklady &empty;'. $lapelAppend , 'col_class'=>'bg-blue', 'th_class'=>'lined-top'],
				'gain_avg'=>['name'=>'Zisk &empty;'. $lapelAppend, 'col_class'=>'bg-blue', 'th_class'=>'lined-top'],
				'sold_price_item'=>['name'=>'Cena ks'. $lapelAppend, 'th_class'=>'lined-top'],
				'buy_price_item_avg'=>['name'=>'Náklad ks &empty;'. $lapelAppend, 'th_class'=>'lined-top', 'col_class'=>'lined-right'],
				'gain_percent_avg'=>['name'=>'Zisk % &empty;', 'col_class'=>'bg-green'],
			 ];
		 }

		 $params = [
			'filtrations'=> $filtrations,
			'topActions'=>[] , //$topActions,
			'cols'=>$cols ,
			'posibility'=>[] , //$posibility,
			'paginated_data'=> $items, 
			'noPaginationTop'=> true,
			'heading' => $heading,
			'sumLista' => [
				'name'=> 'Tržba celkem',
				'sold_price'=> number_format( $soldPriceSum , 2, '.','&nbsp;'),
				'sold_price_vat'=> number_format( $soldPriceSumVat , 2, '.','&nbsp;'),
			]
        ];

		if($from_date){
			$this->request->query['created|fromDate|date'] = $from_date->format('d.m.Y');
		}
		if($to_date){
			$this->request->query['created|toDate|date'] = $to_date->format('d.m.Y');
		} 
		if($originalGroupId){
			$this->request->query['ProductConnect__product_group_id'] = $originalGroupId;
		} 
		if($table_id){
			$this->request->query['table_id'] = $table_id;
		} 
		if($system_id){
			$this->request->query['system_id'] = $system_id;
		} 
		
		$this->ViewIndex->load($params);
	}

	private function generateHeading($from_date, $to_date){
		$heading = '';
		if($from_date){
			$heading .= 'Období ';
			if(!$to_date){
				$heading .= 'od ';
			}
			$heading .= $from_date->format('d.m.Y');
		}
		if($to_date){
			if(!$from_date){
				$heading .= 'Období do ';
			}else{
				$heading .= ' - ';
			}
			$heading .= $to_date->format('d.m.Y');
		}
		return $heading;
	}

	/**
	 * Pomocna funkce pro nacteni hodnot nakladovosti pro jednotlive polozky
	 */
	private function loadExpensiveness($items, $params = []){		
		if(!empty($items)){
			$stockItemsIds = Hash::extract($items, '{n}.product_recipes.{n}.stock_item_global_id');
			//pr($stockItemsIds);
			$stock_item_list = null;
			if(!empty($stockItemsIds)){
				$this->loadModel('StockGlobalItems');
				$query = $this->StockGlobalItems->find()
										->select(['id','name'])
										->where(['StockGlobalItems.id IN' => $stockItemsIds])
										->contain([
											'Stocks'=> function($q) use ($params){ 
												$q	->where(['Stocks.stock_type_id IN'=> [1, 5]])
													->select(['Stocks.nakup_price','Stocks.nakup_price_vat','Stocks.created', 'buy_price_item_avg'=>'ROUND(AVG(Stocks.nakup_price),2)', 'buy_price_item_vat_avg'=>'ROUND(AVG(Stocks.nakup_price_vat),2)']); 
												
												if(isset($params['from_date'])){
													$q->where(['DATE(Stocks.created) >='=> $params['from_date']->format('Y-m-d')]);
												}
												if(isset($params['to_date'])){
													$q->where(['DATE(Stocks.created) <='=> $params['to_date']->format('Y-m-d')]);
												}
												return $q;
											} //Potrebujeme nabindovat posledni prijem pro zjisteni ceny
										]) 
										->order(['Stocks.created'=>'DESC'])
										->group('StockGlobalItems.id');

				$stock_item_list = $query->toArray();
			}
			$stock_list = [];
			if($stock_item_list){
				foreach($stock_item_list as $sitem){ 
					if($sitem->stock){
						$sitem->nakup_price = round($sitem->stock->nakup_price, 2);
						$sitem->nakup_price_vat = round($sitem->stock->nakup_price_vat, 2);
						$sitem->nakup_date = $sitem->stock->created;
						unset($sitem->stock);
					}
					$stock_list[$sitem->id] = $sitem;
				}
			

			foreach($items as &$item){
				$item->buy_price = 0;
				$item->buy_price_vat = 0;
				if(isset($item->product_recipes)){
					foreach($item->product_recipes as &$rec){
						if(isset($stock_list[$rec->stock_item_global_id])){
							//---- Posledni nakupni cena polozky
							$rec->buy_price = round($stock_list[$rec->stock_item_global_id]->nakup_price, 2);
							$rec->buy_price_vat = round($stock_list[$rec->stock_item_global_id]->nakup_price_vat, 2);
							//---- Prumerna nakupni cena polozky
							$rec->buy_price_item_avg = round($stock_list[$rec->stock_item_global_id]->buy_price_item_avg, 2); 
							$rec->buy_price_item_vat_avg = round($stock_list[$rec->stock_item_global_id]->buy_price_item_vat_avg, 2);
							$rec->buy_date = $stock_list[$rec->stock_item_global_id]->nakup_date;

							//Celkovy soucet ceny dane polozky
							$item->buy_price_item += $rec->buy_price * $rec->value;
							$item->buy_price_item_vat += $rec->buy_price_vat * $rec->value;
							$item->buy_price_item_avg += $rec->buy_price_item_avg * $rec->value;
							$item->buy_price_item_avg_vat += $rec->buy_price_item_avg_vat * $rec->value;
						}
					}
				
					
					if($item->buy_price_vat == 0 && $item->buy_price > 0){
						$item->buy_price_vat = round($item->buy_price, 2);
					}
					if($item->buy_price_item_vat == 0 && $item->buy_price_item > 0){
						$item->buy_price_item_vat = round($item->buy_price_item, 2);
					}

					//--------------------------------------------   Marze z posledni ceny nakupu
					$item->buy_price = round($item->buy_price_item * $item->sold_units,2 );
					$item->buy_price_vat = round($item->buy_price_item_vat * $item->sold_units, 2);
					$item->gain_item = round($item->sold_price_item - $item->buy_price_item, 2);
					$item->gain = round($item->sold_price - $item->buy_price, 2);
					if($item->sold_price > 0){
						$item->gain_percent = round(($item->sold_price - $item->buy_price) / $item->sold_price * 100, 2);
					}
					//--------------------------------------------   Marze z prumerne ceny nakupu
					$item->buy_price_avg = round($item->buy_price_item_avg * $item->sold_units, 2);
					$item->buy_price_avg_vat = round($item->buy_price_item_avg_vat * $item->sold_units, 2);
					$item->gain_item_avg = round($item->sold_price_item - $item->buy_price_item_avg, 2);
					$item->gain_avg = round($item->sold_price - $item->buy_price_avg, 2);
					if($item->sold_price > 0){
						$item->gain_percent_avg = round(($item->sold_price - $item->buy_price_avg) / $item->sold_price * 100, 2);
					}

					$item->buy_price_item_avg = round($item->buy_price_item_avg, 2);
					$item->buy_price_item_avg_vat = round($item->buy_price_item_avg_vat, 2);
					$item->buy_price_item = round($item->buy_price_item, 2);
					$item->buy_price_item_vat = round($item->buy_price_item_vat, 2);  
					}
				}
			}
		}
		return $items;
	}
	
	/**
	 * Sledovani vyvoje ceny skladovych polozek v case
	 * Zvyrazneni pokud dochazi ke slevneni ci zdrazeni dane polozky
	 */
	public function productItemStats(){
		$this->set('title','Statistika ceníkových položek');
		$this->loadComponent('ViewIndex');
		

		$view_type = 1;	//Zobrazovat vsechny sloupce, jen posledni nakupku nebo jen prumernou nakupku
		$from_date = null;
		$to_date = null;
		
		if(isset($this->request->query['view_type'])){
			$view_type = $this->request->query['view_type'] == '' ? 1 : $this->request->query['view_type'];
			unset($this->request->query['view_type']);
		}
		
		if(isset($this->request->query['created|fromDate|date']) && $this->request->query['created|fromDate|date'] != ''){
			$from_date = new Date($this->request->query['created|fromDate|date']);
			unset($this->request->query['created|fromDate|date']);
		}

		if(isset($this->request->query['created|toDate|date']) && $this->request->query['created|toDate|date'] != ''){
			$to_date = new Date($this->request->query['created|toDate|date']);
			unset($this->request->query['created|toDate|date']);
		}

		$heading = $this->generateHeading($from_date, $to_date);
		
		$conditions = [];
		$conditions = $this->ViewIndex->conditions($conditions);

		 $this->loadModel('Products');
		 $query = 	$this->Products->find()
		 				 ->contain([ 
							  	'ProductRecipes.StockGlobalItems' => function($q){ return $q->select(['StockGlobalItems.name','ProductRecipes.product_id','ProductRecipes.stock_item_global_id','ProductRecipes.value']); },
						 ])
						  ->select(['Products.id', 'Products.name', 'Products.code'])
						  ->where($conditions)
						  ->group('Products.id');
						 //->map(function($data){ return $data; /* $this->loadExpensiveness($data);*/ }); //nefunguje callable [$this, 'loadExpensiveness']
	
		 $items = $this->Paginator->paginate($query, [
			'limit'=>30
		 ])->toArray();

		$stock_list = $this->loadStockCosts($items, ['from_date'=> $from_date, 'to_date'=> $to_date]);

		foreach($items as &$item){
			if(isset($item->product_recipes)){
				$item->buy_price_last = 0;
				$item->buy_price_last_vat= 0;
				$item->buy_price_prelast = 0;
				$item->buy_price_prelast_vat = 0;
				$item->buy_price_avg = 0;
				$item->buy_price_avg_vat = 0;
				foreach($item->product_recipes as $rec){
					$item->buy_price_last += $stock_list[$rec->stock_item_global_id]->buy_price_last;
					$item->buy_price_last_vat += $stock_list[$rec->stock_item_global_id]->buy_price_last_vat;
					$item->buy_price_prelast += $stock_list[$rec->stock_item_global_id]->buy_price_prelast;
					$item->buy_price_prelast_vat += $stock_list[$rec->stock_item_global_id]->buy_price_prelast_vat;
					$item->buy_price_avg += $stock_list[$rec->stock_item_global_id]->buy_price_item_avg;
					$item->buy_price_avg_vat += $stock_list[$rec->stock_item_global_id]->buy_price_item_vat_avg;

					if($item->buy_price_prelast == $item->buy_price_last){
						$item->buy_direction = '<i class="fa fa-long-arrow-right grey"></i>';
					}else if($item->buy_price_prelast < $item->buy_price_last){
						$item->buy_direction = '<i class="fa fa-level-up red"></i>';
					}else{
						$item->buy_direction = '<i class="fa fa-level-down green"></i>';
					}
				}
				$item->recipes = $this->transformRecipesData($item->product_recipes, $stock_list);
				unset($item->product_recipes);
			} 
		}
		/*pr($items);
		die();*/
	
		 

		 $filtrations = [
			'created_from'=>['name'=>'Datum od','key'=>'created|fromDate','type'=>'date'],
			'created_to'=>['name'=>'Datum do','key'=>'created|toDate','type'=>'date'],
			'name'=>['name'=>'Produkt','key'=>'Products__name','type'=>'like'],
			'view_type'=>['name'=>'Typ','key'=>'view_type','type'=>'select','list'=>[ ''=>'Zobrazit vše', 2 => 'Poslední nákup', 3 => 'Průměrný nákup']],
			//'code'=>['name'=>'Kod','key'=>'code','type'=>'like'],
			
			//'stock_item_id'=>['name'=>'Sklad položka','key'=>'stock_item_id','type'=>'select','list'=>[''=>'Skladová položka']+$this->stock_item_list],
		];

	
		// if($view_type == 1){
			$cols = [
				'id'=>['name'=>'ID'],
				'name'=>['name'=>'Produkt'],
				'buy_price_last'=>['name'=>'Cena aktuál'],
				'buy_price_prelast'=>['name'=>'Cena minulá'],
				'buy_direction'=>['name'=>'Změna'],
				'buy_price_avg'=>['name'=>'Cena &empty;'],
				'recipes'=>['name'=>'Receptura (název | počet | cena aktuální | cena minulá | cena &empty;)'],
			 ];
		/* }else if($view_type == 2){
			$cols = [
				'id'=>['name'=>'ID'],
				'name'=>['name'=>'Produkt'],
				'sold_price_item'=>['name'=>'Cena ks'],
				'buy_price_item'=>['name'=>'Náklad ks'],
				'sold_units'=>['name'=>'Prodáno ks'],
				'sold_price'=>['name'=>'Tržba'],
				'buy_price'=>['name'=>'Náklady'],
				'gain'=>['name'=>'Zisk'],
				'gain_percent'=>['name'=>'Zisk %'],
			 ];
		 }else if($view_type == 3){
			$cols = [
				'id'=>['name'=>'ID'],
				'name'=>['name'=>'Produkt'],
				'sold_price_item'=>['name'=>'Cena ks'],
				'buy_price_item_avg'=>['name'=>'Náklad ks &empty;'],
				'sold_units'=>['name'=>'Prodáno ks'],
				'sold_price'=>['name'=>'Tržba'],
				'buy_price_avg'=>['name'=>'Náklady &empty;'],
				'gain_avg'=>['name'=>'Zisk &empty;'],
				'gain_percent_avg'=>['name'=>'Zisk % &empty;'],
			 ];
		 }*/

		 $params = [
			'filtrations'=> $filtrations,
			'topActions'=>[] , //$topActions,
			'cols'=>$cols,
			'posibility'=>[] , //$posibility,
			'paginated_data'=> $items, 
			'noPaginationTop'=> true,
			'heading' => $heading 
        ];

		$this->ViewIndex->load($params);
	}

	/**
	 * Pomocna funkce pro nacteni hodnot nakladovosti pro jednotlive polozky
	 */
	private function loadStockCosts($items, $params = []){	
		$stock_list = [];	
		if(!empty($items)){
			$stockItemsIds = Hash::extract($items, '{n}.product_recipes.{n}.stock_item_global_id');
			
			$this->loadModel('StockGlobalItems');
			$query = $this->StockGlobalItems->find()
									->select(['id','name', 'prices' => 'GROUP_CONCAT(CONCAT(Stocks.created,"|",Stocks.nakup_price,"|",Stocks.nakup_price_vat) SEPARATOR ";")'])
									->where(['StockGlobalItems.id IN' => $stockItemsIds])
									->contain([
										'Stocks'=> function($q) use ($params){ 
											$q	->where(['Stocks.stock_type_id'=> 1])
												->select(['Stocks.created', 'buy_price_item_avg'=>'ROUND(AVG(Stocks.nakup_price),2)', 'buy_price_item_vat_avg'=>'ROUND(AVG(Stocks.nakup_price_vat),2)']); 
											if(isset($params['from_date'])){
												$q->where(['DATE(Stocks.created) >='=> $params['from_date']->format('Y-m-d')]);
											}
											if(isset($params['to_date'])){
												$q->where(['DATE(Stocks.created) <='=> $params['to_date']->format('Y-m-d')]);
											}
											return $q;
										} //Potrebujeme nabindovat posledni prijem pro zjisteni ceny
									
									]) 
									->order(['Stocks.created'=>'DESC'])
									->group('StockGlobalItems.id');

			$stock_item_list = $query->toArray();

			if($stock_item_list){
				foreach($stock_item_list as $sitem){ 
						if(!isset($sitem->buy_price)){
							$sitem->buy_price = 0;
						}
						if(!isset($sitem->buy_price_vat)){
							$sitem->buy_price_vat = 0;
						}
						if($sitem->stock){
							$parts = explode(";", $sitem->prices);
							$sitem->prices = [];
							foreach($parts as $part){
								$item = explode('|', $part);
								if(count($item) == 3){
									$sitem->prices[$item[0]] = (object) ['price' => $item[1], 'price_vat' => $item[2]];
								}
							}
							$dates = array_keys($sitem->prices);
							$count = count($dates);

							$lastDate = $count >= 1 ? $dates[$count - 1] : null; //Posledni datum
							$preLastDate = $count >= 2 ? $dates[$count - 2] : null; //Predposledni datum
							if($lastDate){
								$sitem->buy_price_last = round($sitem->prices[$lastDate]->price, 2);
								$sitem->buy_price_last_vat = round($sitem->prices[$lastDate]->price_vat, 2);
								$sitem->buy_last_date = new Time($lastDate);
								$sitem->buy_price += $sitem->buy_price_last;
								$sitem->buy_price_vat += $sitem->buy_price_last_vat;
							}
							if($preLastDate){
								$sitem->buy_price_prelast = round($sitem->prices[$preLastDate]->price, 2);
								$sitem->buy_price_prelast_vat = round($sitem->prices[$preLastDate]->price_vat, 2);
								$sitem->buy_prelast_date = new Time($preLastDate);
							}
							unset($sitem->stock);
						}
						$stock_list[$sitem->id] = $sitem;
				}
			}

			/*foreach($items as &$item){
				$item->buy_price = 0;
				$item->buy_price_vat = 0;
				if(isset($item->product_recipes)){
					foreach($item->product_recipes as &$rec){
						if(isset($stock_list[$rec->stock_item_global_id])){

						}
					}
				}
			}*/
		}
		return $stock_list;
	}
	
	/**
	 * Pomocna funkce, ktera vytvori tabulku pro vypsani receptur daneho produktu
	 */
	private function transformRecipesData($recepes, $stock_list){
		$html = '';
		if(is_array($recepes)){
			$html .= '<table style="width:100%">';
			foreach($recepes as $i => $item){
				$rec = new \stdClass();
				$rec->id = $item->stock_item_global_id; 
				$rec->name = $item->stock_global_item->name; 
				$rec->count = $item->value;
				$rec->unit = $this->stock_unit_list[$item->unit_id];
				$html .= '<tr>';
					$html .= '<td width="30%">'. $rec->name .'</td>';
					$html .= '<td width="10%">'. $rec->count . $rec->unit .'</td>';
					if(isset($stock_list[$rec->id])){
						$html .= '<td width="20%">'. ($stock_list[$rec->id]->buy_price_last ? $stock_list[$rec->id]->buy_price_last . ',-' : '').'</td>';
						$html .= '<td width="20%">'. ($stock_list[$rec->id]->buy_price_prelast ? $stock_list[$rec->id]->buy_price_prelast . ',-' : '') .'</td>';
					}else{
						$html .= '<td width="20%">0,-</td>';
						$html .= '<td width="20%">0,-</td>';
					}
					$html .= '<td width="20%">'. ( $stock_list[$rec->id]->buy_price_item_avg ? '&empty; '. $stock_list[$rec->id]->buy_price_item_avg .',-' : '' ).'</td>';
				$html .= '</tr>';
			}
			$html .= '</table>';
		}
		return $html;
	}

	/**
	 * Sledovani vyvoje ceny skladovych polozek v case
	 * Zvyrazneni pokud dochazi ke slevneni ci zdrazeni dane polozky
	 */
	public function stockItemStats(){
		$this->set('title','Statistika skladových položek');
		$this->loadComponent('ViewIndex');
		
		$system_id = null;
		$from_date = null;
		$to_date = null;

		$this->loadModel('Branches');
		$this->branch_list = $this->Branches->branchesList();

		if(isset($this->request->query['system_id'])){
			$system_id = $this->request->query['system_id'] == '' ? null : $this->request->query['system_id'];
			unset($this->request->query['system_id']);
		}

		if(isset($this->request->query['created|fromDate|date']) && $this->request->query['created|fromDate|date'] != ''){
			$from_date = new Date($this->request->query['created|fromDate|date']);
			unset($this->request->query['created|fromDate|date']);
		}

		if(isset($this->request->query['created|toDate|date']) && $this->request->query['created|toDate|date'] != ''){
			$to_date = new Date($this->request->query['created|toDate|date']);
			unset($this->request->query['created|toDate|date']);
		}

		$heading = $this->generateHeading($from_date, $to_date);
		
		$conditions = [];
		$conditions = $this->ViewIndex->conditions($conditions);

		$params = ['from_date'=> $from_date, 'to_date'=> $to_date];
		 $this->loadModel('StockGlobalItems');
		 $query = 	$this->StockGlobalItems->find()
						  ->select(['StockGlobalItems.id', 'StockGlobalItems.name'])
						  ->where($conditions)
						  ->contain([
							'Stocks'=> function($q) use ($params, $system_id){ 
								$q	->where(['Stocks.stock_type_id'=> 1])
									->select([
										'Stocks.created', 
										'buy_price_last'=>'Stocks.nakup_price',
										'buy_price_avg_vat'=>'ROUND(SUM(Stocks.nakup_price_vat * Stocks.value), 2)',  
										'buy_price_avg'=>'ROUND(SUM(Stocks.nakup_price * Stocks.value), 2)', 
										'buy_price_item_avg'=>'ROUND(AVG(Stocks.nakup_price),2)', 
										'buy_price_item_avg_vat'=>'ROUND(AVG(Stocks.nakup_price_vat),2)', 
										'sum'=>'ROUND(SUM(Stocks.value), 1)'
									]); 
								if($system_id){
									$q->where(['Stocks.system_id'=> $system_id]);
								}
								if(isset($params['from_date'])){
									$q->where(['DATE(Stocks.created) >='=> $params['from_date']->format('Y-m-d')]);
								}
								if(isset($params['to_date'])){
									$q->where(['DATE(Stocks.created) <='=> $params['to_date']->format('Y-m-d')]);
								}
								return $q;
							} //Potrebujeme nabindovat posledni prijem pro zjisteni ceny
						]) 
						->order(['Stocks.created'=>'DESC'])
						->group('StockGlobalItems.id');
						 // ->group('Products.id');
						 //->map(function($data){ return $data; /* $this->loadExpensiveness($data);*/ }); //nefunguje callable [$this, 'loadExpensiveness']
	
		 $items = $this->Paginator->paginate($query, [
			'limit'=>60
		 ])->toArray();


		 $filtrations = [
			'created_from'=>['name'=>'Datum od','key'=>'created|fromDate','type'=>'date'],
			'created_to'=>['name'=>'Datum do','key'=>'created|toDate','type'=>'date'],
			'name'=>['name'=>'Název položky','key'=>'StockGlobalItems__name','type'=>'like'],
			'system_id'=>['name'=>'Pobočka','key'=>'system_id','type'=>'select','list'=>[''=>'Pobočka']+$this->branch_list],
		];

		$cols = [
			'id'=>['name'=>'ID'],
			'name'=>['name'=>'Název polož.'],
			'sum'=>['name'=>'ks'],
			'buy_price_last'=>['name'=>'Cena posl.'],
			'buy_price_item_avg'=>['name'=>'Cena &empty;'],
			'buy_price_avg'=>['name'=>'Celkem'],
		];


		 $params = [
			'filtrations'=> $filtrations,
			'topActions'=>[] , //$topActions,
			'cols'=>$cols,
			'posibility'=>[] , //$posibility,
			'paginated_data'=> $items, 
			'noPaginationTop'=> true,
			'heading' => $heading 
		];
		
		if($system_id){
			$this->request->query['system_id'] = $system_id;
		} 

		$this->ViewIndex->load($params);
	}
}