<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;


ini_set('memory_limit', '228M');

class StocksController extends AppController{

    /**
     * 
     */
    public function initialize(){
        parent::initialize();
       
       /*
       TOTO NEDELAAAT!!! Auth komponenta se tady vubec nepouzivaaaa...
       $this->loadComponent('Auth');
        $this->Auth->allow('saveProductForWeb');*/
       
    }

    /**
     * 
     */
	public function index(){
        $this->set('title','Pohyby na skladě');

		$this->loadComponent('ViewIndex');
        $this->genSubmenu();

        $this->loadModel('ProductGroups');
        $this->group_list = $this->ProductGroups->groupList();
        $this->set('group_list',$this->group_list);

        $this->loadModel('Products');
        $this->product_list = $this->Products->productList();
        $this->set('product_list',$this->product_list);
        
        $this->loadModel('StockGlobalItems');
        $this->stock_item_list = $this->StockGlobalItems->skladItemsList();
        $this->set('stock_item_list',$this->stock_item_list);
        //pr($this->stock_item_list);
        $this->loadModel('Branches');
        $this->branches_list = $this->Branches->branchesList();
        $this->set('branches_list',$this->branches_list);

        $this->set('swithBranches',true);

        // pr($this->stock_unit_list);
		$cols = [
			'id'=>['name'=>'ID'],
			//'group_id'=>['name'=>'Typ skladu'],
            'stock_type_id'=>['name'=>'Typ','list'=>$this->stock_type_list],
			'stock_item_id'=>['name'=>'Sklad položka','list'=>$this->stock_item_list],
            'stock_item_product_id'=>['name'=>'Produkt','list'=>$this->product_list],
            'value'=>['name'=>'Množství'],
            'sum_value'=>['name'=>'Celkem množství'],
            'nakup_price_total'=>['name'=>'Nákupní cena celkem'],
            'nakup_price_total_vat'=>['name'=>'Nákupní cena celkem s DPH'],
            'unit_id'=>['name'=>'Jednotka','list'=>$this->stock_unit_list],
			// 'tax_id'=>['name'=>'Nákupní cena'],
            'invoice'=>['name'=>'Faktura'],
            'created'=>['name'=>'Vytvořeno'],
            'user_name'=>['name'=>'Uživatel'],
            'system_id'=>['name'=>'Pobočka','list'=>$this->branches_list],
            'system_id_to'=>['name'=>'Pobočka na/z','list'=>$this->branches_list],
            'product_group_id'=>['name'=>'Skupina','list'=>$this->group_list],
		];
		$filtrations = [
			//'name'=>['name'=>'Název','key'=>'name','type'=>'like'],
			//'code'=>['name'=>'Kod','key'=>'code','type'=>'like'],
			'stock_type_id'=>['name'=>'Typ','key'=>'stock_type_id','type'=>'select','list'=>[''=>'Typ']+$this->stock_type_list],
			'stock_item_id'=>['name'=>'Sklad položka','key'=>'stock_item_id','type'=>'select','list'=>[''=>'Skladová položka']+$this->stock_item_list],
			'stock_item_product_id'=>['name'=>'Sklad položka','key'=>'stock_item_product_id','type'=>'select','list'=>[''=>'Produkt']+$this->product_list],
			'group'=>['name'=>'Seskupení','key'=>'group_by_id','type'=>'select','list'=>$this->group_stock_list],
			// 'system_id'=>['name'=>'Provozovna','key'=>'system_id','type'=>'select','list'=>[''=>'Provozovna']+$this->branches_list],
			//'system_id_to'=>['name'=>'Provozovna z/do','key'=>'system_id','type'=>'select','list'=>[''=>'Provozovna z/do']+$this->branches_list],
			'created_from'=>['name'=>'Datum od','key'=>'created|fromDate','type'=>'date'],
			'created_to'=>['name'=>'Datum do','key'=>'created|toDate','type'=>'date'],
			'product_group_id'=>['name'=>'Skupina','key'=>'product_group_id','type'=>'select','list'=>[''=>'Skupina']+$this->group_list],
			'id'=>['name'=>'ID','key'=>'id','type'=>'text'],
		];
		$topActions = [
			'edit?type_id=1'=>['name'=>'Příjem','url'=>'/stocks/edit/?type_id=1'],
			'edit?type_id=2'=>['name'=>'Převodka','url'=>'/stocks/edit/?type_id=2'],
			'edit?type_id=3'=>['name'=>'Odpis','url'=>'/stocks/edit/?type_id=3'],
			//'products'=>['name'=>'Skladové produkty','url'=>'/stocks/products/'],
			//'import'=>['name'=>'Import produktů','url'=>'/stocks/importProducts/','type'=>'ajax'],
		];
		$posibility = [
			//'edit'=>['name'=>'Editovat','url'=>'/stocks/edit/'],
			//'trash'=>['name'=>'Smazat','url'=>'/trash/stocks/'],
        ];
        
		//$conditions = ['system_id'=>$this->loggedUser->system_id];
		$conditions = [];
		$conditions = $this->ViewIndex->conditions($conditions);
		// pr($conditions);
		$mapper = function ($data, $key, $mapReduce) {
			if (!empty($data->connected)){
				$data->connected_check = 1;
			} else {
				
				$data->connected_check = 0;
			}
			$mapReduce->emit($data); 
        };

        $fields = [
			'id',
            'stock_type_id',
            'stock_item_id',
            'invoice',
            'order_id',
            'stock_item_product_id',
            'value',
            'system_id',
            'system_id_to',
            'nakup_price',
            'nakup_price_vat',
            'nakup_price_total',
            'nakup_price_total_vat',
            'tax_id',
            'created',
            'unit_id',
            'user_name',
            'product_group_id',
		
		];
        $data = $this->Stocks->find();

        if (isset($conditions['group_by_id'])){
            if ($conditions['group_by_id'] == 2){
                $fields = $fields+['sum_value' => $data->func()->sum('Stocks.value')];
                // $data->group(['stock_item_id','stock_type_id']);
            } else {
                unset($cols['sum_value']);
            }
            unset($conditions['group_by_id']);
        }

        $data
            ->where($conditions)
            ->order('id DESC')
            // ->group('local_id')
            
            ->select($fields);

		if (empty($data)){
			$data = null;
        }
        
        $sumList = [
            'value'=>'text',
            'nakup_price_total'=>'price',
            'nakup_price_total_vat'=>'price',
        ];
        $this->set('sumList',$sumList);
        $sumFields = [];
        $sumDataQuery = $this->Stocks->find();
        foreach(array_keys($sumList) AS $s){
            $sumFields[$s] = 'sum('.$s.')';
        }

        $sumDataQuery
            ->select($sumFields)
            ->where($conditions) ;
        $sumData = $sumDataQuery->first();
        
        if ($sumData){
            $this->set('sumData',$sumData);
        }

		$params = [
			'filtrations'=>$filtrations,
			'topActions'=>$topActions,
			'filtrBranch'=>true,
			'cols'=>$cols,
			'posibility'=>$posibility,
			'data'=>$data,
            'sumList'=>['value'=>0],
			"rowConditions"		=> ['storno'=> 'stock_type_id=12']
        ];

		$this->ViewIndex->load($params);
    }
    
    /**
     * 
     */
    public function trash($model,$id){
        $this->loadModel($model);

        if (!$this->$model->query()
            ->update()
            ->set(['trash' => time()])
            ->where(['id' => $id])
            ->execute()
        ){

        }
        return $this->redirect($this->referer(true));
    }
	
	/**
     * 
     */
	public function edit($id = null){
		$this->loadModel('StockProducts');
		$this->set('stockProductsList',$this->stockProductsList = $this->StockProducts->productList());
		$this->set('stockProducts',$this->stockProducts = $this->StockProducts->productList(true));
        // $this->set('simpleLayout',true);
        
        $this->loadModel('SkladCiselniks');
        $this->set('supplier_list',$this->supplier_list = $this->SkladCiselniks->supplierlList());
        $this->set('stock_group_list',$this->stock_group_list = $this->SkladCiselniks->skladGrouplList());
        
        $this->loadModel('StockGlobalItems');
        $this->loadModel('StocksItems');
	    $this->set('sklad_items_list',$this->sklad_items_list = $this->StockGlobalItems->skladItemsList());
	    $this->set('stock_items',$this->stock_items = $this->StockGlobalItems->skladItems());
    
        $this->loadModel('Products');
	    $this->set('product_list',$this->product_list = $this->Products->productList());
    
        $this->loadModel('Branches');
        $this->branches_list = $this->Branches->branchesList();
        
        // Příjem
        if (isset($this->request->query["type_id"])) {
            if ($this->request->query["type_id"] == 1) {
                $this->set("simpleLayout", true);
            }
        }

        $this->checkLogged();
	    $data = $this->Stocks->newEntity();
        //pr($data);
        
		if ($id != null){
			$conditions = ['id'=>$id];
			$data = $this->Stocks->find()
            ->where($conditions)
                //->select(['id'])
            ->first();
        }

		$this->set(compact("data"));
        
		if (!empty($this->request->data)){   

			if (!empty($this->request->data['connected'])){
				$this->request->data['connected'] = json_encode($this->request->data['connected']);
				$this->request->data['is_connected'] = 1;
			} else {
				$this->request->data['is_connected'] = 0;
			}
            if (isset($this->request->data['save_data'])){
                $save_data = json_decode($this->request->data['save_data'],true);
                $this->loadModel('StockQueues');

                $this->loadModel('Branches');
                $this->branches_list = $this->Branches->branchesList();

                foreach($save_data AS $save){
                    $save['createdTime'] = time();

                    if ($save['system_id_to'] > 0){
                        $saveTmp = $save;
                        $save_queues = [];
                        $save_queues['system_id'] = $save['system_id_to'];
                        $save_queues['system_id_from'] = $save['system_id'];
                        $save_queues['system_name_from'] = $this->branches_list[$save['system_id']];
                        $saveTmp['system_id'] = $save['system_id_to'];
                        $saveTmp['stock_type_id'] = 11;
                        $save_queues['data'] = json_encode($saveTmp);
                        $save_entity_queues = $this->StockQueues->newEntity($save_queues);
                        $this->StockQueues->save($save_entity_queues);
                        $save['system_id_to'] =  $save_queues['system_id'];
                    }    
                    $save_entity = $this->Stocks->newEntity($save);
                    $this->Stocks->save($save_entity);
                }
                die(json_encode(['result'=>true,'message'=>__('Uloženo')]));
            } else {
                $saveData = $this->Stocks->patchEntity($data,$this->request->data);
                $this->check_error($data);
                $this->Stocks->save($saveData);
                die(json_encode(['result'=>true,'message'=>'Uloženo']));
            }  
        }
        $this->loadModel('StockGlobalItems');
        $this->set('sklad_item_list',$this->sklad_item_list = $this->StockGlobalItems->skladItemsList());
        $this->loadModel('ProductAddons');
    }

    /**
     * 
     */
    public function getStockItems($group_id){
        $this->loadModel('StockGlobalItems');
        $items = $this->StockGlobalItems->stockItemsList($group_id);
        die(json_encode(['data'=>$items]));
    }
    
    /**
     * 
     */
    public function getStockItemsPrice($type,$id){
        $this->loadModel('Stocks');
        if ($type == 1){
            $conditions = ['stock_item_id'=>$id];
        } 
        if ($type == 2){
            $conditions = ['stock_item_product_id'=>$id];
        } 
        $item = $this->Stocks->find()
            ->select([
                'nakup_price',
                'nakup_price_vat',
            ])
            ->order('id DESC')
            ->where($conditions)
            ->first();
        // pr($item);
        die(json_encode(['data'=>$item]));
    }

    /**
     * 
     */
	private function checkCode($data){
        $conditions = ['code'=>$data['code'], 'id !='=>$data['id']];
        $data = $this->StockProducts->find()
            ->where($conditions)
            ->select(['code'])
            ->first()
        ;
        if($data){
            die(json_encode(['result'=>false,'message'=>'Kód je již použit']));
        }

    }
	
	/**
     * 
     */
    public function productsEdit($id = null){
		
		$this->checkLogged();
	    $this->loadModel('StockProducts');
        $data = $this->StockProducts->newEntity();
        //pr($data);
        
		if ($id != null){
			$conditions = ['id'=>$id];
			$data = $this->StockProducts->find()
                        ->where($conditions)
                            //->select(['id'])
                        ->first();
        }
        
		$this->set(compact("data"));
        
		if (!empty($this->request->data)){   
			//pr($this->request->data);die();
            $this->checkCode($this->request->data); //overeni code
			$saveData = $this->StockProducts->patchEntity($data,$this->request->data);

            $this->check_error($data);
            //pr($saveData);die();

            $this->StockProducts->save($saveData);
           	die(json_encode(['result'=>true,'message'=>'Uloženo']));
			  
        }
    }

    /**
     * 
     */
    public function productsTrash($id){
        $this->loadModel('StockProducts');
        $conditions = ['id'=>$id];
        $this->StockProducts->trashAll($conditions);
        $this->redirect('/stocks/products/');
    }
	
    /**
     * Gets stocks data as API.
     * 
     * @param int $id - System ID.
     * @return mixed
     */
    public function getDataOllies($id = null)
    {
        $this->loadModel('Branches');
        $this->loadModel('Stocks');
        $this->loadModel('StockGlobalItems');
        $this->loadModel('Products');
        $this->loadModel('ProductConnects');
        $this->loadModel('ProductRecipes');

        $data = (object) [];
        $conditions = [];

        if ($id != null) {
            $conditions['id'] = $id;
        }
        $conditions['view_api'] = 1;

        $branches = $this->Branches->find()
                    ->select([
                        'id',
                        'view_api'
                    ])
                    ->where($conditions)
                    ->all();
                    //pr($conditions);

        $products = $this->Products->find()
                    ->select([
                        'id',
                        'name',
                        'code',
                        'reserve',
                        'view_api'
                    ])
                    ->contain(['ProductConnects' => function ($q) {
                        return $q->select([
                            'id',
                            'product_id',
                            'product_group_id'
                        ]);
                    }])
                    ->where(['view_api' => 1])
                    ->all();

        $toRecalculate = [];

        // PREPARE STOCKS CAKES
            foreach ($products as $pr) {
                $isCake = false;
                $isCakeToRecalculate = false;

                foreach ($pr->product_connects as $pc) {
                    if ($pc->product_group_id == 39) {
                        $isCake = true;
                        $isCakeToRecalculate = true;
                    }

                    if (
                        $pc->product_group_id == 8 ||
                        $pc->product_group_id == 40 ||
                        $pc->product_group_id == 71 ||
                        $pc->product_group_id == 75
                    ) {
                        $isCake = true;
                    }
                }

               
                if ($isCake) {
                    $pr->isCake = $isCake;
                }
            }
        // pr($branches);die();
        // pr($products);die();
        //todo (pouze ostré provozy -> u branches view_api = 1, stejně tak zobrazovat pouze produkty s view_api = 1)
        foreach ($branches as $br) {
            $data->{$br->id} = (object) [
                //'view_api' => 1,
            ];
            foreach ($products as $pr) {
                if (isset($pr->isCake) && $pr->isCake) {
                    continue;
                }
                if($br->id && $pr->code){
                    if (isset($this->request->query['debug'])) {
                        $data->{$br->id}->{$pr->code} = (object) [
                            'name'      => $pr->name,
                            'state'     => 0,
                            'view_api'  => $pr->view_api,
                            'id'        => $pr->id,
                            'code'      => $pr->code,
                        ];
                    } else {
                        $data->{$br->id}->{$pr->code} = (object) [
                            'state'     => 0,
                            'code'      => $pr->code
                        ];
                    }
                }
                $products_recipes = $this->ProductRecipes->find()
                    ->select([
                        'id',
                        'product_id',
                        'stock_item_global_id',
                        'value'
                    ])
                    ->where(['product_id' => $pr->id])
                    ->all();

                foreach ($products_recipes as $re) {
                    $products_recipes_ids = $re->stock_item_global_id;
                }
                
                if (isset($products_recipes_ids) && !empty($products_recipes_ids)) {
                    $stocks_values = $this->Stocks->find()
                    ->select([
                        'id',
                        'system_id',
                        'stock_item_id',
                        'value',
                        'created',
                        'stock_type_id',
                        'current_state' => 'SUM(IF(Stocks.stock_type_id IN (2,3,4,6,10,14), (value * -1), value))'
                    ])
                    ->order(['id DESC'])
                    ->where(['stock_item_id IN' => $products_recipes_ids, 'system_id' => $br->id])
                    ->all();
                    // pr($products_recipes_ids);
                    // pr($stocks_values);die();
                    $count = [];

                    foreach ($products_recipes as $re) {
                        foreach ($stocks_values as $st) {
                            if ($re->stock_item_global_id == $st->stock_item_id) {
                                if ($re->value > 0) {
                                    if (isset($toRecalculate[$st->stock_item_id])) {
                                        $count[$st->id_item_id] = ($st->current_state + $toRecalculate[$st->stock_item_id]) / $re->value;
                                    } else {
                                        $count[$st->id_item_id] = $st->current_state / $re->value;
                                    }
                                } else {
                                    $count[$st->id_item_id] = 0;
                                }
                                
                                if ($count[$st->id_item_id] < 0) {
                                    $count[$st->id_item_id] = 0;
                                }

                                break;
                            }
                        }
                    }

                    $status = 0;
                    $first = true;
                    foreach ($count as $c) {
                        if ($first) {
                            $status = $c;
                            $first = false;
                        } else {
                            if ($c < $status) {
                                $status = $c;
                            }
                        }
                    }

                    if($br->id && $pr->code){
                        if ($status > 0) {
                            $data->{$br->id}->{$pr->code}->state = floor($status) - $pr->reserve;
                        } elseif ($status <= 0) {
                            $data->{$br->id}->{$pr->code}->state = 0;
                        }
                    }
                }
            }
        }

        if (isset($this->request->query['debug'])) {
            pr($data);
            die();
        } else {
            return $data;
        }
    }

    /**
     * Gets stocks products data as API.
     * 
     * @return mixed
     */
    public function saveProductForWeb($id=null)
    {
        function microtime_float(){
            list($usec, $sec) = explode(" ", microtime());
            return ((float)$usec + (float)$sec);
        }
        $t = microtime_float();
        $this->loadModel('WebProducts');
        $data = $this->getDataOllies($id);
        $toSave = [];
        foreach($data AS $key=>$products){
            foreach($products AS $product){
                if(!isset($toSave[$product->code])){
                    $toSave[$product->code] = [];
                }
                switch ($key) {
                    //PRO TEST VYPNUTY DATA Z AVIONU A PLNENO DATY Z TESTOVACI POKLADNY 1->9
                    case 1: //avion
                        $toSave[$product->code]['state_avion'] = $product->state;   
                        break;

                    case 9: //testovaci
                        $toSave[$product->code]['state_avion'] = $product->state;   
                        break;

                    case 2: //vitkovice
                        $toSave[$product->code]['state_vitkovice'] = $product->state;   
                        break;

                    case 3: //poruba
                        $toSave[$product->code]['state_poruba'] = $product->state;   
                        break;

                    case 4: //olomouc
                        $toSave[$product->code]['state_olomouc'] = $product->state;   
                        break;

                    case 7: //manifesto
                        $toSave[$product->code]['state_manifesto'] = $product->state;   
                        break;
                    case 12: //balbin
                        $toSave[$product->code]['state_balbin'] = $product->state;   
                        break;
                }
            }
        }
       
        $i = 0;
        if(!empty($toSave)){
            foreach($toSave AS $code=>$updateData){
                $this->WebProducts->updateAll($updateData,['ean'=>$code]);
                //pr($toSave);
                $i++;
            }
        }
        //LOG TO FILE
        $t = round( microtime_float()-$t, 3);
        $msg = date('d.m.Y H:i').' updatovano produktu: '.$i.'  | time: '.$t.' s' ;
        @file_put_contents( ROOT . '/logs/cron_log_send_web_data.log', $msg . PHP_EOL, FILE_APPEND);
        
        die($msg);
    } 


    

    /**
     * Gets stocks products data as API.
     * 
     * @return mixed
     */
    public function getDataOlliesProducts()
    {
        $this->loadModel('Products');

        $products = $this->Products->find()
        ->select([
            'id',
            'code',
            'name',
            'view_api'
        ])
        ->where(['view_api' => 1])
        ->all();
        
        $data = (object) [];
        
        $iterator = 0;
        foreach ($products as $pr) {
            $data->{$iterator} = (object) [
                'code'      => $pr->code,
                'name'      => $pr->name
            ];

            $iterator++;
        }

        if (isset($this->request->query['debug'])) {
            pr($data);
        } else {
            echo json_encode($data);
        }
        
        die();
    }
}