<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;

class SettingsController  extends AppController{
    
	
	public function index(){

		$this->checkLogged();
		$this->edit(1);
	}
	
    public function index2(){
		$this->checkLogged();
		$this->loadComponent('ViewIndex');
		
		$cols = [
			'id'=>['name'=>'ID'],
			'name'=>['name'=>'Provoz'],
		];
		$topActions = [
			'edit'=>['name'=>'Nová položka','url'=>'/branches/edit/'],
		];
		$filtrations = [
			'name'=>['name'=>'Provoz','key'=>'name','type'=>'like'],
		];
		$posibility = [
			'edit'=>['name'=>'Editovat','url'=>'/branches/edit/'],
			'trash'=>['name'=>'Smazat','url'=>'/branches/trash/'],
		];
		
		$conditions = [];
		$conditions = $this->ViewIndex->conditions($conditions);
		//pr($conditions);
		$data = $this->Settings->find()
		->where($conditions)
		->select([
			'id',
			'name',
		]);
		//->toArray();
		
		if (empty($data)){
			$data = null;
		}
		
		$params = [
			'filtrations'=>$filtrations,
			'topActions'=>$topActions,
			'cols'=>$cols,
			'posibility'=>$posibility,
			'data'=>$data,
		];
		
		$this->ViewIndex->load($params);
		
	
	}

    public function edit($id = null){
		
		$this->checkLogged();
        $data = $this->Settings->newEntity(); 
        //pr($data);
		if ($id != null){
			$conditions = ['id'=>$id];
			$data = $this->Settings->find()
            ->where($conditions)
            ->first()
            ;
		
		}
		//pr($data);
		$data->password_tmp = $data->password2 = $data->password;
		$this->set(compact("data"));
        $this->render('index');
		if (!empty($this->request->data)){   
			//pr($this->request->data);die();
			$saveData = $this->Settings->patchEntity($data,$this->request->data);
			 $this->check_error($data);
	 
			//pr($saveData);die();
			$this->Settings->save($saveData);
           	die(json_encode(['result'=>true,'message'=>'Uloženo']));
			  
        }
    }
    public function trash($id){
        $this->loadModel('Settings');
        $conditions = ['id'=>$id];
        $this->Settings->trashAll($conditions);
        $this->redirect('/branches/');


    }
    
  

}
