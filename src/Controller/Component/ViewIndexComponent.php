<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\ORM\Entity;
use App\View\Helper\FastestHelper;

class ViewIndexComponent extends Component
{
	public function load($params=null){
		$this->controller = $this->_registry->getController();

		if(isset($params["paginated_data"])){
			$params["data"] = $params["paginated_data"];
			unset($params["paginated_data"]);
		}else{
			$params['data'] = $this->controller->paginate($params['data']);
			$params["data"] = $params["data"]->toArray();
		}
		$this->controller->set('params',$params);
	
		if ($this->request->isAjax){
			$this->controller->render('../ViewIndex/items');
		} else {
			$this->controller->render('../ViewIndex/index');
		}
	}
	
	public function checkConditions(){
        $session = $this->request->session();	

		$this->controller = $this->_registry->getController();
		if ($session->check('historyConditions'.$this->request->params['controller'])){
			$h = $session->read('historyConditions'.$this->request->params['controller']);
			$this->controller->set('filtrData',$h);
			//  pr($h);
			 foreach($h AS $k=>$item){
				if (substr($k,0,10) == 'system_id_'){
					if (!isset($system_id)){
						$system_id = [];
					}
					if ($item == 1)
					$system_id[substr($k,10,12)] = substr($k,10,12);
					unset($h[$k]);
					unset($h[$k]);
					// pr(substr($k,10,12));
				}
			 }
			 if (isset($system_id)){
				$h['system_id IN'] = $system_id;
			 }

			// pr($h);die();
			//return [];
			return $conditions = $h;
            // pr($this->request->query);
            //pr($h);
		}
		
    }

	public function conditions($conditions){
		$this->controller = $this->_registry->getController();
		$session = $this->request->session();	
		//DELALO bordel v mezi moduly Orders a WebOrder kdy se prenasela ze sessiony filtrace a rozbijela to...
		/*if (empty($this->request->query && !$this->request->isAjax)){
			if (!$this->request->isAjax){
				$con = $this->checkConditions();
				if (!empty($con)){
					$this->request->query = $con;
				}
			}
		}*/

		/*pr($this->request->query); 
		die('aaa');*/
		if (!empty($this->request->query)){
			$disable_key = [
				'page',
				'h',
			];
			function validateDate($date, $format = 'd.m.Y')
			{
				$d = \DateTime::createFromFormat($format, $date);
				return $d && $d->format($format) == $date;
			}
			
			foreach($this->request->query AS $key=>$query){
				if ($query != '' && !in_array($key,$disable_key)){
					$key = strtr($key,['__'=>'.']);
					// pr($key);
					if ($key == 'ProductConnects.product_group_id'){
						$this->controller->loadModel('ProductConnects');
						$this->conList = $this->controller->ProductConnects->conList($query);
						if (!empty($this->conList)){
							$conditions['id IN'] = $this->conList;
						
						} else {
							$conditions['id IN'] = -1;
						
						}
						unset($this->request->query[$key]);
						unset($$key);
						continue;
					}
					if (strpos($key,'|') == true){
						list($key,$params) = explode('|',$key);
						if (validateDate($query)){
							$date = explode('.',$query);
							$query = $date[2].'-'.$date[1].'-'.$date[0];
						}
						$key = strtr($key,['__'=>'.']);
						switch($params){
							case 'fromDateTimestamp':
								$date = date($query);
								$dateStamp = strtotime($date);
								$conditions[$key.' >= '] = $dateStamp;

							break;
							case 'toDateTimestamp':
								$date = date($query);
								$dateStamp = strtotime($date);
								$conditions[$key.' <= '] = $dateStamp;
							break;
							case 'fromDate':
								$conditions['date('.$key.') >= '] = $query;
							break;
							case 'toDate':
								$conditions['date('.$key.') <= '] = $query;
							break;
							case 'date':
								$conditions['date(' . $key . ') LIKE '] = '%' . $query . '%';
							break;
							case 'like':
								$conditions[$key.' LIKE '] = '%'.$query.'%';
							break;
							case 'text':
								$conditions[$key] = $query;
				
							break;
						}
						
					} else {

						if (substr($key,0,10) == 'system_id_'){
							if (!isset($system_id)){
								$system_id = [];
							}
							if ($query == 1){
								$system_id[substr($key,10,12)] = substr($key,10,12);
								unset($conditions[$key]);
								unset($conditions[$key]);
							}
							if (isset($system_id)){
								$conditions['system_id IN'] = $system_id;
							}
						} else {
							$conditions[$key] = $query;
					
						}
					}
				}
			}
		}
		
		if(isset($_GET['resetConditions'])){
			$session->delete('historyConditions'. $this->request->params['controller']);
		}
		if (!empty($conditions)){
			$session->write('historyConditions'. $this->request->params['controller'], $this->request->query);
		} else {
			$session->delete('historyConditions'. $this->request->params['controller']);
		}
	
		return $conditions;
	}
}