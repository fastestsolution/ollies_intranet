<?php

namespace App\Controller;
 
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Hash;
use Cake\I18n\Time;

class ApisController  extends AppController
{
	var $apiUrl = 'http://intr.fastestdev.cz/api/';
	
	public function initialize(){
		parent::initialize();
		/*
		$pos = strpos($_SERVER['HTTP_HOST'],'.localhost');
		if($pos == true){
			$this->apiUrl = 'http://'.$_SERVER['HTTP_HOST'].'/api/';
		}
		*/
		//pr($this->apiUrl);die();
	}
	
	public function index(){
	
	}

	public function saveTable(){
		$data = $_POST;
		if(isset($data['system_id']) && isset($data['id'])){
			$id = $data['id'];
			$data['local_id'] = $id;
			unset($data['id']);

			$this->loadModel('Tables');
			$exist = $this->Tables->find()->where([
				'local_id' => $id,
				'system_id' => $data['system_id']
			])->first();

			
			if(!$exist){
				$exist = $this->Tables->newEntity($data);
			}else{
				$exist = $this->Tables->patchEntity($exist, $data);
			}
			
			if($this->Tables->save($exist)){
				die(json_encode([
					'result'=>true
				]));
			}else{
				die(json_encode([
					'result'=>false
				]));
			}
		}
		die(json_encode([
			'result'=>false,
			'message'=>'Missing data'
		]));
	}
	
	public function stornoOrder(){
		$data = $_POST;
	
		$result = ['result'=>true, 'message'=>'Storno bylo ulozeno'];
		try{
			if(isset($data['system_id'] , $data['order_id'], $data['storno_type'], $data['storno_user_id'])){
				$this->loadModel('Orders');
				$order = $this->Orders->find()->where([
					'local_id' =>  $data['order_id'],
					'stock_type_id' =>  $data['stock_type_id'],
					'system_id' =>  $data['system_id'],
				])->first();

				if(!$order){
					throw new \Exception('Objednavka nebyla na serveru nalezena');
				}

				if($order->storno == 1){
					throw new \Exception('Objednavka jiz byla stornovana');
				}
				$order->storno = 1;
				$order->storno_user_id = $data['storno_user_id'];
				$order->storno_type = $data['storno_type'];

				$result['sended'] = (new Time())->format('Y-m-d H:i:s');
				if(isset($data['storno_reason']) && trim($data['storno_reason']) != ''){ 
					$order->storno_reason = $data['storno_reason'];
				}
				if(isset($data['storno_fik'])){ 
					$order->storno_fik = $data['storno_fik'];
				}
				if(isset($data['storno_bkp'])){ 
					$order->storno_bkp = $data['storno_bkp'];
				}
				if($this->Orders->save($order, ['validate'=>false])){ 
					$this->loadModel('Stocks');
					$stockItems = $this->Stocks->find()->where([
						'local_id IS NOT NULL',
						'system_id' => $data['system_id'],
						'order_id' => $data['order_id']	//v local_id je ulozeno ID  skladove polozky takze se musi vyhledavat podle order_id
					])->toArray();

					$saveStornoStock = [];
					if($stockItems){
						foreach($stockItems as $sItem){
							unset($sItem->id);
							$newStock = $this->Stocks->newEntity($sItem->toArray());
							$newStock->value = abs($newStock->value);
							$newStock->storno_date = new Time(); 
							$newStock->stock_type_id = 12;  //TYP STORNO
							$saveStornoStock[] = $newStock; 
							$this->Stocks->save($newStock); 
						} 
					} 
					$result['items'] = $saveStornoStock;
				}
			}else{
				throw new \Exception('Chybna vstupni data: '. json_encode($data));
			}
		}catch(\Exception $e){
			$result['result'] = false;
			$result['message'] = $e->getMessage();
			@file_put_contents( ROOT . '/tmp/logs/order_storno.log', date('Y-m-d H:i:s') . ' - ' . $e->getMessage() . PHP_EOL, FILE_APPEND);
		}
		die(json_encode($result));
	}

	public function storeLocalStocks(){
		$result = ['result' => true, 'message'=>null];
		try{
			if(isset($_POST['save_data'])){
				$stockData = json_decode($_POST['save_data'], true);
				if($stockData){
					$sended = new Time();
					$this->loadModel('Stocks');
					$stockEntity = $this->Stocks->newEntity($stockData);
					if(isset($stockData['id'])){
						//Pokud je zaznam jiz v cloudu zapsan, tak zasleme zpet pouze datum jeho zapsani, aby si pokladna mohla poznacit cas, a neposilala ho znovu
						$rowExist = $this->Stocks->find()->select(['id', 'created'])->where([
							'stock_type_id'=> $stockData['stock_type_id'], 
							'local_id'=> $stockData['id'], 
							'system_id'=>$stockData['system_id']
						])->first();
						if($rowExist){
							$result['message'] =  'Zaznam jiz je v cloudu zapsan'. ($rowExist->created ? ' ze dne '. $rowExist->created->format('d.m.Y H:i:s') : '');
							$result['sended'] =   $rowExist->created ? $rowExist->created->format('Y-m-d H:i:s') : (new Time())->format('Y-m-d H:i:s');
							$result['result'] =   true;
							$result['id'] =   $rowExist->id;
							die(json_encode($result));
						}
						$stockEntity->local_id = $stockData['id'];
					}
					$stockEntity->sended = $sended;
					unset($stockEntity->id);
					unset($stockEntity->modified);
					unset($stockEntity->created);
		
					if(!($resultDb = $this->Stocks->save($stockEntity))){
						throw new \Exception('Chyba ulozeni');
					} 
					$result['id'] = $resultDb->id; 
					$result['sended'] = $sended->format('Y-m-d H:i:s');
					//pr($stockData);
					//Pokud se zasle prevodka minus tak se vytvori pozadavek na prijmuti
					if($resultDb && isset($resultDb->stock_type_id) && in_array($resultDb->stock_type_id, [2, 14])){
							$this->loadModel('Branches');
							$branch = $this->Branches->find()->where(['id'=>$resultDb->system_id])->select(['name'])->first();

							$transferData = [
								'system_id'=>$resultDb->system_id_to, //Tady se musi prohodit aby se vytvorila spravna plusova prevodka
								'system_id_to'=>$resultDb->system_id ,
								'stock_item_id'=>$resultDb->stock_item_id,
								'stock_item_product_id'=>$resultDb->stock_item_product_id,
								'stock_type_id'=>$resultDb->stock_type_id,
								'value'=>$resultDb->value,
								'tax_id'=>$resultDb->tax_id,
								'unit_id'=>$resultDb->unit_id,
								'product_group_id'=>$resultDb->product_group_id,
								'nakup_price'=>$resultDb->nakup_price,
								'nakup_price_vat'=>$resultDb->nakup_price_vat,
								'nakup_price_total'=>$resultDb->nakup_price_total,
								'nakup_price_total_vat'=>$resultDb->nakup_price_total_vat,
								'jednotka_id'=>$resultDb->jednotka_id,
								'faktura'=>$resultDb->faktura,
								'note'=>$resultDb->note,
								'createdTime'=>$resultDb->createdTime,
							];
							$this->loadModel('StockQueues');
							$this->StockQueues->save($this->StockQueues->newEntity([
								'confirm'=>0,
								'system_id'=> $resultDb->system_id_to,
								'system_id_from'=> $resultDb->system_id,
								'data'=> json_encode($transferData),
								'system_name_from'=> $branch ? $branch->name : null,
							]));
							/*pr([
								'confirm'=>0,
								'system_id'=> $stockData['system_id_to'],
								'system_id_from'=> $stockData['system_id'],
								'data'=> json_encode($stockData),
								'system_name_from'=> $branch ? $branch->name : null,
							]); 
							
							die();*/
					}
				}else{
					throw new \Exception('Nespravny format dat '. $_POST['save_data']);
				}
			}else{
				throw new \Exception('Nezaslany skladova data');
			}
		}catch(\Exception $e){
			$result['result'] = false;
			$result['message'] = $e->getMessage();
		}
		die(json_encode($result));
	}  

	public function storeLocalOrders(){
		$result = ['result' => true, 'message'=>null];
		try{
			if(isset($_POST['save_data'])){
				$orderData = json_decode($_POST['save_data'], true);
				//pr($orderData); die(); 
				if($orderData){
					$localId = null;
					$rowExist = false;
					$sended = new Time();
					$this->loadModel('Orders'); 
					
					if(isset($orderData['id'])){ 
						$localId = $orderData['id'];
						unset($orderData['id']);
						//Pokud je zaznam jiz v cloudu zapsan, tak zasleme zpet pouze datum jeho zapsani, aby si pokladna mohla poznacit cas, a neposilala ho znovu
						//UPDATE - namisto vraceni datumu je objednavka updatovana a polozky upraveny pro pripad ze byla zaslana upravena objednavka - k tomu muze dojit v pripade ze je pridan dalsi produkt k uzavrene polozce
						/** 
						 * Nepouzivano od verze 1.78
							$rowExist = $this->Orders->find()->select(['id', 'created'])->where(['local_id'=> $orderData['id'], 'system_id'=>$orderData['system_id']])->first();
							if($rowExist){
								$result['message'] =  'Zaznam jiz je v cloudu zapsan'. ($rowExist->created ? ' ze dne '. $rowExist->created->format('d.m.Y H:i:s') : '');
								$result['sended'] =   $rowExist->created ? $rowExist->created->format('Y-m-d H:i:s') : (new Time())->format('Y-m-d H:i:s');
								$result['id'] =   $rowExist->id;
								die(json_encode($result));
							}
							$orderEntity->local_id = $orderData['id']; 
							$orderEntity->order_id = $orderData['id'];
						*/
						$rowExist = $this->Orders->find()
												 ->where([
													 'Orders.local_id'=> $localId,
													 'Orders.system_id'=> $orderData['system_id']])
												->first();
					}

					

					if(!$rowExist){
						$orderEntity = $this->Orders->newEntity($orderData);
					}else{
						$orderEntity = $this->Orders->patchEntity($rowExist, $orderData, ['associated' => false]);

						$this->loadModel('OrderItems');
						$existing_order_items = $this->OrderItems->find()->where(['OrderItems.order_id'=>$rowExist->id])->toArray();
						if(!empty($existing_order_items)){
							$existing_order_items = Hash::combine($existing_order_items, '{n}.queue', '{n}');
						}
					}
					$orderEntity->order_items = [];
					if(isset($orderData['order_items'])){
						$this->loadModel('OrderItems');
						foreach($orderData['order_items'] as $item){
							unset($item['id']); 
						
							if(isset($existing_order_items[$item['queue']])){
								$orderEntity->order_items[$item['queue']] = $this->OrderItems->patchEntity($existing_order_items[$item['queue']], $item);
							}else{
								$orderEntity->order_items[$item['queue']] = $this->OrderItems->newEntity($item);
							}
							if(isset($item['created']) && $item['created']){
								$orderEntity->order_items[$item['queue']]->created = (new Time($item['created']))->format('Y-m-d H:i:s');
							}
							if(isset($item['modified']) && $item['modified']){
								$orderEntity->order_items[$item['queue']]->modified = (new Time($item['modified']))->format('Y-m-d H:i:s');
							}
						}
					}
					
					$orderEntity->created = $sended;
					$orderEntity->table_open = (isset($orderData['table_open']) ? new Time($orderData['table_open']) : null);
					$orderEntity->table_close = (isset($orderData['table_close']) ? new Time($orderData['table_close']) : null);
					$orderEntity->table_paid = (isset($orderData['table_paid']) ? new Time($orderData['table_paid']) : null);
					unset($orderEntity->modified);
					if(isset($orderEntity->order_items) && !empty($orderEntity->order_items)){
						foreach($orderEntity->order_items as &$item){
							$item->table_closed = (isset($item->table_closed) && $item->table_closed ? new Time($item->table_closed) : null);
						}
					} 
					$orderEntity->local_id = $localId; 
					$orderEntity->order_id = $localId;

					//TODO order items ID and localId
					if(!($resultDb = $this->Orders->save($orderEntity, ['associated' => ['OrderItems']]))){
						throw new \Exception('Chyba ulozeni');
					}
					$result['data'] = $resultDb;
					$result['id'] = $resultDb->id;
					$result['sended'] = $sended->format('Y-m-d H:i:s');
				}else{
					throw new \Exception('Nespravny format dat '. $_POST['save_data']);
				}
			}else{
				throw new \Exception('Nezaslany data objednavky');
			}
		}catch(\Exception $e){
			$result['result'] = false;
			$result['message'] = $e->getMessage();
		}
		die(json_encode($result));
	} 
	
	/**
	 * import objednavek z pokladny
	 */
	public function receiveOrders($dataStock,$dataOrder){
		// pr($dataStock);
		// pr($dataOrder['created']);die();
		if (isset($dataOrder)){
			
			$this->loadModel('Orders');
			
			if (!$dataOrder){
				die(json_encode(['result'=>false,'Nenalezeny zadne data objednavek']));
			}

			$dataOrder['order_id'] = $dataOrder['id'];
			unset($dataOrder['id']);	
			$dataOrder['user_id'] = $dataStock['user_id'];
			$dataOrder['user_name'] = $dataStock['user_name'];
			$dataOrder['terminal_id'] = $dataStock['terminal_id'];
			$dataOrder['created'] = new Time($dataOrder['created']);
			$dataOrder['modified'] = new Time($dataOrder['modified']);
			$dataOrder['table_open'] = new Time($dataOrder['table_open']);
			$dataOrder['table_close'] = new Time($dataOrder['table_close']);
			foreach($dataOrder['order_items'] AS $k=>$oi){
				$dataOrder['order_items'][$k]['createdTime'] = strtotime($dataOrder['created']);
				unset($dataOrder['order_items'][$k]['id']);
			}
			$entity = $this->Orders->newEntity($dataOrder);
			// pr($entity);die();
			try { 
				// pr($entity);die();
				$this->Orders->save($entity);

				
			} catch (\Exception $e) {
				die(json_encode(['result'=>false,'message'=>$e]));
			}
		
			// pr($saveStocks);
			//die(json_encode(['result'=>true]));
		} else {
			//die(json_encode(['result'=>false,'message'=>'Nejsou poslany zadne produkty']));
		}
	}



	/**
	 * import skladovych polozek z pokladny
	 */
	public function receiveStocks($system_id){

		$rawData = file_get_contents("php://input");
		$dataList = json_decode($rawData,true);

		if (isset($dataList)){
			$emptyRecepies = [];
			$this->saveIds = [];
			foreach($dataList AS $data){
				if (!isset($data['stockList']['data'])){
					continue;
				}

				$product_ids = array_keys($data['stockList']['data']);
				$conditions = ['code IN'=>$product_ids];
				// pr($conditions);die();
				$this->loadModel('Products');
				$dataLoad = $this->Products->find()
				->contain([
					
					'ProductRecipes' => function($q){
						return $q->select([
							'id',
							'product_id',
							'stock_item_global_id',
							'value',
							'loss',
							'unit_id',
						]);
					},
				])
				->where($conditions)
				->select([
					'id',
					'name',
					'code',
					'price',
					'price_without_tax',
					'tax_id',
				])
				->toArray();
				if (!$dataLoad){
					die(json_encode(['result'=>false,'Nenalezeny zadne produkty','conditions'=>$conditions]));
				}
				// pr($data);
				$saveStocks = [];
				if (isset($data['orderData'])){
					$this->receiveOrders($data['stockList'],$data['orderData']);
				}	
				
				foreach($dataLoad AS $d){
					// pr($data);die();
					if (empty($d['product_recipes'])){
						$emptyRecepies[] = $d->code .' - '.$d->name;
					}
					foreach($d['product_recipes'] AS $recepies){
						// pr($recepies);die();
						$saveStocks[] = [
							'order_id'=>$data['stockList']['order_id'],
							'stock_item_id'=>$recepies->stock_item_global_id,
							'stock_item_product_id'=>$recepies->product_id,
							'value'=>$recepies->value * $data['stockList']['data'][$d->code]['count'],
							'stock_type_id'=>4,
							'system_id'=>$system_id,
							'tax_id'=>$d->tax_id,
							'unit_id'=>$recepies->unit_id,
							'user_id'=>$data['stockList']['user_id'],
							'user_name'=>$data['stockList']['user_name'],
							'terminal_id'=>$data['stockList']['terminal_id'],
							'product_group_id'=>$data['stockList']['data'][$d->code]['product_group_id'],
							'createdTime'=>time(),
						];
						// pr($saveStocks);die();
					}
				}
				$this->loadModel('Stocks');
				// pr($saveStocks);die();
				$entities = $this->Stocks->newEntities($saveStocks);
				// pr($entities);die();
				
				foreach($entities AS $entity){
					try { 
						// pr($entity);die();
						$res = $this->Stocks->save($entity);
						$this->saveIds[] = $res->id;
						
					} catch (\Exception $e) {
						die(json_encode(['result'=>false,'message'=>$e]));
					}
				}
			}
			// pr($saveStocks);
			die(json_encode(['result'=>true,'emptyRecepies'=>implode(',',$emptyRecepies),'ids'=>implode(',',$this->saveIds)]));
		} else {
			die(json_encode(['result'=>false,'Nejsou poslany zadne produkty']));
		}
	}



	/**
	 * get stock StockQeues
	 */
	public function getStockQueues($system_id){
		$conditions = ['system_id'=>$system_id,'confirm'=>0];
		
		$this->loadModel('StockQueues');


		$mapper = function ($data, $key, $mapReduce) {
			$data->data = json_decode($data->data,true);
			$mapReduce->emit($data);  
		};
		

		$data = $this->StockQueues->find()
		->where($conditions)
		->mapReduce($mapper)
		->toArray();
		//pr($data);

		$this->loadModel('StockGlobalItems');
		$globalList = $this->StockGlobalItems->skladItemsList();
		// pr($globalList);

		if ($data){
			$dataList = [];
			foreach($data AS $d){
				if (!isset($d->data[0])){
					$tmp = $d->data;
					$d->data = [];
					$d->data[0] = $tmp;
				}
				// pr($d);die();
				$list = [];
				foreach ($d->data AS $dd){
					//pr($d);die();
					if (isset($globalList[$dd['stock_item_id']]))
					$list[] = $globalList[$dd['stock_item_id']].' '.$dd['value'];
				}
				$dataList[] = [
					'id'=>$d->id,
					'created'=>$d->created,
					'from_name'=>$d->system_name_from,
					'list' => $list
				];
			}
			die(json_encode(['result'=>true,'data'=>$dataList,'count'=>count($dataList)]));
		} else {
			die(json_encode(['result'=>false]));
		}
	}
	
	/**
	 * export pobocek
	 */
	public function exportBranches(){
		$this->loadModel('Branches');
		$this->branches_list = $this->Branches->branchesList();
		// pr($this->branches_list);
		die(json_encode(['result'=>true,'data'=>$this->branches_list]));
	} 


	/**
	 * potvrzeni prevodky z pokladny
	 */
    public function confirmPrevodka($id){
		$this->loadModel('StockQueues');
		$this->loadModel('Stocks');
		$find = $this->StockQueues->find()
		->where(['id'=>$id,'confirm'=>0])
		->first();
		if ($find){
			$this->StockQueues->updateAll(
				['confirm' => 1], // fields
				['id' => $id]
			);
			$dataTmp = json_decode($find->data,true);
			$data = [];
			if (!isset($dataTmp[0])){
				$data[] = $dataTmp;

			} else {
				$data = $dataTmp;
			}
			// pr($find);
			foreach($data AS $d){
				// pr($d);
				// pr($d);
				$d['stock_type_id'] = 11;
				$d['system_id_to'] = $find->system_id_from;
				//pr($d);
				$saveStocks = $d;
				$entity = $this->Stocks->newEntity($saveStocks);
				// pr($entity);die();
				$result = $this->Stocks->save($entity);
			}


			die(json_encode(['result'=>true,'message'=>'Převodka ID '.$id.' potvrzena '.$result->id]));
		
		} else {
			die(json_encode(['result'=>false,'message'=>'Převodka ID '.$id.' již byla potvrzena']));
			
		}

	}

	/**
	 * Nove potvrzeni pro lokalni sklady
	 */
	public function confirmStockTransfer($id){
		$this->loadModel('StockQueues');
		$find = $this->StockQueues->find()
					->where(['id'=>$id,'confirm'=>0])
					->first();
		if ($find){
			$result = $this->StockQueues->updateAll(
				['confirm' => 1], // fields
				['id' => $id]
			);
			die(json_encode(['result'=>true,'message'=>'Převodka ID '.$id.' potvrzena ', 'data'=>json_decode($find->data, true)]));
		} else {
			die(json_encode(['result'=>false,'message'=>'Převodka ID '.$id.' již byla potvrzena']));
		}
	}


	/**
	 * export produktu do pokladny
	 */
	public function  exportProducts($system_id = null){
		//sleep(5);
		if (!$system_id){
			die(json_encode(['result'=>false,'message'=>'Neni kod']));
		}
		$this->system_id = $system_id;
		// pr($system_id);die();
		$this->loadModel('Products');
		$conditions = [];
		$mapper = function ($data, $key, $mapReduce) {
			// if (isset($data->product_prices[0]) && !empty($data->product_prices[0]->price)){
			if (isset($data->product_prices[0]) && !empty($data->product_prices[0]->price)){
				$data->price = $data->product_prices[0]->price;
				$data->price2 = $data->product_prices[0]->price2;
				$data->price3 = $data->product_prices[0]->price3;
				$data->price4 = $data->product_prices[0]->price4;
			}

			if (isset($data->product_prices[0]->checked) && $data->product_prices[0]->checked == 0){
				$data->price = -1;
				unset($data);
			}
			
			if (isset($data)){
				unset($data->product_prices);
				$mapReduce->emit($data);
			}  
		};

		$data = $this->Products->find()
			->contain([
				'ProductPrices' => function($q){
					return $q->select([
						'product_id',
						'id',
						'price',
						'price2',
						'price3',
						'price4',
						'checked',
					])->where(['system_id'=>$this->system_id]);
				},
				'ProductConnects' => function($q){
					return $q->select([
						'id',
						'product_id',
						'product_group_id',
						'pref',

					]);
				},
			])
			->mapReduce($mapper)
			->where($conditions)
			->select([
				'id',
				'name',
				'code',
				'num',
				'price',
				'price_without_tax',
				'tax_id',
				'addons',
				'group_trzba_id',
				'is_mirror', 
				//'is_dashboard',
			])
			->toArray();
			
		$this->loadModel('ProductGroups');
		$groups = $this->ProductGroups->find()->toArray();

		$this->loadModel('ProductConnAddons');
		$conn_addons = $this->ProductConnAddons->find()->toArray();

		$this->loadModel('ProductAddons');
		$addons = $this->ProductAddons->find()->toArray();
		foreach($addons AS $k=>$ad){
			unset($addons[$k]['created']);
			unset($addons[$k]['modified']);
		}
		
		$this->loadModel('Branches');
		$branches = $this->Branches->find()->toArray();

		$this->loadModel('ProductRecipes');
		$product_recipes = $this->ProductRecipes->find()->toArray();

		$this->loadModel('StockGlobalItems');
		$stock_global_items = $this->StockGlobalItems->find()->toArray();

		$this->result(['result'=>true,'addons'=>$addons,'con_addons'=>$conn_addons,'groups'=>$groups,'data'=>$data,'branches'=>$branches, 'product_recipes' => $product_recipes, 'stock_global_items' => $stock_global_items]);
	}   

	/**
	 * export kategorie produktu
	 */
	public function exportCategories(){
		$this->loadModel('ProductGroups');
		$conditions = [];
		$data = $this->ProductGroups->find()
			->where($conditions)
			->select([
				'id',
				'parent_id',
				'level',
				'lft',
				'rght',
				'name',
				'order_num',
			])
			->toArray();

		$this->result(['result'=>true,'data'=>$data]);
	}


	/**
	 * importovani dat z pokladny do skladu
	 */
	public function importStocks(){ 
		$data = $_POST['type'];

		$this->result(['result'=>true,'data'=>$data]);
	
	}

	/**
	 * testovaci funckce pro import do skladu
	 */
	public function testSendStock(){
		$ch = curl_init();
		$post = [
			'type'=>'exit',
			//'type'=>'enter',
		];
		//pr($this->apiUrl.'importStocks/');die();
		curl_setopt($ch, CURLOPT_URL, $this->apiUrl.'importStocks/');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		$result = json_decode($result);
		//pr($result);
		curl_close ($ch);
		$this->result($result);
	
	}



	/**
	 * export result
	 */
	private function result($data){
		if (isset($this->request->query['debug'])){
			pr($data);
		}
		die(json_encode($data));

	}
	
	public function saveMessage(){
		//sleep(6);
		
		$this->loadModel('ErrorMessages');
		$_POST['provoz_name'] = $this->system_list[$_POST['system_id']]['name'];
		$save_entity = $this->ErrorMessages->newEntity($_POST);
        //pr($save_entity); die();
        $this->sendErrorEmail();
		if (!$resultDb = $this->ErrorMessages->save($save_entity)){
			die(json_encode(['result'=>false,'message'=>'Chyba uložení zprávy']));	
		} else {
			die(json_encode(['result'=>true,'message'=>'Zpráva uložena']));	
		
		}
		$this->Logs->deleteAll(['created <' => date('Y-m-d 00:00:00', strtotime("-3 days"))]);
	
	}
	
	public function saveLog(){
		//sleep(6);
		
		$this->loadModel('Logs');
		$save_entity = $this->Logs->newEntity($_POST);
        //pr($save_entity); die();
        $this->sendLogEmail();
		if (!$resultDb = $this->Logs->save($save_entity)){
			die(json_encode(['result'=>false,'message'=>'Chyba uložení objednávky']));	
		} else {
			die(json_encode(['result'=>true,'message'=>'Log uložen']));	
		
		}
		$this->Logs->deleteAll(['created <' => date('Y-m-d 00:00:00', strtotime("-3 days"))]);
	
	}
	public function sendErrorEmail(){
		$this->loadComponent('Email');
		$data_email = 'Hlášení z pokladny: '.$this->system_list[$_POST['system_id']]['name'].' ('.date('d.m.y H:i:s').")<br />";
		$data_email .= 'Zpráva: '.$_POST['message'].'<br />';
		$data_email .= 'Telefon: '.$_POST['phone'].'<br />';
		$data_email .= 'Email: '.$this->system_list[$_POST['system_id']]['email'].'<br />';
		$data_email .= 'Jméno: '.$_POST['name'].'<br />';
		$opt = [
			'to'=>['tyson@fastest.cz'],
			'fromEmail'=>$this->system_list[$_POST['system_id']]['email'],
			'subject'=>'Hlášení z pokladny: '.$this->system_list[$_POST['system_id']]['name'].' ('.date('d.m.y H:i:s').')',
			'data'=>$data_email,
		];
		$this->Email->send($opt);
	}
	
	public function sendLogEmail(){
		$this->loadComponent('Email');
		$data_email = 'Chyba pokladny: '.$this->system_list[$_POST['system_id']]['name'].' ('.date('d.m.y H:i:s').")<br />";
		$data_email .= 'Message: '.$_POST['message'].'<br />';
		$data_email .= 'URL: '.$_POST['url'].'<br />';
		$data_email .= 'Error: '.$_POST['error'].'<br />';
		
		
		$opt = [
			'to'=>['test@fastest.cz'],
			'subject'=>'Chyba pokladny: '.$this->system_list[$_POST['system_id']]['name'].' ('.date('d.m.y H:i:s').')',
			'data'=>$data_email,
		];
		if ($_POST['message'] != 'Vypršel maximální čas pro zobrazení požadavku (10 sekund)'){
			$this->Email->send($opt);
		}
	}
	
	
    public function login(){
		$results = [
			'result'=>true,
			'data'=>[
				'user'=>[
					'hash'=>'abcdaa',
					'id'=>1,
					'name'=>'Fastest',
				]
			]
		];
    	$this->set([
			'results' => $results,
			'_serialize' => 'results'
		]);
	
	}
	
	public function test(){
		$this->viewBuilder()->layout('angular');
		$hp = 'test hp';
		$test = 'test data';
		
		$this->loadModel('Users');
		$data = $this->Users->find()->toArray();
		//pr($data);die('a');
		
		$results = [
			'result'=>true,
			'message'=>null,
			'data'=>[
				'data'=>$data,
				'test'=>$test,
				'hp'=>$hp,
			]
		];
		$this->set([
			'results' => $results,
			'_serialize' => 'results'
		]);
		
		$this->render('render');
	}
    
}
