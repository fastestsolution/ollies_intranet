<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;

class BranchesStatsController extends AppController{
    
	
	
    public function index(){
		$this->set('title','Statistika poboček');
		$this->checkLogged();
        $this->loadComponent('ViewIndex');
        
        // $this->loadModel('BranchesStats');
        // $this->branches_list = $this->BranchesStats->branchesStatsList();
        // pr($this->branches_list);die();
        $this->loadModel('Branches');
        $this->branches_list = $this->Branches->branchesList();
        $this->set('branches_list',$this->branches_list);

		$cols = [
			'id'=>['name'=>'ID'],
			'branch_id'=>['name'=>'Provoz','list'=>$this->branches_list],
			'date'=>['name'=>'Datum'],
			'value'=>['name'=>'Předpoklad'],
		];
		$topActions = [
			'edit'=>['name'=>'Nová položka','url'=>'/branches-stats/edit/'],
		];
		$filtrations = [
			'date'=>['name'=>'Datum','key'=>'date','type'=>'date'],
			'branch_id'=>['name'=>'Provoz','key'=>'branch_id','type'=>'select','list'=>[''=>'Provoz']+$this->branches_list],
		];
		$posibility = [
			'edit'=>['name'=>'Editovat','url'=>'/branches-stats/edit/'],
			'trash'=>['name'=>'Smazat','url'=>'/branches-stats/trash/'],
		];
		
		$conditions = [];
		$conditions = $this->ViewIndex->conditions($conditions);
		//pr($conditions);
		$data = $this->BranchesStats->find()
		->where($conditions)
		->select([
			'id',
			'branch_id',
			'date',
			'value',
		]);
		//->toArray();
		
		if (empty($data)){
			$data = null;
		}
		
		$params = [
			'filtrations'=>$filtrations,
			'topActions'=>$topActions,
			'cols'=>$cols,
			'posibility'=>$posibility,
			'data'=>$data,
		];
		
		$this->ViewIndex->load($params);
		
	
	}

    public function edit($id = null){
		$this->loadModel('Branches');
        $this->branches_list = $this->Branches->branchesList();
        $this->set('branches_list',$this->branches_list);

		$this->checkLogged();
        $data = $this->BranchesStats->newEntity(); 
        
		if ($id != null){
			$conditions = ['id'=>$id];
			$data = $this->BranchesStats->find()
            ->where($conditions)
            ->first()
            ;
		
        }
		$data->password_tmp = $data->password2 = $data->password;
		$this->set(compact("data"));
        
		if (!empty($this->request->data)){   
			//pr($this->request->data);die();
			$saveData = $this->BranchesStats->patchEntity($data,$this->request->data);
			 $this->check_error($data);
	 
			//pr($saveData);die();
			$this->BranchesStats->save($saveData);
			// Cache::delete('branches_statsList');
           	die(json_encode(['result'=>true,'message'=>'Uloženo']));
			  
        }
    }
    public function trash($id){
        $this->loadModel('BranchesStats');
        $conditions = ['id'=>$id];
        $this->BranchesStats->trashAll($conditions);
        $this->redirect('/branches-stats/');


    }
    
  

}
