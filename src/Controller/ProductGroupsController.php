<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;

class ProductGroupsController extends AppController{
    
	/*public function logout(){
		$session = $this->request->session();
		$session->delete('loggedUser');
		$this->redirect('/');
		
	}
	*/
	/*public function login(){
		if (!empty($this->request->data)){
			//pr(md5($this->request->data['password']));
			$find = $this->Users->find()
			->where([
				'username'=>$this->request->data['username'],
				'password'=>md5($this->request->data['password']),
			])
			->first();
			if (!$find){
				$result = ['result'=>false,'message'=>'Uživatel nenalezen'];
			} else {
				$result = ['result'=>true,'message'=>'Uživatel bude přihlášen'];
				$session = $this->request->session();
				$session->write('loggedUser',$find);
				
				
			}
			//pr($this->request->data);die();
			
			die(json_encode($result));
		}
		
	}*/
	
    public function index(){
		$this->checkLogged();
		$this->set('title','Nastavení skupin produktů');
        $this->genSubmenu();
        $conditions = [];
        $data = $this->ProductGroups->find('threaded')
            ->where($conditions)
            ->toArray()
        ;
       // pr($data);

        if($data){
            $this->set('data',$data);
		}
		$this->loadModel('ProductGroups');
		$menus = $this->ProductGroups->find('threaded')->order('lft ASC')->toArray();
		//pr($menus);
		$this->set('menus',$menus);

	}
	public function menusSave(){
		//pr($this->request->data);die('a');
		$connection = ConnectionManager::get('default');
		$results = $connection->execute('TRUNCATE TABLE product_groups'); 
		
		$this->loadModel('ProductGroups');
		$menus = $this->ProductGroups->find('threaded')->hydrate(false)->toArray();
		$menus = $this->request->data;
		//pr($menus);
		//$menus[0]['name'] = 'test testa';
		foreach($menus as $menu){ 
			$this->saveNode($menu,1);
		}
		Cache::clear(false);
		die(json_encode(['r'=>true]));
	}
	private function saveNode($node, $level){
	    //pr($node);die();
		$entity = $this->ProductGroups->newEntity([
            'name'=>$node['name'],
            'level'=>$level,
            'parent_id'=>( isset($node['parent_id']) && !empty($node['parent_id']) ? $node['parent_id'] : null)
        ]);
        if(isset($node['id']) && $node['id']){
            $entity->id = $node['id'];
        }
        //pr($entity);
		$this->ProductGroups->save($entity);

        if(isset($node['children'])){
            foreach($node['children'] as $subnode){
                $subnode['parent_id'] = $entity->id;
                $this->saveNode($subnode, $level + 1);
            }
        }
    } 

    public function edit($id = null){
		
		$this->checkLogged();
        $data = $this->Products->newEntity();
        
		if ($id != null){
			$conditions = ['id'=>$id];
			$data = $this->Products->find()
            ->where($conditions)
            ->first()
            ;
		
        }
		//$data->password_tmp = $data->password2 = $data->password;
		$this->set(compact("data"));
        
		if (!empty($this->request->data)){
			
			//pr($this->request->data);die();
			$saveData = $this->Products->patchEntity($data,$this->request->data);
			 $this->check_error($data);
	 
			//pr($saveData);die();
			$this->Products->save($saveData);
           	die(json_encode(['result'=>true,'message'=>'Uloženo']));
			  
        }
    }
    public function trash($id){
        $this->loadModel('Products');
        $conditions = ['id'=>$id];
        $this->Products->trashAll($conditions);
        $this->redirect('/products/');


    }

    function apiProductGroups(){
        $this->loadModel('ProductGroups');
        $data = $this->ProductGroups->find()
            //->where()
            ->select(['name','id','level','lft','rght'])->contain(['ProductConnects.Products'])->toArray();
        //pr($data);
        die(json_encode($data));
    }
    
  

}
