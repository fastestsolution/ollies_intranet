<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;

class SystemLogsController extends AppController{
    
	
	
    public function index(){
		$this->set('title','Logace systému');
		$this->checkLogged();
        $this->loadComponent('ViewIndex');
        

        $this->loadModel('Users');
        $this->usersList = $this->Users->usersList();
        
        $cols = [
			'id'=>['name'=>'ID'],
			'created'=>['name'=>'Vytvořeno'],
			'user_id'=>['name'=>'Uživatel','list'=>$this->usersList],
			'controller'=>['name'=>'Modul','list'=>$this->controllerList],
			'action'=>['name'=>'Akce'],
			'url'=>['name'=>'URL'],
		];
		$topActions = [
			// 'edit'=>['name'=>'Nová položka','url'=>'/branches/edit/'],
		];
		$filtrations = [
			'user_id'=>['name'=>'Uživatel','key'=>'user_id','type'=>'select','list'=>[''=>'Uživatelé']+$this->usersList],
			'controller'=>['name'=>'Modul','key'=>'controller','type'=>'select','list'=>[''=>'Modul']+$this->controllerList],
		];
		$posibility = [
			// 'edit'=>['name'=>'Editovat','url'=>'/branches/edit/'],
			// 'trash'=>['name'=>'Smazat','url'=>'/branches/trash/'],
		];
		
		$conditions = [];
		$conditions = $this->ViewIndex->conditions($conditions);
		//pr($conditions);
		$data = $this->SystemLogs->find()
		->where($conditions)
        ->order('id DESC')
        ->select([
			'id',
			'created',
			'user_id',
			'controller',
			'action',
			'url',
		]);
		//->toArray();
		
		if (empty($data)){
			$data = null;
		}
		
		$params = [
			'filtrations'=>$filtrations,
			'topActions'=>$topActions,
			'cols'=>$cols,
			'posibility'=>$posibility,
			'data'=>$data,
		];
		
		$this->ViewIndex->load($params);
		
	
	}

    public function edit($id = null){
		
		$this->checkLogged();
        $data = $this->SystemLogs->newEntity(); 
        
		if ($id != null){
			$conditions = ['id'=>$id];
			$data = $this->SystemLogs->find()
            ->where($conditions)
            ->first()
            ;
		
        }
		$data->password_tmp = $data->password2 = $data->password;
		$this->set(compact("data"));
        
		if (!empty($this->request->data)){   
			//pr($this->request->data);die();
			
			$saveData = $this->SystemLogs->patchEntity($data,$this->request->data);
			 $this->check_error($data);
	 
			//pr($saveData);die();
			$resultDb = $this->SystemLogs->save($saveData);
			if ($resultDb){
				if ($this->request->data['branches_from']>0){
					$this->loadModel('ProductPrices');
					$this->ProductPrices->deleteAll(['system_id' => $resultDb->id]);
					$loadPrices = $this->ProductPrices->find()
						->where(['system_id'=>$this->request->data['branches_from']])
						->select(['checked','product_id','price','price2','price3','price4'])
						->hydrate(false)
						->toArray();
					$savePrices = [];
					foreach($loadPrices AS $k=>$p){
						$p['system_id'] = $resultDb->id;
						$savePrices[] = $p;
					}	
					// pr($savePrices);
					$savePricesEntities = $this->ProductPrices->newEntities($savePrices);
					// pr($savePricesEntities);die();
					
					foreach($savePricesEntities AS $ent){
						//pr($ent);
						$resultDb2 = $this->ProductPrices->save($ent);
						
					}
				
				}
			}
			//die();
			Cache::delete('branchesList');
           	die(json_encode(['result'=>true,'message'=>'Uloženo']));
			  
        }
    }
    public function trash($id){
        $this->loadModel('SystemLogs');
        $conditions = ['id'=>$id];
        $this->SystemLogs->trashAll($conditions);
        $this->redirect('/branches/');


    }
    
  

}
