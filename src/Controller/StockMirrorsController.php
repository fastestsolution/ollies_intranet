<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

ini_set('memory_limit', '228M');

class StockMirrorsController extends AppController{

    public function beforeFilter(\Cake\Event\Event $event){

        parent::beforeFilter($event);
        $this->loadModel('SkladCiselniks');
        $this->set('sklad_group_list',$this->sklad_group_list = $this->SkladCiselniks->skladGrouplList());
    }


    public function index(){
        die('deprecated');
        $this->set('title','Zrcadlo skladu');
        $this->checkLogged();
        $this->loadComponent('ViewIndex');
        $this->genSubmenu();


        

        $cols = [
            'id'=>['name'=>'ID'],
            'name'=>['name'=>'Název'],
            // 'price'=>['name'=>'Cena'],
            'system_id'=>['name'=>'Provoz','list'=>$this->branches_list],
            'is_confirm'=>['name'=>'Potvrzeno','list'=>$this->yes_no,'trClass'=>[0=>'red',1=>'']],
            'created'=>['name'=>'Vytvořeno'],
        ];
        $filtrations = [
            'name'=>['name'=>'Název','key'=>'name','type'=>'like'],
            //'code'=>['name'=>'Kod','key'=>'code','type'=>'like'],
           'system_id'=>['name'=>'Provoz','key'=>'system_id','type'=>'select','list'=>[''=>'Provoz']+$this->branches_list],
           'is_confirm'=>['name'=>'Potvrzeno','key'=>'is_confirm','type'=>'select','list'=>[''=>'Potvrzeno']+$this->yes_no],
        ];
        $topActions = [
            // 'edit'=>['name'=>'Nové zrcadlo','url'=>'/stock-mirrors/edit/'],
            //'products'=>['name'=>'Skladové produkty','url'=>'/stocks/products/'],
            //'import'=>['name'=>'Import produktů','url'=>'/stocks/importProducts/','type'=>'ajax'],
        ];
        $posibility = [
            'edit'=>['name'=>'Editovat','url'=>'/stock-mirrors/edit/'],
            // 'trash'=>['name'=>'Smazat','url'=>'/stock-mirrors/trash/'],
        ];
        
        //$conditions = ['system_id'=>$this->loggedUser->system_id];
        $conditions = [];
        $conditions = $this->ViewIndex->conditions($conditions);
        //pr($conditions);
        $mapper = function ($data, $key, $mapReduce) {
            $mapReduce->emit($data);
        };
        $data = $this->StockMirrors->find()
            ->where($conditions)
            ->order('id DESC')
            ->select([

                //'id',
                //'name',
                //'code',
                //'is_connected',
                'system_id',
                'id',
                'name',
                'created',
                'is_confirm',

            ])
            //->mapReduce($mapper)
        ;
        //->toArray();
        //pr($data->toArray());
        if (empty($data)){
            $data = null;
        }

        $params = [
            'filtrations'=>$filtrations,
            'topActions'=>$topActions,
            'cols'=>$cols,
            'posibility'=>$posibility,
            'data'=>$data,
        ];
        //pr($data->toArray());
        $this->ViewIndex->load($params);


    }

   

    public function edit($id = null){
        $this->loadModel('Branches');
        $this->set('branchesList',$this->branchesList = $this->Branches->branchesList());
        // $this->set('stockProducts',$this->stockProducts = $this->StockProducts->productList(true));
        $this->checkLogged();


        // pr($this->stockItemsList);



        $data = $this->StockMirrors->newEntity();
        //pr($ids);
        
        //pr($data);

        if ($id != null){
            $conditions = ['id'=>$id];
            $data = $this->StockMirrors->find()
                ->where($conditions)
                //->select(['id'])
                ->first()
            ;
            // pr($data->date_step2);die();
            // $data->date_step2 = $data->date_step2->format('y-m-d H:i:s');
            //pr( json_decode($data->data_step1, true));
           /* pr( $data);
            die(); */
        }

        $this->set(compact("data"));

        if (!empty($this->request->data)){

            // pr($this->request->data);die();

            if (isset($this->request->query['confirm'])){
                $this->request->data['is_confirm'] = 1;
            }
            $saveData = $this->StockMirrors->patchEntity($data,$this->request->data);

            $this->check_error($data);
            //pr($data->data_diff); die();
            if (!empty($data->data_diff)){
                $this->loadModel('Stocks');
                $this->loadModel('StocksItems');
                $this->loadModel('StockGlobalItems');
                $this->sklad_items = $this->StocksItems->skladItems();
                $this->sklad_items_list = $this->StockGlobalItems->skladItems();
                // pr($this->sklad_items_list);die();
                $diff = json_decode($data->data_diff);
                $data_step_change = json_decode($data->data_step_change);
                // pr($diff);die();
                if (isset($this->request->query['confirm'])){
                    //die('a');
                    if (!empty($diff))
                    foreach($diff AS $difId=>$value){
                        $saveStocks = [
                            'order_id'=>'',
                            'stock_item_id'=>$difId,
                            'stock_item_product_id'=>'',
                            'value'=>(($value<0)?$value*-1:$value),
                            'stock_type_id'=>(($value >= 0)?5:6),
                            'system_id'=>$data->system_id,
                            'tax_id'=>(isset($this->sklad_items_list[$difId])?$this->sklad_items_list[$difId]->tax_id:''),
                            'unit_id'=>(isset($this->sklad_items_list[$difId])?$this->sklad_items_list[$difId]->jednotka:''),
                            'user_id'=>$this->loggedUser['id'],
                            'user_name'=>$this->loggedUser['name'],
                            'terminal_id'=>'',
                            'product_group_id'=>'',
                            'createdTime'=>time(),
                        ];
                        if ($difId == 216){
                            // pr($saveStocks);
                        
                        }
                        // pr($saveStocks);
                        $entity = $this->Stocks->newEntity($saveStocks);
                        $this->Stocks->save($entity);
                        // pr($saveStocks);die();
                    }
                    // die();

                    if (!empty($data_step_change))
                    foreach($data_step_change AS $difId=>$value){
                        $saveStocks = [
                            'order_id'=>'',
                            'stock_item_id'=>$difId,
                            'stock_item_product_id'=>'',
                            'value'=>$value,
                            'stock_type_id'=>(($value > 0)?9:10),
                            'system_id'=>$data->system_id,
                            'tax_id'=>(isset($this->sklad_items_list[$difId])?$this->sklad_items_list[$difId]->tax_id:''),
                            'unit_id'=>(isset($this->sklad_items_list[$difId])?$this->sklad_items_list[$difId]->jednotka:''),
                            'user_id'=>$this->loggedUser['id'],
                            'user_name'=>$this->loggedUser['name'],
                            'terminal_id'=>'',
                            'product_group_id'=>'',
                            'createdTime'=>time(),
                        ];

                        $entity = $this->Stocks->newEntity($saveStocks);
                        $this->Stocks->save($entity);
                        // pr($saveStocks);die();
                    }
                }
            }
            
            // pr($saveData);die();

            $this->StockMirrors->save($saveData);

            // Cache::clear();
            die(json_encode(['result'=>true,'message'=>'Uloženo']));

        }
    }

    public function loadData($system_id){
        $this->loadModel('Stocks');
        $this->mirrorList = $this->Stocks->stockMirrorList($system_id);
        // die('a');
        // pr($this->mirrorList);
        foreach($this->mirrorList AS $k=>$m){
            $this->mirrorList[$k]->unit = (isset($this->stock_unit_list[$m->stock_global_item->jednotka_id])?$this->stock_unit_list[$m->stock_global_item->jednotka_id]:'');
            $this->mirrorList[$k]->real = $this->mirrorList[$k]->sum;
        }
       
        $this->set('mirrorList',$this->mirrorList);
        die(json_encode(['result'=>true,'data'=>$this->mirrorList]));
    }


   
    public function trash($id){
        $conditions = ['id'=>$id];
        $this->loadModel('StockMirrors');
        $this->StockMirrors->trashAll($conditions);
        $this->redirect('/stock-mirrors/');


    }




}