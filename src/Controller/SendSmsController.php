<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class SendSmsController extends AppController{
    
	public function send(){
		$this->checkLogged();
		
		$this->loadModel('Users');
		$this->phone_list = $this->Users->phoneList();
		$this->set('phone_list',$this->phone_list);
		if (!empty($this->request->data)){
			if (empty($this->request->data['phone_list'])){
				die(json_encode(['result'=>false,'message'=>'Vyberte telefon']));
			}
			if (empty($this->request->data['text'])){
				die(json_encode(['result'=>false,'message'=>'Napište zprávu']));
			}
			$this->loadComponent('Sms');
			$this->Sms->send(1,$this->request->data['phone_list'],$this->request->data['text']);
			die(json_encode(['result'=>true,'message'=>'Zpráva poslána']));
			//pr($this->request->data);
		}
	}
	

	
}