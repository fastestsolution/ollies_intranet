<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;

class BranchesController extends AppController{
    
	
	
    public function index(){
		$this->set('title','Správa poboček');
		$this->checkLogged();
        $this->loadComponent('ViewIndex');
        
		$cols = [
			'id'=>['name'=>'ID'],
			'name'=>['name'=>'Provoz'],
			'email'=>['name'=>'Email'],
			'eet_dic'=>['name'=>'EET DIC'],
		];
		$topActions = [
			'edit'=>['name'=>'Nová položka','url'=>'/branches/edit/'],
		];
		$filtrations = [
			'name'=>['name'=>'Provoz','key'=>'name','type'=>'like'],
			'email'=>['name'=>'Email','key'=>'email','type'=>'like'],
		];
		$posibility = [
			'edit'=>['name'=>'Editovat','url'=>'/branches/edit/'],
			'trash'=>['name'=>'Smazat','url'=>'/branches/trash/'],
		];
		
		$conditions = [];
		$conditions = $this->ViewIndex->conditions($conditions);
		//pr($conditions);
		$data = $this->Branches->find()
		->where($conditions)
		->select([
			'id',
			'name',
			'email',
			'eet_dic',
		]);
		//->toArray();
		
		if (empty($data)){
			$data = null;
		}
		
		$params = [
			'filtrations'=>$filtrations,
			'topActions'=>$topActions,
			'cols'=>$cols,
			'posibility'=>$posibility,
			'data'=>$data,
		];
		
		$this->ViewIndex->load($params);
		
	
	}

    public function edit($id = null){
		
		$this->checkLogged();
        $data = $this->Branches->newEntity(); 
        
		if ($id != null){
			$conditions = ['id'=>$id];
			$data = $this->Branches->find()
            ->where($conditions)
            ->first()
            ;
		
        }
		$data->password_tmp = $data->password2 = $data->password;
		$this->set(compact("data"));
        
		if (!empty($this->request->data)){   
			//pr($this->request->data);die();
			
			$saveData = $this->Branches->patchEntity($data,$this->request->data);
			 $this->check_error($data);
	 
			//pr($saveData);die();
			$resultDb = $this->Branches->save($saveData);
			if ($resultDb){
				if ($this->request->data['branches_from']>0){
					$this->loadModel('ProductPrices');
					$this->ProductPrices->deleteAll(['system_id' => $resultDb->id]);
					$loadPrices = $this->ProductPrices->find()
						->where(['system_id'=>$this->request->data['branches_from']])
						->select(['checked','product_id','price','price2','price3','price4'])
						->hydrate(false)
						->toArray();
					$savePrices = [];
					foreach($loadPrices AS $k=>$p){
						$p['system_id'] = $resultDb->id;
						$savePrices[] = $p;
					}	
					// pr($savePrices);
					$savePricesEntities = $this->ProductPrices->newEntities($savePrices);
					// pr($savePricesEntities);die();
					
					foreach($savePricesEntities AS $ent){
						//pr($ent);
						$resultDb2 = $this->ProductPrices->save($ent);
						
					}
				
				}
			}
			//die();
			Cache::delete('branchesList');
           	die(json_encode(['result'=>true,'message'=>'Uloženo']));
			  
        }
    }
    public function trash($id){
        $this->loadModel('Branches');
        $conditions = ['id'=>$id];
        $this->Branches->trashAll($conditions);
        $this->redirect('/branches/');


    }
    
  

}
