<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use Cake\I18n\Date;
use App\Component\vIComponent;
use Cake\Utility\Hash;

class OrdersController extends AppController
{

	public function beforeFilter( \Cake\Event\Event $event){
		parent::beforeFilter($event);

		$this->createdTo = null;
		$this->createdFrom = null;

		if(isset($this->request->query['created|toDate|date']) && !empty($this->request->query['created|toDate|date'])){
			$this->createdTo = new Date($this->request->query['created|toDate|date']);
		}

		if(isset($this->request->query['created|fromDate|date']) && !empty($this->request->query['created|fromDate|date'])){
			$this->createdFrom = new Date($this->request->query['created|fromDate|date']);
		}
		
	}
	/**
	 * A view of a list of orders.
	 * 
	 * @return void
	 */
	public function index()
	{
		//$this->request->session()->destroy('historyConditionsOrders');
		$this->checkLogged();
		$this->set('title', 'Tržby');
		$this->loadComponent('ViewIndex');
		$this->loadModel('Branches');
		$this->branches_list = $this->Branches->branchesList();
		$this->set('branches_list', $this->branches_list);
		$this->set('swithBranches', true);
		$this->genSubmenu();

		$cols = [
			'id'			=> ['name' => 'ID'],
			'order_id' 		=> ['name' => 'ID objednávky'],
			'user_name' 	=> ['name' => 'Uživatel'],
			'system_id' 	=> ['name' => 'Pobočka', 'list' => $this->branches_list],
			'created' 		=> ['name' => 'Datum vytvoření'],
			'status' 		=> ['name' => 'Status', 'list' => $this->yes_no],
			//'total_price' 	=> ['name' => 'Celková cena'],
			'total_price'	=>	['name'=>'Celková cena'],
		  
			'pay_casch' 	=> ['name' => 'Hotovost'],
			'pay_card' 		=> ['name' => 'Kartou'],
			'pay_voucher' 	=> ['name' => 'Voucher'],
			'pay_gastro_ticket'=> ['name' => 'Stravenka'],
			'pay_gastro_card'=> ['name' => 'Estravenka'],
			'pay_discount'	=> ['name' => 'Sleva'],
			'pay_employee' 	=> ['name' => 'Zaměstnanecká sleva'],
			'pay_company'	=> ['name' => 'Na firmu'],
			'delivery'		=> ['name' => 'Rozvoz', 'list' => $this->yes_no],
			'storno' 		=> ['name' => 'Storno', 'list' => $this->yes_no],
		];
		$topActions = [];
		$filtrations = [
			'id' 					=> ['name' => 'ID', 'key' => 'id', 'type' => 'text'],
			'created_from' 			=> ['name' => 'Datum od', 'key' => 'created|fromDate', 'type' => 'date'],
			'created_to' 			=> ['name' => 'Datum do', 'key' => 'created|toDate', 'type' => 'date'],
			'expression' 			=> ['name' => 'Cena', 'key' => 'pay_casch', 'type' => 'select', 'list' => ['' => 'Výraz pro celkovou cenu', '=' => 'Rovná se', '>=' => 'Větší nebo rovná se', '<=' => 'Menší nebo rovná se']],
			'total_price_value' 	=> ['name' => 'Hledaná částka', 'key' => 'total_price', 'type' => 'text'],
		];
		$posibility = [
			'edit' 					=> ['name' => 'Editovat', 'url' => '/orders/edit/']
		];
		$conditions = ['OR' => [
			'Orders.pay_casch > 0', 
			'Orders.pay_card > 0', 
			'Orders.pay_gastro_ticket > 0', 
			'Orders.pay_gastro_card > 0'
		]];

		$conditions = $this->ViewIndex->conditions($conditions);

		if (isset($conditions['pay_casch']) && isset($conditions['total_price'])) {
			$conditions['total_price' . ' ' . $conditions['pay_casch'] . ' '] = $conditions['total_price'];
			unset($conditions['pay_casch']);
			unset($conditions['total_price']);
		}

		if (isset($conditions['pay_casch'])) {
			unset($conditions['pay_casch']);
		}

		if (isset($conditions['total_price'])) {
			unset($conditions['total_price']);
		}

		if (isset($conditions['system_id IN']) && empty($conditions['system_id IN'])) {
			unset($conditions['system_id IN']);
		}

		if (isset($conditions['order_date'])) {
			unset($conditions['order_date']);
		}

		$data = $this->Orders->find()
			->where($conditions)
			->order('created DESC');
		$sumQuery = clone $data;

		$data->select([
			'id',
			'order_id',
			'user_name',
			'system_id',
			'created',
			'status',
			'total_price'=>'ROUND( IF(Orders.pay_casch IS NOT NULL, Orders.pay_casch, 0) + IF(Orders.pay_card IS NOT NULL, Orders.pay_card, 0) + IF(Orders.pay_voucher IS NOT NULL, Orders.pay_voucher, 0) + IF(Orders.pay_gastro_ticket IS NOT NULL, Orders.pay_gastro_ticket, 0) + IF(Orders.pay_gastro_card IS NOT NULL, Orders.pay_gastro_card, 0), 2)',
			'pay_casch',
			'pay_card',
			'pay_voucher',
			'pay_discount',
			'pay_gastro_ticket',
			'pay_gastro_card',
			'pay_employee',
			'pay_company',
			'delivery',
			'storno'
		]);

		$total_price_sum = -1;
		/*if (empty($data)) {
			$data = null;
		} else {
			$total_price_sum = 0;
			foreach ($data as $d) {
				if ($d['storno'] == 0) {
					$total_price_sum += $d['total_price'];
				}
			}
		}*/

		//Dotaz na sumaci se provadi pouze pokud je nastaveno rozmezi od - do... a zaroven je mensi nez 6 mesicu
		if($this->createdTo && $this->createdFrom && $this->createdFrom->format('Y-m-d') <= $this->createdTo->format('Y-m-d')){
			$limitFrom = clone $this->createdTo;
			$limitFrom->modify('-190 DAYS');
			if($limitFrom->format('Y-m-d') < $this->createdFrom->format('Y-m-d')){
				$total_price_sum = $sumQuery->select([
					'sum'=>
						'(SUM(IF(pay_casch IS NOT NULL, pay_casch, 0)) +'.
						'SUM(IF(pay_card IS NOT NULL, pay_card, 0)) +'.
						'SUM(IF(pay_gastro_ticket IS NOT NULL, pay_gastro_ticket, 0)) +'.
						'SUM(IF(pay_gastro_card IS NOT NULL, pay_gastro_card, 0)) +'.
						'SUM(IF(pay_voucher IS NOT NULL, pay_voucher, 0)))'
				])->where([
					'OR'=> [
						'storno'=>0,
						'storno IS'=>null
					]
				])->first()->sum;
				//pr('aaaaaaaaaaaa');
				//pr($total_price_sum);die();
			}
		}

		$params = [
			'cols' 				=> $cols,
			'topActions' 		=> $topActions,
			'filtrations' 		=> $filtrations,
			'posibility' 		=> $posibility,
			'data' 				=> $data,
			'noPaginationTop' 	=> true,
			'filtrBranch' 		=> true,
			'rowConditions'		=> ['storno' => 'storno=1', 'delivery' => 'delivery=1', 'employee' => 'pay_employee>=0', 'company' => 'pay_company>=0'],
			'totalPriceSum' 	=> $total_price_sum
		];

		$this->ViewIndex->load($params);
	}

	/**
	 * A view of an order.
	 * 
	 * @param int $id - Optional. Stands for an ID of an order. Default null.
	 * @return void
	 */
	public function edit($id = null)
	{
		$this->checkLogged();
		$this->set('simpleLayout', true);
		$data = $this->Orders->newEntity();

		if ($id != null) {
			$conditions = ['id' => $id];

			$data = $this->Orders->find()
			->select([
				'id','order_id', 'user_name','system_id','created','status','pay_casch','pay_gastro_card','pay_gastro_ticket',
				'pay_card','pay_voucher','pay_discount',	'pay_employee','pay_company','storno','storno_reason',
				'fik','bkp','table_name','table_type_id','delivery', 
				'total_price'=>'ROUND( IF(Orders.pay_casch IS NOT NULL, Orders.pay_casch, 0) + IF(Orders.pay_card IS NOT NULL, Orders.pay_card, 0) + IF(Orders.pay_voucher IS NOT NULL, Orders.pay_voucher, 0) + IF(Orders.pay_gastro_ticket IS NOT NULL, Orders.pay_gastro_ticket, 0) + IF(Orders.pay_gastro_card IS NOT NULL, Orders.pay_gastro_card, 0), 2)',
			])
			->where($conditions)
			->contain(['OrderItems' => function($q) {
				return $q->select([
					'id',
					'name',
					'code',
					'price',
					'price_without_tax',
					'count',
					'order_id',
					'storno',
					'storno_type',
					'soft_storno_count',
					'repair_count',
					'tax_id'
				]);
			}])
			->first();
			/*if(isset($data->order_items)){
				foreach($data->order_items as &$item){
					if(isset($item->storno_type) && $item->storno_type > 0){
						switch($item->storno_type){
							case 2:
								$newItem = clone $item;
								$newItem->count = $newItem->soft_storno_count;
								$data->order_items[] = $newItem;
								break;
							case 3:
								$newItem = clone $item;
								$newItem->count = $newItem->repair_count;
								$data->order_items[] = $newItem;
								break;
						}
						$item->storno_type = null;
					}
				}
			}*/
		}
		//pr($data); die();
		$this->set(compact('data'));

		if (!empty($this->request->data)) {
			$saveData = $this->Orders->patchEntity($data, $this->request->data);
			$this->check_error($data);
			$this->Orders->save($saveData);
			//$this->redirect('/orders/');
			Cache::delete('ordersList');
           	die(json_encode(['result'=>true,'message'=>'Uloženo']));
		}
	}


	/**
	 * A view of a list of eshop orders.
	 * 
	 * @return void
	 */
	public function ordersWeb()
	{
		$this->checkLogged();
		$this->set('title', 'Tržby');
		$this->loadComponent('ViewIndex');
		$this->loadModel('WebOrders');
		$this->loadModel('Branches');
		$this->branches_list = $this->Branches->branchesList();
		$this->set('branches_list', $this->branches_list);
		$this->loadModel('WebBranches');
		$this->webBranchesList = $this->WebBranches->getList();
		$this->set('webBranchesList', $this->webBranchesList);
		$this->genSubmenu();

		$cols = [
			'id'						=> ['name' => 'ID'],
			'shop_doprava_id'			=> ['name' => 'Pobočka', 'list' => $this->webBranchesList],
			'custom_name' 				=> ['name' => 'Zákazník'],
			'custom_email'				=> ['name' => 'E-Mail'],
			'custom_telefon' 			=> ['name' => 'Telefon'],
			'created'					=> ['name' => 'Datum vytvoření'],
			'custom_pickup_datetime'	=> ['name' => 'Datum vyzvednutí'],
			'price_with_tax'			=> ['name' => 'Celková cena'],
			'delivery'					=> ['name' => 'Rozvoz', 'list' => $this->yes_no],
			'status'					=> ['name' => 'Status', 'list' => $this->yes_no],
			'kos' 						=> ['name' => 'Koš', 'list' => $this->yes_no]
		];
		$topActions = [];
		$filtrations = [
			'id' 						=> ['name' => 'ID', 'key' => 'id', 'type' => 'text'],
			'shop_doprava_id'			=> ['name' => 'Pobočka', 'key' => 'shop_doprava_id', 'type' => 'select', 'list' => ['' => 'Pobočka'] + $this->webBranchesList],
			'custom_name'				=> ['name' => 'Zákazník', 'key' => 'name', 'type' => 'like'],
			'email'						=> ['name' => 'E-Mail', 'key' => 'email', 'type' => 'like'],
			'telefon'					=> ['name' => 'Telefon', 'key' => 'telefon', 'type' => 'like'],
			'created_from' 				=> ['name' => 'Datum vytvo. od', 'key' => 'created|fromDate', 'type' => 'date'],
			'created_to' 				=> ['name' => 'Datum vytvo. do', 'key' => 'created|toDate', 'type' => 'date'],
			'order_date'				=> ['name' => 'Datum vyzve.', 'key' => 'order_date', 'type' => 'date'],
			'delivery'					=> ['name' => 'Rozvoz', 'key' => 'delivery', 'type' => 'select', 'list' => ['' => 'Rozvoz'] + $this->yes_no]
		];
		$posibility = [
			'edit' 						=> ['name' => 'Editovat', 'url' => '/orders-web/edit-web/']
		];
		$conditions = [
			//'type_id' => 1
		];
		$conditions = $this->ViewIndex->conditions($conditions);

		$mapper = function($data, $key, $mapReduce) {
			$data->custom_name .= (isset($data->web_client->jmeno)) ? $data->web_client->jmeno . ' ' : '';
			$data->custom_name .= (isset($data->web_client->prijmeni)) ? $data->web_client->prijmeni : '';

			if (isset($data->web_branch->name) && !empty($data->web_branch->name)) {
				$data->custom_branch_name = $data->web_branch->name;
			} else {
				$data->custom_branch_name = 'neuvedeno';
			}
			
			if (isset($data->web_client->email)) {
				$data->custom_email = $data->web_client->email;
			}
			
			if (isset($data->web_client->telefon)) {
				$data->custom_telefon = $data->web_client->telefon;
			}

			if (isset($data->web_client->telefon)) {
				$matches = [];

				if (preg_match('/^(\d{3})(\d{3})(\d{3})$/', $data->web_client->telefon, $matches))
				{
					$data->custom_telefon = $matches[1] . ' ' .$matches[2] . ' ' . $matches[3];
				}
			}
			
			$data->custom_pickup_datetime = $data->order_date . ' ' . $data->pickup_time;

			$mapReduce->emit($data);
		};
		
		if (isset($conditions['telefon LIKE '])) {
			$conditions['replace(telefon, \' \', \'\') LIKE '] = str_replace(' ', '', $conditions['telefon LIKE ']);
			unset($conditions['telefon LIKE ']);
		}

		if (isset($conditions['system_id IN']) && empty($conditions['system_id IN'])) {
			unset($conditions['system_id IN']);
		}

		$this->loadModel('WebClients');
		
		$data = $this->WebOrders->find()
								->select([
									'id', 'created', 'status', 'kos', 'price_with_tax',
									'order_date', 'pickup_time', 'shop_client_id', 'shop_doprava_id', 'delivery',
									'type_id'
								])
								->contain([
										'WebClients' => function($q) {
											return $q->select([
												'id',
												'jmeno',
												'prijmeni',
												'email',
												'telefon'
											]);
										}]
								)
								->where($conditions)
								->mapReduce($mapper)
								->order('WebOrders.created DESC');

		if (empty($data)) {
			$data = null;
		} else {
			$total_price_sum = 0;
			foreach ($data as $d) {
				if ($d['kos'] == 0 && $d['status'] == 1) {
					$total_price_sum += $d['price_with_tax'];
				}
			}
		}

		$params = [
			'cols' 				=> $cols,
			'topActions' 		=> $topActions,
			'filtrations' 		=> $filtrations,
			'posibility' 		=> $posibility,
			'data' 				=> $data,
			'noPaginationTop' 	=> true,
			'rowConditions'		=> ['delivery' => 'delivery=1', 'storno' => 'kos=1', 'storno' => 'status=0'],
			'totalPriceSum' 	=> $total_price_sum
		];

		$this->ViewIndex->load($params);
	}

	/**
	 * A view of an eshop order.
	 * 
	 * @param int $id - Optional. Stands for an ID of an eshop order. Default null.
	 * @return void
	 */
	public function editWeb($id = null)
	{
		$this->checkLogged();
		$this->set('simpleLayout', true);
		$this->loadModel('WebBranches');
		$this->webBranchesList = $this->WebBranches->getList();
		$this->set('webBranchesList', $this->webBranchesList);
		$this->loadModel('WebClients');
		$this->loadModel('WebOrders');
		$this->loadModel('WebOrderItems');
		$data = $this->WebOrders->newEntity();

		if ($id != null) {
			$conditions = ['WebOrders.id' => $id];

			$data = $this->WebOrders->find()
			->select([
				'id',
				'created',
				'status',
				'kos',
				'price_with_tax',
				'order_date',
				'pickup_time',
				'shop_client_id',
				'shop_doprava_id',
				'delivery',
				'history'
			])
			->where($conditions)
			->contain(['WebOrderItems' => function($q) {
				return $q->select([
					'id',
					'name',
					'price_with_tax',
					'ks',
					'shop_order_id'
				]);
			}, 'WebClients' => function($q) {
				return $q->select([
					'id',
					'jmeno',
					'prijmeni',
					'ulice',
					'mesto',
					'psc',
					'email',
					'telefon'
				]);
			}])
			->first();
		}
		
		$this->set(compact('data'));

		if (!empty($this->request->data)) {

			$this->request->data['order_date'] = new Date($this->request->data['order_date']);
			// pr($this->request->data['order_date']); die();
			$history = '';

			if ($data->web_client->jmeno != $this->request->data['WebClients_jmeno']) {
				$history .= 'Jméno: ' . $data->web_client->jmeno . ' => ' . $this->request->data['WebClients_jmeno'] . '&lt;br&gt;';
			}

			if ($data->web_client->prijmeni != $this->request->data['WebClients_prijmeni']) {
				$history .= 'Příjmení: ' . $data->web_client->prijmeni . ' => ' . $this->request->data['WebClients_prijmeni'] . '&lt;br&gt;';
			}

			if ($data->web_client->email != $this->request->data['WebClients_email']) {
				$history .= 'E-Mail: ' . $data->web_client->email . ' => ' . $this->request->data['WebClients_email'] . '&lt;br&gt;';
			}

			if ($data->web_client->telefon != $this->request->data['WebClients_telefon']) {
				$history .= 'Telefon: ' . $data->web_client->telefon . ' => ' . $this->request->data['WebClients_telefon'] . '&lt;br&gt;';
			}

			if ($data->web_client->ulice != $this->request->data['WebClients_ulice']) {
				$history .= 'Ulice: ' . $data->web_client->ulice . ' => ' . $this->request->data['WebClients_ulice'] . '&lt;br&gt;';
			}

			if ($data->web_client->mesto != $this->request->data['WebClients_mesto']) {
				$history .= 'Město: ' . $data->web_client->mesto . ' => ' . $this->request->data['WebClients_mesto'] . '&lt;br&gt;';
			}

			if ($data->web_client->psc != $this->request->data['WebClients_psc']) {
				$history .= 'PSČ: ' . $data->web_client->psc . ' => ' . $this->request->data['WebClients_psc'] . '&lt;br&gt;';
			}

			if ($data->order_date != $this->request->data['order_date']) {
				$history .= 'Datum vyzvednutí: ' . $data->order_date . ' => ' . $this->request->data['order_date'] . '&lt;br&gt;';
			}

			if ($data->pickup_time != $this->request->data['pickup_time']) {
				$history .= 'Čas vyzvednutí: ' . $data->pickup_time . ' => ' . $this->request->data['pickup_time'] . '&lt;br&gt;';
			}

			if ($data->shop_doprava_id != $this->request->data['shop_doprava_id']) {
				$history .= 'Pobočka: ' . $this->webBranchesList[$data->shop_doprava_id] . ' => ' . $this->webBranchesList[$this->request->data['shop_doprava_id']] . '&lt;br&gt;';
			}

			if ($data->delivery != $this->request->data['delivery']) {
				$history .= 'Rozvoz: ' . $this->yes_no[$data->delivery] . ' => ' . $this->yes_no[$this->request->data['delivery']] . '&lt;br&gt;';
			}

			if ($data->price_with_tax != $this->request->data['price_with_tax']) {
				$history .= 'Cena: ' . $data->price_with_tax . ' => ' . $this->request->data['price_with_tax'] . '&lt;br&gt;';
			}

			if ($data->status != $this->request->data['status']) {
				$history .= 'Status: ' . $this->yes_no[$data->status] . ' => ' . $this->yes_no[$this->request->data['status']] . '&lt;br&gt;';
			}

			if ($data->kos != $this->request->data['kos']) {
				if ($this->request->data['kos'] == 1) {
					$history .= 'Koš: ' . $this->yes_no[0] . ' => ' . $this->yes_no[$this->request->data['kos']] . '&lt;br&gt;';
				} else {
					$history .= 'Koš: ' . $this->yes_no[1] . ' => ' . $this->yes_no[$this->request->data['kos']] . '&lt;br&gt;';
				}
			}

			if ($history != '') {
				$history = date('d.m. Y') . ' v ' . date('H:i:s') . ' bylo změněno:&lt;br&gt;' . $history . '&lt;br&gt;';
				$this->request->data['history'] = $history . $this->request->data['history'];
			}

			$saveData = $this->WebOrders->patchEntity($data, $this->request->data);
			$this->check_error($data);
			$this->WebOrders->save($saveData);
			
			$dataClient = [
				'id' 		=> $this->request->data['WebClients_id'],
				'jmeno'		=> $this->request->data['WebClients_jmeno'],
				'prijmeni'	=> $this->request->data['WebClients_prijmeni'],
				'email'		=> $this->request->data['WebClients_email'],
				'ulice'		=> $this->request->data['WebClients_ulice'],
				'psc'		=> $this->request->data['WebClients_psc'],
				'telefon'	=> $this->request->data['WebClients_telefon'],
				'mesto'		=> $this->request->data['WebClients_mesto']
			];
			$saveDataClient = $this->WebClients->newEntity($dataClient);
			$this->WebClients->save($saveDataClient);
			
           	die(json_encode(['result' => true, 'message' => 'Uloženo']));
		}
	}

	public function poklWebOrderSave()
	{
		

		$result = ['result' => false, 'message' => null];

		if (isset($_POST['save_data'])) {
			$this->loadModel('WebOrders');
			$orderData = json_decode($_POST['save_data'], true);

			if (isset($orderData['id'], $orderData['system_id'])) {
				$localID = $orderData['id'];
				
				$rowExist = $this->WebOrders->find()
								->where([
									'WebOrders.local_id' => $localID,
									'WebOrders.system_id' => $orderData['system_id']
								])
								->first();

				unset($orderData['id']);

				$patched = $this->WebOrders->patchEntity($rowExist, $orderData, ['associated' => false]);

				$resultDb = $this->WebOrders->save($patched);
				
				$sended = new Time();
				$result = [];
				$result['id'] = $resultDb->id;
				$result['sended'] = $sended->format('Y-m-d H:i:s');
				$result['result'] = true;
				$result['message'] = null;

				die(json_encode($result));
			}else{
				$result['message'] = 'Nezaslano Local ID nebo system_id';
				die(json_encode($result));
			}

			/*$orderData['order_date'] = new Date($orderData['order_date']);

			$entity = $this->WebOrders->newEntity($orderData, [
				'associated' => [
					'WebOrderItems' => ['validate' => false],
					'WebClients' => ['validate' => false]
				]
			]);

			$resultDb = $this->WebOrders->save($entity);
			
			$sended = new Time();
			$result = [];
			$result['id'] = $resultDb->local_id;
			$result['sended'] = $sended->format('Y-m-d H:i:s');
			$result['result'] = true;
			$result['message'] = null;*/
		}
		
		die(json_encode($result));
	}
    
	/**
	* save nova dochazka
	*/
	public function save(){
		foreach($_POST['data'] AS $k=>$d){
			//pr($_POST['data'][$k]);die('a');
			$_POST['data'][$k]['created'] = new Time($_POST['data'][$k]['created']);
			$_POST['data'][$k]['modified'] = new Time($_POST['data'][$k]['modified']);
		}
		$this->product_groups = $_POST['product_groups'];
		unset($_POST['product_groups']);
		$this->saveProductGroups();
		$save_entities = $this->Orders->newEntities($_POST['data'],['associated' => ['OrderItems']]);
	  
		foreach($save_entities AS $save_entity){
            if (!$resultDb = $this->Orders->save($save_entity)){
				die(json_encode(['result'=>false,'message'=>'Chyba uložení objednávky']));	
			}
		}
        die(json_encode(['result'=>true,'message'=>'Objednávky uloženy do cloudu']));	
	}
	
	private function saveProductGroups(){
		$this->loadModel('ProductGroups');
		$save = [];
		foreach($this->product_groups AS $k=>$name){
			$save[] = [
				'id'=>$k,
				'name'=>$name,
			];
		}
		$save_entities = $this->ProductGroups->newEntities($save,['associated' => ['OrderItems']]);
        //pr($save_entities); die();
        foreach($save_entities AS $save_entity){
            if (!$resultDb = $this->ProductGroups->save($save_entity)){
				die(json_encode(['result'=>false,'message'=>'Chyba uložení objednávky']));	
			}
		}
        
	}
	
	
	private function saveAttendaces($saveData){
		$save_entity = $this->Orders->newEntity($saveData);
		$this->Orders->save($save_entity);
	}
	
	
	public function test(){
		$this->autoRender = false;
		$ch = curl_init();
		$post = [
			'type'=>'exit',
			//'type'=>'enter',
			'name'=>'Kuba',
			'code'=>'111',
			'system_id'=>1,
		];
		
		curl_setopt($ch, CURLOPT_URL, 'http://chacharint.fastest.cz/api/attendances/save/');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		curl_close ($ch);
		//pr('curl result');
		//pr($result);
		
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro uložení docházky, zkontrolujte pripojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
		//	pr($result);
			$result = json_decode($result,true);
		}
		pr($result);
		
		
	}
	
	/**
	 * API Funkce ktera slouzi k odesilani objednavek na jednotlive pokladny
	 * Pocita se s tim ze pokladna si bude kazde 3 minuty stahovat aktualni seznam objednavek z posledniho dne...
	 */
	public function getOrdersFromWeb($system_id , $lastId = null){
		$this->loadModel('WebOrders');
		$orders = $this->WebOrders->find()->contain(['WebOrderItems','WebClients','WebPlatbas'])->where([
			'WebOrders.system_id' => $system_id,  // nutne povolit az bude dopracovan atribut system_id v objednavkach na eshopu 
		]);

		if(!$lastId){
			$lastId = 0;
		}
		
		//Pokud je zaslano posledni ID, tak se berou vsechny objednavky od tohoto ID, jinak se berou posledni objednavky z 24hodin
		if($lastId !== null){
			$orders->where(['WebOrders.id >'=> $lastId]);
		}else{
			$date = new Time();
			$orders->where(['WebOrders.created >'=> $date->modify('-1 DAY')->format('Y-m-d H:i:s')]);
		}
		
		$data = [];
		
		foreach($orders as $order){
			$type_id = (($order && $order->order_date->format('Y-m-d') == date('Y-m-d')) ? 2 : 1); //Nastaveni zda se jedna o Live Order nebo na jmeno
			$item = [
				'web_id' => $order->id,
				'price' => $order->price,
				'type_id' => $type_id,
				'system_id' => $system_id,
				'delivery' => $order->delivery,	//Rozvoz nebo na pobocce
				'price_with_tax' => $order->price_with_tax,
				'doprava_price' => $order->doprava_price,
				'doprava_price_tax' => $order->doprava_price_tax,
				'platba_price' => $order->platba_price,
				'platba_price_tax' => $order->platba_price_tax,
				'doprava_zdarma' => $order->doprava_zdarma,
				'shop_doprava_id' => $order->shop_doprava_id,
				'shop_platba_id' => $order->shop_platba_id,
				'platba_name' => (isset($order->web_platba) ? $order->web_platba->name : null),
				'paid' => $order->pay, 
				'poznamka' => $order->poznamka,
				'poznamka_interni' => $order->poznamka_interni,
				'web_created' => $order->created,
				'web_client_id'=> $order->shop_client_id,
				'client_name'=> (isset($order->web_client) ? (isset($order->web_client->jmeno) ? $order->web_client->jmeno . ' ' : '') . (isset($order->web_client->prijmeni) ? $order->web_client->prijmeni : '') : ''),
				'client_firstname'=> (isset($order->web_client->jmeno) ? $order->web_client->jmeno : ''),
				'client_lastname'=>  (isset($order->web_client->prijmeni) ? $order->web_client->prijmeni : ''),
				'client_street'=>  (isset($order->web_client->ulice) ? $order->web_client->ulice : ''),
				'client_psc'=>  (isset($order->web_client->psc) ? $order->web_client->psc : ''),
				'client_city'=>  (isset($order->web_client->mesto) ? $order->web_client->mesto : ''),
				'client_address'=> (isset($order->web_client) ? ($order->web_client->mesto ? $order->web_client->mesto . ', ' : '') . ($order->web_client->ulice ? $order->web_client->ulice . ', ' : '') . ($order->web_client->psc ? $order->web_client->psc : '') : ''),
				'client_email'=> (isset($order->web_client) && isset($order->web_client->email) ? $order->web_client->email : ''),
				'client_phone'=> (isset($order->web_client) && isset($order->web_client->telefon) ? $order->web_client->telefon : ''),
				'order_date'=> $order->order_date && $order->order_date != '0000-00-00' ? $order->order_date : null,
				'pickup_time'=> $order->pickup_time && $order->pickup_time != '' ? $order->pickup_time : null,
				'gifted_name'=> $order->gifted_name,
				'gifted_phone'=> $order->gifted_phone,
				'web_order_items' => []
			];
			foreach($order->web_order_items as $oItem){
				$oItem->web_id = $oItem->id;
				unset($oItem->id);
				$item['web_order_items'][] = $oItem->toArray(); 
			}
			$data[] = $item;
		}
		
		if (!empty($data)) {
			die(json_encode(['result'=> true, 'data'=>$data]));
		} else {
			die(json_encode(['result' => false]));
		}
	}


	/**
     * CRON function - download new orders from eshop to intranet db
     * @return mixed
     */
    public function syncRemoteWebOrders()
    {
		$eshopId = 1; //Hlavni eshop
        function microtime_float(){
            list($usec, $sec) = explode(" ", microtime());
            return ((float)$usec + (float)$sec);
        }
		$t = microtime_float();
		
		$this->loadModel('RemoteWebOrders');
		$this->loadModel('RemoteWebOrderItems');
		$this->loadModel('WebOrders'); 
		$this->loadModel('WebClients'); 

        $lastId = $this->WebOrders->find()->select(['lastId'=>'MAX(web_id)'])->where(['eshop_id'=> $eshopId])->first();

		$query = $this->RemoteWebOrders->find()
										//->where(['RemoteWebOrders.shop_client_id'=> 141])
										->where(['RemoteWebOrders.id >'=> ($lastId && $lastId->lastId ? $lastId->lastId : 0)])
										->contain(['RemoteWebOrderItems', 'RemoteWebClients'])
										->hydrate(false)
										->limit(30);
		$data = $query->toArray();

		if(!empty($data)){

			foreach ($data AS $dataOrderKey => $dataOrder) {

				// removes unpaied gopay orders from array
				if ($data[$dataOrderKey]['shop_platba_id'] == 3 && $data[$dataOrderKey]['pay'] == 0) {
					unset($data[$dataOrderKey]);
				}
			}

			$ids = Hash::extract($data, '{n}.web_id');
			$client_ids = Hash::extract($data, '{n}.shop_client_id');
			$existIds = [];
			$existClientIds = [];

			if (!empty($ids)) {
				$existIds = $this->WebOrders->find('list', ['keyField'=>'web_id', 'valueField'=>'id'])->where(['web_id IN'=>$ids, 'eshop_id'=> $eshopId])->toArray();
			}
			if (!empty($client_ids)) {
				$existClientIds = $this->WebClients->find('list', ['keyField'=>'web_id', 'valueField'=>'id'])->where(['web_id IN'=>$client_ids, 'eshop_id'=> $eshopId])->toArray();
			}

			$ok = $err = $exist = 0;
			$founded = Count($data);
			$errors = [];

				foreach($data as $remoteOrder){ 
					//Vkladame pouze ty objednavky ktere zatim nejsou vlozeny
					if(!isset($existIds[$remoteOrder['id']])){
						$existingClient = false;
						//Transformace dat pred ulozenim
						$remoteOrder['doprava_zdarma'] = (!isset($remoteOrder['doprava_zdarma']) || $remoteOrder['doprava_zdarma'] === null ? 0 : $remoteOrder['doprava_zdarma']);
								
						$remoteOrder['eshop_id'] = $eshopId;
						$remoteOrder['web_id'] = $remoteOrder['id'];
						$remoteOrder['id'] = null;
						unset($remoteOrder['id']);

						//Byly asi pridany nove atributy, ktere nemuzou byt NULL... proto tady nastavuju aspon takto... melo by se vymyslet lepe...
						if(!isset($remoteOrder['paymentSessionId']) || !$remoteOrder['paymentSessionId']){
							$remoteOrder['paymentSessionId'] = '';
						}
						if(!isset($remoteOrder['vs']) || !$remoteOrder['vs']){
							$remoteOrder['vs'] = '';
						}
						if(!isset($remoteOrder['vs2']) || !$remoteOrder['vs2']){
							$remoteOrder['vs2'] = '';
						}
						if(!isset($remoteOrder['balik_code']) || !$remoteOrder['balik_code']){
							$remoteOrder['balik_code'] = '';
						}
						if(!isset($remoteOrder['odeslane_email']) || !$remoteOrder['odeslane_email']){
							$remoteOrder['odeslane_email'] = '';
						}
						if(!isset($remoteOrder['odeslane_sms']) || !$remoteOrder['odeslane_sms']){
							$remoteOrder['odeslane_sms'] = '';
						}
						if(!isset($remoteOrder['historie']) || !$remoteOrder['historie']){
							$remoteOrder['historie'] = '';
						}
						if(isset($remoteOrder['remote_web_client'])){
							$remoteOrder['remote_web_client']['eshop_id'] = $eshopId;
							if(!isset($existClientIds[$remoteOrder['remote_web_client']['id']])){
								$remoteOrder['remote_web_client']['web_id'] = $remoteOrder['remote_web_client']['id'];
								$remoteOrder['remote_web_client']['id'] = null;
								$remoteOrder['shop_client_id'] = null;
							}else{
								//Pokud jiz klient existuje tak mu priradime existujici ID
								$remoteOrder['remote_web_client']['web_id'] = $remoteOrder['remote_web_client']['id'];
								$remoteOrder['remote_web_client']['id'] = $existClientIds[$remoteOrder['remote_web_client']['id']];
								$remoteOrder['shop_client_id'] = $remoteOrder['remote_web_client']['id'];
								$existingClient = true;
							}
							$remoteOrder['web_client'] = $remoteOrder['remote_web_client'];
							if(!isset($remoteOrder['web_client']['ulice']) || !$remoteOrder['web_client']['ulice']){
								$remoteOrder['web_client']['ulice'] = '-';
							}
							if(!isset($remoteOrder['web_client']['mesto']) || !$remoteOrder['web_client']['mesto']){
								$remoteOrder['web_client']['mesto'] = '-';
							}
							if(!isset($remoteOrder['web_client']['psc']) || !$remoteOrder['web_client']['psc']){
								$remoteOrder['web_client']['psc'] = '-';
							}
							if(!isset($remoteOrder['web_client']['telefon']) || !$remoteOrder['web_client']['telefon']){
								$remoteOrder['web_client']['telefon'] = '';
							}
							if(!isset($remoteOrder['web_client']['email']) || !$remoteOrder['web_client']['email']){
								$remoteOrder['web_client']['email'] = '';
							}
							unset($remoteOrder['remote_web_client']);
						}
						if(isset($remoteOrder['remote_web_order_items'])){
							foreach($remoteOrder['remote_web_order_items'] as &$item){
								$item['eshop_id'] = $eshopId;
								$item['web_id'] = $item['id'];
								$item['id'] = null;
								$item['shop_order_id'] = null;
								/*
									Cele dorty maji v Kodu nakonci hvezdicku a jinak maji stejny kod jako 1/12
									Pokud ma dort nastaveno variant = 1 pak je cely pokud variant = 2 pak je 1/12
								*/			
								if(isset($item['ean']) && $item['ean'] != ''){	
									$item['code'] = $item['ean'];				
									if(isset($item['variant']) && $item['variant'] == 1){
										$item['code'] =  $item['code'] . '*';
									}
								}
							}
							$remoteOrder['web_order_items'] = $remoteOrder['remote_web_order_items'];
							unset($remoteOrder['remote_web_order_items']);
						}
						$remoteOrder['type_id'] = ($remoteOrder['order_date'] == date('Y-m-d') ? 2 : 1); //Nastaveni zda se jedna o Live Order nebo na jmeno
				
						$order = $this->WebOrders->newEntity($remoteOrder);
						$order->synchronized = new Time();

						if($existingClient){
							unset($order->web_client);
							//TODO update samostatne pouze klienta
							//$order->web_client->isNew(false);
						}
			
						try{
							if($order = $this->WebOrders->save($order)){
								if(isset($order->web_client)){
									$existClientIds[$order->web_client->web_id] = $order->web_client->id;
								}
								//$printOrders[] = $this->orderDataForPrint($order);
								$ok++;
							}
						}catch(\PDOException $e){
							$errors[$remoteOrder['web_id']] = $e->getMessage();
							$err++;
						}
					}else{
						$exist++;
					}
				}
			}
	   
		//LOG TO FILE
        $t = microtime_float() - $t;
		$msg = date('Y-m-d H:i:s') . ' - SHOP: '. $eshopId .' | T: '. round($t, 2) .' | FOUNDED: ' . $founded . ' | OK: '. $ok .' | ERR: ' .$err . ' ' . (!empty($errors) ? PHP_EOL . "\t" . "\t" .' ERRORS: ' . json_encode($errors) : '');
		@file_put_contents( ROOT . '/logs/order_sync_eshop.log', $msg . PHP_EOL, FILE_APPEND);
		die($msg);
    } 
}