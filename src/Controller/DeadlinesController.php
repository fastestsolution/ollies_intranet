<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class DeadlinesController extends AppController {
	
	/**
	 * A view of a list of deadlines.
	 * 
	 * @return void
	 */
	public function index()
	{
		$this->set("title", "Správa uzávěrek");
		$this->checkLogged();
		$this->loadComponent('ViewIndex');
		$this->loadModel("Branches");
		$this->branches_list = $this->Branches->branchesList();
		$this->set("branches_list", $this->branches_list);
		$this->loadModel("Orders");
		$this->set("swithBranches", true);

		$view = (object) [
			"show"					=> false,
			"url"					=> "/deadlines/edit/"
		];
		
		$cols = [
			"id"					=> ["name" => "ID"],
			"branch_id"				=> ["name" => "Pobočka", "list" => $this->branches_list],
			"created"				=> ["name" => "Vytvořeno", "style" => "text-align: center;"],
			"custom_total_price"	=> ["name" => "Celkem", "style" => "text-align: right;"],
			"custom_storno"			=> ["name" => "Storna", "style" => "text-align: right;"],
			"user_id"				=> ["name" => "ID uživatele", "style" => "text-align: center;"],
			"order_id_from"			=> ["name" => "Účtenky od", "style" => "text-align: center;"],
			"order_id_to"			=> ["name" => "Účtenky do", "style" => "text-align: center;"]
		];
		$topActions = [];
		$filtrations = [
			"id" 					=> ["name" => "ID", "key" => "id", "type" => "text"],
			"created_from" 			=> ["name" => "Datum od", "key" => "created|fromDate", "type" => "date"],
			"created_to" 			=> ["name" => "Datum do", "key" => "created|toDate", "type" => "date"],
			"user_id"				=> ["name" => "ID uživatele", "key" => "user_id", "type" => "text"]
		];
		$posibility = [
			"edit"					=> ["name" => "Zobrazit", "url" => "/deadlines/edit/"]
		];
		/* Pro zobrazení uzávěrek pouze lokální pokladny staticky odkomentuj následující řádek. */
		$conditions = [/*"system_id" => $this->loggedUser->system_id*/];
		$conditions = $this->ViewIndex->conditions($conditions);
		$mapper = function($data, $key, $mapReduce) {
			$json = json_decode($data["data"], true);

			$data->custom_total_price = number_format(json_encode($json["sum_total_complete"]["total_price"]), 2, ".", " ") . " Kč";
			$data->custom_storno = number_format(json_encode($json["sum_total_storno"]["total_price"]), 2, ".", " ") . " Kč";

			$mapReduce->emit($data);
		};

		if (isset($conditions["system_id IN"])) {
			$conditions["branch_id IN"] = $conditions["system_id IN"];
			unset($conditions["system_id IN"]);
		}

		if (isset($conditions["branch_id IN"]) && empty($conditions["branch_id IN"])) {
			unset($conditions["branch_id IN"]);
		}

		if (isset($conditions["date(created) >= "]) || isset($conditions["date(created) <= "])) {
			$view->show = true;
		}
		
		$data = $this->Deadlines->find()
								->select([
									"id",
									"branch_id",
									"order_id_from",
									"order_id_to",
									"created",
									"user_id",
									"data"
								])
								->where($conditions)
								->mapReduce($mapper)
								->limit(100)
								->order("id DESC");
		
		if (empty($data)) {
			$data = null;
		} else {
			$ids = "";

			foreach ($data as $d) {
				$ids .= $d->id . ",";
			}

			$ids = substr($ids, 0, -1);
			
			$view->url .= $ids . "/";
		}

		$params = [
			"cols" 				=> $cols,
			"topActions" 		=> $topActions,
			"filtrations" 		=> $filtrations,
			"posibility" 		=> $posibility,
			"data" 				=> $data,
			"noPaginationTop" 	=> true,
			"filtrBranch" 		=> true,
			"view"				=> $view
		];
		
		$this->ViewIndex->load($params);
	}

	/**
	 * A view of deadline.
	 * 
	 * Uses edit.ctp template and leaves you to view specific deadline.
	 * 
	 * @param int|string $id
	 * @return void
	 */
	public function edit($id = false)
	{
		$this->set("simpleLayout", true);

		if ($id != false) {
			$ids = explode(",", $id);
		} else if (isset($this->request->data["ids"])) {
			$ids = explode(",", str_replace("/deadlines/edit/", "", $this->request->data["ids"]));
		} else {
			$ids = [0];
		}
		
		$conditions = [
			"id IN" => $ids
		];

		$deadlines = $this->Deadlines->find()
									->select([
										"id",
										"pokladna_id",
										"branch_id",
										"data"
									])
									->where($conditions)
									->order("id DESC")
									->all();
									
		$json = (object) [];
		
		$pokladna_ids = [];
		$branch_ids = [];
		$iterator = 0;
		foreach ($deadlines as $dead) {
			$json->{$dead->id} = (object) [
				"system_id" 	=> $dead->branch_id,
				"json" 			=> json_decode($dead->data),
			];

			$pokladna_ids[$iterator] = $dead->pokladna_id;
			$branch_ids[$iterator] = $dead->branch_id;
			$iterator++;
		}

		$json->url = $this->encode_long_url([
			"id"			=> $ids,
			"pokladna_id"	=> $pokladna_ids,
			"branch_id"		=> $branch_ids,
			"view_all"		=> false
		]);

		$json->url_privileged = $this->encode_long_url([
			"id"			=> $ids,
			"pokladna_id"	=> $pokladna_ids,
			"branch_id"		=> $branch_ids,
			"view_all"		=> true
		]);

		$json->ids = $ids;

		if (isset($this->request->query["debug"])) {
			$this->set("debug", true);
		}
	
		$this->set(compact("json"));
	}

	/**
	 * Sends a deadline to be printed.
	 * 
	 * @return void
	 */
	public function send()
	{
		if (isset($this->request->data["url"])) {
			$this->request->session()->write("url", $this->request->data["url"]);
		}
		
		$this->redirect([
			"controller" => "Statistics",
			"action" => "show"
		]);
	}
	
	/**
	 * Sends a deadline to be printed.
	 * 
	 * @param int $id
	 * @return void
	 */
	public function show($id)
	{
		$ids = explode(",", $id);

		$conditions = [
			"id IN" => $ids
		];

		$find = $this->Deadlines->find()
		->select([
			"id",
			"pokladna_id",
			"system_id"
		])
		->where($conditions)
		->order("id DESC")
		->all();

		$hash = [
			"ids"				=> $ids,
			"pokladna_id"		=> $find->pokladna_id,
			"system_id"			=> $find->system_id
		];
		
		$hash_encode = $this->encode_long_url($hash);
		$this->redirect("/statistics/show/" . $hash_encode);
	}
	
	/**
	* save nova dochazka
	*/
	public function save(){
		//sleep(6);
		//print_r($_POST);die();
		foreach($_POST AS $k=>$d){
			$_POST[$k]['created'] = new Time($_POST[$k]['created']);
			$_POST[$k]['modified'] = new Time($_POST[$k]['modified']);
			
		}
		//$save_entity = $this->Orders->newEntity($_POST,['associated' => ["Clients"=>['validate' => 'default','accessibleFields'=>['id' => true]], "Clients.ClientAddresses","OrderItems"]]);
		$save_entities = $this->Deadlines->newEntities($_POST);
        //pr($save_entities); die();
        foreach($save_entities AS $save_entity){
            //pr($save_entity);die();
			//unset($_POST['modified']);
		
			if (!$resultDb = $this->Deadlines->save($save_entity)){
				die(json_encode(['result'=>false,'message'=>'Chyba uložení uzávěrky']));	
			} else {
				$this->sendEmail($resultDb);
			}
		}
        die(json_encode(['result'=>true,'message'=>'Uzávěrky uloženy do cloudu']));	
		    
        
		
		
	}
	
	public function sendEmail($saveData=null){
		
		$this->loadModel("Branches");
		$this->branches_list_all = $this->Branches->branchesListAll();
		// pr($this->branches_list);
		// pr($saveData);
		if ($saveData){
			$hash = [
				'id'=>$saveData->id,
				'pokladna_id'=>$saveData->pokladna_id,
				'branch_id'=>$saveData->system_id,
			];
		} else {
			$hash = [
				'id'=>1,
				'pokladna_id'=>6,
				'branch_id'=>1000
			];

		}
		
		$this->autoRender = false;
		$hash_encode = $this->encode_long_url($hash);
		
		//pr($_SERVER['SERVER_NAME']);die();
		
		$this->loadComponent('Email');
		if (isset($this->branches_list_all[$hash['branch_id']])){
			$data_email = 'Uzávěrka z pokladny: '.$this->branches_list_all[$hash['branch_id']]['name'].' ('.date('d.m.y H:i:s').")<br />";
			$link = 'http://'.$_SERVER['SERVER_NAME'].'/statistics/show/'.$hash_encode;
			$data_email .= '<br />Aktuální Uzávěrku naleznete na adrese: <a href="'.$link.'">'.$link.'</a><br />';
			// pr($link);
			$opt = [
				'to'=>[
					$this->branches_list_all[$hash['branch_id']]['email'],
					'test@fastest.cz',
					'placek@fastest.cz',
					'heyduk@fastest.cz',
					'tereza.gondkova@ollies.cz',
					'olga.gondkova@ollies.cz',
					'jakub.gondek@ollies.cz',
					'ivo.gondek@ollies.cz'
				],
				'subject'=>'Uzávěrka z pokladny: '.$this->branches_list_all[$hash['branch_id']]['name'].' ('.date('d.m.y H:i:s').')',
				'data'=>$data_email,
			];

			// pr($this->branches_list_al);
			// if (!strpos($_SERVER['HTTP_HOST'],'localhost'))
			// pr($_SERVER);die();
			// pr($opt);die();
			//if (!empty($this->system_list[$hash['system_id']]['email']))
			// pr($this->branches_list_all[$hash['system_id']]['email']);die();

			if (!empty($this->branches_list_all[$hash['branch_id']]['email'])){
				if (!strpos($_SERVER['HTTP_HOST'],'localhost')){
					$this->Email->send($opt);
				} else {
					
				}
			
			}
		}
		
	}
	
	
	
}