<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;

use Cake\Cache\Cache;

class UsersController extends AppController{
    
	public function logout(){
		$session = $this->request->session();
		$session->delete('loggedUser');
		$this->redirect('/');
		
	}

	public function login(){
		if (!isset($this->loggedUser)) {
			if (!empty($this->request->data)){
				
				//pr(md5($this->request->data['password']));
				$find = $this->Users->find()
				->where([
					'username'=>$this->request->data['username'],
					'password'=>md5($this->request->data['password']),
				])
				->first();
				if (!$find){
					$result = ['result'=>false,'message'=>'Uživatel nenalezen'];
				} else {
					$result = ['result'=>true,'message'=>'Uživatel bude přihlášen'];
					$session = $this->request->session();
					$session->write('loggedUser',$find);
				}
				//pr($this->request->data);die();
				die(json_encode($result));
				//$this->redirect($_SESSION["requestedURL"]);
				//die(json_encode($result));
			} else {
				$this->set('simpleLayout',true);
			}
		} else {
			$this->redirect("/");
		}
	}
	
    public function index(){
		$this->checkLogged();
		$this->set('title','Správa uživatelů');
		$this->loadComponent('ViewIndex');
		
		$this->getBranchesList();
		
		$cols = [
			'id'=>['name'=>'ID'],
			'branch_id'=>['name'=>'Provoz','list'=>$this->branches_list],
			'name'=>['name'=>'Jméno, Příjmení'],
			'username'=>['name'=>'Uživatelské jméno'],
			'phone'=>['name'=>'Telefon'],
			'email'=>['name'=>'Email'],
			'url'=>['name'=>'URL'],
			'group_id'=>['name'=>'Skupina','list'=>$this->users_group_list],
		];
		$topActions = [
			'edit'=>['name'=>'Nová položka','url'=>'/users/edit/'],
		];
		$filtrations = [
			'name'=>['name'=>'Jméno, příjmení','key'=>'name','type'=>'like'],
			'username'=>['name'=>'Už. jméno','key'=>'username','type'=>'like'],
			'branch_id'=>['name'=>'Provoz','key'=>'branch_id','type'=>'select','list'=>[''=>'Vyberte provoz']+$this->branches_list],
			'group_id'=>['name'=>'Skupina','key'=>'group_id','type'=>'select','list'=>[''=>'Skupina']+$this->users_group_list],
		];
		$posibility = [
			'edit'=>['name'=>'Editovat','url'=>'/users/edit/'],
			'trash'=>['name'=>'Smazat','url'=>'/users/trash/'],
		];
		
		$conditions = [];
		$conditions = $this->ViewIndex->conditions($conditions);
		//pr($conditions);
		$data = $this->Users->find()
		->where($conditions)
		->select([
			'id',
			'name',
			'username',
			'phone',
			'email',
			'url',
			'branch_id',
			'group_id',
		]);
		//->toArray();
		// pr($data->toArray());
		if (empty($data)){
			$data = null;
		}
		
		$params = [
			'filtrations'=>$filtrations,
			'topActions'=>$topActions,
			'cols'=>$cols,
			'posibility'=>$posibility,
			'data'=>$data,
		];
		
		$this->ViewIndex->load($params);
		
	
	}

    public function edit($id = null){
		
		$this->checkLogged();

		$this->getBranchesList();
        $data = $this->Users->newEntity(); 
        
		if ($id != null){
			$conditions = ['id'=>$id];
			$data = $this->Users->find()
            ->where($conditions)
            ->first()
            ;
		
        }
		$data->password_tmp = $data->password2 = $data->password;
		$this->set(compact("data"));
        
		if (!empty($this->request->data)){   
			if ($this->request->data['password'] != $this->request->data['password2']){
				die(json_encode(['result'=>false,'message'=>'Hesla se neshodují']));
			}
			if ($this->request->data['password'] == $this->request->data['password_tmp']){
				unset($this->request->data['password']);
			} else {
				$this->request->data['password'] = md5($this->request->data['password']);
			}
			
			//pr($this->request->data);die();
			$saveData = $this->Users->patchEntity($data,$this->request->data);
			 $this->check_error($data);
	 
			//pr($saveData);die();
			$this->Users->save($saveData);

			Cache::delete('usersList');
           	die(json_encode(['result'=>true,'message'=>'Uloženo']));
			  
        }
	}
	
	private function getBranchesList(){
		$this->loadModel('Branches');
        $this->branches_list = $this->Branches->branchesList();
        $this->set('branches_list',$this->branches_list);
      
	}

    public function trash($id){
        $this->loadModel('Users');
        $conditions = ['id'=>$id];
        $this->Users->trashAll($conditions);
        $this->redirect('/users/');


    }
    
  

}
