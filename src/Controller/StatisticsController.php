<?php
namespace App\Controller;

use Cake\Routing\Router;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;
use Cake\View\View;

class StatisticsController extends AppController
{   
    var $hoursList = [
		// '03',
		// '04',
		// '05',
		// '06',
		'07',
		'08',
		'09',
		'10',
		'11',
		'12',
		'13',
		'14',
		'15',
		'16',
		'17',
		'18',
		'19',
		'20',
		'21',
		'22',
		'23',
	];

	public function index(){
		//die('aaa');
		$this->checkLogged();
        $this->set('simpleLayout',true);
        
        //načtení skupin
        $this->loadModel('ProductGroups');
        $groupList = $this->ProductGroups->groupList();
		$this->set('groupList',$groupList);
		
		$this->loadModel('Branches');
        $branchesList = $this->Branches->branchesList();
		$this->set('branchesList',$branchesList);

		
		$conditions = [];
		if (isset($this->request->data['created_date'],$this->request->data['created_date2'])){
			$conditions = [
				'createdTime >=' => $this->request->data['created_date'],
				'createdTime <=' => $this->request->data['created_date2'],
			];
		}

		//if ($system_id != null){
			
			//$conditions['system_id'] = $system_id;
		//}

		/*$this->loadModel('Products');
		$query = $this->Products->find()
		//->contain(['OrderItems'])
		->where($conditions)
		//->order('Orders.created ASC')
		->select([
			//'id',
		])
		;
		//pr($this->request->data);
		$data = $query->toArray();

		//pr($data);die();
		if(!empty($data)){
			$this->genGrafStats($data);
			$this->getGrafData();
		}*/

		//$this->genGrafStats($data);
		$this->getGrafData();
	}
	
	private function genGrafStats($data){
		$this->statsResult = [
			'products'=>[]
		];
		foreach($data AS $item){

			if (!isset($this->statsResult['products'][$item->product_group_id]['list'][$item->product_id])){
				$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['name'] = $item->name;
				$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['count'] = 0;
				$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['price'] = 0;
				$this->statsResult['products'][$item->product_group_id]['sum'] = 0;
			}
			$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['count'] ++;
			$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['price'] += $item->price;
			$this->statsResult['products'][$item->product_group_id]['sum'] += $item->price;
			

			/*if (!isset($this->statsResult['date'][$d->created->format('d.m.Y')])){
				$this->statsResult['date'][$d->created->format('d.m.Y')] = [
					'done'=>0,
					'close'=>0,
					'done_price'=>0,
					'close_price'=>0,
				];
			}
			$total_price = 0;
			foreach($d->order_items AS $it){
				$total_price += $it['price'] * $it['count'];
			}
				
			if ($d->close_order == 0){
				$this->statsResult['date'][$d->created->format('d.m.Y')]['done'] += 1;
				$this->statsResult['date'][$d->created->format('d.m.Y')]['done_price'] += $total_price;
			} else {
				$this->statsResult['date'][$d->created->format('d.m.Y')]['close'] += 1;
				$this->statsResult['date'][$d->created->format('d.m.Y')]['close_price'] += $total_price;
				
			}
			
			// area list
			if (!isset($this->statsResult['areas'][$d->area_id])){
				$this->statsResult['areas'][$d->area_id] = 0;
				//pr($d);die(); 
			}
			$this->statsResult['areas'][$d->area_id] ++;
			
			// source list
			if (!isset($this->statsResult['sources'][$d->source_id])){
				$this->statsResult['sources'][$d->source_id] = 0;
				//pr($d);die(); 
			}
			$this->statsResult['sources'][$d->source_id] ++;
			
			// maps
			if (!empty($d->lat)){
				$this->statsResult['maps'][] = [
					'lat'=>$d->lat,
					'lng'=>$d->lng,
				];
			}
			
			// cats list
			foreach($d->order_items AS $item){
				if (!isset($this->statsResult['cats'][$item->product_group_id])){
					$this->statsResult['cats'][$item->product_group_id] = 0;
					//pr($d);die(); 
				}
				$this->statsResult['cats'][$item->product_group_id] ++;
				
				
				if (!isset($this->statsResult['products'][$item->product_group_id]['list'][$item->product_id])){
					$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['name'] = $item->name;
					$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['count'] = 0;
					$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['price'] = 0;
					$this->statsResult['products'][$item->product_group_id]['sum'] = 0;
				}
				$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['count'] ++;
				$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['price'] += $item->price;
				$this->statsResult['products'][$item->product_group_id]['sum'] += $item->price;
				
			}*/
			
			
		}
		//pr($this->statsResult['products']);die();
		//pr($this->statsResult['areas']);
		//pr($this->statsResult['maps']);die();
		//pr($this->statsResult);
		
		$this->loadModel('ProductGroups');
		$this->group_list = $this->ProductGroups->groupList();
		$this->set('group_list',$this->group_list);
		
		//pr($this->group_list);die();
		$this->graphResult = [];
		
		// dle ks
		$this->graphResult['sales']['cols'] = [
        	["id"=>"","label"=>"Datum","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Dokončeno","pattern"=>"","type"=>"number"],
        	["id"=>"","label"=>"Nedokončeno","pattern"=>"","type"=>"number"],
		];
		/// dle ceny
		$this->graphResult['sales_price']['cols'] = [
        	["id"=>"","label"=>"Datum","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Dokončeno","pattern"=>"","type"=>"number"],
        	["id"=>"","label"=>"Nedokončeno","pattern"=>"","type"=>"number"],
		];
		
		// oblasti
		$this->graphResult['areas']['cols'] = [
        	["id"=>"","label"=>"Oblast","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Počet objednávek","pattern"=>"","type"=>"number"],
		];
		
		// kategorie
		$this->graphResult['cats']['cols'] = [
        	["id"=>"","label"=>"Kategorie","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Počet produktů","pattern"=>"","type"=>"number"],
		];
		
		// zdroj
		$this->graphResult['sources']['cols'] = [
        	["id"=>"","label"=>"Zdroj","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Počet objednávek","pattern"=>"","type"=>"number"],
		];
		// LineGraf 
		$this->graphResult['line_graf']['cols'] = [
        	["id"=>"","label"=>"Tržba","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Hodina","pattern"=>"","type"=>"number"],
		];
		
		// maps
		//$this->graphResult['maps'] = $this->statsResult['maps'];
		
		// products
		$this->graphResult['products'] = $this->statsResult['products'];
		
		foreach(['cat1'=>55,'cat2'=>135,'cat3'=>155] AS $cat_id=>$value){
			
			$this->graphResult['cats']['rows'][] = ["c"=>[
				["v"=>$cat_id,"f"=>null],
				["v"=>$value,"f"=>null],
			]];
		}
		
		
		// datum objednavky
		/*foreach($this->statsResult['date'] AS $date=>$value){
			$this->graphResult['sales_price']['rows'][] = ["c"=>[
				["v"=>$date,"f"=>null],
				["v"=>$value['done_price'],"f"=>null],
				["v"=>$value['close_price'],"f"=>null]
			]];
			$this->graphResult['sales']['rows'][] = ["c"=>[
				["v"=>$date,"f"=>null],
				["v"=>$value['done'],"f"=>null],
				["v"=>$value['close'],"f"=>null]
			]];
			
		}
		
		// oblasti
		foreach($this->statsResult['areas'] AS $area_id=>$value){
			//pr($this->areaList);
			$this->graphResult['areas']['rows'][] = ["c"=>[
				
				["v"=>(isset($this->areaList['coords']->$area_id)?$this->areaList['coords']->$area_id->name:'Neznámá oblast '),"f"=>null],
				["v"=>$value,"f"=>null],
			]];
		}
		
		// kategorie
		foreach($this->statsResult['cats'] AS $cat_id=>$value){
			
			$this->graphResult['cats']['rows'][] = ["c"=>[
				["v"=>$this->group_list[$cat_id],"f"=>null],
				["v"=>$value,"f"=>null],
			]];
		}
		
		// zdroj
		foreach($this->statsResult['sources'] AS $source_id=>$value){
			
			$this->graphResult['sources']['rows'][] = ["c"=>[
				["v"=>$this->source_list[$source_id],"f"=>null],
				["v"=>$value,"f"=>null],
			]];
		}*/
		
/*
		foreach($this->statsResult['date'] AS $date=>$value){
			$this->graphResult['sales'][] = [
				$date,
				$value['done'],
				$value['close'],
			];
		}
		*/
		//pr(json_encode($this->graphResult));
		$this->set('graphResult',$this->graphResult);
		//pr($this->graphResult);
		
		//pr($this->statsResult);
	}

	public function getGrafData (){
		$this->graphResult = [];
		
		// dle ks
		$this->graphResult['sales']['cols'] = [
        	["id"=>"","label"=>"Datum","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Dokončeno","pattern"=>"","type"=>"number"],
        	["id"=>"","label"=>"Nedokončeno","pattern"=>"","type"=>"number"],
		];
		/// dle ceny
		$this->graphResult['sales_price']['cols'] = [
        	["id"=>"","label"=>"Datum","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Dokončeno","pattern"=>"","type"=>"number"],
        	["id"=>"","label"=>"Nedokončeno","pattern"=>"","type"=>"number"],
		];
		
		// oblasti
		$this->graphResult['areas']['cols'] = [
        	["id"=>"","label"=>"Oblast","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Počet objednávek","pattern"=>"","type"=>"number"],
		];
		
		// kategorie
		$this->graphResult['cats']['cols'] = [
        	["id"=>"","label"=>"Kategorie","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Počet produktů","pattern"=>"","type"=>"number"],
		];
		
		// zdroj
		$this->graphResult['sources']['cols'] = [
        	["id"=>"","label"=>"Zdroj","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Počet objednávek","pattern"=>"","type"=>"number"],
		];
		
		// maps
		//$this->graphResult['maps'] = $this->statsResult['maps'];
		
		// products
		$this->graphResult['products'] = $this->statsResult['products'];
		
		$this->graphResult['sales_price'] ='{"cols":[{"id":"","label":"Datum","pattern":"","type":"string"},{"id":"","label":"Tržby","pattern":"","type":"number"},{"id":"","label":"Zisk","pattern":"","type":"number"},{"id":"","label":"DPH","pattern":"","type":"number"},{"id":"","label":"Storno","pattern":"","type":"number"}],"rows":[{"c":[{"v":"15.05.2018","f":null},{"v":20432,"f":null},{"v":278,"f":null},{"v":1219,"f":null},{"v":1040,"f":null}]},{"c":[{"v":"16.05.2018","f":null},{"v":15972,"f":null},{"v":0,"f":null},{"v":188,"f":null},{"v":719,"f":null}]},{"c":[{"v":"17.05.2018","f":null},{"v":26082,"f":null},{"v":0,"f":null},{"v":1596,"f":null},{"v":1539,"f":null}]},{"c":[{"v":"18.05.2018","f":null},{"v":25499,"f":null},{"v":0,"f":null},{"v":726,"f":null},{"v":1299,"f":null}]},{"c":[{"v":"19.05.2018","f":null},{"v":30009,"f":null},{"v":0,"f":null},{"v":0,"f":null},{"v":1700,"f":null}]}]}';
		$this->graphResult['sources'] ='{"cols":[{"id":"","label":"Zdroj","pattern":"","type":"string"},{"id":"","label":"Po\u010det objedn\u00e1vek","pattern":"","type":"number"}],"rows":[{"c":[{"v":"Cukrárny","f":null},{"v":176,"f":null}]},{"c":[{"v":"E-shop","f":null},{"v":252,"f":null}]},{"c":[{"v":"Svatební dorty","f":null},{"v":24,"f":null}]},{"c":[{"v":"Bistro","f":null},{"v":22,"f":null}]}]}';
		$this->graphResult['areas'] ='{"cols":[{"id":"","label":"Oblast","pattern":"","type":"string"},{"id":"","label":"Tržby","pattern":"","type":"number"}],"rows":[{"c":[{"v":"Pondělí","f":null},{"v":12,"f":null}]},{"c":[{"v":"Uterý","f":null},{"v":107,"f":null}]},{"c":[{"v":"Středa","f":null},{"v":29,"f":null}]},{"c":[{"v":"Čtvrtek","f":null},{"v":11,"f":null}]},{"c":[{"v":"Pátek","f":null},{"v":87,"f":null}]},{"c":[{"v":"Sobota","f":null},{"v":39,"f":null}]},{"c":[{"v":"Neděle","f":null},{"v":8,"f":null}]}]}';
		$this->graphResult['cats'] ='{"cols":[{"id":"","label":"Kategorie","pattern":"","type":"string"},{"id":"","label":"Po\u010det produkt\u016f","pattern":"","type":"number"}],"rows":[{"c":[{"v":"Dorty","f":null},{"v":150,"f":null}]},{"c":[{"v":"Dezerty","f":null},{"v":431,"f":null}]},{"c":[{"v":"Makronky","f":null},{"v":335,"f":null}]},{"c":[{"v":"Sladké pečivo","f":null},{"v":41,"f":null}]},{"c":[{"v":"Zmrzlina","f":null},{"v":53,"f":null}]},{"c":[{"v":"Dárky","f":null},{"v":48,"f":null}]},{"c":[{"v":"Nápoje","f":null},{"v":392,"f":null}]},{"c":[{"v":"Menu","f":null},{"v":85,"f":null}]},{"c":[{"v":"Burgery","f":null},{"v":77,"f":null}]},{"c":[{"v":"Snídaně","f":null},{"v":69,"f":null}]},{"c":[{"v":"Svatební dorty","f":null},{"v":130,"f":null}]}]}';
		
		
		// datum objednavky
		/*foreach($this->statsResult['date'] AS $date=>$value){
			$this->graphResult['sales_price']['rows'][] = ["c"=>[
				["v"=>$date,"f"=>null],
				["v"=>$value['done_price'],"f"=>null],
				["v"=>$value['close_price'],"f"=>null]
			]];
			$this->graphResult['sales']['rows'][] = ["c"=>[
				["v"=>$date,"f"=>null],
				["v"=>$value['done'],"f"=>null],
				["v"=>$value['close'],"f"=>null]
			]];
			
		}
		
		// oblasti
		foreach($this->statsResult['areas'] AS $area_id=>$value){
			//pr($this->areaList);
			$this->graphResult['areas']['rows'][] = ["c"=>[
				
				["v"=>(isset($this->areaList['coords']->$area_id)?$this->areaList['coords']->$area_id->name:'Neznámá oblast '),"f"=>null],
				["v"=>$value,"f"=>null],
			]];
		}
		
		// kategorie
		foreach($this->statsResult['cats'] AS $cat_id=>$value){
			
			$this->graphResult['cats']['rows'][] = ["c"=>[
				["v"=>$this->group_list[$cat_id],"f"=>null],
				["v"=>$value,"f"=>null],
			]];
		}
		
		// zdroj
		foreach($this->statsResult['sources'] AS $source_id=>$value){
			
			$this->graphResult['sources']['rows'][] = ["c"=>[
				["v"=>$this->source_list[$source_id],"f"=>null],
				["v"=>$value,"f"=>null],
			]];
		}
		

		foreach($this->statsResult['date'] AS $date=>$value){
			$this->graphResult['sales'][] = [
				$date,
				$value['done'],
				$value['close'],
			];
		}
		*/
		//pr(json_encode($this->graphResult));
		$this->set('graphResult',$this->graphResult);
		
		//pr($this->statsResult);
	
	}

	private function trzba(){
		//pr($this->productData);
		foreach ($this->branches_list as $id_b=>$name_b){
		$this->sumace['trzba'][$id_b]=0;

		}	
		if (isset($this->productData) && !empty($this->productData)) {
			foreach ($this->productData as $key=>$item){
				if(!isset($this->sumace['trzba'][$item->system_id])){
					$this->sumace['trzba'][$item->system_id] = 0;
				}
				//$this->sumace['trzba'][$item->system_id] += $item->price_sum_without_tax;
				$this->sumace['trzba'][$item->system_id] += $item->price_sum;
			}
		}
		//pr($this->sumace);
		//pr($this->productData);
	}

	private function loadProductDashboard(){
	
		$this->graphResult['watchdog_dashboard']=[];

		$this->loadModel('Products');
		$data_products = $this->Products->find('list')->where(['is_dashboard'=> 1])->toArray();
		//pr($data_products);
		$this->graphResult['watchdog_dashboard']['cols'] = [
        	["id"=>"","label"=>"Produkty","pattern"=>"","type"=>"string"],
        	// ["id"=>"","label"=>"Tady","pattern"=>"","type"=>"number"],
        	// ["id"=>"","label"=>"Sebou","pattern"=>"","type"=>"number"],
		];

		if (isset($this->branches_list)){
			foreach ($this->branches_list as $b){
				$this->graphResult['watchdog_dashboard']['cols'][] =
					["id"=>"","label"=>$b,"pattern"=>"","type"=>"number"];			
			}//pr($this->productData);die();
		}

		$this->sumace['watchdog_dashboard'] = [];
		if (isset($this->productData) && !empty($this->productData)) {
			foreach ($this->productData as $key=>$item){
				// pr($item);die();
					if(isset($data_products[$item->product_id])){
						// pr($data_products);
						if(!isset($this->sumace['watchdog_dashboard'][$item->product_id] )){
							$this->sumace['watchdog_dashboard'][$item->product_id] = [];
						}
						if(!isset($this->sumace['watchdog_dashboard'][$item->product_id][$item->system_id] )){
							foreach ($this->branches_list as $branch_id=>$b){
								$this->sumace['watchdog_dashboard'][$item->product_id][$branch_id] = 0;
							}
						}
						// pr($item);die();
						$this->sumace['watchdog_dashboard'][$item->product_id][$item->system_id] += $item->value;
					}else{
						//pr('neni');
					}

					// pr($item);
					// die();

				}
				// pr($this->sumace['watchdog_dashboard']);die();
		}

		$result = [];
		//pr($this->sumace['grafLine']);die();
		foreach($this->sumace['watchdog_dashboard'] AS $product_id=>$data){
			foreach($data AS $system_id=>$value){
				// pr($system_id);
				$result[$product_id][$system_id] = ["v"=>$value,"f"=>null];
			
			
			}
		}
		
		// pr($result);die();
		foreach($result AS $product_id=>$data){
			
			$c = [["v"=>$data_products[$product_id],"f"=>null]];
			foreach($data AS $system_id=>$values){
				$c[] =$values; //	["v" => $values['v'], "f"=>null];
				
			}
			$this->graphResult['watchdog_dashboard']['rows'][] = ["c"=> $c];
		}

		// pr($this->graphResult['watchdog_dashboard']);die();
	
		
	}

	private function genGrafCats(){
		// pr($this->productData);
		
		$this->sumace['grafCats']=[];

		if (isset($this->productData) && !empty($this->productData)) {
			foreach ($this->productData as $key=>$item){
				if (!isset($this->sumace['grafCats'][$item->product_group_id])) {
					$this->sumace['grafCats'][$item->product_group_id] = 0;
				}
				
				$this->sumace['grafCats'][$item->product_group_id] += $item->value;
		
				}
		}
				// kategorie
				$this->graphResult['cats']['cols'] = [
					["id"=>"","label"=>"Kategorie","pattern"=>"","type"=>"string"],
					["id"=>"","label"=>"Počet produktů","pattern"=>"","type"=>"number"],
				];

						
				// kategorie
				$this->loadModel('ProductGroups');
				$this->group_list_level = $this->ProductGroups->groupListLevel2();
				$this->group_list = $this->ProductGroups->groupList();
				//pr($this->group_list);
				foreach($this->sumace['grafCats'] AS $cat_id=>$value){
					if (isset($this->group_list_level[$cat_id]))
					$this->graphResult['cats']['rows'][] = ["c"=>[
						["v"=>$this->group_list[$this->group_list_level[$cat_id]],"f"=>null],
						["v"=>$value,"f"=>null],
					]];
				}
		//pr($this->graphResult);
		//pr($this->productData);

	}


	private function genLineGraf(){
		//pr($this->productData);
		// použít strtotime
		//seskupovat hodiny
		//pole  vypsat natvrdo
		
		$this->sumace['grafCats']=[];
		//pr($this->productData);die();
		$braches = [];
		
		// pr(count($this->productData));
		if (isset($this->productData) && !empty($this->productData)) {
			$sum = 0;
			foreach ($this->productData as $key=>$item){
				$sum += $item->price_sum_without_tax;
				$hour = date('H',strtotime($item->created));
	
				if (!isset($this->sumace['grafLine'][$item->system_id])) {
					foreach($this->hoursList AS $h){
						$this->sumace['grafLine'][$item->system_id][$h] = 0;
					}
				}
				$braches['cols'][$item->system_id] = ["id"=>"","label"=>$this->branches_list[$item->system_id],"pattern"=>"","type"=>"number"];
				if (in_array($hour,$this->hoursList)){
					$this->sumace['grafLine'][$item->system_id][$hour] += $item->price_sum_without_tax;
				}
				
			}
			// pr($sum);
				// pr($this->sumace['grafLine']);die();
		}

			$tmpBranches = [];
			if (isset($braches['cols'])){
				foreach($braches['cols'] AS $b){
					// pr($b);
					$tmpBranches[] = $b;
				} 
				$braches['cols'] = $tmpBranches;
			
		
				$this->graphResult['line_graf']['cols'] = [
					["id"=>"","label"=>"Hodina","pattern"=>"","type"=>"string"],
				];
				$this->graphResult['line_graf']['cols'] = array_merge($this->graphResult['line_graf']['cols'],$braches['cols']);
				
				$result = [];
				//pr($this->sumace['grafLine']);die();
				foreach($this->sumace['grafLine'] AS $system_id=>$data){
					foreach($data AS $hour=>$value){
						$result[$hour][$system_id] = ["v"=>$value,"f"=>null];
					
					
					}
				}
				// pr($result);die();
				foreach($result AS $hour=>$data){
					
					$c = [["v"=>$hour,"f"=>null]];
					foreach($data AS $system_id=>$values){
						$c[] =$values; //	["v" => $values['v'], "f"=>null];
						
					}
					$this->graphResult['line_graf']['rows'][] = ["c"=> $c];
				}
			} else {
				$this->graphResult['line_graf'] = '';
			}
			// pr($this->graphResult['line_graf']);die(); 

		}


	private function loadTrzbaTady(){
		$this->graphResult['trzbaTady']=[];

		$this->loadModel('ProductConnects');
		$this->sebouList = $this->ProductConnects->groupListSebou();
		//pr($this->sebouList);die();
		
		$this->graphResult['tady_sebou']['cols'] = [
        	["id"=>"","label"=>"Datum","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Tady","pattern"=>"","type"=>"number"],
        	["id"=>"","label"=>"Sebou","pattern"=>"","type"=>"number"],
		];

		if (isset($this->productData) && !empty($this->productData)) {
			$this->result = [];
			foreach($this->productData AS $item){
				$date = $item->created->format('d.m.Y');
				if (!isset($this->result[$date])){
					$this->result[$date] = ['tady'=>0,'sebou'=>0];
				}
				// pr($this->sebouList['sebou']);die();
				if (in_array($item->product_group_id,$this->sebouList['sebou'])){
					$this->result[$date]['sebou'] += $item->value;
				} else {
					$this->result[$date]['tady'] += $item->value;
				
				}
			}
			foreach($this->result AS $date=>$res){
				$this->graphResult['tady_sebou']['rows'][] = ["c"=>[
					["v"=>$date,"f"=>null],
					["v"=>$res['tady'],"f"=>null],
					["v"=>$res['sebou'],"f"=>null]
				]];
			}
		}
	}

	private function loadTrzbaPredpoklad(){
		// pr($this->productData);die();
		$this->loadModel('BranchesStats');
		$this->branches_stats = $this->BranchesStats->branchesStatsList();
		// pr($this->branches_stats);die();
		//pr($this->condition_system_id);

		$this->sumace['grafCats']=[];
		$this->sumData = [];
		foreach($this->productData AS $k=>$item){
			if (isset($item->created)){
				// pr($item->created);die();
				$date = $item->created->format('d.m.Y');

				if(!isset($this->sumData[$date])){
					$this->sumData[$date] = 0;
				}
				// pr($item);die();
				// pr();
				 $this->sumData[$date] += $item->price_sum_without_tax;
			}
		}
		// pr($this->sumData );

		$this->graphResult['sales_price_predpoklad']['cols'] = [
        	["id"=>"","label"=>"Datum","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Předpoklad","pattern"=>"","type"=>"number"],
        	["id"=>"","label"=>"Reálný","pattern"=>"","type"=>"number"],
		];
		// pr($this->branches_stats);
		if (!isset($this->condition_system_id)){
			$this->predpoklad = $this->branches_stats['total'];
		} else {
			$this->predpoklad = (isset($this->branches_stats['branches'][$this->condition_system_id])?$this->branches_stats['branches'][$this->condition_system_id]:0);
			
		}

		foreach($this->sumData AS $date=>$value){
			$dateCon = Date('Y-m-d',strtotime($date));
			//pr($dateCon);die();
			$this->graphResult['sales_price_predpoklad']['rows'][] = ["c"=>[
				["v"=>$date,"f"=>null],
				["v"=>(isset($this->predpoklad[$dateCon])?$this->predpoklad[$dateCon]:0),"f"=>null],
				["v"=>$value,"f"=>null]
			]];
			
		}
		// pr($this->graphResult['sales_price_predpoklad']);die();
		//pr($this->sumace['trzba']); 
		//die();

		
	}

	private function loadProductStats(){
		$this->sumace['productSum'] = [
			'total'=>0,
			'total2'=>0,
		];
		// pr($this->statsData);die();
		$this->sumace['productSum']['total'] = count($this->statsData);
		foreach($this->statsData AS $d){
			// pr($d);
			$this->sumace['productSum']['total2'] += $d->value;
		}
		// pr($this->sumace['productSum']);
		// die('a');

	}


	private function prumernaObj(){
		//pr($this->productData);
		foreach ($this->branches_list as $id_b=>$name_b){
		$this->sumace['prumernaObj'][$id_b]=[
			'orders_price' => [],
			'sum_orders_price' => 0,
			'count_orders_price' => 0,
			'prumer' => 0,
		];

		}	
		if (isset($this->productData) && !empty($this->productData)) {
			foreach ($this->productData as $key=>$item){

					if (!isset($this->sumace['prumernaObj'][$item->system_id]['orders_price'][$item->order_id])){
						$this->sumace['prumernaObj'][$item->system_id]['orders_price'][$item->order_id] = 0;
						$this->sumace['prumernaObj'][$item->system_id]['sum_orders_price'] = 0;
						$this->sumace['prumernaObj'][$item->system_id]['count_orders_price'] = 0;
						$this->sumace['prumernaObj'][$item->system_id]['prumer'] = 0;
					}
					$this->sumace['prumernaObj'][$item->system_id]['orders_price'][$item->order_id] += $item->price_sum_without_tax;
					$this->sumace['prumernaObj'][$item->system_id]['sum_orders_price'] += $this->sumace['prumernaObj'][$item->system_id]['orders_price'][$item->order_id];
					$this->sumace['prumernaObj'][$item->system_id]['count_orders_price'] += count($this->sumace['prumernaObj'][$item->system_id]['orders_price'][$item->order_id]);
					$this->sumace['prumernaObj'][$item->system_id]['prumer'] = $this->sumace['prumernaObj'][$item->system_id]['sum_orders_price'] / $this->sumace['prumernaObj'][$item->system_id]['count_orders_price'];
				}
		}
		//pr($this->sumace);
		//pr($this->productData);
	}
	
	public function genStats(){
		//die();
		$this->viewBuilder()->autoLayout(false);


		// seznam pobocek
		$this->loadModel('Branches');
		$this->branches_list = $this->Branches->branchesList();
		$this->set('branches_list',$this->branches_list);


		$this->graphResult = [];

		$this->sumace=[
			'trzba'=>[],

		];
		

		// pr($this->branches_list);



		$this->loadModel('Stocks'); 
		
		
		// $conditions=['stock_item_product_id >'=>0];
		$conditions=['storno'=>0];
		$this->loadComponent('ViewIndex');
		$conditions = $this->ViewIndex->conditions($conditions);

		$conditionsOrders = $conditions;
		$conditionsOrders['Orders.storno'] = 0;
		unset($conditionsOrders['storno']); 
		unset($conditionsOrders['stock_item_product_id >']); 
		// pr($conditionsOrders);
		
		// pokud neni nstaveno datum nastav aktualni den
		if (!isset($conditions['Stocks.createdTime >= '])){
			$dateFrom = date('Y-m-d 00:00:00');
			$dateTo = date('Y-m-d 23:59:59');
			//pr($dateFrom);
			//pr($dateTo);
			$conditions['Stocks.createdTime >= '] = strtotime($dateFrom); 
			$conditions['Stocks.createdTime <= '] = strtotime($dateTo); 

			$conditionsOrders['OrderItems.createdTime >= '] = strtotime($dateFrom); 
			$conditionsOrders['OrderItems.createdTime <= '] = strtotime($dateTo); 
		}

		// nastaveni aktualni datum pro view 
		if (isset($conditions['Stocks.createdTime >= '])){
			$currentDateFrom = date('d.m.Y H:i',$conditions['Stocks.createdTime >= ']);
			$this->set('currentDateFrom',$currentDateFrom);
			// pr($currentDateFrom);die();
		}
		if (isset($conditions['Stocks.createdTime <= '])){
			$currentDateTo = date('d.m.Y H:i',$conditions['Stocks.createdTime <= ']);
			$this->set('currentDateTo',$currentDateTo);
			// pr($currentDateFrom);die();
		}
		if (isset($conditionsOrders['Stocks.createdTime >= '])){
			$dateFromConverted = strtotime(date("Y-m-d H:i:s", $conditionsOrders['Stocks.createdTime >= ']));
			$dateToConverted = strtotime(date("Y-m-d H:i:s", $conditionsOrders['Stocks.createdTime <= '] + (23 * 3600) + 3599));
			$conditionsOrders['Orders.createdTime >= '] = $dateFromConverted;
			$conditionsOrders['OrderItems.createdTime >= '] = $dateFromConverted;
			unset($conditionsOrders['Stocks.createdTime >= ']);
			$conditionsOrders['Orders.createdTime <= '] = $dateToConverted;
			$conditionsOrders['OrderItems.createdTime <= '] = $dateToConverted;
			unset($conditionsOrders['Stocks.createdTime <= ']);
		}

		if (isset($conditionsOrders["system_id"])) {
			$conditionsOrders["Orders.system_id"] = $conditionsOrders["system_id"];
			$conditionsOrders["OrderItems.system_id"] = $conditionsOrders["system_id"];
			unset($conditionsOrders["system_id"]);
		}
		// pr($conditionsOrders); die();
		
		$mapper = function ($data, $key, $mapReduce) {
			// pr($data);
			if (isset($data->product->product_prices)){
				foreach ($data->product->product_prices as $it){
					if ($it->system_id = $data->system_id){
						$data->price= $it->price;
					}
				}
				unset($data->product->product_prices);
			}
			if (!isset($data->tax_id)){
				$data->tax_id = 1;
			}
			$data->price_without_tax = round($data->price - ($data->price*$this->price_tax_list_con[$data->tax_id]),2);
			$data->price_sum = $data->price * $data->value;
			$data->price_sum_without_tax = $data->price_without_tax * $data->value;
			//pr($this->dph_conf);
			//pr($data);die();
			$nakup_price = 0;
			$nakup_price_vat = 0;
				
			if (isset($data->product_recepturas) && !empty($data->product_recepturas)){
				//pr(json_encode($data->product_receptura));die('a');
				
				foreach($data->product_recepturas AS $res){
					if (isset($res->sklad_item) && isset($res->sklad_item->sklad)){
						$nakup_price += $res->sklad_item->sklad->nakup_price;
						$nakup_price_vat += $res->sklad_item->sklad->nakup_price_vat;
						unset($data->product_recepturas);
					}
				}
			}
			$data->nakup_price = $nakup_price;
			$data->nakup_price_vat = $nakup_price_vat;
			
			$mapReduce->emit($data);
		};
		$fields = [];
		// pr($conditionsOrders);
		//  die();
		$data = $this->Stocks->find();
		$fields = [
			'id',
			'stock_item_product_id',
			'value',
			'stock_type_id',
			'system_id',
			'nakup_price',
			'nakup_price_vat',
			'nakup_price_total',
			'nakup_price_total_vat',
			'product_group_id',
			'tax_id',
			'unit_id',
			'Products.id',
			'Products.name',
			'Products.code',
			'Products.price',
			'order_id',
			'created',
			//'value' => $data->func()->sum('value') 
		];
		// pr($conditions);
		if (isset($conditions['system_id'])){
			$this->condition_system_id = $conditions['system_id']; 
		}
		// $conditions = [];
		// $conditions['Stocks.createdTime >=' ] = 1528063560;
    	// $conditions['Stocks.createdTime <= '] = 1528322340;
		// pr($conditions);
		//$conditions = ['date(Stocks.created)'=>'2018-05-23'];
		
		// pr($conditionsOrders);die();

		
		$mapperOrders = function ($data, $key, $mapReduce) {
			// pr($data);die();
			$tmp = [
				'id'=>$data->id,
				'product_id'=>$data->product_id,
				'stock_item_product_id'=>$data->product_id,
				'value'=>$data->count,
				'stock_type_id'=>null, // todo
				'system_id'=>$data->system_id, 
				'nakup_price'=>null, //todo 
				'nakup_price_vat'=>null, //todo 
				'nakup_price_total'=>null, //todo 
				'nakup_price_total_vat'=>null, //todo 
				'product_group_id'=>$data->product_group_id,  
				'tax_id'=>$data->tax_id,  
				'unit_id'=>null, // todo  
				'price'=>$data->price, // todo  
				'order_id'=>$data->order_id,   
				'created'=>$data->created,   
				'product'=>[
					'id'=>$data->product_id,
					'name'=>$data->name,
					'code'=>$data->code,
					'price'=>$data->price,

				]
			];
			if (!isset($data->tax_id)){
				$data->tax_id = 1;
			}
			$tmp['price_without_tax'] = round($data->price - ($data->price*$this->price_tax_list_con[$data->tax_id]),2);
			$tmp['price_sum'] = $data->price * $data->count;
			$tmp['price_sum_without_tax'] = $data->price_without_tax * $data->count;
			//pr($this->dph_conf);
			// pr($tmp);die();
			$nakup_price = 0;
			$nakup_price_vat = 0;
				
			if (isset($data->product_recepturas) && !empty($data->product_recepturas)){
				//pr(json_encode($data->product_receptura));die('a');
				
				foreach($data->product_recepturas AS $res){
					if (isset($res->sklad_item) && isset($res->sklad_item->sklad)){
						$nakup_price += $res->sklad_item->sklad->nakup_price;
						$nakup_price_vat += $res->sklad_item->sklad->nakup_price_vat;
						unset($data->product_recepturas);
					}
				}
			}
			$tmp['nakup_price'] = $nakup_price;
			$tmp['nakup_price_vat'] = $nakup_price_vat;
			
			// pr($data);
			// pr($tmp);die();
			$mapReduce->emit($tmp);
		};

		$this->loadModel('OrderItems');
		$fieldsOrders = [];
		$dataOrdersQuery = $this->OrderItems->find();
		$dataOrdersQuery
			->mapReduce($mapperOrders)
			->contain([/*'ProductRecipes',*/ 'Orders'])
            ->order('count DESC')
			->where($conditionsOrders);
		
		$dataOrders = $dataOrdersQuery->toArray();
		
		$statsDataOrders = $dataOrdersQuery->group('product_id')
		->select(array_merge($fieldsOrders,['count' => $data->func()->sum('count') ]))
		->toArray();

		// pr($dataOrders);die();
		// pr($statsDataOrders);die();
		$this->statsData =  $statsData = json_decode(json_encode($statsDataOrders), FALSE);
		//pr($this->statsData);die();
		$this->productData = json_decode(json_encode($dataOrders), FALSE) ; 
		foreach($this->productData AS $k=>$p){
			$this->productData[$k]->created = new Time($this->productData[$k]->created);
		}
		
		/*
		$data
		->select($fields)
		->order('Stocks.value DESC')
		->where($conditions)
		//->limit(20)
		//->group('Stocks.stock_item_product_id')
		->mapReduce($mapper)
		
		->contain(['Products', 'Products.ProductPrices'=> function ($q) {
			//pr($system_id);
			//pr($q);
			//die();
			return $q;
		},
		])
		->toArray();


		$this->productData = $data->toArray();
		$statsData = $data->group('Stocks.stock_item_product_id')
		->select(array_merge($fields,['value' => $data->func()->sum('value') ]))
		->toArray();
*/

		// pr($this->productData);die();
		// pr($statsData);

		// $this->statsData = $statsData;
		//generování tržby 
		$this->trzba();
		$this->prumernaObj();
		$this->genGrafCats();
		$this->loadTrzbaPredpoklad();
		$this->loadTrzbaTady();
		$this->loadProductStats();
		$this->genLineGraf();
		$this->loadProductDashboard();

		if ($data) {
			$this->set('stats_data',$statsData);
			$this->set('sumace',$this->sumace);
			$this->set('graphResult',$this->graphResult);
		}
	}
	
	public function index2($system_id=null){
		header('Content-Type: text/html; charset=utf-8');
		$this->set('title','Statistiky');
		
		if ($system_id * 1 == 0){
			
		
		$system_id = substr($system_id,3,-3);
		$system_id = (base64_decode($system_id)*1);	
		} else {
			$system_id = $system_id *1;
		}
		
		//pr($system_id);die();
		if (!$system_id){
			$this->checkLogged();
			$system_id = $this->system_id;
		}
		$this->set('system_id',$system_id);
		
		//$this->request->session()->delete('isSuperadmin');
		
		/*
		if (isset($this->request->query['superadmin']) || $this->request->session()->read('isSuperadmin') || $this->loggedUser->system_id == 1000){
			$this->isSuperadmin = true;
			$this->set('isSuperadmin',$this->isSuperadmin);
			$this->request->session()->write('isSuperadmin',true);
		}
		*/
		
		if (!isset($this->isSuperadmin) && !isset($this->deadlineData)){
			if (!is_int($system_id) || $system_id == 0){
				die('Chyba ověření');
			}
		}
		//if (!empty($this->request->data)){
		if($this->request->is("ajax") || isset($this->deadlineData)){
			if (!isset($this->deadlineData)){
		
		die();		
				
				if (empty($this->request->data['date_from'])) die(json_encode(['result'=>false,'message'=>'Vyplňte datum od']));
				if (empty($this->request->data['date_from'])) die(json_encode(['result'=>false,'message'=>'Vyplňte datum do']));
				if (isset($this->request->data['system_id'])){
					$system_id = $this->request->data['system_id'];
				}
				
				$date_f = explode('.',$this->request->data['date_from']);
				$date_t = explode('.',$this->request->data['date_to']);
				$date_from = strtotime($date_f[2].'-'.$date_f[1].'-'.$date_f[0]);
				$date_to = strtotime($date_t[2].'-'.$date_t[1].'-'.$date_t[0].' 23:59:59');
				$date_to = strtotime($this->request->data['date_to']);
				
				$conditions = [
					'createdTime >=' => $date_from,
					'createdTime <=' => $date_to,
				];
				if ($system_id != null){
					
					$conditions['system_id'] = $system_id;
				}
			} else {
				$conditions = [
					'operating'=>0,
					'system_id'=>$this->deadlineData->system_id,
					'pokladna_id >'=>$this->deadlineData->order_id_from,
					'pokladna_id <='=>$this->deadlineData->order_id_to,
				];
				//pr($conditions);
				//pr($this->deadlineData);die();
				$this->set('statsTitle',$this->deadlineData->created->format('d.m.Y. H:i:s'));
			}
			//pr($conditions);
			$this->loadModel('Orders');
			$query = $this->Orders->find()
			->contain(['OrderItems'])
			->where($conditions)
			->order('Orders.created ASC')
			->select([
				//'id',
			])
			;
			
			$data = $query->toArray();
			//pr(($data));die();
			if ($data){
				$this->getStock($data);
			}
			$this->loadModel('MapAreas');
			$this->areaList = $this->MapAreas->getAreaList($system_id);
			
			//if ($_SERVER['REMOTE_ADDR'] == '46.135.83.248'){
				$delivery_list = [];
				
				$this->loadModel('MobileProvozs');
				$provoz = $this->MobileProvozs->systemListLoad($conditions['system_id']);
				//pr($provoz);
				$this->loadModel('MobileDeliverys');
				$this->delivery_list = $this->MobileDeliverys->deliveryList($provoz->id);
				//pr($this->delivery_list);
				
				
				$this->loadModel('MobileOrders');
				$mobileOrders = $this->MobileOrders->find()
				->where([
					'shop_provoz_id'=>$conditions['system_id'],
					'order_id >'=>$conditions['pokladna_id >'],
					'order_id <='=>$conditions['pokladna_id <='],
				])
				//->limit(5)
				->toArray();
				foreach($mobileOrders AS $m){
					if (!isset($delivery_list[$m->rozvozce_id])){
						$delivery_list[$m->rozvozce_id] = [
							'name'=>$this->delivery_list[$m->rozvozce_id],
							'count_orders'=>0,
							'count_time'=>[
								1=>0,
								2=>0,
								3=>0,
								4=>0,
								'other'=>0,
							],
							'count_orders_over'=>0,
						];
					}
					$delivery_list[$m->rozvozce_id]['count_orders'] ++;
					if (strtotime($m->doba_date) < strtotime($m->rozvozce_date_vylozeni)){
						$delivery_list[$m->rozvozce_id]['count_orders'] ++;
						//die('a');
					}
					if (isset($delivery_list[$m->rozvozce_id]['count_time'][$m->doba_id])){
						$delivery_list[$m->rozvozce_id]['count_time'][$m->doba_id] ++;
					
					} else {
						$delivery_list[$m->rozvozce_id]['count_time']['other'] ++;
					}
				}
				if (!empty($delivery_list)){
					$this->delivery_list_stats = $delivery_list;
					$this->set('delivery_list',$delivery_list);
				}
				//pr($delivery_list);
				//pr($mobileOrders);
				//pr($conditions);
				//die();
			//}
			
			if ($data){
				$this->genStats($data);
			}
			
			//pr(count($data));
			//pr($date_to);
			//pr($this->request->data);
		}
		
		if($this->request->is("ajax") || isset($this->deadlineData)){
			$this->render('items');
		} else {
			$this->render('index');
			
		}
	}
	
	public function getStock($data){
		$stock_products = [];
		$code_list = [];
		$code_list_search = [];
		//pr($data);
		foreach($data AS $d){ 
			foreach($d->order_items AS $oi){
			//	if (in_array($oi->code,['016A','024A'])){
				
				$code_list_search[] = $oi->code;
				if ($d->storno != 1 || $d->storno_type == 2){
				
				if (!isset($code_list[$oi->code])){
					$code_list[$oi->code] = 1;
				} else {
					$code_list[$oi->code] ++;
				}
				} else {
			//		pr($oi->name);
				//	pr($d->storno_type);
				}
				
				//}
			}
		}
		//pr($code_list);
		
		$this->loadModel('StockProducts');
		$this->productList = $this->StockProducts->productList(true);
		$this->set('productList',$this->productList);
		//pr($this->productList);
		$this->loadModel('Stocks');
		$findStocks = $this->Stocks->find()
		->where(['code IN'=>$code_list_search])
		->select(['id','connected','code','name'])
		->map(function($row){
			$row->connected = json_decode($row->connected);
			return $row;
		})
		->toArray();
		//pr($findStocks);
		//pr($code_list);
		$stockList = [];
		if($findStocks){
			foreach($findStocks AS $stock){
				
				foreach($stock->connected AS $scon){
					//pr($scon);
					$scon->count = strtr($scon->count,[','=>'.']);
					if (isset($code_list[$stock->code])){
						if (!isset($stockList[$scon->id])){
							$stockList[$scon->id] = 0;
						}
						$stockList[$scon->id] += (($scon->loss > 0)?$scon->count-($scon->count/100)*$scon->loss:$scon->count) * $code_list[$stock->code];
						//pr($stockList[$scon->id]);
					}
				}
			}
		}
		//pr($stockList);
		if ($stockList){
			arsort($stockList);
			//pr($stockList);die();
			$stock_products = $stockList;
			$this->set('stock_products',$stock_products);
		}
		//pr($stockList);
		//pr($findStocks);
	}
	
	/**
	 * Prepares a deadline to be printed and transfered to .pdf file.
	 * Also used for debugging with ?debug parameter.
	 * 
	 * @return void
	 */
	public function show()
	{
		if ($this->request->session()->check("url")) {
			$hash = $this->request->session()->read("url");
		} else {
			if (isset($this->request->data["url"])) {
				$hash = $this->request->data["url"];
			} elseif (isset($this->request->data[0])) {
				$hash = $this->request->data[0];
			} elseif (isset($this->request->params["pass"][0])) {
				$hash = $this->request->params["pass"][0];
			}
		}
		
		$this->viewBuilder()->layout('pdf');
		$this->loadModel("Branches");
		$this->branches_list = $this->Branches->branchesList();
		
		$hash = $this->decode_long_url($hash);
		
		if (!empty($hash)) {
			$this->loadModel('Deadlines');

			if (!is_array($hash["id"])) {
				$hash["id"] = [
					$hash["id"]
				];
			}

			if (!is_array($hash["pokladna_id"])) {
				$hash["pokladna_id"] = [
					$hash["pokladna_id"]
				];
			}

			if (!is_array($hash["branch_id"])) {
				$hash["branch_id"] = [
					$hash["branch_id"]
				];
			}

			$conditions = [
				"id IN" 			=> $hash["id"],
				"pokladna_id IN" 	=> $hash["pokladna_id"],
				"branch_id IN" 		=> $hash["branch_id"]
			];

			$deadline = $this->Deadlines->find()
			->where($conditions)
			->select(["id", "order_id_from", "order_id_to", "created", "branch_id", "data", "terminal_id"])
			->order("id DESC")
			->all();
			
			if ($deadline) {
				$deadline->data = (object) [];

				$json = (object) [];
				$this->deadlineCount = 0;

				foreach ($deadline as $dead) {
					$this->deadlineCount++;
				}

				$iterator = 1;
				foreach ($deadline as $dead) {
					$json->{$dead->id} = (object) [
						"system_id" 	=> $dead->branch_id,
						"json" 			=> json_decode($dead->data),
					];
					
					if ($this->deadlineCount > 1) {
						if ($iterator == $this->deadlineCount) {
							$this->deadlineDateFrom = $json->{$dead->id}->json->header->titleDate;
						} else if ($iterator == 1) {
							$this->deadlineDateTo = $json->{$dead->id}->json->header->titleDate;
						}
					} else {
						$this->deadlineDateFrom = $json->{$dead->id}->json->header->titleDate;
					}
					$iterator++;
				}

				$this->deadlineData = $json;
				
				$this->index($hash['branch_id']);
				$this->set('deadlineData', $this->deadlineData);
				// create a view instance
				
				if (!isset($this->request->query['render'])) {
					$this->autoRender = false;
					$view = new View($this->request);
					$view->layout('ajax');
					
					if (isset($this->request->query['debug'])) {
						$view->set('debug',true);
					}

					$view->set("view_all", (isset($hash["view_all"])) ? $hash["view_all"] : false);
					$view->set("branches_list", $this->branches_list);
					$view->set('deadlineData', $this->deadlineData);
					$html = $view->render('Statistics/show');
					echo "yes";
					$this->genPdf($this->deadlineData, $html);
				}
			}
		}
	}

	/**
	 * Prints a given deadline.
	 * 
	 * If internet connection is lost then after connecting again it will automatically
	 * start downloading again from the point where it was before.
	 * 
	 * @param array $deadlineData - Deadline data.
	 * @param mixed $html - HTML template to print.
	 * @return void
	 */
	public function genPdf($deadlineData, $html)
	{
		set_time_limit(0);

		$this->loadModel('Branches');
		$branchesList = $this->Branches->branchesList();

		$branch = strtolower(str_replace(' ', '-', $branchesList[reset($deadlineData)->system_id]));
		$czechLetters = ['ě', 'é', 'š', 'č', 'ř', 'ž', 'ý', 'á', 'í', 'ú', 'ů', 'ť', 'ó', 'ď', 'ň'];
		$czechLettersReformed = ['e', 'e', 's', 'c', 'r', 'z', 'y', 'a', 'i', 'u', 'u', 't', 'o', 'd', 'n'];

		foreach ($czechLetters as $key => $value) {
			$branch = str_replace($value, $czechLettersReformed[$key], $branch);
		}

		if ($this->deadlineCount > 1) {
			$file_name = 'uzaverka-od-'.$this->deadlineDateFrom.'-do-'.$this->deadlineDateTo;
		} else {
			$file_name = 'uzaverka-'.$branch.'-'.$this->deadlineDateFrom;
		}

		//$pdf_generate_link = "http://scripts.fastesthost.cz/mpdf/";
		$pdf_generate_link = "http://scripts.fastesthost.cz/mpdf2/";
		// pr($data);die();
		$post_data = array(
			// PDF settings - require values
			'SECOND_PAGE' => true,
			'PDF_ARRAY' => false,
			'PDF_FILE' => $file_name,
			'PDF_ARRAY_FILE' => $file_name,
			'PDF_TITLE' => $file_name,
			'PDF_SUBJECT' => 'Tisk',
			//'PDF_TEMPLATE' =>'http://ointr.localhost/statistics/show/eNqrVspMUbIyMtRRKsjPzklMyUuMBwuY6CglFSXmJWeAuUqWSrVcMBcsDO4,',
			'PDF_TEMPLATE' => 'http://'.$_SERVER['HTTP_HOST'].'/pdf.html',
			//'PDF_TEMPLATE' =>'http://89.24.112.210/uploaded/pdf.html',
			//'PDF_TEMPLATE' =>'http://intr.fastestdev.cz/pdf.html',
			
			// variable constants
			'TABLE' => $html
		);


		if ($_SERVER['HTTP_HOST'] == 'ointr.localhost' || $_SERVER["HTTP_HOST"] == "olliesintranet.localhost"){
			$post_data['PDF_TEMPLATE'] = 'http://intr.fastestdev.cz/pdf.html';
		}
		// pr($_SERVER);die();
		// if ($ids != null){
			// $post_data['PDF_ARRAY'] = true;
			// $post_data['PDF_ARRAY_FILE'] = 'uzaverka'.date('Y-m-d H:i:s');
			// $post_data_array[] = $post_data;
		// }
		
		if (isset($this->request->query['debug'])){
			pr($post_data);
			die();
			$this->set('debug',true);
		}
		//$post_url = $this->encode_long_url($post_data);
		$post_url = $this->encode_long_url($post_data);
		//Header("Location: ".$pdf_generate_link."?params=".$post_url."");
		$url_open = $pdf_generate_link."?params=".$post_url;		
		
		//$this->redirect($url_open);
		
		$ch = curl_init();
		$post = [
			'data' => $post_url
		];
		curl_setopt($ch, CURLOPT_URL, $pdf_generate_link);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);

		curl_close($ch);
		header('Cache-Control: public'); 
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="'.$file_name.'.pdf"');
		header('Content-Length: '.strlen($result));
		echo $result;
	}
	
	private function genStats2($data){
		$this->statsResult = [
			
		];
		
		foreach($data AS $d){
			//pr($d);
			//pr($data);die();
			if (!isset($this->statsResult['date'][$d->created->format('d.m.Y')])){
				$this->statsResult['date'][$d->created->format('d.m.Y')] = [
					'done'=>0,
					'close'=>0,
					'storno'=>0,
					'free'=>0,
					'done_price'=>0,
					'close_price'=>0,
					'storno_price'=>0,
					'free_price'=>0,
				];
			}
			$total_price = 0;
			foreach($d->order_items AS $it){
				if($it->free){
					$this->statsResult['date'][$d->created->format('d.m.Y')]['free'] += 1;
					$this->statsResult['date'][$d->created->format('d.m.Y')]['free_price'] += $it->price_default;
					//pr($it);
					//die('a');
				} else {
					$total_price += $it['price'] * $it['count'];
					
				}
			}
			
			
			
			if ($d->close_order == 0 && $d->storno == 0){
				$this->statsResult['date'][$d->created->format('d.m.Y')]['done'] += 1;
				$this->statsResult['date'][$d->created->format('d.m.Y')]['done_price'] += $total_price;
			}else if ($d->storno == 1){
				$this->statsResult['date'][$d->created->format('d.m.Y')]['storno'] += 1;
				$this->statsResult['date'][$d->created->format('d.m.Y')]['storno_price'] += $total_price;
			} else {
				$this->statsResult['date'][$d->created->format('d.m.Y')]['close'] += 1;
				$this->statsResult['date'][$d->created->format('d.m.Y')]['close_price'] += $total_price;
				
			}
			
			
			// area list
			if (!isset($this->statsResult['areas'][$d->area_id])){
				$this->statsResult['areas'][$d->area_id] = 0;
				//pr($d);die(); 
			}
			$this->statsResult['areas'][$d->area_id] ++;
			
			// source list
			if (!isset($this->statsResult['sources'][$d->source_id])){
				$this->statsResult['sources'][$d->source_id] = 0;
				//pr($d);die(); 
			}
			$this->statsResult['sources'][$d->source_id] ++;
			
			// maps
			if (!empty($d->lat)){
				$this->statsResult['maps'][] = [
					'lat'=>$d->lat,
					'lng'=>$d->lng,
				];
			}
			
			$this->checkDoprava = false;
			foreach($d->order_items AS $dd){
				if ($dd->code == 'dop'){
					$this->checkDoprava = true;
					
				}
			}
			if (!$this->checkDoprava){
				$this->statsResult['noTransport'][] = [
					'name'=>$d->name,
					'bon'=>$d->pokladna_id,
				];
				//pr($d);
			}
			
			// cats list
			foreach($d->order_items AS $item){
				
				if (!isset($this->statsResult['cats'][$item->product_group_id])){
					$this->statsResult['cats'][$item->product_group_id] = 0;
					//pr($d);die(); 
				}
				$this->statsResult['cats'][$item->product_group_id] ++;
				
				
				if (!isset($this->statsResult['products'][$item->product_group_id]['list'][$item->product_id])){
					$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['name'] = $item->name;
					$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['count'] = 0;
					$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['price'] = 0;
				}
				if (!isset($this->statsResult['products'][$item->product_group_id]['sum'])){
						$this->statsResult['products'][$item->product_group_id]['sum'] = 0;
				
				}
				$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['count'] ++;
				$this->statsResult['products'][$item->product_group_id]['list'][$item->product_id]['price'] += $item->price;
				
				$this->statsResult['products'][$item->product_group_id]['sum'] += $item->price;
				
			}
			
			
		}
		//pr($this->statsResult['date'][$d->created->format('d.m.Y')]);
		//pr($this->statsResult['products']);die();
		//pr($this->statsResult['areas']);
		//pr($this->statsResult['maps']);die();
		//pr($this->statsResult);die();
		
		$this->loadModel('ProductGroups');
		$this->group_list = $this->ProductGroups->groupList();
		$this->set('group_list',$this->group_list);
		
		//pr($this->group_list);die();
		$this->graphResult = [];
		if (isset($this->delivery_list_stats)){
			
		 	$this->graphResult['delivery_stats']['cols'] = [
				["id"=>"","label"=>"Rozvozce","pattern"=>"","type"=>"string"],
				["id"=>"","label"=>"Celkem","pattern"=>"","type"=>"number"],
				["id"=>"","label"=>"60min","pattern"=>"","type"=>"number"],
				["id"=>"","label"=>"75min","pattern"=>"","type"=>"number"],
				["id"=>"","label"=>"90min","pattern"=>"","type"=>"number"],
				["id"=>"","label"=>"120min","pattern"=>"","type"=>"number"],
				["id"=>"","label"=>"Ostatni","pattern"=>"","type"=>"number"],
				["id"=>"","label"=>"Prekročeno","pattern"=>"","type"=>"number"],
			];
			foreach($this->delivery_list_stats AS $ds){
			
				$this->graphResult['delivery_stats']['rows'][] = ["c"=>[
					["v"=>$ds['name'],"f"=>null],
					["v"=>$ds['count_orders'],"f"=>null],
					["v"=>$ds['count_time'][1],"f"=>null],
					["v"=>$ds['count_time'][2],"f"=>null],
					["v"=>$ds['count_time'][3],"f"=>null],
					["v"=>$ds['count_time'][4],"f"=>null],
					["v"=>$ds['count_time']['other'],"f"=>null],
					["v"=>$ds['count_orders_over'],"f"=>null],
				]];
			}
		}
		// dle ks
		$this->graphResult['sales']['cols'] = [
        	["id"=>"","label"=>"Datum","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Dokončeno","pattern"=>"","type"=>"number"],
        	["id"=>"","label"=>"Nedokončeno","pattern"=>"","type"=>"number"],
        	["id"=>"","label"=>"Storno","pattern"=>"","type"=>"number"],
        	["id"=>"","label"=>"Zdarma","pattern"=>"","type"=>"number"],
		];
		/// dle ceny
		$this->graphResult['sales_price']['cols'] = [
        	["id"=>"","label"=>"Datum","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Dokončeno","pattern"=>"","type"=>"number"],
        	["id"=>"","label"=>"Nedokončeno","pattern"=>"","type"=>"number"],
        	["id"=>"","label"=>"Storno","pattern"=>"","type"=>"number"],
        	["id"=>"","label"=>"Zdarma","pattern"=>"","type"=>"number"],
		];
		
		// oblasti
		$this->graphResult['areas']['cols'] = [
        	["id"=>"","label"=>"Oblast","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Počet objednávek","pattern"=>"","type"=>"number"],
		];
		
		// kategorie
		$this->graphResult['cats']['cols'] = [
        	["id"=>"","label"=>"Kategorie","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Počet produktů","pattern"=>"","type"=>"number"],
		];
		
		// zdroj
		$this->graphResult['sources']['cols'] = [
        	["id"=>"","label"=>"Zdroj","pattern"=>"","type"=>"string"],
        	["id"=>"","label"=>"Počet objednávek","pattern"=>"","type"=>"number"],
		];
		
		// maps
		$this->graphResult['maps'] = $this->statsResult['maps'];
		
		// products
		$this->graphResult['products'] = $this->statsResult['products'];
		
		// notransport
		$this->graphResult['noTransport'] = $this->statsResult['noTransport'];
		
		
		
		
		// datum objednavky
		foreach($this->statsResult['date'] AS $date=>$value){
			$this->graphResult['sales_price']['rows'][] = ["c"=>[
				["v"=>$date,"f"=>null],
				["v"=>$value['done_price'],"f"=>null],
				["v"=>$value['close_price'],"f"=>null],
				["v"=>$value['storno_price'],"f"=>null],
				["v"=>$value['free_price'],"f"=>null],
			]];
			$this->graphResult['sales']['rows'][] = ["c"=>[
				["v"=>$date,"f"=>null],
				["v"=>$value['done'],"f"=>null],
				["v"=>$value['close'],"f"=>null],
				["v"=>$value['storno'],"f"=>null],
				["v"=>$value['free'],"f"=>null],
			]];
			
		}
		
		// oblasti
		foreach($this->statsResult['areas'] AS $area_id=>$value){
			//pr($this->areaList);
			$this->graphResult['areas']['rows'][] = ["c"=>[
				
				["v"=>(isset($this->areaList['coords']->$area_id)?$this->areaList['coords']->$area_id->name:'Neznámá oblast '),"f"=>null],
				["v"=>$value,"f"=>null],
			]];
		}
		
		// kategorie
		foreach($this->statsResult['cats'] AS $cat_id=>$value){
			
			$this->graphResult['cats']['rows'][] = ["c"=>[
				["v"=>$this->group_list[$cat_id],"f"=>null],
				["v"=>$value,"f"=>null],
			]];
		}
		
		// zdroj
		foreach($this->statsResult['sources'] AS $source_id=>$value){
			
			$this->graphResult['sources']['rows'][] = ["c"=>[
				["v"=>$this->source_list[$source_id],"f"=>null],
				["v"=>$value,"f"=>null],
			]];
		}
		
/*
		foreach($this->statsResult['date'] AS $date=>$value){
			$this->graphResult['sales'][] = [
				$date,
				$value['done'],
				$value['close'],
			];
		}
		*/
		//pr(json_encode($this->graphResult));
		$this->set('graphResult',$this->graphResult);
		
		//pr($this->statsResult);
	}
	
	
	/**
	* save nova dochazka
	*/
	public function save(){
		//sleep(6);
		//print_r($_POST);die();
		//$save_entity = $this->Orders->newEntity($_POST,['associated' => ["Clients"=>['validate' => 'default','accessibleFields'=>['id' => true]], "Clients.ClientAddresses","OrderItems"]]);
		$save_entities = $this->Orders->newEntities($_POST,['associated' => ["OrderItems"]]);
        //pr($save_entity); die();
        foreach($save_entities AS $save_entity){
            if (!$resultDb = $this->Orders->save($save_entity)){
				die(json_encode(['result'=>false,'message'=>'Chyba uložení objednávky']));	
			} else {
			}
		}
        die(json_encode(['result'=>true,'message'=>'Objednávky uloženy do cloudu']));	
		    
        
		
		
	}
	
	
	
	private function saveAttendaces($saveData){
		$save_entity = $this->Orders->newEntity($saveData);
		$this->Orders->save($save_entity);
	}
	
	
	public function test(){
		$this->autoRender = false;
		$ch = curl_init();
		$post = [
			'type'=>'exit',
			//'type'=>'enter',
			'name'=>'Kuba',
			'code'=>'111',
			'system_id'=>1,
		];
		
		curl_setopt($ch, CURLOPT_URL, 'http://chacharint.fastest.cz/api/attendances/save/');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		curl_close ($ch);
		//pr('curl result');
		//pr($result);
		
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro uložení docházky, zkontrolujte pripojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
		//	pr($result);
			$result = json_decode($result,true);
		}
		pr($result);
		
		
	}
	
	
}