<?php
namespace App\Controller;

use DateTime;
use DatePeriod;
use DateInterval;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\Utility\Hash;
use App\Component\vIComponent;

class WorkShopController extends AppController
{
    private $vyrobna_system_id = 10;
    /**
     * 
     */
    public function beforeFilter( \Cake\Event\Event $event){
        parent::beforeFilter($event);
        $this->checkLogged();
    }

    /**
     * 
     */
    public function beforeRender(\Cake\Event\Event $event)
    {
        parent::beforeRender($event);
    }

    /**
     * A view of a list of eshop orders.
     * 
     * Workshoppers are going to know how much products have
     * to be made.
     * 
     * @param string $dateBeginning - From where to start
     *                                the view data.
     * @return void
     */
    public function show($dateBeginning = null)
    {
        $this->set('title', 'Výrobna');
        $this->loadModel('Products');
        $this->loadModel('ProductRecipes');
        $this->loadModel('WebOrders');
        $this->loadModel('WebOrderItems');
        
        $conditions = [];
        $conditions['WebOrders.order_date IS NOT'] = null;
        $conditions['WebOrders.kos'] = 0;
        
        if ($dateBeginning == null) {
            $period = new DatePeriod(new DateTime(), new DateInterval('P1D'), 6);
            $fromDate = new DateTime();
            $toDate = new DateTime();
        } else {
            $period = new DatePeriod(new DateTime($dateBeginning), new DateInterval('P1D'), 6);
            $fromDate = new DateTime($dateBeginning);
            $toDate = new DateTime($dateBeginning);
        }
        
        $toDate->modify('+7 day');

        $dataProducts = $this->Products->find()
        ->select([
            'id',
            'name',
            'code',
            'view_workshop'
        ])
        ->contain([
            'ProductRecipes' => function ($q) {
                return $q->select([
                    'id',
                    'product_id',
                    'stock_item_global_id',
                    'value'
                ]);
            }
        ])
        ->all();

        $data = $this->WebOrderItems->find()
                                    ->select([
                                        'id',
                                        'name',
                                        'code',
                                        'shop_order_id',
                                        'shop_product_id',
                                        'pocet_kusu' => 'sum(ks)'
                                    ])
                                    ->contain([
                                        'WebOrders' => function($q) use($fromDate, $toDate) {
                                            return $q->select([
                                                'id',
                                                'type_id',
                                                'name',
                                                'price_with_tax',
                                                'order_date'
                                            ])
                                            ->where(['order_date >=' => $fromDate->format('Y-m-d'), 'AND' => ['order_date <' => $toDate->format('Y-m-d')]]);
                                        }
                                    ])
                                    ->where($conditions)
                                    ->group(['code', 'order_date'])
                                    ->order(['WebOrderItems.code DESC', 'WebOrderItems.name ASC', 'WebOrders.order_date ASC'])
                                    ->all();
        
        $result = [];

        foreach ($data as $product) {
            if (isset($result[$product->code . '*'])) {
                $iterator = 0;

                foreach ($period as $day) {
                    if (isset($product->web_order->order_date)) {
                        $date = new DateTime($product->web_order->order_date);

                        if ($date->format('Y-m-d') == $day->format('Y-m-d')) {
                            foreach ($dataProducts as $pr) {
                                if ($pr->code == ($product->code . '*')) {
                                    if (isset($pr->product_recipes) && !empty($pr->product_recipes)) {
                                        foreach ($pr->product_recipes as $prr) {
                                            $value = $product->pocet_kusu;
                                            $valueForward = 0;

                                            while ($value >= $prr->value) {
                                                $value -= $prr->value;
                                                $valueForward++;
                                            }

                                            if ($value > 0) {
                                                $valueForward++;
                                            }
                                            
                                            $result[$product->code . '*'][$iterator] += $valueForward;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $iterator++;
                }
            } else {
                if (!isset($result[$product->code])) {
                    $result[$product->code] = ['', '', '', '', '', '', ''];
                }
                
                $iterator = 0;

                foreach ($period as $day) {
                    if (isset($product->web_order->order_date)) {
                        $date = new DateTime($product->web_order->order_date);

                        if ($date->format('Y-m-d') == $day->format('Y-m-d')) {
                            if ($product->pocet_kusu == 0) {
                                $result[$product->code][$iterator] = null;
                            } else {
                                $result[$product->code][$iterator] = $product->pocet_kusu;
                            }
                        }
                    }

                    $iterator++;
                }
            }
        }
        
        $this->set('dateBeginning', $dateBeginning);
        $this->set('result', $result);
        $this->set('data', $data);
        $this->set('period', $period);
    }

    /**
     * A simple form for workshop for employees.
     *
     * @param int $system_id - A system_id for the web order. Default 1 (Vítkovice).
     * @return void
     */
    public function order($system_id = 1)
    {
        $this->set('title', 'Výrobna');
        $this->loadModel('Branches');
        $this->loadModel('Products');
        $this->loadModel('ProductRecipes');
        $this->loadModel('Stocks');
        $this->loadModel('WebOrders');
        $this->loadModel('WebOrderItems');

        $this->branches_list = $this->Branches->branchesList();
        $this->set('branches_list', $this->branches_list);

        $dateTomorrow = new DateTime();
        $dateTomorrow->modify('+1 day');

        $conditionsProducts = [
            'view_workshop' => 1
        ];

        $dataProducts = $this->Products->find()
        ->select([
            'id',
            'name',
            'code',
            'view_workshop'
        ])
        ->contain([
            'ProductRecipes' => function ($q) {
                return $q->select([
                    'id',
                    'product_id',
                    'stock_item_global_id',
                    'value'
                ]);
            }
        ])
        ->where($conditionsProducts)
        ->all();
        
            // pr($dataWebOrderItems);
        $conditionsStocks = [
            'system_id' => $system_id
        ];

        $dataStocks = $this->Stocks->find()
        ->select([
            'id',
            'system_id',
            'stock_item_id',
            'stock_type_id',
            'value',
            'current_state' => 'SUM(IF(Stocks.stock_type_id IN (2,4,6,10,3,10), (value * -1), value))'
        ])
        ->where($conditionsStocks)
        ->group(['stock_item_id'])
        ->all();
        
        $conditionsWebOrderItemsToday = [
            'WebOrderItems.id IS NOT' => 2,
            'WebOrders.type_id' => 2,
            'WebOrders.system_id' => $system_id,
            'WebOrders.order_date' => $dateTomorrow
        ];

        $dataWebOrderItemsToday = $this->WebOrderItems->find()
        ->select([
            'id',
            'name',
            'code',
            'ean',
            'shop_order_id',
            'shop_product_id',
            'ks'
        ])
        ->contain([
            'WebOrders' => function ($q) {
                return $q->select([
                    'id',
                    'type_id',
                    'system_id',
                    'order_date'
                ]);
            }
        ])
        ->where($conditionsWebOrderItemsToday)
        ->all();
        $this->set('system_id', $system_id);

        // pokud je testovaci pobocka nastavit stahovani na avion
        if ($system_id == 9){
            $system_id = 1;
        }
        $conditionsWebOrderItems = [
            'WebOrderItems.id IS NOT' => 2,
            'WebOrders.type_id' => 1,
            'WebOrders.system_id' => $system_id,
            'WebOrders.order_date' => $dateTomorrow
        ];

        $dataWebOrderItems = $this->WebOrderItems->find()
        ->select([
            'id',
            'name',
            'code',
            'ean',
            'ks',
            'shop_order_id',
            'shop_product_id',
            'pocet_kusu' => 'sum(ks)'
        ])
        ->contain([
            'WebOrders' => function ($q) {
                return $q->select([
                    'id',
                    'type_id',
                    'system_id',
                    'order_date'
                ]);
            }
        ])
        ->where($conditionsWebOrderItems)
        // ->group(['shop_product_id'])
        ->group(['code'])
        ->all();
        $data = (object) [];
        // pr($dataProducts);
        // pr($dataWebOrderItems);
        if (!empty($dataProducts) && !empty($dataWebOrderItems)) {
            foreach ($dataProducts as $pr) {
                if (isset($pr->code) && $pr->code != null) {
                    if (!isset($data->{$pr->code})) {
                        $data->{$pr->code} = (object) [
                            'id'            => $pr->id,
                            'code'          => $pr->code,
                            'name'          => $pr->name,
                            'ean'           => null,
                            'stock'         => null,
                            'ks'            => null,
                            'ks_ks'         => null,
                            'total'         => 0,
                            'left'          => null,
                            'total_final'   => null
                        ];
                    }

                    if (isset($pr->product_recipes) && !empty($pr->product_recipes)) {
                        foreach ($pr->product_recipes as $prr) {
                            foreach ($dataStocks as $st) {
                                if ($prr->stock_item_global_id == $st->stock_item_id) {
                                    if ($st->current_state <= 0) {
                                        $data->{$pr->code}->stock = null;
                                    } else {
                                        $data->{$pr->code}->stock = number_format($st->current_state, 0);
                                    }
                                }
                            }
                        }
                    }
                    if (!empty($dataWebOrderItems)) {
                        foreach ($dataWebOrderItems as $woi) {
                            
                            if (isset($woi->code) && $woi->code != null) {
                                if ($pr->code == $woi->code) {
                                    $data->{$pr->code}->ean         = $woi->ean;
                                    $data->{$pr->code}->ks          = $woi->pocet_kusu;
                                    $data->{$pr->code}->total       = $data->{$pr->code}->total + $woi->pocet_kusu;
                                    $data->{$pr->code}->total_final = 0;
                                } elseif ($pr->code == ($woi->code . '*')) {
                                    // pr($pr->code . '/'. $woi->code. '/' .$woi->pocet_kusu);
                                    $data->{$pr->code}->ks_ks = 0;
                                    
                                    if (isset($pr->product_recipes) && !empty($pr->product_recipes)) {
                                        foreach ($pr->product_recipes as $prr) {
                                            $ks_ks = $woi->pocet_kusu;

                                            while ($ks_ks >= $prr->value) {
                                                $ks_ks -= $prr->value;
                                                $data->{$pr->code}->ks += 1;
                                            }

                                            if ($ks_ks == 0) {
                                                $data->{$pr->code}->ks_ks = null;
                                                $data->{$pr->code}->total = $data->{$pr->code}->ks;
                                            } else {
                                                // pr($woi);
                                                $data->{$pr->code}->ks_ks = $ks_ks;
                                                $data->{$pr->code}->left = $prr->value - $ks_ks;
                                                $data->{$pr->code}->total = $data->{$pr->code}->total + 1;
                                                // pr($woi->pocet_kusu.$data->{$pr->code}->total);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            foreach ($data as $pr) {
                $totalSum = 0;

                if (isset($data->{$pr->code})) {
                    if (isset($data->{$pr->code}->total) && $data->{$pr->code}->total > 0) {
                        $totalSum += $data->{$pr->code}->total;
                    }

                    $data->{$pr->code}->total_final = $totalSum;
                }
            }
        }
        
        
        $this->set('data', $data);
        $this->set('dataToday', $dataWebOrderItemsToday);
    }

    /**
     * Creates new web order and saves it with type_id 2.
     *
     * @return void
     */
    public function orderSend()
    {
        $this->loadModel('WebOrders');
        $this->loadModel('WebOrderItems');
        
        if (isset($this->request->data['items'])) {
            $defaultValues = [
                'web_order' => [
                    'type_id'               => 2,
                    'system_id'             => $this->request->data['system_id'],
                    'eshop_id'              => null,
                    'web_id'                => null,
                    'local_id'              => null,
                    'shop_client_id'        => null,
                    'shop_doprava_id'       => null,
                    'shop_platba_id'        => null,
                    'name'                  => '',
                    'status'                => 1,
                    'kos'                   => 0,
                    'nd'                    => 0,
                    'cms_user_id'           => null,
                    'price'                 => null,
                    'price_with_tax'        => null,
                    'pay_casch'             => null,
                    'pay_card'              => null,
                    'pay_gastro_ticket'     => null,
                    'pay_gastro_card'       => null,
                    'doprava_price'         => null,
                    'platba_price'          => null,
                    'platba_price_with_tax' => null,
                    'doprava_zdarma'        => 0,
                    'poznamka'              => '',
                    'poznamka_interni'      => '',
                    'pay'                   => 0,
                    'stav'                  => 1,
                    'odeslane_email'        => '',
                    'odeslane_sms'          => '',
                    'historie'              => '',
                    'fa_code'               => null,
                    'fa_code2'              => null,
                    'dopropis_code'         => null,
                    'fa_date'               => null,
                    'order_date'            => null, // required important
                    'pickup_time'           => null,
                    'gifted_name'           => null,
                    'gifted_phone'          => null,
                    'gdpr'                  => '-',
                    'delivery'              => 0,
                    'synchronized'          => null
                ],
                'web_order_items' => [
                    'system_id'             => $this->request->data['system_id'],
                    'eshop_id'              => null,
                    'web_id'                => null,
                    'local_id'              => null,
                    'shop_product_id'       => 0,
                    'pokl_product_id'       => 0,
                    'code'                  => null, // required important
                    'ean'                   => null, // required important
                    'name'                  => null, // required important
                    'status'                => 1,
                    'kos'                   => 0,
                    'nd'                    => 0,
                    'price'                 => null,
                    'price_with_tax'        => null,
                    'row_price'             => null,
                    'row_price_with_tax'    => null,
                    'ks'                    => null, // required important
                    'dph'                   => 0,
                    'shop_dodavatel_id'     => null,
                    'bezlepkovy'            => null,
                    'darkove_baleni'        => 0,
                    'cislovka'              => null,
                    'napis'                 => null
                ]
            ];

            if (isset($this->request->data['id']) && $this->request->data['id'] != null) {
                $conditions = [
                    'WebOrders.id' => $this->request->data['id']
                ];

                $webOrder = $this->WebOrders->find()
                ->select([
                    'id',
                    'type_id',
                    'system_id',
                    'order_date'
                ])
                ->contain([
                    'WebOrderItems' => function ($q) {
                        return $q->select([
                            'id',
                            'shop_order_id',
                            'code',
                            'ean',
                            'name',
                            'ks'
                        ]);
                    }
                ])
                ->where($conditions)
                ->first();

                $iterator = 0;
                $webOrderItems = [];
                $webOrderItemsNewAdded = [];
                
                foreach ($webOrder['web_order_items'] as $we) {
                    foreach ($this->request->data['items'] as $item) {
                        try {
                            if ($item['is_new'] == 1 && !in_array($item['code'], $webOrderItemsNewAdded)) {
                                $webOrderItems[$iterator]               = $defaultValues['web_order_items'];
                                $webOrderItems[$iterator]['name']       = $item['name'];

                                if ($item['ks'] != null) {
                                    $webOrderItems[$iterator]['ks']         = $item['ks'];
                                } else {
                                    $webOrderItems[$iterator]['ks']         = 0;
                                }
                                
                                $webOrderItems[$iterator]['code']       = $item['code'];
                                $webOrderItems[$iterator]['ean']        = $item['ean'];
                                array_push($webOrderItemsNewAdded, $item['code']);
                            } elseif ($we->code == $item['code'] || ($we->code == $item['code'] . '*')) {
                                $webOrderItems[$iterator]['id']         = $we['id'];
                                $webOrderItems[$iterator]['name']       = $we['name'];

                                if ($item['ks'] != null) {
                                    $webOrderItems[$iterator]['ks']         = $item['ks'];
                                } else {
                                    $webOrderItems[$iterator]['ks']         = 0;
                                }

                                $webOrderItems[$iterator]['code']       = $we['code'];
                                $webOrderItems[$iterator]['ean']        = $we['ean'];
                            }

                            $iterator++;
                        } catch (Exception $e) {
                            $errors[$item['code']] = $e->getMessage();
                        }
                    }
                }

                $data = [];
                $data['web_order_items'] = $webOrderItems;

                $entity = $this->WebOrders->patchEntity($webOrder, $data, ['associated' => [
                    'WebOrderItems' => ['validate' => false]
                ]]);
            } else {
                $iterator = 0;
                $webOrder = $defaultValues['web_order'];
                $webOrderItems = [];

                foreach ($this->request->data['items'] as $item) {
                    try {
                        $webOrderItems[$iterator]               = $defaultValues['web_order_items'];
                        $webOrderItems[$iterator]['name']       = $item['name'];

                        if ($item['ks'] != null) {
                            $webOrderItems[$iterator]['ks']         = $item['ks'];
                        } else {
                            $webOrderItems[$iterator]['ks']         = 0;
                        }
                        
                        $webOrderItems[$iterator]['code']       = $item['code'];
                        $webOrderItems[$iterator]['ean']        = $item['ean'];
                        $iterator++;
                    } catch (Exception $e) {
                        $errors[$item['code']] = $e->getMessage();
                    }
                }

                $dateTomorrow = new DateTime();
                $dateTomorrow->modify('+1 day');
                $webOrder['order_date'] = $dateTomorrow;
                $webOrder['web_order_items'] = $webOrderItems;

                $entity = $this->WebOrders->newEntity($webOrder, [
                    'associated' => [
                        'WebOrderItems' => ['validate' => false]
                    ]
                ]);
            }

            if ($resultDb = $this->WebOrders->save($entity)) {
                // success
            } else {
                // failure
            }
        }
        
        $this->redirect('/work-shop/order/' . $this->request->data['system_id']);
    }

     /**
     * 
     * 
     * 
     * 
     * 
     * @param string $date
     * @return void
     */
    public function dispatch($type = 1, $branchId = null, $date = null)
    {
        $types = [
            1 => 'Převodka na jméno',
            2 => 'Převodka volný prodej',
        ];
        $this->set('title', isset($types[$type]) ? $types[$type] : 'Převodka volný prodej');
        //$this->set('simpleLayout', true);

        if(!empty($this->request->data())){
            //Vytvoreni a ulozeni prevodky na danou pobocku
            if(($errors = $this->createMoveStock($this->request->data(), $type)) !== true){
                $this->Flash->set('Během ukládání došlo k chybě u některých záznamů: '. json_encode($errors), [
                    'element' => 'error'
                ]);
            }else{
                $this->Flash->set('Převodka byla úspěšně uložena');
                return $this->redirect(['action'=>'dispatch', $type, $branchId, $date]);
            }
        }   

        $date = new Date($date);

        //TODO nacteni prevodek pro danou pobocku a den aby nebylo mozno odeslat dalsi kusy na jiz odeslanou prevodku

        $orders = null;
        $stockItems = [];
        $stockMoves = [];
        $recipesCounts = [];
        $data = [];

        $this->loadModel('Branches');
        $branches = $this->Branches->find('list')->where(['id !='=> $this->vyrobna_system_id])->toArray();

        if($branchId){
            $this->loadModel('WebOrders');
            $this->loadModel('WebOrderItems');
            $this->loadModel('Products');

            $orderQuery = $this->WebOrders->find()->select(['id'])->where([
                'order_date' => $date->format('Y-m-d'),
                'system_id' => $branchId
            ]);
            $orderQuery->where([
                'type_id' => $type,
                $type == 1 ? '(eshop_id > 0 OR local_id > 0)' : 'local_id > 0'
            ]);
            $order_ids = $orderQuery->toArray();

            if($order_ids){
           
                $order_ids = Hash::extract($order_ids, '{n}.id');
                $orders = $this->WebOrderItems->find()
                                            ->select([  
                                                'id',
                                                'name',
                                                'shop_order_id',
                                                'code',
                                                'pocet_kusu' => 'sum(ks)'
                                            ])
                                            ->where([
                                                'shop_order_id IN'=> $order_ids
                                            ])
                                            ->group(['code'])
                                            ->toArray();
                $codes = Hash::extract($orders, '{n}.code');       
 
                if(!empty($codes)){                     
                    $stockData = $this->Products->find()
                                                ->select(['id','code'])
                                                ->contain(['ProductRecipes'=> function($q){
                                                    return $q->select(['value', 'stock_item_global_id','product_id']);
                                                }])
                                                ->where(['code IN'=> $codes])
                                                ->toArray();

                    foreach($stockData as &$item){
                        if(isset($item->product_recipes) ){
                            //TODO jak se ma chovat v pripade ze se sklada z vice receptur?
                            foreach($item->product_recipes as $rec){
                                $item->stock_item_id = $rec->stock_item_global_id;
                                $item->count_per_item = $rec->value;
                                $stockItems[$item->code] = $item;
                            }
                            unset($item->product_recipes);
                        }
                    }
                }
            }
            if(!empty($orders)){
                foreach($orders as $item){
                    if(isset($stockItems[$item->code])){
                        if(!isset($data[$stockItems[$item->code]->stock_item_id])){
                            $data[$stockItems[$item->code]->stock_item_id] = (object)[
                                'stock_item_id' => $stockItems[$item->code]->stock_item_id,
                                'name' => $item->name,
                                'count_item' => 0
                            ];
                        }
                        $data[$stockItems[$item->code]->stock_item_id]->count_item += $stockItems[$item->code]->count_per_item * $item->pocet_kusu;
                    }
                }
            }
            // pr($data);

            $this->loadModel('Stocks');
            $stockMoves = $this->Stocks->find()
                                        ->select([
                                            'count' => 'SUM(value)',
                                            'stock_item_id'
                                        ])
                                        ->where([
                                            'stock_type_id'=> $type == 1 ? 14 : 2, //Pouze rezervace minus
                                            'system_id_to'=> $branchId,
                                            'DATE(created)' => $date->format('Y-m-d')
                                        ])
                                        ->group('stock_item_id')
                                        ->toArray();
            $stockMoves = Hash::combine($stockMoves, '{n}.stock_item_id', '{n}.count');
        }
        $this->set('data', $data);
        $this->set('branches', $branches);
        $this->set('orders', $orders);
        $this->set('date', $date);
        $this->set('branchId', $branchId);
        $this->set('stockMoves', $stockMoves);
        $this->set('typeId', $type);
        $this->set('types', $types);
    }

    /**
     * Vytvoreni nove prevodky z dat zaslanym formularem ve funkci dispatch
     */
    private function createMoveStock($data, $type){

        $this->loadModel('Stocks');
        $this->loadModel('StockQueues');
        
        $defaultValues = [
            'system_id'=> $this->vyrobna_system_id, // $this->system_id,
            'system_id_to'=> null,
            'value'=> 1,
            'tax_id'=> 1,
            'unit_id'=> 1,
            "nakup_price" => null,
            "nakup_price_vat" => null,
            "nakup_price_total" => null,
            "nakup_price_total_vat" => null,
            'user_id'=> $this->loggedUser['id'],
            'user_name'=> $this->loggedUser['name'],
            'invoice'=> null,
            'note'=> null, 
            'stock_type_id'=> $type == 1 ? 14 : 2, //PREVOD MINUS -> PRI ODESLANI NA CLOUD JE NUTNE VYTVORIT NA DANE PROVOZOVNE PREVOD PLUS?
            'sended'=> null,
            "createdTime" => time()
        ];  
     
        $saveStock = [];
        foreach($data['items'] as &$item){  
            if(isset($item['count']) && $item['count'] > 0){
                $saveStock[] = $this->Stocks->newEntity(array_merge($defaultValues, [
                    'system_id_to' => $data['system_id_to'],
                    'value' => $item['count'] * 12,
                    'note' => $item['note'],
                    'stock_item_id' => $item['stock_item_id'],
                ]));
            }
        }
        $errors = [];
        if(!empty($saveStock)){
            foreach($saveStock as $item){
                try{
                    //Po ulozeni do skladovych pohybu je nutne ulozit take do fronty prevodek
                    if($this->Stocks->save($item)){
                        $saveQueue = array_merge($defaultValues, [
                            'system_id' => $data['system_id_to'],
                            'system_id_to' => $this->vyrobna_system_id,
                            'system_name_from' => 'Vyrobna',
                            'value' => $item->count * 12,
                            'note' => $item->note,
                            'stock_item_id' => $item->stock_item_id,
                        ]);
                        $tmpData = $saveQueue;
                        $tmpData['stock_type_id'] = $type == 1 ? 13 : 11; //Prevodka plus
                        $saveQueue['data'] = json_encode($tmpData);
 
                        $saveQueue = $this->StockQueues->newEntity($saveQueue);
                        $this->StockQueues->save($saveQueue);
                    }
                }catch(Exception $e){
                    $errors[$item['stock_item_id']] = $e->getMessage();
                }
            }
        }
        return empty($errors) ? true : $errors;
    }

}