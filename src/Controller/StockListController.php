<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class StockListController extends AppController{

    public function index(){
        $this->set('title','Skladové položky');
        $this->checkLogged();
        $this->loadComponent('ViewIndex');
        $this->genSubmenu();

        $cols = [
            'id'=>['name'=>'ID'],
            //'name'=>['name'=>'Název'],
            //'code'=>['name'=>'Kód'],
            //'is_connected'=>['name'=>'Napárováno','list'=>$this->yes_no],
            'jednotka_id'=>['name'=>'Jednotka','list'=>$this->stock_unit_list],
            'dodavatel_jednotka' =>['name'=>'Jednotka dodavatel'],
            'prevod'=>['name'=>'Převodní konstanta'],
        ];
        $filtrations = [
            'jednotka_id'=>['name'=>'Jednotka','key'=>'jednotka_id','type'=>'select','list'=>[''=>'Vyberte jednotku']+$this->stock_unit_list],
            'dodavatel_jednotka'=>['name'=>'Dodavatel jednotka','key'=>'dodavatel_jednotka','type'=>'like'],
        ];
        $topActions = [
            'edit'=>['name'=>'Nová položka','url'=>'/stocks-list/edit/'],
            //'products'=>['name'=>'Skladové produkty','url'=>'/stocks/products/'],
         //   'import'=>['name'=>'Import produktů','url'=>'/stocks/importProducts/','type'=>'ajax'],
        ];
        $posibility = [
            'edit'=>['name'=>'Editovat','url'=>'/stocks-list/edit/'],
            'trash'=>['name'=>'Smazat','url'=>'/trash/stock-list/'],
        ];

        //$conditions = ['system_id'=>$this->loggedUser->system_id];
        $conditions = [];
        $conditions = $this->ViewIndex->conditions($conditions);
        //pr($conditions);
        $mapper = function ($data, $key, $mapReduce) {
            if (!empty($data->connected)){
                $data->connected_check = 1;
            } else {

                $data->connected_check = 0;
            }
            $mapReduce->emit($data);
        };
        $data = $this->StockList->find()
            ->where($conditions)
            ->order('id DESC')
            ->select([

                'id',
                //'prevod',
                //'code',
                //'is_connected',
                //'full_name',
                'jednotka_id',
                'dodavatel_jednotka',
                'prevod',
            ])
            //->mapReduce($mapper)
        ;
        //->toArray();
        //pr($data->toArray());
        if (empty($data)){
            $data = null;
        }

        $params = [
            'filtrations'=>$filtrations,
            'topActions'=>$topActions,
            'cols'=>$cols,
            'posibility'=>$posibility,
            'data'=>$data,
        ];
        //pr($data->toArray());
        $this->ViewIndex->load($params);


    }

    
    public function edit($id = null){
        $this->loadModel('StockProducts');
        $this->set('stockProductsList',$this->stockProductsList = $this->StockProducts->productList());
        $this->set('stockProducts',$this->stockProducts = $this->StockProducts->productList(true));

        $this->set('simpleLayout',true);
	
        $this->checkLogged();
        $data = $this->StockList->newEntity();
        //pr($data);

        if ($id != null){
            $conditions = ['id'=>$id];
            $data = $this->StockList->find()
                ->where($conditions)
                //->select(['id'])
                ->first()
            ;

        }

        $this->set(compact("data"));

        if (!empty($this->request->data)){

            if (!empty($this->request->data['connected'])){
                $this->request->data['connected'] = json_encode($this->request->data['connected']);
                $this->request->data['is_connected'] = 1;
            } else {
                $this->request->data['is_connected'] = 0;

            }
            //pr($this->request->data);die();
            $saveData = $this->StockList->patchEntity($data,$this->request->data);

            $this->check_error($data);
            //pr($saveData);die();

            $this->StockList->save($saveData);
            die(json_encode(['result'=>true,'message'=>'Uloženo']));


        }
    }

    private function checkCode($data){
        $conditions = ['code'=>$data['code'], 'id !='=>$data['id']];
        $data = $this->StockProducts->find()
            ->where($conditions)
            ->select(['code'])
            ->first()
        ;
        if($data){
            die(json_encode(['result'=>false,'message'=>'Kód je již použit']));
        }

    }


    public function productsEdit($id = null){

        $this->checkLogged();
        $this->loadModel('StockList');
        $data = $this->StockList->newEntity();
        //pr($data);

        if ($id != null){
            $conditions = ['id'=>$id];
            $data = $this->StockList->find()
                ->where($conditions)
                //->select(['id'])
                ->first()
            ;

        }

        $this->set(compact("data"));

        if (!empty($this->request->data)){


            //pr($this->request->data);die();
            $this->checkCode($this->request->data); //overeni code
            $saveData = $this->StockProducts->patchEntity($data,$this->request->data);

            $this->check_error($data);
            //pr($saveData);die();

            $this->StocksList->save($saveData);
            die(json_encode(['result'=>true,'message'=>'Uloženo']));

        }
    }
    public function productsTrash($id){
        $this->loadModel('StockList');
        $conditions = ['id'=>$id];
        $this->StockProducts->trashAll($conditions);
        $this->redirect('/stocks/products/');


    }




}