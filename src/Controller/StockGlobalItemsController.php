<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class StockGlobalItemsController extends AppController{

    public function beforeFilter(\Cake\Event\Event $event){

        parent::beforeFilter($event);
        $this->loadModel('SkladCiselniks');
        $this->set('sklad_group_list',$this->sklad_group_list = $this->SkladCiselniks->skladGrouplList());
    }
    public function index(){

        $this->set('title','Globální skladové položky');
        $this->checkLogged();
        $this->loadComponent('ViewIndex');
        $this->genSubmenu();

        $cols = [
            'id'=>['name'=>'ID'],
            'name'=>['name'=>'Název'],
            //'code'=>['name'=>'Kód'],
            //'is_connected'=>['name'=>'Napárováno','list'=>$this->yes_no],
            //'id',
            //'name',
            //'stav_real',
            'jednotka_id'=>['name'=>'Jednotka','list'=>$this->stock_unit_list],
            'ean'=>['name'=>'ean'],
            'bidfood_id'=>['name'=>'Bidfood'],
            'group_id'=>['name'=>'Skupina','list'=>$this->sklad_group_list],
            'dodavatel_id'=>['name'=>'Dodavatel'],
            'min'=>['name'=>'min'],
            'max'=>['name'=>'max'],
            'tax_id'=>['name'=>'DPH','list'=>$this->price_tax_list],
            'created'=>['name'=>'Vytvořeno'],
        ];
        $filtrations = [
            'name'=>['name'=>'Název','key'=>'name','type'=>'like'],
            //'code'=>['name'=>'Kod','key'=>'code','type'=>'like'],
           'group_id'=>['name'=>'Skupina','key'=>'group_id','type'=>'select','list'=>[''=>'Skupina']+$this->sklad_group_list],
        ];
        $topActions = [
            'edit'=>['name'=>'Nová položka','url'=>'/global-items/edit/'],
            //'products'=>['name'=>'Skladové produkty','url'=>'/stocks/products/'],
            //'import'=>['name'=>'Import produktů','url'=>'/stocks/importProducts/','type'=>'ajax'],
        ];
        $posibility = [
            'edit'=>['name'=>'Editovat','url'=>'/global-items/edit/'],
            'trash'=>['name'=>'Smazat','url'=>'/global-items/trash/'],
        ];

        //$conditions = ['system_id'=>$this->loggedUser->system_id];
        $conditions = [];
        $conditions = $this->ViewIndex->conditions($conditions);
        //pr($conditions);
        $mapper = function ($data, $key, $mapReduce) {
            if (!empty($data->connected)){
                $data->connected_check = 1;
            } else {

                $data->connected_check = 0;
            }
            $mapReduce->emit($data);
        };
        $data = $this->StockGlobalItems->find()
            ->where($conditions)
            ->order('id DESC')
            ->select([

                //'id',
                //'name',
                //'code',
                //'is_connected',
                //'full_name',
                'id',
                'name',
                //'stav_real',
                'jednotka_id',
                'ean',
                'bidfood_id',
                'group_id',
                'dodavatel_id',
                'min',
                'max',
                'tax_id',
                'created',

            ])
            //->mapReduce($mapper)
        ;
        //->toArray();
        //pr($data->toArray());
        if (empty($data)){
            $data = null;
        }

        $params = [
            'filtrations'=>$filtrations,
            'topActions'=>$topActions,
            'cols'=>$cols,
            'posibility'=>$posibility,
            'data'=>$data,
        ];
        //pr($data->toArray());
        $this->ViewIndex->load($params);


    }

   

    public function edit($id = null){
        $this->loadModel('StockProducts');
        $this->set('stockProductsList',$this->stockProductsList = $this->StockProducts->productList());
        $this->set('stockProducts',$this->stockProducts = $this->StockProducts->productList(true));
        $this->checkLogged();
        $data = $this->StockGlobalItems->newEntity();
        //pr($data);

        if ($id != null){
            $conditions = ['id'=>$id];
            $data = $this->StockGlobalItems->find()
                ->where($conditions)
                //->select(['id'])
                ->first()
            ;

        }

        $this->set(compact("data"));

        if (!empty($this->request->data)){

            if (!empty($this->request->data['connected'])){
                $this->request->data['connected'] = json_encode($this->request->data['connected']);
                $this->request->data['is_connected'] = 1;
            } else {
                $this->request->data['is_connected'] = 0;

            }
            //pr($this->request->data);die();
            $saveData = $this->StockGlobalItems->patchEntity($data,$this->request->data);

            $this->check_error($data);
            //pr($saveData);die();

            $this->StockGlobalItems->save($saveData);

            Cache::clear();
            die(json_encode(['result'=>true,'message'=>'Uloženo']));

        }
    }

    private function checkCode($data){
        $conditions = ['code'=>$data['code'], 'id !='=>$data['id']];
        $data = $this->StockProducts->find()
            ->where($conditions)
            ->select(['code'])
            ->first()
        ;
        if($data){
            die(json_encode(['result'=>false,'message'=>'Kód je již použit']));
        }

    }


    public function productsEdit($id = null){

        $this->checkLogged();
        $this->loadModel('StockProducts');
        $data = $this->StockProducts->newEntity();
        //pr($data);

        if ($id != null){
            $conditions = ['id'=>$id];
            $data = $this->StockProducts->find()
                ->where($conditions)
                //->select(['id'])
                ->first()
            ;

        }

        $this->set(compact("data"));

        if (!empty($this->request->data)){


            //pr($this->request->data);die();
            $this->checkCode($this->request->data); //overeni code
            $saveData = $this->StockProducts->patchEntity($data,$this->request->data);

            $this->check_error($data);
            //pr($saveData);die();

            $this->StockProducts->save($saveData);
            die(json_encode(['result'=>true,'message'=>'Uloženo']));

        }
    }
    public function trash($id){
        $conditions = ['id'=>$id];
        $this->StockGlobalItems->trashAll($conditions);
        $this->redirect('/global-items/');


    }




}