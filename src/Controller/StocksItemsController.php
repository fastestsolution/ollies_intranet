<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class StocksItemsController extends AppController{

    public function index(){
        $this->set('title','Skladové položky');
        $this->checkLogged();
        $this->loadComponent('ViewIndex');
        $this->genSubmenu();

        $cols = [
            'id'=>['name'=>'ID'],
            'name'=>['name'=>'Název'],
            //'code'=>['name'=>'Kód'],
            //'is_connected'=>['name'=>'Napárováno','list'=>$this->yes_no],
        ];
        $filtrations = [
            'name'=>['name'=>'Název','key'=>'name','type'=>'like'],
            //'code'=>['name'=>'Kod','key'=>'code','type'=>'like'],
            //'is_connected'=>['name'=>'Napárováno','key'=>'is_connected','type'=>'select','list'=>[''=>'Napárováno']+$this->yes_no],
        ];
        $topActions = [
            'edit'=>['name'=>'Nová položka','url'=>'/stocks-items/edit/'], 
            //'products'=>['name'=>'Skladové produkty','url'=>'/stocks/products/'],
            //'import'=>['name'=>'Import produktů','url'=>'/stocks/importProducts/','type'=>'ajax'],
        ];
        $posibility = [
            'edit'=>['name'=>'Editovat','url'=>'/stocks-items/edit/'],
            'copy'=>['name'=>'Kopírovat','url'=>'/stocks-items/copy/','type'=>'ajax'],
		    'trash'=>['name'=>'Smazat','url'=>'/stocks-items/trash/'],
        ];

        //$conditions = ['system_id'=>$this->loggedUser->system_id];
        $conditions = [];
        $conditions = $this->ViewIndex->conditions($conditions);
        //pr($conditions);
        $mapper = function ($data, $key, $mapReduce) {
            if (!empty($data->connected)){
                $data->connected_check = 1;
            } else {

                $data->connected_check = 0;
            }
            $mapReduce->emit($data);
        };
        $data = $this->StocksItems->find()
            ->where($conditions)
            ->order('id DESC')
            ->select([

                'id',
                'name',
                //'full_name',

            ])
            //->mapReduce($mapper)
        ;
        //->toArray();
        //pr($data->toArray());
        if (empty($data)){
            $data = null;
        }

        $params = [
            'filtrations'=>$filtrations,
            'topActions'=>$topActions,
            'cols'=>$cols,
            'posibility'=>$posibility,
            'data'=>$data,
        ];
        //pr($data->toArray());
        $this->ViewIndex->load($params);


    }

   
   

    public function edit($id = null){
        $this->loadModel('StocksItems');
        $this->loadModel('StockProducts');
        $this->set('stockProductsList',$this->stockProductsList = $this->StockProducts->productList());
        $this->set('stockProducts',$this->stockProducts = $this->StockProducts->productList(true));

        $this->loadModel('SkladCiselniks');
        $this->set('supplier_list',$this->supplier_list = $this->SkladCiselniks->supplierlList());

        $this->loadModel('StockGlobalItems');
        $stock_global_list = $this->StockGlobalItems->skladItemsList();
        $this->set('stock_global_list',$stock_global_list);

        $this->set('simpleLayout',true);
        
        $this->checkLogged();
        $data = $this->StocksItems->newEntity();

        //pr($data);

        if ($id != null){
            $conditions = ['id'=>$id];
            $data = $this->StocksItems->find()
                ->where($conditions)
                //->select(['id'])
                ->first()
            ;

        }

        $this->set(compact("data"));

        if (!empty($this->request->data)){

            if (!empty($this->request->data['connected'])){
                $this->request->data['connected'] = json_encode($this->request->data['connected']);
                $this->request->data['is_connected'] = 1;
            } else {
                $this->request->data['is_connected'] = 0;

            }
            //pr($this->request->data);die();
            $saveData = $this->StocksItems->patchEntity($data,$this->request->data);

            $this->check_error($data);
            //pr($saveData);die();

            $this->StocksItems->save($saveData);
            die(json_encode(['result'=>true,'message'=>'Uloženo']));

        }
    }

    private function checkCode($data){
        $conditions = ['code'=>$data['code'], 'id !='=>$data['id']];
        $data = $this->StockProducts->find()
            ->where($conditions)
            ->select(['code'])
            ->first()
        ;
        if($data){
            die(json_encode(['result'=>false,'message'=>'Kód je již použit']));
        }

    }


    public function productsEdit($id = null){

        $this->checkLogged();
        $this->loadModel('StockProducts');
        $data = $this->StockProducts->newEntity();
        //pr($data);

        if ($id != null){
            $conditions = ['id'=>$id];
            $data = $this->StockProducts->find()
                ->where($conditions)
                //->select(['id'])
                ->first()
            ;

        }

        $this->set(compact("data"));

        if (!empty($this->request->data)){


            //pr($this->request->data);die();
            $this->checkCode($this->request->data); //overeni code
            $saveData = $this->StockProducts->patchEntity($data,$this->request->data);

            $this->check_error($data);
            //pr($saveData);die();

            $this->StockProducts->save($saveData);
            die(json_encode(['result'=>true,'message'=>'Uloženo']));

        }
    }
    public function trash($id){
        $this->loadModel('StocksItems');
        $conditions = ['id'=>$id];
        $this->StocksItems->trashAll($conditions);
        $this->redirect('/stocks-items/');


    }
    public function productsTrash($id){
        $this->loadModel('StockProducts');
        $conditions = ['id'=>$id];
        $this->StockProducts->trashAll($conditions);
        $this->redirect('/stocks/products/');


    }

    public function copy($id){
        header('Content-Type: text/html; charset=utf-8');
		$data_load = $this->StocksItems->find()
		->where(['StocksItems.id'=>$id])
		->select([])
		// ->contain(['ProductConnects','ProductConnAddons','ProductRecipes','ProductPrices'])
		->map(function($row){
			//pr($row);
			// if (isset($row->product_conn_addons)){
			// 	foreach($row->product_conn_addons AS $k=>$ra){
			// 		$row->product_conn_addons[$k]->id = '';
			// 		$row->product_conn_addons[$k]->product_id = '';
			// 		//$row->product_conn_addons[$k]->id = null;
			// 		//unset($row->product_conn_addons[$k]->id);
			// 		unset($row->product_conn_addons[$k]->created);
			// 		unset($row->product_conn_addons[$k]->modified);
			// 	}
			// }
			// if (isset($row->product_connects)){
			// 	foreach($row->product_connects AS $k=>$ra){
			// 		//unset($row->product_connects[$k]->id);
			// 		$row->product_connects[$k]->id = '';
			// 		$row->product_connects[$k]->product_id = '';
			// 		unset($row->product_connects[$k]->created);
			// 		unset($row->product_connects[$k]->modified);
			// 	}
			// }
            
            // if (isset($row->product_prices)){
			// 	foreach($row->product_prices AS $k=>$ra){
			// 		//unset($row->product_connects[$k]->id);
			// 		$row->product_prices[$k]->id = '';
			// 		$row->product_prices[$k]->product_id = '';
			// 		unset($row->product_prices[$k]->created);
			// 		unset($row->product_prices[$k]->modified);
			// 	}
			// }
            
            // if (isset($row->product_recipes)){
			// 	foreach($row->product_recipes AS $k=>$ra){
			// 		//unset($row->product_connects[$k]->id);
			// 		$row->product_recipes[$k]->id = '';
			// 		$row->product_recipes[$k]->product_id = '';
			// 		unset($row->product_recipes[$k]->created);
			// 		unset($row->product_recipes[$k]->modified);
			// 	}
			// }
            // unset($row->lock);
            $row->code = '';
			$row->name = $row->name.' - kopie';
			$row->id = '';
            unset($row->created);
            unset($row->modified);
			return $row;
		})
		->first()
		->toArray();
        // pr($data_load);die();
        //unset($data_load->id);
		
		//$data_load->id = '';
		unset($data_load['created']);
		unset($data_load['modified']);
		// unset($data_load->name);
		// pr($data_load);die();
		$cfg = [];
        //$cfg['validate'] = 'OnlyCheck';
        $this->StocksItems->validator()->remove('code');
		$save_clone = $this->StocksItems->newEntity($data_load);
		// pr($save_clone);die();
		$res = $this->StocksItems->save($save_clone);
		// pr($res);
		die(json_encode(['result'=>true,'id'=>$res->id,'message'=>'Položka zkopírována','redirect'=>'self']));  
    }





}