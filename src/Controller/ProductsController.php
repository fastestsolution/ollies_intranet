<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Utility\Hash;
use Cake\Cache\Cache;

//use App\View\Helper\FastestHelper;
use Cake\Datasource\ConnectionManager;

class ProductsController extends AppController{
    
	/*public function logout(){
		$session = $this->request->session();
		$session->delete('loggedUser');
		$this->redirect('/');
		
	}
	*/
	/*public function login(){
		if (!empty($this->request->data)){
			//pr(md5($this->request->data['password']));
			$find = $this->Users->find()
			->where([
				'username'=>$this->request->data['username'],
				'password'=>md5($this->request->data['password']),
			])
			->first();
			if (!$find){
				$result = ['result'=>false,'message'=>'Uživatel nenalezen'];
			} else {
				$result = ['result'=>true,'message'=>'Uživatel bude přihlášen'];
				$session = $this->request->session();
				$session->write('loggedUser',$find);
				
				
			}
			//pr($this->request->data);die();
			
			die(json_encode($result));
		}
		
	}*/
	
    public function index(){
        $this->checkLogged();
        $this->set('title','Správa produktů');
        $this->set('sendUpdate',true);
		//die('aaa');
		$this->loadComponent('ViewIndex');
        $this->genSubmenu();

        $this->set('swithBranches',true);
        if($this->request->here == '/products') $this->redirect('/products/');

        //pr($cols['price_without_tax']);

        $this->loadModel('ProductGroups');
        $this->group_list = $this->ProductGroups->groupList();

		$cols = [
			'id'=>['name'=>'ID'],
			'name'=>['name'=>'Název produktu'],
            'code'=>['name'=>'Kod Produktu'],
       //     'product_group_ide'=>['name'=>'Skupina produktu'],
            'num'=>['name'=>'Číslo produktu'],
            // 'price'=>['name'=>'Cena s DPH'],
            'is_dashboard'=>['name'=>'Dashboard','list'=>$this->yes_no],
            'is_mirror'=>['name'=>'Zrcadlo','list'=>$this->yes_no],
         //   'price_without_tax'=>['name'=>'Cena bez DPH'],
			//'amount'=>['name'=>'Množství'],
            'created'=>['name'=>'Vytvořeno'],

        ];
        if (!empty($this->branches_list)){
            $i = 0;
            foreach($this->branches_list AS $bid=>$b){

                $cols['branch'.$bid] = ['name'=>$b,'color'=> (($i++ % 2) ? '' : '#efefef')];
                
            }
        }
        $cols['reserve'] = ['name' => 'Rezerva'];
        $cols['view_api'] = ['name' => 'Zobrazit na webu', 'list' => $this->yes_no];
        $cols['view_workshop'] = ['name' => 'Zobrazit ve výrobně', 'list' => $this->yes_no];
        // pr($cols);die();
		$topActions = [
			'edit'=>['name'=>'Nová položka','url'=>'/products/edit/'],
		];
		$filtrations = [
			'id'=>['name'=>'ID','key'=>'id','type'=>'text'],
			'name'=>['name'=>'Název produktu','key'=>'name','type'=>'like'],
			'product_group_id'=>['name'=>'Skupina','key'=>'ProductConnects__product_group_id','type'=>'select','list'=>[''=>'Skupina']+$this->group_list],
			'is_dashboard'=>['name'=>'Dashboard','key'=>'is_dashboard','type'=>'select','list'=>[''=>'Dashboard']+$this->yes_no],
			'is_mirror'=>['name'=>'Zrcadlo','key'=>'is_mirror','type'=>'select','list'=>[''=>'Zrcadlo']+$this->yes_no],
		];
		$posibility = [
			'edit'=>['name'=>'Editovat','url'=>'/products/edit/'],
			'copy'=>['name'=>'Kopírovat','url'=>'/products/copy/','type'=>'ajax'],
			'trash'=>['name'=>'Smazat','url'=>'/products/trash/'],
		];
		
		// $conditions = ['id'=>9];
		$conditions = [];
		$conditions = $this->ViewIndex->conditions($conditions);
        // pr($conditions);
    	// $conditions = [];
	    
        $mapper = function ($data, $key, $mapReduce) {
            if (!empty($data->product_prices)){
                $product_prices = Hash::combine((array)$data->product_prices, '{n}.system_id', '{n}');
                $branch = [];
                
                foreach($product_prices AS $k=>$p){
                    if (!isset($branch[$k])){
                        $branch[$k] = '';
                    }
                    if ($p['checked'] == 1){
                        $branch[$k] .= '<span class="fa fa-check"></span>';                    
                    } else {

                        $branch[$k] .= '<span class="fa fa-close"></span>';
                    }
                    if (!empty($p['price'])) $branch[$k] .= ' Cena: '.$p['price'].',-';
                    if (!empty($p['price2'])) $branch[$k] .= ' | Zam: '.$p['price2'].',-';
                    
                                        
                    $data->{'branch'.$k} = $branch[$k];
                    unset($product_prices[$k]);
                }
                // pr($product_prices);
                unset($data->product_prices) ;
                //$data = array_merge($data,$product_prices);
            }
            $mapReduce->emit($data);
        };

		$data = $this->Products->find()
        ->where($conditions)
        ->contain(['ProductPrices','ProductConnects'])
        ->mapReduce($mapper)
        ->order('id DESC')
        ->select([
			'id',
			'name',
			'code',
			//'web_id',
			'product_group_id',
			//'web_group_id',
			'num',
			'price',
            'price_without_tax',//=>'price*'.$this->select_config['price_tax_list_con']
            'amount',
            'created',
            'is_mirror',
            'is_dashboard',
            'reserve',
            'view_api',
            'view_workshop'
        ]);
        
		//->toArray();
		// pr($data->toArray());
		if (empty($data)){
			$data = null;
		}
		/*foreach($data as &$item){
            if((isset($item->price_without_tax))&&($item->price_without_tax == NULL)&&(isset($item->price))&&($item->price > 0)){
                $item->price_without_tax = $item->price*$this->select_config['price_tax_list_con'];
            }
        }*/

		$params = [
			'filtrations'=>$filtrations,
			'topActions'=>$topActions,
			'cols'=>$cols,
			'posibility'=>$posibility,
			'data'=>$data,
		];
        
        $this->getUpdate();

		$this->ViewIndex->load($params);
		
	
    }

    public function getRecipes(){
        $load = $this->Products->find()
        ->contain(['ProductRecipes'])
        ->toArray();
        $noRec = [];
        foreach($load AS $l){
            if (empty($l->product_recipes)){

                $noRec[] = $l;
            }
        }
        pr($noRec);
        die();
    }

    public function getUpdate(){
        $path = 'uploaded/updated.txt';
        $last_update = '';
        if (file_exists($path)){
            $file = file_get_contents($path);
            $last_update = $file;
            //pr($file);
        }
        $this->set('last_update',$last_update);
    }

    public function sendUpdate(){
        $this->writeUpdateDate();
        die(json_encode(['result'=>true,'message'=>'Aktualizace poslána na pokladny']));
    }

    public function copy($id){
        header('Content-Type: text/html; charset=utf-8');
		
		$data_load = $this->Products->find()
		->where(['Products.id'=>$id])
		->select([])
		->contain(['ProductConnects','ProductConnAddons','ProductRecipes','ProductPrices'])
		->map(function($row){
			//pr($row);
			if (isset($row->product_conn_addons)){
				foreach($row->product_conn_addons AS $k=>$ra){
					$row->product_conn_addons[$k]->id = '';
					$row->product_conn_addons[$k]->product_id = '';
					//$row->product_conn_addons[$k]->id = null;
					//unset($row->product_conn_addons[$k]->id);
					unset($row->product_conn_addons[$k]->created);
					unset($row->product_conn_addons[$k]->modified);
				}
			}
			if (isset($row->product_connects)){
				foreach($row->product_connects AS $k=>$ra){
					//unset($row->product_connects[$k]->id);
					$row->product_connects[$k]->id = '';
					$row->product_connects[$k]->product_id = '';
					unset($row->product_connects[$k]->created);
					unset($row->product_connects[$k]->modified);
				}
			}
            
            if (isset($row->product_prices)){
				foreach($row->product_prices AS $k=>$ra){
					//unset($row->product_connects[$k]->id);
					$row->product_prices[$k]->id = '';
					$row->product_prices[$k]->product_id = '';
					unset($row->product_prices[$k]->created);
					unset($row->product_prices[$k]->modified);
				}
			}
            
            if (isset($row->product_recipes)){
				foreach($row->product_recipes AS $k=>$ra){
					//unset($row->product_connects[$k]->id);
					$row->product_recipes[$k]->id = '';
					$row->product_recipes[$k]->product_id = '';
					unset($row->product_recipes[$k]->created);
					unset($row->product_recipes[$k]->modified);
				}
			}
            unset($row->lock);
            $row->code = '';
			$row->name = $row->name.' - kopie';
			$row->id = '';
			return $row;
		})
		->first()
		->toArray();
        // pr($data_load);die();
        //unset($data_load->id);
		
		//$data_load->id = '';
		unset($data_load['created']);
		unset($data_load['modified']);
		// unset($data_load->name);
		// pr($data_load);die();
		$cfg = [];
        //$cfg['validate'] = 'OnlyCheck';
        $this->Products->validator()->remove('code');
		$save_clone = $this->Products->newEntity($data_load);
		// pr($save_clone);die();
		$res = $this->Products->save($save_clone,['associated' => ['ProductConnects','ProductConnAddons','ProductRecipes','ProductPrices']]);
		//pr($res);
		die(json_encode(['result'=>true,'id'=>$res->id,'message'=>'Položka zkopírována','redirect'=>'self']));  
    }

    public function edit($id = null){
		
		$this->checkLogged();
        $data = $this->Products->newEntity();
        $this->loadModel('MenuItems');
        $menu_list = $this->MenuItems->menuList();
        $this->loadModel('ProductGroups');

        $productGroups = $this->ProductGroups->find()
        ->select([
            'id',
            'parent_id',
            'level'
        ])
        ->all();
        $this->set('productGroups', json_encode($productGroups));

		if ($id != null){
            $conditions = ['id'=>$id];
            
            $mapper = function ($data, $key, $mapReduce) {
                if (!empty($data->product_prices)){
                    // $product_prices_selected = Hash::combine((array)$data->product_prices, '{n}.system_id', '{n}');
                }
                $mapReduce->emit($data);
            };

			$data = $this->Products->find()
            ->where($conditions)
            ->contain(['ProductConnects','ProductConnAddons','ProductRecipes','ProductPrices'])
            ->mapReduce($mapper)
            ->first()
            ;
		    // pr($data);die();
        /*
            if (empty($data->product_prices)){
                $product_prices['product_prices'][1] = [
                    'system_id'=>1,
                    'price'=>$data->product_connects[0]->price,
                    'price2'=>$data->product_connects[0]->price2,
                    'price3'=>$data->product_connects[0]->price3,
                    'price4'=>$data->product_connects[0]->price4,
                ];
                //$product_prices; 
                $data = $this->Products->patchEntity($data,$product_prices);

            }
            */
            $data->product_prices = Hash::combine((array)$data->product_prices, '{n}.system_id', '{n}');

            // pr($data);     
            $product_prices_selected = Hash::combine((array)$data->product_prices, '{n}.system_id', '{n}.checked');
            // pr($product_prices_selected);
            $this->set('product_prices_selected',$product_prices_selected);
        } else {
            $this->set('product_prices_selected',[]);
     
        }




        if (isset($data->product_connects) && !empty($data->product_connects)){
            $products_group = [];
            $pcon = [];
            foreach($data->product_connects AS $pc){
                $pcon[$pc->product_group_id] = $pc;
                //pr($pc);
                $products_group[] = $pc->product_group_id;
            }
            $data->product_connects = $pcon;
            //pr($pcon);
            $this->set('products_group',$products_group);
        }


        if (isset($data->product_conn_addons) && !empty($data->product_conn_addons)){
            $addon_list_load = [];
            foreach($data->product_conn_addons AS $pc){
                //pr($pc);
                $addon_list_load[] = $pc->product_addon_id;
            }
            $this->set('addon_list_load',$addon_list_load);
        }


        $this->loadModel('StockGlobalItems');
        $this->set('sklad_item_list',$this->sklad_item_list = $this->StockGlobalItems->stockItemsList());
        $this->set('unit_list',$this->unit_list = $this->StockGlobalItems->stockGlobaUnitList());
        $this->loadModel('ProductAddons');
        $addons = $this->ProductAddons->find()
            ->where([])
            ->toArray();

        $privlastky_list = [];
        foreach($addons AS $a){
            $privlastky_list[$a->group_id][] = $a;
        }
        $this->set('privlastky_list',$privlastky_list);

        
        
        $this->loadModel('SkladCiselniks');
        $this->set('stock_group_list',$this->stock_group_list = $this->SkladCiselniks->skladGrouplList());
		//$data->password_tmp = $data->password2 = $data->password;
		$this->set(compact("data",'menu_list'));
        
		if (!empty($this->request->data)){
			if (!empty($this->request->data['product_prices'])){
                //pr($this->request->data['product_prices']);
                foreach($this->request->data['product_prices'] AS $system_id=>$item){
                    //pr($item['system_id']);
                    $this->request->data['product_prices'][$system_id]['system_id'] = $system_id;
                    if ($item['system_id'] > 0){
                        $this->request->data['product_prices'][$system_id]['checked'] = 1;
                   
                    } else {
                        $this->request->data['product_prices'][$system_id]['checked'] = 0;
                        // $this->request->data['product_prices'][$system_id]['price'] = null;
                        // $this->request->data['product_prices'][$system_id]['price2'] = null;
                        // $this->request->data['product_prices'][$system_id]['price3'] = null;
                        // $this->request->data['product_prices'][$system_id]['price4'] = null;
                    }
                }
                if ($this->request->data['id'] > 0){
                    $this->loadModel('ProductPrices');
                    $this->ProductPrices->deleteAll(['product_id' => $this->request->data['id']]);
                } 
            } else {
                if ($this->request->data['id'] > 0){
                    $this->loadModel('ProductPrices');
                    $this->ProductPrices->deleteAll(['product_id' => $this->request->data['id']]);
                }
            }
			// pr($this->request->data);die();
            // pr($this->request->data['product_prices']);die();
            $this->checkCode();

            //Osetreni desetinne carky v mnozstvi
            if(isset($this->request->data['product_recipes']) && !empty($this->request->data['product_recipes'])){
                foreach($this->request->data['product_recipes'] as &$rec){
                    if(isset($rec['value']) && strpos( $rec['value'], ',') !== false){
                        $rec['value'] = str_replace(',','.',$rec['value']);
                    }
                }
            }
         
			$saveData = $this->Products->patchEntity($data,$this->request->data,['associated' => ["ProductRecipes",'ProductPrices']]);
            $this->check_error($data);
              

            if ($result = $this->Products->save($data)) {
                
                $this->loadModel('ProductConnects');
                $this->loadModel('ProductRecipes');
                //pr($result);
                if (!empty($result->product_recipes)){
                    //$receptura_ids = Hash::combine($result->product_recipes, '{n}.id','{n}.id');      //Joke by Tajsnn :-)
                    $receptura_ids = Hash::extract($result->product_recipes, '{n}.id');
                    $this->ProductRecipes->deleteAll(['id NOT IN' => $receptura_ids,'product_id' => $result->id]);
                } else {
                    $this->ProductRecipes->deleteAll(['product_id' => $result->id]);
                }

                if (!empty($this->request->data['product_connects'])){
                    foreach($this->request->data['product_connects'] AS $pid=>$data){
                        if ($data['checked'] == 0){
                            unset($this->request->data['product_connects'][$pid]);
                        }
                    }
                    //pr($this->request->data['product_connects']);die();
                    $this->ProductConnects->deleteAll(['product_id' => $result->id]);

                    foreach($this->request->data['product_connects'] AS $k=>$group){
                        if (!empty($group)){
                            $save_connect = $this->ProductConnects->newEntity([
                                'product_group_id'=>$k,
                                /*
                                'price'=>$group['price'],
                                'price2'=>$group['price2'],
                                'price3'=>$group['price3'],
                                'price4'=>$group['price4'],
                                */
                                'pref'=>$group['pref'],
                                'product_id'=>$result->id,
                            ]);
                            //pr($save_connect);die();
                            $this->ProductConnects->save($save_connect);
                            /*
                            $this->Products->updateAll(
                                ['group_id'.($k+1) => $group], // fields
                                ['id' => $result->id]
                            );
                            */
                        }
                    }

                }
                if (!empty($this->request->data['addon_list'])){
                    $this->loadModel('ProductConnAddons');
                    $this->ProductConnAddons->deleteAll(['product_id' => $result->id]);
                    foreach($this->request->data['addon_list']['addon_id'] AS $k=>$addon){
                        if ($addon == 1){
                            $save_connect = $this->ProductConnAddons->newEntity([
                                'product_addon_id'=>$k,
                                'product_id'=>$result->id,
                            ]);
                            $this->ProductConnAddons->save($save_connect);
                        }
                    }

                }

                //pr($this->request->data['product_connects'])
                //pr($result->id);
                Cache::clear(false);
                die(json_encode(['result'=>true,'message'=>__('Uloženo'),'id'=>$result->id]));
            }
			//pr($saveData);die();
			$this->Products->save($saveData);
           	die(json_encode(['result'=>true,'message'=>'Uloženo']));
			  
        }

    }

    private function checkCode(){
        //pr($this->request);
        $con = [
            'code'=>$this->request->data['code'],
        ];
        if (!empty($this->request->data['id'])){
            $con['id !='] = $this->request->data['id'];
        }

        $find = $this->Products->find()
        ->select([])
        ->where($con)
        ->first();
        if ($find){
            $results = [
                'result'=>false,
                'message'=>__('Kód produktu je již použit u produktu: '.$find->name.'('.$find->id.')'),
            ];
            die(json_encode($results));  
    
        }
    }

    public function trash($id){
        $this->loadModel('Products');
        $conditions = ['id'=>$id];
        $this->Products->trashAll($conditions); 
        $this->redirect('/products/');


    }


    public function importPrice(){
        $data = $this->Products->find()
            ->where()
            ->contain(['ProductConnects','ProductPrices'])
            ->toArray();
            // pr($data);
        foreach($data AS $k=>$item){    
            // pr($data[$k]->product_prices);
            if (empty($data[$k]->product_prices) && !empty($data[$k]->product_connects)){
                $prices = [
                    'price'=>null,
                    'price2'=>null,
                    'price3'=>null,
                    'price4'=>null,
                ];
                foreach($data[$k]->product_connects AS $pc){
                    if (!empty($pc->price)){
                        $prices['price'] = $pc->price;
                        $prices['price2'] = $pc->price2;
                        $prices['price3'] = $pc->price3;
                        $prices['price4'] = $pc->price4;
                    }
                }
                // pr($prices);
                //die('a');
                $product_prices['product_prices'][1] = [
                    'system_id'=>1,
                    'checked'=>1,
                    'price'=>$prices['price'],
                    'price2'=>$prices['price2'],
                    'price3'=>$prices['price3'],
                    'price4'=>$prices['price4'],
                ];
                //$product_prices; 
                $product_prices['product_prices'][2] = [
                    'system_id'=>2,
                    'checked'=>0,
                    'price'=>null,
                    'price2'=>null,
                    'price3'=>null,
                    'price4'=>null,
                ];
                $product_prices['product_prices'][3] = [
                    'system_id'=>3,
                    'checked'=>0,
                    'price'=>null,
                    'price2'=>null,
                    'price3'=>null,
                    'price4'=>null,
                ];
                $product_prices['product_prices'][4] = [
                    'system_id'=>4,
                    'checked'=>0,
                    'price'=>null,
                    'price2'=>null,
                    'price3'=>null,
                    'price4'=>null,
                ];
                //pr($product_prices); 
                $data[$k] = $this->Products->patchEntity($data[$k],$product_prices);
                $result = $this->Products->save($data[$k]);
                // or
                // pr($data[$k]);die();
            }
        }
        die('emd');    
    }

    public function getStocksItems($stock_group_id){
        $this->loadModel('StockGlobalItems');
        $items = $this->StockGlobalItems->stockItemsList($stock_group_id);
        if (isset($this->request->query['debug'])){
            pr($items);
        }
        die(json_encode(['data'=>$items]));
      }
    
  function apiProducts(){
        $this->loadModel('Products');
        $data = $this->Products->find()
            //->where()
            ->select(['id','name','code','price'])->toArray();
        //pr($data);
        die(json_encode($data));
  }

}
