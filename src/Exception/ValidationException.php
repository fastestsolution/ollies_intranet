<?php

namespace App\Exception;

use Cake\Core\Exception\Exception;

class ValidationException extends Exception
{

    public function __construct($message, $errors, $code = 1100, $previous = null)
    {
        if (is_array($errors)) {
            $this->validation_errors = $errors;
           // $message = vsprintf($this->_messageTemplate, $message);*/
        }
        parent::__construct($message, $code, $previous);
       
    }
}
