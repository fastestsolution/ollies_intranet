<?php
namespace App\Error;
use Rollbar\Rollbar;
use Rollbar\Payload\Level;
use Cake\Error\BaseErrorHandler;
use Cake\Error\ExceptionRenderer;
use Cake\Core\Configure;

class AppError extends BaseErrorHandler
{
    protected $_options = [];

    /**
     * zobrazeni warning
     */
    public function _displayError($error, $debug)
    {
        // echo '<div style="background:red; color:#fff;padding: 3px;">Warning aplikace!</div>';
        // echo '<div style="background:#efefef; color:#000;padding: 3px;">'. var_dump($error) .'</div>';
        pr($error);
    }

    /**
     * zachyceni exception z cake
     */
    public function _displayException($exception) 
    {

        $this->initReporting();
		
        echo '<div style="padding:5px; font-family:arial;">';   
        echo '<div style="background:red; color:#fff;padding: 3px; margin-bottom: 10px;">Chyba aplikace!</div>';
        echo $exception->getMessage().' <br />';
		echo '<div style="text-align: center">';
		echo '<p>Nebojte tato chyba byla odeslaná na podporu pro její diagnostiku</p>';
		// echo '<img src="http://sources.fastestdev.cz/snek_oci_350.jpg">';
//        echo 'FILE: '.$exception->getFile();
  //      echo ' LINE: '.$exception->getLine();
		echo '</div>'; 
        echo '<div style="background: #efefef;">';
        pr($exception);
        echo '</div>';
        echo '</div>';
        // ExceptionRenderer::render();
    }
    
    /**
     * kontrola o jake prostredi se jedna
     */
    private function checkEnv(){
        if (strpos($_SERVER['HTTP_HOST'],'localhost') > 0){
            $env = 'local';
        } else if (strpos($_SERVER['HTTP_HOST'],'fastestdev.cz') > 0){
            $env = 'development';
        } else {
            $env = 'production';
        }
        return $env;
    }

    /**
     * inicializace reportovaci aplikace
     */
    public function initReporting(){
        
        $env = $this->checkEnv();
        
        session_name('NMCORE');
		if (session_status() < 1) {
            session_start();
        }
        // print_r($_SESSION);die('bb');
        
        if ($env != 'local' || Configure::read('monitoring')['enableLocal']){
            
            $config = array(
                'access_token' => Configure::read('monitoring')['access_token'],
                'environment' => $env,
                'post_client_item'=> 'f615d7a64f11448ab38b0bef7339ae28',
            );  
            if (isset($_SESSION['Auth']['User']['id'])){
                $config['person'] = array(
                    'id' => $_SESSION['Auth']['User']['id'], // required - value is a string
                    'username' => $_SESSION['Auth']['User']['name'], // optional - value is a string
                );
            }
            Rollbar::init($config);
        }
        $_ENV['env'] = $env;
    }

}