<?php

$sql = array (
  'orders' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'order_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'terminal_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'local_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'user_name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'table_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'table_type_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
        'default' => '1',
      ),
      'note' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'close_order' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'total_price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'payment_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
        'default' => '1',
      ),
      'createdTime' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'storno' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'storno_type' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '4',
      ),
      'fik' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '250',
      ),
      'bkp' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '250',
      ),
      'table_open' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'table_close' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'table_paid' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'table_close_history' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'pay_casch' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_card' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_voucher' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_discount' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_employee' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_company' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'pay_gastro_ticket' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'pay_gastro_card' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'print_user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'storno_user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'storno_reason' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'table_name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'delivery' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
    ),
    'indexs' => 
    array (
      'close_order' => 
      array (
        'col' => 
        array (
          0 => 'close_order',
        ),
      ),
      'created' => 
      array (
        'col' => 
        array (
          0 => 'created',
        ),
      ),
      'createdTime' => 
      array (
        'col' => 
        array (
          0 => 'createdTime',
        ),
      ),
      'local_id' => 
      array (
        'col' => 
        array (
          0 => 'local_id',
        ),
      ),
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'payment_id' => 
      array (
        'col' => 
        array (
          0 => 'payment_id',
        ),
      ),
      'print_user_id' => 
      array (
        'col' => 
        array (
          0 => 'print_user_id',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
      'storno' => 
      array (
        'col' => 
        array (
          0 => 'storno',
        ),
      ),
      'storno_type' => 
      array (
        'col' => 
        array (
          0 => 'storno_type',
        ),
      ),
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'table_id' => 
      array (
        'col' => 
        array (
          0 => 'table_id',
        ),
      ),
      'table_type_id' => 
      array (
        'col' => 
        array (
          0 => 'table_type_id',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
    ),
  ),
)

?>