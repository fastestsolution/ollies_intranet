<?php

$sql = array (
  'product_groups' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'parent_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'level' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '5',
        'default' => '1',
      ),
      'lft' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'rght' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'printer_id' => 
      array (
        'type' => 'smallint',
        'null' => true,
        'length' => '5',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '80',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'web_id' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '255',
      ),
      'order_num' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
    ),
    'indexs' => 
    array (
      'parent_id' => 
      array (
        'col' => 
        array (
          0 => 'parent_id',
        ),
      ),
    ),
  ),
)

?>