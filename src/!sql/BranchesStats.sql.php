<?php

$sql = array (
  'branches_stats' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'branch_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'value' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'date' => 
      array (
        'type' => 'date',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
    ),
  ),
)

?>