<?php

$sql = array (
  'attendances' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'code' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '10',
      ),
      'delivery_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'date_from' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'date_to' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '20',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'code' => 
      array (
        'col' => 
        array (
          0 => 'code',
        ),
      ),
      'delivery_id' => 
      array (
        'col' => 
        array (
          0 => 'delivery_id',
        ),
      ),
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
    ),
  ),
)

?>