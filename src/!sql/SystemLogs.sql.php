<?php

$sql = array (
  'system_logs' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'message' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '200',
      ),
      'error' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'controller' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'action' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'url' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '150',
      ),
      'data' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'controller' => 
      array (
        'col' => 
        array (
          0 => 'controller',
        ),
      ),
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'user_id' => 
      array (
        'col' => 
        array (
          0 => 'user_id',
        ),
      ),
    ),
  ),
)

?>