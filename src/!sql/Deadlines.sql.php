<?php

$sql = array (
  'deadlines' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'pokladna_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'terminal_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'branch_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'order_id_from' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'order_id_to' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'type_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '2',
      ),
      'price_total' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
        'default' => '1',
      ),
      'nodelete' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'createdTime' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'data' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'pokladna_id' => 
      array (
        'col' => 
        array (
          0 => 'pokladna_id',
        ),
      ),
      'created' => 
      array (
        'col' => 
        array (
          0 => 'created',
        ),
      ),
      'createdTime' => 
      array (
        'col' => 
        array (
          0 => 'createdTime',
        ),
      ),
      'order_id_from' => 
      array (
        'col' => 
        array (
          0 => 'order_id_from',
        ),
      ),
      'order_id_to' => 
      array (
        'col' => 
        array (
          0 => 'order_id_to',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
      'branch_id' => 
      array (
        'col' => 
        array (
          0 => 'branch_id',
        ),
      ),
      'terminal_id' => 
      array (
        'col' => 
        array (
          0 => 'terminal_id',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
      'type_id' => 
      array (
        'col' => 
        array (
          0 => 'type_id',
        ),
      ),
      'user_id' => 
      array (
        'col' => 
        array (
          0 => 'user_id',
        ),
      ),
    ),
  ),
)

?>