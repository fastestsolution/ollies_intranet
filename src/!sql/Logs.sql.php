<?php

$sql = array (
  'logs' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'message' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '200',
      ),
      'error' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
    ),
  ),
)

?>