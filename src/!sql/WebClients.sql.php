<?php

$sql = array (
  'web_clients' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'system_id' => 
      array (
        'type' => 'smallint',
        'null' => true,
        'length' => '5',
      ),
      'eshop_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'web_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'local_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'jmeno' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '30',
      ),
      'prijmeni' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '30',
      ),
      'ulice' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '50',
      ),
      'mesto' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '50',
      ),
      'psc' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '10',
      ),
      'telefon' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '20',
      ),
      'email' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '100',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'updated' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'eshop_id' => 
      array (
        'col' => 
        array (
          0 => 'eshop_id',
        ),
      ),
      'web_id' => 
      array (
        'col' => 
        array (
          0 => 'web_id',
        ),
      ),
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'local_id' => 
      array (
        'col' => 
        array (
          0 => 'local_id',
        ),
      ),
      'email' => 
      array (
        'col' => 
        array (
          0 => 'email',
        ),
      ),
    ),
  ),
)

?>