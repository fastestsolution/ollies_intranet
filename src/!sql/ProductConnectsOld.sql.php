<?php

$sql = array (
  'product_connects_old' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'product_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'product_group_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'price2' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'price3' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'price4' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'pref' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '5',
      ),
    ),
    'indexs' => 
    array (
      'product_group_id' => 
      array (
        'col' => 
        array (
          0 => 'product_group_id',
        ),
      ),
      'product_id' => 
      array (
        'col' => 
        array (
          0 => 'product_id',
        ),
      ),
    ),
  ),
)

?>