<?php

$sql = array (
  'stock_products' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'code' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'group_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'unit_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'loss' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
        'default' => '1000',
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
        'default' => '1',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'group_id' => 
      array (
        'col' => 
        array (
          0 => 'group_id',
        ),
      ),
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
    ),
  ),
)

?>