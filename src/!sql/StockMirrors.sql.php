<?php

$sql = array (
  'stock_mirrors' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'data_step1' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'step_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'data_step2' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'date_step1' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'date_step2' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'data_diff' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'is_confirm' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'data_step_change' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'date_step_change' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'is_confirm' => 
      array (
        'col' => 
        array (
          0 => 'is_confirm',
        ),
      ),
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
    ),
  ),
)

?>