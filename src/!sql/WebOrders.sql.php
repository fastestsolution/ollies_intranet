<?php

$sql = array (
  'web_orders' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'type_id' => 
      array (
        'type' => 'smallint',
        'null' => false,
        'length' => '5',
        'default' => '1',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'eshop_id' => 
      array (
        'type' => 'smallint',
        'null' => true,
        'length' => '5',
      ),
      'web_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'local_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'shop_client_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'shop_doprava_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'shop_platba_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '150',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'updated' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
        'default' => '1',
      ),
      'kos' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
      ),
      'nd' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
      ),
      'cms_user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'price_with_tax' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'doprava_price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'doprava_price_with_tax' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'platba_price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'platba_price_with_tax' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'doprava_zdarma' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
      ),
      'paymentSessionId' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '50',
      ),
      'poznamka' => 
      array (
        'type' => 'text',
        'null' => false,
      ),
      'poznamka_interni' => 
      array (
        'type' => 'text',
        'null' => false,
      ),
      'pay' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '4',
      ),
      'vs' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '10',
      ),
      'vs2' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '10',
      ),
      'balik_code' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '25',
      ),
      'stav' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
        'default' => '1',
      ),
      'odeslane_email' => 
      array (
        'type' => 'text',
        'null' => false,
      ),
      'odeslane_sms' => 
      array (
        'type' => 'text',
        'null' => false,
      ),
      'historie' => 
      array (
        'type' => 'text',
        'null' => false,
      ),
      'fa_code' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '11',
      ),
      'fa_code2' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '11',
      ),
      'dopropis_date' => 
      array (
        'type' => 'date',
        'null' => true,
      ),
      'dobropis_code' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'fa_date' => 
      array (
        'type' => 'date',
        'null' => true,
      ),
      'order_date' => 
      array (
        'type' => 'date',
        'null' => true,
      ),
      'pickup_time' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '20',
      ),
      'gifted_name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'gifted_phone' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '15',
      ),
      'gdpr' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '15',
      ),
      'delivery' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'synchronized' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'eshop_id' => 
      array (
        'col' => 
        array (
          0 => 'eshop_id',
        ),
      ),
      'web_id' => 
      array (
        'col' => 
        array (
          0 => 'web_id',
        ),
      ),
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'local_id' => 
      array (
        'col' => 
        array (
          0 => 'local_id',
        ),
      ),
      'cms_user_id' => 
      array (
        'col' => 
        array (
          0 => 'cms_user_id',
        ),
      ),
      'kos' => 
      array (
        'col' => 
        array (
          0 => 'kos',
        ),
      ),
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'order_date' => 
      array (
        'col' => 
        array (
          0 => 'order_date',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
      'type_id' => 
      array (
        'col' => 
        array (
          0 => 'type_id',
        ),
      ),
    ),
  ),
)

?>