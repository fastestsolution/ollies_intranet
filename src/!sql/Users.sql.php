<?php

$sql = array (
  'users' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '100',
      ),
      'username' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '255',
      ),
      'password' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '255',
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
        'default' => '1',
      ),
      'kos' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'email' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'url' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'phone' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '9',
      ),
      'branch_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'group_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
        'default' => '1',
      ),
    ),
    'indexs' => 
    array (
      'branch_id' => 
      array (
        'col' => 
        array (
          0 => 'branch_id',
        ),
      ),
      'group_id' => 
      array (
        'col' => 
        array (
          0 => 'group_id',
        ),
      ),
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
      'kos' => 
      array (
        'col' => 
        array (
          0 => 'kos',
        ),
      ),
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'username' => 
      array (
        'col' => 
        array (
          0 => 'username',
        ),
      ),
    ),
  ),
)

?>