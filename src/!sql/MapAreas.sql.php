<?php

$sql = array (
  'map_areas' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'coords' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
    ),
  ),
)

?>