<?php

$sql = array (
  'intranet_logs' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '150',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => false,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => false,
        'default' => '0000-00-00 00:00:00',
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
        'default' => '1',
      ),
      'kos' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
      ),
      'nd' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
      ),
      'browser' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '255',
      ),
      'ip' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '255',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'type' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '255',
      ),
      'read_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
    ),
    'indexs' => 
    array (
      'kos' => 
      array (
        'col' => 
        array (
          0 => 'kos',
        ),
      ),
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
    ),
  ),
)

?>