<?php

$sql = array (
  'tables' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '5',
      ),
      'local_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '50',
      ),
      'table_type_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
        'default' => '1',
      ),
      'date_open' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'created' => 
      array (
        'type' => 'timestamp',
        'null' => false,
        'default' => 'CURRENT_TIMESTAMP',
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'num' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'group_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'on_map' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'data' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'total_price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'terminal_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'local_id' => 
      array (
        'col' => 
        array (
          0 => 'local_id',
        ),
      ),
      'table_type_id' => 
      array (
        'col' => 
        array (
          0 => 'table_type_id',
        ),
      ),
      'terminal_id' => 
      array (
        'col' => 
        array (
          0 => 'terminal_id',
        ),
      ),
    ),
  ),
)

?>