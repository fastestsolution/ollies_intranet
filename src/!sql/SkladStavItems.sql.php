<?php

$sql = array (
  'sklad_stav_items' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'sklad_stav_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'kos' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'sklad_item_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'stav' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'stav_real' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'stav_count' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
    ),
    'indexs' => 
    array (
      'sklad_item_id' => 
      array (
        'col' => 
        array (
          0 => 'sklad_item_id',
        ),
      ),
    ),
  ),
)

?>