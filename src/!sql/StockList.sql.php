<?php

$sql = array (
  'stock_list' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'jednotka_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'dodavatel_jednotka' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '5',
      ),
      'prevod' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'jednotka_id' => 
      array (
        'col' => 
        array (
          0 => 'jednotka_id',
        ),
      ),
      'dodavatel_jednotka' => 
      array (
        'col' => 
        array (
          0 => 'dodavatel_jednotka',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
    ),
  ),
)

?>