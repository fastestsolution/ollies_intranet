<?php

$sql = array (
  'web_order_items' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'system_id' => 
      array (
        'type' => 'smallint',
        'null' => true,
        'length' => '5',
      ),
      'eshop_id' => 
      array (
        'type' => 'smallint',
        'null' => true,
        'length' => '5',
      ),
      'web_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'local_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'shop_order_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'shop_product_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'pokl_product_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'code' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '10',
      ),
      'ean' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '100',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '150',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'updated' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
        'default' => '1',
      ),
      'kos' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
      ),
      'nd' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
      ),
      'price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'price_with_tax' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'row_price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'row_price_with_tax' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'ks' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'dph' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'shop_dodavatel_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'bezlepkovy' => 
      array (
        'type' => 'smallint',
        'null' => true,
        'length' => '6',
      ),
      'darkove_baleni' => 
      array (
        'type' => 'smallint',
        'null' => true,
        'length' => '6',
      ),
      'cislovka' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '20',
      ),
      'napis' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '20',
      ),
    ),
    'indexs' => 
    array (
      'eshop_id' => 
      array (
        'col' => 
        array (
          0 => 'eshop_id',
        ),
      ),
      'web_id' => 
      array (
        'col' => 
        array (
          0 => 'web_id',
        ),
      ),
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'local_id' => 
      array (
        'col' => 
        array (
          0 => 'local_id',
        ),
      ),
      'code' => 
      array (
        'col' => 
        array (
          0 => 'code',
        ),
      ),
      'kos' => 
      array (
        'col' => 
        array (
          0 => 'kos',
        ),
      ),
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
    ),
  ),
)

?>