<?php

$sql = array (
  'orders2' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'table_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'source_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
        'default' => '1',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
        'default' => '1',
      ),
      'note' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'close_order' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'total_price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'payment_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
        'default' => '1',
      ),
      'createdTime' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'storno' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'fik' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '250',
      ),
      'bkp' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '250',
      ),
      'table_open' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'table_close' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'pay_casch' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_card' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_bonus' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_voucher' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_discount' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_employee' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'print_user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
    ),
    'indexs' => 
    array (
      'close_order' => 
      array (
        'col' => 
        array (
          0 => 'close_order',
        ),
      ),
      'created' => 
      array (
        'col' => 
        array (
          0 => 'created',
        ),
      ),
      'createdTime' => 
      array (
        'col' => 
        array (
          0 => 'createdTime',
        ),
      ),
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'payment_id' => 
      array (
        'col' => 
        array (
          0 => 'payment_id',
        ),
      ),
      'source_id' => 
      array (
        'col' => 
        array (
          0 => 'source_id',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
      'storno' => 
      array (
        'col' => 
        array (
          0 => 'storno',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
    ),
  ),
)

?>