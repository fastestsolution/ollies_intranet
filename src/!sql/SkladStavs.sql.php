<?php

$sql = array (
  'sklad_stavs' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'date' => 
      array (
        'type' => 'date',
        'null' => true,
      ),
      'from_sklad_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'to_sklad_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'kos' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
    ),
    'indexs' => 
    array (
    ),
  ),
)

?>