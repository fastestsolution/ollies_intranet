<?php

$sql = array (
  'stocks' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'local_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'createdTime' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'invoice' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '20',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'order_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'stock_item_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'stock_item_product_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'value' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,4',
      ),
      'stock_type_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'system_id_to' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'nakup_price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'nakup_price_vat' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'nakup_price_total' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'nakup_price_total_vat' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'tax_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'unit_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'note' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'user_name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'terminal_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'storno_date' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'product_group_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
    ),
    'indexs' => 
    array (
      'created' => 
      array (
        'col' => 
        array (
          0 => 'created',
        ),
      ),
      'stock_type_id' => 
      array (
        'col' => 
        array (
          0 => 'stock_type_id',
        ),
      ),
      'local_id' => 
      array (
        'col' => 
        array (
          0 => 'local_id',
        ),
      ),
      'product_group_id' => 
      array (
        'col' => 
        array (
          0 => 'product_group_id',
        ),
      ),
      'stock_item_id' => 
      array (
        'col' => 
        array (
          0 => 'stock_item_id',
        ),
      ),
      'stock_item_product_id' => 
      array (
        'col' => 
        array (
          0 => 'stock_item_product_id',
        ),
      ),
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'invoice' => 
      array (
        'col' => 
        array (
          0 => 'invoice',
        ),
      ),
      'storno_date' => 
      array (
        'col' => 
        array (
          0 => 'storno_date',
        ),
      ),
      'terminal_id' => 
      array (
        'col' => 
        array (
          0 => 'terminal_id',
        ),
      ),
      'user_id' => 
      array (
        'col' => 
        array (
          0 => 'user_id',
        ),
      ),
    ),
  ),
)

?>