<?php

$sql = array (
  'branches' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'eet_active' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '1',
      ),
      'eet_certificate' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '255',
      ),
      'eet_cert_pass' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '200',
      ),
      'eet_dic' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'eet_dic_pover' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'eet_branch_id' => 
      array (
        'type' => 'mediumint',
        'null' => true,
        'length' => '9',
      ),
      'email' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'view_api' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '1',
      ),
    ),
    'indexs' => 
    array (
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
    ),
  ),
)

?>