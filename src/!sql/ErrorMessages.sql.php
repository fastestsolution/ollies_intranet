<?php

$sql = array (
  'error_messages' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'message' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '200',
      ),
      'phone' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'provoz_name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
    ),
    'indexs' => 
    array (
    ),
  ),
)

?>