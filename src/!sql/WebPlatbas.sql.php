<?php

$sql = array (
  'web_platbas' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '150',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => false,
      ),
      'updated' => 
      array (
        'type' => 'datetime',
        'null' => false,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
        'default' => '1',
      ),
      'kos' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
      ),
      'nd' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
      ),
      'cms_user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'price' => 
      array (
        'type' => 'float',
        'null' => false,
        'length' => '10,2',
      ),
      'price_with_tax' => 
      array (
        'type' => 'float',
        'null' => false,
        'length' => '10,2',
      ),
      'dph_list' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'code' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '6',
      ),
      'poradi' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'text' => 
      array (
        'type' => 'text',
        'null' => false,
      ),
    ),
    'indexs' => 
    array (
      'cms_user_id' => 
      array (
        'col' => 
        array (
          0 => 'cms_user_id',
        ),
      ),
      'kos' => 
      array (
        'col' => 
        array (
          0 => 'kos',
        ),
      ),
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'poradi' => 
      array (
        'col' => 
        array (
          0 => 'poradi',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
    ),
  ),
)

?>