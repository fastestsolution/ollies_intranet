<?php

$sql = array (
  'intranet_actuals' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '150',
      ),
      'text' => 
      array (
        'type' => 'text',
        'null' => false,
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => false,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => false,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
        'default' => '1',
      ),
      'kos' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
      ),
      'nd' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
      ),
      'type' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'files' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'kos' => 
      array (
        'col' => 
        array (
          0 => 'kos',
        ),
      ),
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
      'type' => 
      array (
        'col' => 
        array (
          0 => 'type',
        ),
      ),
    ),
  ),
)

?>