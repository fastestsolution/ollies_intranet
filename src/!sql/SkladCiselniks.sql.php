<?php

$sql = array (
  'sklad_ciselniks' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'kos' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'type' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
        'default' => '1',
      ),
    ),
    'indexs' => 
    array (
      'kos' => 
      array (
        'col' => 
        array (
          0 => 'kos',
        ),
      ),
      'type' => 
      array (
        'col' => 
        array (
          0 => 'type',
        ),
      ),
    ),
  ),
)

?>