<?php

$sql = array (
  'product_prices' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'checked' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'product_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'price2' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'price3' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'price4' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
    ),
    'indexs' => 
    array (
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'product_id' => 
      array (
        'col' => 
        array (
          0 => 'product_id',
        ),
      ),
    ),
  ),
)

?>