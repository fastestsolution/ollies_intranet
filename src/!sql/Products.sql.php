<?php

$sql = array (
  'products' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'code' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '10',
      ),
      'web_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'product_group_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '6',
      ),
      'web_group_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '6',
      ),
      'num' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '6',
      ),
      'price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'price_without_tax' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'amount' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '10',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
        'default' => '1',
      ),
      'nodelete' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'tax_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'addons' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'group_trzba_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'is_mirror' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'is_dashboard' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '3',
      ),
      'reserve' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'view_api' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'view_workshop' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
    ),
    'indexs' => 
    array (
      'code' => 
      array (
        'col' => 
        array (
          0 => 'code',
        ),
      ),
      'is_dashboard' => 
      array (
        'col' => 
        array (
          0 => 'is_dashboard',
        ),
      ),
      'is_mirror' => 
      array (
        'col' => 
        array (
          0 => 'is_mirror',
        ),
      ),
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'product_group_id' => 
      array (
        'col' => 
        array (
          0 => 'product_group_id',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
      'view_api' => 
      array (
        'col' => 
        array (
          0 => 'view_api',
        ),
      ),
      'web_group_id' => 
      array (
        'col' => 
        array (
          0 => 'web_group_id',
        ),
      ),
      'web_id' => 
      array (
        'col' => 
        array (
          0 => 'web_id',
        ),
      ),
    ),
  ),
)

?>