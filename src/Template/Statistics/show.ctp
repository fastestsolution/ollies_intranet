<?php

/**
 * Change $value to currency format.
 * 
 * @since 1.54
 * @param mixed $value
 * @return float
 */
function n_format($value)
{
    return number_format((float)$value, 2, '.', ' ');
}

/**
 * Fill the product list.
 * 
 * @since 1.54
 * @param string $name
 * @return array
 */
function productList($name, $js)
{
    $data = (object) [];

    if (property_exists($js, $name)) {
        foreach ($js->{$name} as $list) {
            if (!isset($data->{$list->name})) {
                $data->{$list->name} = (object) [
                    "name"          => $list->name,
                    "units"         => $list->total_units,
                    "price"         => $list->total_price,
                    "items"         => (object) []
                ];
            } else {
                if (!empty((array) $list->items)) {
                    $data->{$list->name}->units += $list->total_units;
                    $data->{$list->name}->price += $list->total_price;
                }
            }
            
            foreach ($list->items as $item) {
                if (!isset($data->{$list->name}->items->{$item->name})) {
                    $data->{$list->name}->items->{$item->name} = (object) [
                        "name"          => $item->name,
                        "units"         => $item->units,
                        "price"         => $item->price
                    ];
                } else {
                    $data->{$list->name}->items->{$item->name}->units += $item->units;
                    $data->{$list->name}->items->{$item->name}->price += $item->price;
                }
            }
        }
    }

    return $data;
}

if (isset($deadlineData)) {

    $json               = $deadlineData;
    $big                = '------------------------------------------------------------------------';
    $small              = '-------------------------';
    $undefined          = 'nedefinováno';

    $data = (object) [
        'title'                 => '',
        'dateFrom'              => date('Y-m-d 00:00:00'),
        'dateTo'                => date('Y-m-d 00:00:00'),
        'branches'              => (object) [],
        'sum_total_complete'    => (object) [
            'price'                 => 0,
            'orders'                => 0,
            'items'                 => 0
        ],
        'sum_total_storno'      => (object) [
            'price'                 => 0,
            'orders'                => 0,
            'items'                 => 0
        ],
        'sum_groups'            => (object) [],
        'soft_storno'           => (object) [
            'price'                 => 0,
            'count'                 => 0,
            'items'                 => (object) []
        ],
        'payed_cards'           => (object) [
            'price'                 => 0,
            'items'                 => (object) []
        ],
        'product_list'          => (object) [],
        'product_list_delivery' => (object) [],
        'product_list_employee' => (object) [],
        'product_list_company'  => (object) []
    ];
    
    $iterator = 1;
    $count = 0;

    foreach ($json as $row) {
        if (is_object($row) && property_exists($row, 'json')) {
            $count++;
        }
    }
    
    foreach ($json as $row) {
        if (is_object($row) && property_exists($row, 'json')) {
            $js = $row->json;
    
            if ($count > 1) {
                if ($iterator == $count) {
                    $data->dateFrom = $js->header->titleDate;
                } else if ($iterator == 1) {
                    $data->title = $js->header->title;
                    $data->dateTo = $js->header->titleDate;
                }
            } else {
                $data->title = $js->header->title;
                $data->dateFrom = $js->header->titleDate;
            }
    
            if (!in_array($branches_list[$row->system_id], (array) $data->branches)) {
                $data->branches->{$iterator} = $branches_list[$row->system_id];
            }
    
            $data->sum_total_complete->price += $js->sum_total_complete->total_price;
            $data->sum_total_complete->orders += $js->sum_total_complete->orders;
            $data->sum_total_complete->items += $js->sum_total_complete->units;
    
            $data->sum_total_storno->price += $js->sum_total_storno->total_price;
            $data->sum_total_storno->orders += $js->sum_total_storno->orders;
            $data->sum_total_storno->items += $js->sum_total_storno->items;
    
            foreach ($js->sum_group as $group) {
                if (!isset($data->sum_groups->{$group->name})) {
                    $data->sum_groups->{$group->name} = (object) [
                        'name'              => $group->name,
                        'price'             => $group->total_with_tax,
                        'price_without_tax' => $group->total_without_tax,
                        'taxs'              => (object) [
                            '15%'               => $group->taxs->{'1'},
                            '21%'               => $group->taxs->{'2'},
                            '0%'                => $group->taxs->{'3'},
                            '10%'               => $group->taxs->{'4'}
                        ]
                    ];
                } else {
                    if ($group->total_with_tax > 0) {
                        $data->sum_groups->{$group->name}->price += $group->total_with_tax;
                        $data->sum_groups->{$group->name}->price_without_tax += $group->total_without_tax;
                        $data->sum_groups->{$group->name}->taxs->{'15%'} += $group->taxs->{'1'};
                        $data->sum_groups->{$group->name}->taxs->{'21%'} += $group->taxs->{'2'};
                        $data->sum_groups->{$group->name}->taxs->{'0%'} += $group->taxs->{'3'};
                        $data->sum_groups->{$group->name}->taxs->{'10%'} += $group->taxs->{'4'};
                    }
                }
            }
    
            $data->soft_storno->price += $js->soft_storno->price;
            $data->soft_storno->count += $js->soft_storno->count;
    
            foreach ($js->soft_storno->items as $item) {
                if (!isset($data->soft_storno->items->{$item->name})) {
                    $data->soft_storno->items->{$item->name} = (object) [
                        'name'      => $item->name,
                        'price'     => $item->price,
                        'units'     => $item->units
                    ];
                } else {
                    $data->soft_storno->items->{$item->name}->price += $item->price;
                    $data->soft_storno->items->{$item->name}->units += $item->units;
                }
            }
    
            $data->payed_cards->price += $js->payed_cards->total_price;
            $data->payed_cards->total_units = 0;

            foreach ($js->payed_cards->order_items as $item) {
                /*$data->payed_cards->items->{$item->id} = (object) [
                    "id"        => $item->id,
                    "price"     => $item->total_price
                ];*/
                $data->payed_cards->total_units += 1;
            }
    
            $data->product_list             = productList('products_list', $js);
            $data->product_list_delivery    = productList('products_list_delivery', $js);
            $data->product_list_employee    = productList('products_list_employee', $js);
            $data->product_list_company     = productList('products_list_company', $js);

            $iterator++;
        }
    }

    if (!isset($debug)) {
        echo
            '
            <style>

            tr, td {
                line-height: 4px;
                padding: 0;
                margin: 0;
            }

            </style>
            ';
        } 

        echo
        '<table>
            <tr>
                <td style="text-align: center;" colspan="3">', $data->title, '</td>
            </tr>';

        if ($count > 1) {
            echo
                '<tr>
                    <td style="text-align: center;" colspan="3">od ', $data->dateFrom, '</td>
                </tr>
                <tr>
                    <td style="text-align: center; padding-bottom: 16px;" colspan="3">do ', $data->dateTo, '</td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-bottom: 16px;" colspan="3">Z poboček:</td>
                </tr>';

            $countBranches      = count((array) $data->branches);
            $iterator           = 1;
            foreach ($data->branches as $branch) {
                if ($iterator == $countBranches) {
                    echo
                        '<tr>
                            <td style="text-align: left; padding-bottom: 16px;" colspan="3">', $branch, '</td>
                        </tr>';
                } else {
                    echo
                        '<tr>
                            <td style="text-align: left;" colspan="3">', $branch, '</td>
                        </tr>';
                }
                
                $iterator++;
            }
        } else {
            echo
                '<tr>
                    <td style="text-align: center; padding-bottom: 16px;" colspan="3">', $data->dateFrom, '</td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-bottom: 16px;" colspan="3">Z pobočky:</td>
                </tr>';

            foreach ($data->branches as $branch) {
                echo
                    '<tr>
                        <td style="text-align: left; padding-bottom: 16px;" colspan="3">', $branch, '</td>
                    </tr>';
            }
        }

        echo
        '<tr>
            <td style="text-align: left;">Produkt</td>
            <td style="text-align: center;">Ks</td>
            <td style="text-align: right;">Cena</td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="3">', $big, '</td>
        </tr>
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="2">Tržba celkem</td>
            <td style="text-align: right; font-weight: bold;">', n_format($data->sum_total_complete->price), '</td>
        </tr>
        <tr>
            <td style="text-align: left;" colspan="3">', $small, '</td>
        </tr>
        <tr>
            <td style="text-align: left;" colspan="2">Objednávek</td>
            <td style="text-align: right;">', $data->sum_total_complete->orders, '</td>
        </tr>
        <tr>
            <td style="text-align: left;" colspan="2">Položek</td>
            <td style="text-align: right;">', $data->sum_total_complete->items, '</td>
        </tr>
        <tr>
            <td style="text-align: left;" colspan="3">', $small, '</td>
        </tr>
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="2">Storna</td>
            <td style="text-align: right; font-weight: bold;">', n_format($data->sum_total_storno->price), '</td>
        </tr>
        <tr>
            <td style="text-align: left;" colspan="2">Celkem</td>
            <td style="text-align: right;">', n_format($data->sum_total_storno->price), '</td>
        </tr>
        <tr>
            <td style="text-align: left;" colspan="2">Objednávek</td>
            <td style="text-align: right;">', $data->sum_total_storno->orders, '</td>
        </tr>
        <tr>
            <td style="text-align: left;" colspan="2">Položek</td>
            <td style="text-align: right;">', $data->sum_total_storno->items, '</td>
        </tr>
        <tr>
            <td style="text-align: left;" colspan="3">', $small, '</td>
        </tr>
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="2">Rozpis skupin</td>
            <td style="text-align: right; font-weight: bold;">Cena</td>
        </tr>';

        foreach ($data->sum_groups as $group) {
            echo
                '<tr>
                    <td style="text-align: left;" colspan="2">', $group->name, '</td>
                    <td style="text-align: right;">', n_format($group->price_without_tax), '</td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2">15%</td>
                    <td style="text-align: right;">', n_format($group->taxs->{'15%'}), '</td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2">21%</td>
                    <td style="text-align: right;">', n_format($group->taxs->{'21%'}), '</td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2">0%</td>
                    <td style="text-align: right;">', n_format($group->taxs->{'0%'}), '</td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2">10%</td>
                    <td style="text-align: right;">', n_format($group->taxs->{'10%'}), '</td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="2">Celkem s DPH</td>
                    <td style="text-align: right;">', n_format($group->price), '</td>
                </tr>
                <tr>
                    <td style="text-align: left;" colspan="3">', $small, '</td>
                </tr>';
        }

        if (!empty($data->soft_storno->items)) {
            echo
                '<tr>
                    <td style="text-align: left; font-weight: bold;">Měkké storno</td>
                    <td style="text-align: center; font-weight: bold;">', $data->soft_storno->count, '</td>
                    <td style="text-align: right; font-weight: bold;">', n_format($data->soft_storno->price), '</td>
                </tr>';

            foreach ($data->soft_storno->items as $item) {
                echo
                    '<tr>
                        <td style="text-align: left;">', $item->name, '</td>
                        <td style="text-align: center;">', $item->units, '</td>
                        <td style="text-align: right;">', n_format($item->price), '</td>
                    </tr>';
            }

            echo
                '<tr>
                    <td style="text-align: left;" colspan="3">', $small, '</td>
                </tr>';
        }
        
        if ($view_all) {
            if (!empty($data->payed_cards->items)) {
                echo
                    '<tr>
                        <td style="text-align: left; font-weight: bold;">Platba kartou</td>
                        <td style="text-align: center; font-weight: bold;">', $data->payed_cards->total_units, '</td>
                        <td style="text-align: right; font-weight: bold;">', $data->payed_cards->price, '</td>
                    </tr>';
    
                /*foreach ($data->payed_cards->items as $item) {
                    echo
                        '<tr>
                            <td style="text-align: left;" colspan="2">', $item->id, '</td>
                            <td style="text-align: right;">', n_format($item->price), '</td>
                        </tr>';
                }
    
                echo
                    '<tr>
                        <td style="text-align: left;" colspan="2">Celkem</td>
                        <td style="text-align: right;">', n_format($data->payed_cards->price), '</td>
                    </tr>';*/
            }
    
            if (!empty($data->product_list)) {
                foreach ($data->product_list as $list) {
                    if (!empty($list->items) && $list->units > 0) {
                        echo
                            '<tr>
                                <td style="text-align: left;" colspan="3">', $big, '</td>
                            </tr>
                            <tr>
                                <td style="text-align: left; font-weight: bold;">', $list->name, '</td>
                                <td style="text-align: center; font-weight: bold;">', $list->units, '</td>
                                <td style="text-align: right; font-weight: bold;">', n_format($list->price), '</td>
                            </tr>';
    
                        foreach ($list->items as $item) {
                            echo
                                '<tr>
                                    <td style="text-align: left;">', $item->name, '</td>
                                    <td style="text-align: center;">', $item->units, '</td>
                                    <td style="text-align: right;">', n_format($item->price), '</td>
                                </tr>';
                        }
                    }
                }
            }

            if (!empty($data->product_list_delivery)) {
                echo
                    '<tr>
                        <td style="text-align: left;" colspan="3">', $big, '</td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-weight: bold;">Rozvoz</td>
                        <td style="text-align: center; font-weight: bold;">Ks</td>
                        <td style="text-align: right; font-weight: bold;">Cena</td>
                    </tr>';
                
                foreach ($data->product_list_delivery as $list) {
                    if (!empty($list->items) && $list->units > 0) {
                        echo
                            '<tr>
                                <td style="text-align: left;" colspan="3">', $big, '</td>
                            </tr>
                            <tr>
                                <td style="text-align: left; font-weight: bold;">', $list->name, '</td>
                                <td style="text-align: center; font-weight: bold;">', $list->units, '</td>
                                <td style="text-align: right; font-weight: bold;">', n_format($list->price), '</td>
                            </tr>';
    
                        foreach ($list->items as $item) {
                            echo
                                '<tr>
                                    <td style="text-align: left;">', $item->name, '</td>
                                    <td style="text-align: center;">', $item->units, '</td>
                                    <td style="text-align: right;">', n_format($item->price), '</td>
                                </tr>';
                        }
                    }
                }
            }

            if (!empty($data->product_list_employee)) {
                echo
                    '<tr>
                        <td style="text-align: left;" colspan="3">', $big, '</td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-weight: bold;">Zaměstnanci</td>
                        <td style="text-align: center; font-weight: bold;">Ks</td>
                        <td style="text-align: right; font-weight: bold;">Cena</td>
                    </tr>';
                
                foreach ($data->product_list_employee as $list) {
                    if (!empty($list->items) && $list->units > 0) {
                        echo
                            '<tr>
                                <td style="text-align: left;" colspan="3">', $big, '</td>
                            </tr>
                            <tr>
                                <td style="text-align: left; font-weight: bold;">', $list->name, '</td>
                                <td style="text-align: center; font-weight: bold;">', $list->units, '</td>
                                <td style="text-align: right; font-weight: bold;">', n_format($list->price), '</td>
                            </tr>';
    
                        foreach ($list->items as $item) {
                            echo
                                '<tr>
                                    <td style="text-align: left;">', $item->name, '</td>
                                    <td style="text-align: center;">', $item->units, '</td>
                                    <td style="text-align: right;">', n_format($item->price), '</td>
                                </tr>';
                        }
                    }
                }
            }

            if (!empty($data->product_list_company)) {
                echo
                    '<tr>
                        <td style="text-align: left;" colspan="3">', $big, '</td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-weight: bold;">Firma</td>
                        <td style="text-align: center; font-weight: bold;">Ks</td>
                        <td style="text-align: right; font-weight: bold;">Cena</td>
                    </tr>';
                
                foreach ($data->product_list_company as $list) {
                    if (!empty($list->items) && $list->units > 0) {
                        echo
                            '<tr>
                                <td style="text-align: left;" colspan="3">', $big, '</td>
                            </tr>
                            <tr>
                                <td style="text-align: left; font-weight: bold;">', $list->name, '</td>
                                <td style="text-align: center; font-weight: bold;">', $list->units, '</td>
                                <td style="text-align: right; font-weight: bold;">', n_format($list->price), '</td>
                            </tr>';
    
                        foreach ($list->items as $item) {
                            echo
                                '<tr>
                                    <td style="text-align: left;">', $item->name, '</td>
                                    <td style="text-align: center;">', $item->units, '</td>
                                    <td style="text-align: right;">', n_format($item->price), '</td>
                                </tr>';
                        }
                    }
                }
            }
        } else {
            if (!empty($data->product_list_delivery)) {
                $deliveryTotalPrice = 0;
                $deliveryTotalUnits = 0;

                foreach ($data->product_list_delivery as $list) {
                    $deliveryTotalPrice += $list->price;
                    $deliveryTotalUnits += $list->units;
                }

                echo
                    '<tr>
                        <td style="text-align: left; font-weight: bold;">Rozvoz</td>
                        <td style="text-align: center; font-weight: bold;">', $deliveryTotalUnits, '</td>
                        <td style="text-align: right; font-weight: bold;">', n_format($deliveryTotalPrice), '</td>
                    </tr>';
            }

            if (!empty($data->product_list_employee)) {
                $employeeTotalPrice = 0;
                $employeeTotalUnits = 0;

                foreach ($data->product_list_employee as $list) {
                    $employeeTotalPrice += $list->price;
                    $employeeTotalUnits += $list->units;
                }

                echo
                    '<tr>
                        <td style="text-align: left; font-weight: bold;">Zaměstnanci</td>
                        <td style="text-align: center; font-weight: bold;">', $employeeTotalUnits, '</td>
                        <td style="text-align: right; font-weight: bold;">', n_format($employeeTotalPrice), '</td>
                    </tr>';
            }

            if (!empty($data->product_list_company)) {
                $companyTotalPrice = 0;
                $companyTotalUnits = 0;

                foreach ($data->product_list_company as $list) {
                    $companyTotalPrice += $list->price;
                    $companyTotalUnits += $list->units;
                }

                echo
                    '<tr>
                        <td style="text-align: left; font-weight: bold;">Firma</td>
                        <td style="text-align: center; font-weight: bold;">', $companyTotalUnits, '</td>
                        <td style="text-align: right; font-weight: bold;">', n_format($companyTotalPrice), '</td>
                    </tr>';
            }
        }
    
    echo
    '</table>';
}

?>