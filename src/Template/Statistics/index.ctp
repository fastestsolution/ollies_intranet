<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmRIyQUbK5x_c_cm78ZV5ODf-sqVO1-2s&v=3.exp&libraries=places,geometry,visualization&sensor=false"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
<?php 
/*
echo 'Přihlášen: '.$loggedUser['name']; ?>
<br />
<br />
<?php echo $this->Html->link('Odhlásit','/logout/',['class'=>'btn btn-primary']); 
*/
?>



<!-- <p>dodělat coly ,  kde budou statisiky prodeje a grafiky, možná rozložit grafy</p>
<p>Brát z tabulky Stock a tam je stock_item_product_id || jinak je to na staré pokladně statistiky</p>
<p>udělat si ješte field, jenom potrebne id ktere potrebuji a potrebuje sumaci toho vseho </p>
<p>Pomoct si group by a sum ||| cake 3 php example</p>
<p>nachystat inputy dle staré poklady a layout</p> -->

<div class='container-fluid mobileTopMargin'>
    <p class='h2 text-center text-muted'>Ollies <?= $_SERVER['HTTP_HOST'] == 'test-intr.fastestdev.cz' ? 'TESTOVACI': '' ?>| Dashboard </p>
	<div id="statsRender"></div>
    
    <div class='row'>
        <div class='col-sm-12 col-md-12 middle-statistics'>
            <h3 class='text-center text-muted'>Statistiky prodeje produktů</h3>    
            <div class="middle-statistics">
            <p class="h4 text-muted text-center">Filtrace</p>
                <div class="row">
                    <div class='col-sm-6 col-md-3 pull-left'>
						<?php echo $this->Form->input("user_id", ['placeholder' => __("Pracovník"),'data-fparams'=>'Stocks__user_id|text','options'=>[''=>'Pracovník'],'label'=>false,'class'=>'fst_h m-t float price save_item clear_save pull-right input-own form-control form-control-sm','data-type'=>'user_id']); ?>
                        <?php //echo $this->Form->input("tax_id", ['label' => __("DPH"),'class'=>' save_item pull-right input-own form-control','options'=>$price_tax_list]); ?>
                        <?php //echo $this->Form->input("value", ['label' => __("Množství"),'class'=>'float price_ks save_item clear_save pull-right input-own form-control']); ?>
                        <?php //echo $this->Form->input("storno", ['placeholder' => __("Storno"),'data-fparams'=>'Stocks__user_id|text','label'=>false,'options'=>[2=>'Storno']+ $yes_no,'class'=>'float price save_item clear_save pull-right input-own form-control form-control-sm','data-type'=>'storno']); ?>
                    </div>

                    <div class='col-sm-6 col-md-3 pull-left'>
                        <?php echo $this->Form->input("product_group_id", ['placeholder' => __("Skupina"),'data-fparams'=>'Stocks__product_group_id|text','options'=>[''=>'Skupina']+ $groupList,'label'=>false,'class'=>'fst_h m-t float price  save_item clear_save pull-right input-own form-control form-control-sm','data-type'=>'product_group_id']); ?>
                        
                    </div>
                    <div class='col-sm-6 col-md-3 pull-left'>
                        <?php echo $this->Form->input("price_tax_id", ['placeholder' => __("DPH"),'data-fparams'=>'Stocks__tax_id|text','label'=>false,'options'=>[''=>'DPH']+ $price_tax_list,'class'=>'fst_h m-t float price save_item clear_save pull-right input-own form-control form-control-sm','data-type'=>'price_tax_id']); ?>
                    </div>
                    <div class='col-sm-6 col-md-3 pull-left'>
                        <?php echo $this->Form->input("price_tax_id", ['placeholder' => __("Pobočka"),'data-fparams'=>'system_id|text','label'=>false,'options'=>[''=>'Pobočky']+ $branchesList,'class'=>'fst_h m-t float price save_item clear_save pull-right input-own form-control form-control-sm','data-type'=>'price_tax_id']); ?>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class='col-sm-12 col-md-12 pull-right own-scroll2'>
				<div id="fst_history">	
				<div id='stats-items'>
                        <?php 
							//echo $this->element('stats/table');
                        ?>
					</div>
					</div>
                </div>
                <div class="changeDate">
                    <div class="row">
                        <div class="col col-6">
                            <?php echo $this->Form->input("created_date", ['placeholder' => __("Datum od"),'data-fparams'=>'Stocks__createdTime|fromDateTimestamp','label'=>false,'class'=>'fst_h m-t float price save_item clear_save pull-right input-own form-control form-control-sm date','data-type'=>'created_date','value'=>(isset($currentDateFrom)?$currentDateFrom:date('d.m.Y'))]); ?>
                        </div>
                        <div class="col col-6">
                            <?php echo $this->Form->input("created_date2", ['placeholder' => __("Datum do"),'data-fparams'=>'Stocks__createdTime|toDateTimestamp','label'=>false,'class'=>'fst_h m-t float price save_item clear_save pull-right input-own form-control form-control-sm date','data-type'=>'created_date2','value'=>(isset($currentDateTo)?$currentDateTo:date('d.m.Y'))]); ?>    
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
</div>






<div id="page_preloader"></div>
<div id="page_url" class="none"><?php echo $here;?></div>
<script type="text/javascript">
//<![CDATA[
   function genStats(){
	pagePreloader();
	var saveReg = new Request.HTML({
					url:'/statistics/genStats/',
					update:'stats-items',
					onError: function(json){
						pagePreloader(true);
						//button_preloader(button);
                    },
					onComplete: function(json){
                        date_picker();
                        history();
						pagePreloader(true);
						//button_preloader(button);
					}
				});
				saveReg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				saveReg.send();		
   }
	function history (){
		var options = {
			'complete_fce': function(){
				if ($('statsRender') && $('statsTopData')){
				$('statsRender').adopt($('statsTopData'));
				date_picker();
				}
			},
			'url': '/statistics/genStats/'
		};
		window.fsthistory = new FstHistory(options);

	}

   genStats();
//]]>
</script>

<?php if (isset($graphResult)){ ?>



<?php } else { ?>
	<div class="alert alert-warning">
		<strong>Upozornění: </strong>Žádné data pro zobrazení, upravte filtraci. (nebo pokladna neposílá data)
	</div>
<?php } ?>


 