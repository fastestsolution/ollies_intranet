<?php 
    if(isset($stats_data)){
?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <h3 class='text-center text-muted'>Tržba dle doby</h3>   
        <div id="line_graf" class="chart"></div>
    </div>
</div>

<!--<div class='col-sm-12 col-md-3 middle-statistics'>                        
        <div class="card-box">
            <h3 class="header-title m-t-0 text-center text-muted">Tržby pobočky</h3>
            <div id="sale_chart_price" class="chart"></div>
        </div>
    </div>
</div>
--> 


<div class='row'>

    <div class='col-sm-12 col-md-4 middle-statistics'>
        <h3 class='text-center text-muted'>Prodeje produktů</h3>    
        <div id="cats_chart" class="chart"></div>
    </div>

    <div class='col-sm-12 col-md-4 middle-statistics'>  
        <h3 class="header-title m-t-0 text-center text-muted">Tržby pobočky</h3>          
          <!--TOP Prodej produktů + Tržby poboček   <div id="area_chart" class="chart">Vymyslet co zde udelat za graf</div>-->
            <div id="sale_chart_price" class="chart"></div>
    </div>

    <div class='col-sm-12 col-md-4 middle-statistics'>               
        <h3 class='text-center text-muted'>Tržby tady/sebou</h3>    
        <div id="tady_sebou_chart" class="chart"></div>
    </div>

</div>

<!--TOP Prodej produktů + Tržby poboček -->
<div class='row'>
    <div class="col-sm-12 col-md-12">
        <div class="table-responsive">
            <div class="tableContainer">
            <div class="tableHeader"></div>    
            <div class="tableWraper">
                    <table class="table table-striped table-hover" id="gen-stats">
                        <thead>
                        <tr>
                            <th><div class="wrap center">ID</div></th>
                            <th><div class="wrap left">Název</div></th>
                            <th><div class="wrap center">Množ.</div></th>
                            <th><div class="wrap center">Cena</div></th>
                            <th><div class="wrap center">Cena bez DPH</div></th>
                            <th><div class="wrap center">Cena cel. s DPH</div></th>
                            <th><div class="wrap center">Cena cel.</div></th>
                            <th><div class="wrap center">NC s DPH</div></th>
                            <th><div class="wrap center">NC</div></th>
                            <th><div class="wrap center">NC cel. s DPH</div></th>
                            <th><div class="wrap center">NC cel.</div></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($stats_data)){ ?>
                            <?php foreach($stats_data AS $key=>$data){ ?>
                                <?php 
                                echo '<tr>';
                                    echo '<td class="id center">'.$data->product_id.'</td>';
                                    echo '<td class="left">'.(isset($data->product->name)?$data->product->name:'Není název').'</td>'; 
                                    echo '<td class="count center">'.$data->value.'</td>';
                                    echo '<td class="price center">'.$data->price.'</td>';
                                    echo '<td class="price center">'.$data->price_without_tax.'</td>';
                                    echo '<td class="price center">'.$data->price_sum.'</td>';
                                    echo '<td class="price center">'.$data->price_sum_without_tax.'</td>';
                                    echo '<td class="price center">'.$data->nakup_price.'</td>';
                                    echo '<td class="price center">'.$data->nakup_price_vat.'</td>';
                                    echo '<td class="price center">'.$data->nakup_price_total.'</td>';
                                    echo '<td class="price center">'.$data->nakup_price_total_vat.'</td>';
                                echo '</tr>';
                                ?>
                                
                                </tr>
                            <?php } ?>
                            <?php } else { ?>
                                <tr><td colspan="13">Nenalezeny žádné data, upravte filtraci</td></tr>
                            <?php } ?>
                        </tbody>	
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END OF Prodej produktů + Tržby poboček -->


<!--Start of Product Dashboard Chart -->

<div class="row">

    <div class="col-sm-12 col-md-12">
        <div class='col-sm-12 col-md-12'>               
            <h3 class='text-center text-muted'>Sledované produkty</h3>    
            <div id="watchdog_dashboard_chart" class="chart"></div>
        </div>
    </div>

</div>

<!--End of Product Dashboard Chart -->

<!--TOP STATS DATA -->

<div id="statsTopData">
    <div class='row'>
        <div class='col-sm-6 col-md-3 top-statistics'>
            <div class='media'>
                <div>
                    <p class='text-muted'>Tržby poboček s DPH</p>
                    <i class="fa fa-usd green" aria-hidden="true"></i>
                </div>
                <div class='right-media'>
                    <?php
                        foreach ($sumace['trzba'] as $b_id=>$value){

                            echo '<div class="branches_pob">';
                            echo '<span class="branches-sum pull-left">'.$branches_list[$b_id].'</span>'.'<span class="branches-price">'.$this->Fastest->price($value).'</span>';
                            echo '</div>';
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class='col-sm-6 col-md-3 top-statistics'>
            <div class='media'>
                <div>
                    <p class='text-muted'>Prodaných produktů</p>
                    <i class="fa fa-shopping-cart yellow" aria-hidden="true"></i>
                </div>
                <div class='right-media'>
                    <!-- <h2><?php //echo count($stats_data);?></h2> -->
                    <?php
                      
                        echo '<div class="branches_pob">';
                        echo '<span class="branches-sum pull-left">Druhů produktů</span>'.'<span class="branches-price">'.$sumace['productSum']['total'].'</span>';
                        echo '</div>';
                      
                        echo '<div class="branches_pob">';
                        echo '<span class="branches-sum pull-left">Celkem produktů</span>'.'<span class="branches-price">'.$sumace['productSum']['total2'].'</span>';
                        echo '</div>';
                    ?>
                    <!-- ($this->sumace['productSum'] -->
                </div>
            </div>
        </div>
        <div class='col-sm-6 col-md-3 top-statistics'>
            <div class='media'>
                <div>
                    <p class='text-muted'>Průměrná objednávka</p>
                    <i class="fa fa-users red" aria-hidden="true"></i>
                </div>
                <div class='right-media'>
                <?php
                    if(isset($sumace['prumernaObj'])){
                        foreach ($sumace['prumernaObj'] as $b_id=>$value){
                            //pr($value);
                            echo '<div class="branches_pob">';
                            echo '<span class="branches-sum pull-left">'.$branches_list[$b_id].'</span>'.'<span class="branches-price">'.$this->Fastest->price($value['prumer']).'</span>';
                            //echo '<span class="branches-sum">'.$branches_list[$b_id].': '.$this->Fastest->price($value['prumer']).'</span>';
                            echo '</div>';
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class='col-sm-6 col-md-3 top-statistics'>
            <div class='media'>
                <div class=''>
                    <p class='text-muted'>Počet objednávek</p>
                    <i class="fa fa-home blue" aria-hidden="true"></i>
                </div>
                <div class='right-media'>
                    <!-- <h2><?php //echo count($branches_list);?></h2> -->
                    <?php
                    if(isset($sumace['prumernaObj'])){
                        foreach ($sumace['prumernaObj'] as $b_id=>$value){
                            //pr($value);
                            echo '<div class="branches_pob">';
                            echo '<span class="branches-sum pull-left">'.$branches_list[$b_id].'</span>'.'<span class="branches-price">'.$value['count_orders_price'].' ks</span>';
                            //echo '<span class="branches-sum">'.$branches_list[$b_id].': '.$this->Fastest->price($value['prumer']).'</span>';
                            echo '</div>';
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
</div>

<!-- END TOP STATS DATA -->
<div id="cats_chart_json" class="none"><?php echo json_encode($graphResult['cats']);?></div>

<?php //echo $this->element('pagination'); ?>
<?php } else { ?>
	<p class="alert alert-warning">Nenalezeny žádné záznamy, upravte filtraci.</p>
<?php } ?>

<script type="text/javascript">
//<![CDATA[
    window.addEvent('domready',function(){    
    $('statsRender').empty();
    $('statsRender').adopt($('statsTopData'));
    });
//]]>
</script>

<?php //pr($graphResult); ?>

<div id="sale_chart_price_json" class="none"><?php echo json_encode($graphResult['sales_price_predpoklad']);?></div>
<!-- <div id="sale_chart_json" class="none"><?php echo json_encode($graphResult['sales']);?></div> -->
<!-- <div id="area_chart_json" class="none"><?php echo ($graphResult['areas']);?></div> -->
<div id="cats_chart_json" class="none"><?php echo json_encode($graphResult['cats']);?></div>
<div id="tadySebou_chart_json" class="none"><?php echo json_encode($graphResult['tady_sebou']);?></div>
<div id="watchdog_dashboard_chart_json" class="none"><?php echo (isset($graphResult['watchdog_dashboard']) ? json_encode($graphResult['watchdog_dashboard']) : '');?></div>

<div id="line_graf_chart_json" class="none"><?php echo (!empty($graphResult['line_graf'])?json_encode($graphResult['line_graf']):'');?></div> 
<!-- <div id="maps_json" class="none"><?php echo json_encode($graphResult['maps']);?></div> -->

<script type="text/javascript">
//<![CDATA[
window.addEvent('domready',function(){
	google.charts.load('current', {packages: ['corechart', 'bar']});
	// google.charts.setOnLoadCallback(saleChart);
	google.charts.setOnLoadCallback(saleChartPrice);
	// google.charts.setOnLoadCallback(areasChart);
	google.charts.setOnLoadCallback(catsChart);
	google.charts.setOnLoadCallback(tadySebouChart);
    google.charts.setOnLoadCallback(lineGraf);
    google.charts.setOnLoadCallback(watchdogDashboardChart);
	//load_heat_map();

	// prodej ks
	function saleChart() {
		var options = {

		  animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
		var jsonData = $('sale_chart_json').get('text');
		var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.ColumnChart(document.getElementById('sale_chart'));
		chart.draw(data,options);
	}
	
	// prodej predpoklad
	function saleChartPrice() {
		var options = {
          legend:'top',  
		  
          vAxis: {title: "Částka"},
          animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
		var jsonData = $('sale_chart_price_json').get('text');
        var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.ColumnChart(document.getElementById('sale_chart_price'));
		chart.draw(data,options);
	}
	
	
	// prodej cena
	function areasChart() {
		var options = {
		  animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
		var jsonData = $('area_chart_json').get('text');
		var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.ColumnChart(document.getElementById('area_chart'));
		chart.draw(data,options);
	}
	
	// category
	function catsChart() {
		var options = {
          
            legend:'top',  
		  animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
		var jsonData = $('cats_chart_json').get('text');
		var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.PieChart(document.getElementById('cats_chart'));
		chart.draw(data,options);
	}


	// tady sebou
	function tadySebouChart() {
		
		var options = {

            vAxis: {title: "Částka"},
          legend:'top',
		  animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
		var jsonData = $('tadySebou_chart_json').get('text');
		var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.ColumnChart(document.getElementById('tady_sebou_chart'));
		chart.draw(data,options);
	}

    // watchdog Dashboard
	function watchdogDashboardChart() {
		
		var options = {

          legend:'top',
          vAxis: {title: "Počet ks"},
		  animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
        var jsonData = $('watchdog_dashboard_chart_json').get('text');

        if(jsonData != ''){
            var data = new google.visualization.DataTable(jsonData);
            var chart = new google.visualization.ColumnChart(document.getElementById('watchdog_dashboard_chart'));
            chart.draw(data,options);
        }
	}
    // start line graf | pobočky | hodiny | tržby 
    function lineGraf() {
		if ($('line_graf_chart_json').get('text') != ''){
            var options = {
            title: 'Tržba dle času',
            //vAxis: { format:'#,### Kč'},
            
            vAxis: {title: "Částka"},
            animation:{
                duration: 2000,
                easing: 'out',
                startup: true
            },
            //   curveType: 'function',
            legend: { position: 'bottom' }
            };
            var jsonData = $('line_graf_chart_json').get('text');
            var data = new google.visualization.DataTable(jsonData);
            var chart = new google.visualization.LineChart(document.getElementById('line_graf'));
            chart.draw(data,options);
        } else {
            $('line_graf').addClass('noData');
        }
	}
	
	// sources
	/*
	function sourcesChart() {
		var options = {
		  animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
		var jsonData = $('sources_chart_json').get('text');
		var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.PieChart(document.getElementById('sources_chart'));
		chart.draw(data,options);
	}
	*/
	// maps
	
});
//]]>
</script>