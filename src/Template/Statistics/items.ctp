<div id="stats_load">
<?php if (isset($graphResult)){ ?>
<?php 
if(isset($statsTitle)){
	echo '<h1>'.$statsTitle.'</h1>';
	echo '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmRIyQUbK5x_c_cm78ZV5ODf-sqVO1-2s&v=3.exp&libraries=places,geometry,visualization&sensor=false"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
';
}
?>
<div class="row">
	<div class="col col-md-6 col-12">
		<strong class="title">Objednávky dle ceny:</strong>
		<div id="sale_chart_price" class="chart"></div>
	</div>
	<div class="col col-md-6 col-12">
		<strong class="title">Objednávky dle množství:</strong>
		<div id="sale_chart" class="chart"></div>
	</div>
	<div class="col col-md-12 col-12">
		<strong class="title">Objednávky dle oblastí:</strong>
		<div id="area_chart" class="chart"></div>
	</div>
	<div class="col col-md-6 col-12">
		<strong class="title">Objednávky dle kategorií:</strong>
		<div id="cats_chart" class="chart"></div>
	</div>
	<div class="col col-md-6 col-12">
		<strong class="title">Objednávky dle zdroje:</strong>
		<div id="sources_chart" class="chart"></div>
	</div>
	<div class="col col-md-12 col-12">
		<strong class="title">Statistika rozvozců:</strong>
		<div id="delivery_chart" class="chart"></div>
	</div>
	<div class="col col-md-12 col-12">
		<strong class="title">Objednávky dle místa:</strong>
		<div id="maps_chart" class="chart"></div>
	</div>
	
</div>

<strong class="title">Produkty dle objednávek:</strong>
<div class="table-responsive">
<table class="stats-table table-striped">
<?php  

function price($value){
	return number_format($value,2,",",".");
}
$total = 0;
//pr($graphResult['products']);die();	
foreach($graphResult['products'] AS $group_id=>$p){
	echo '<tr>';
		echo '<th>'.$group_list[$group_id].'</th>';
		echo '<th>Množství</th>';
		$total += $p['sum'];
		echo '<th class="text_right">'.price($p['sum']).'</th>';
	echo '</tr>';	
		
		array_multisort(array_column($p['list'], 'price'), SORT_DESC,
                array_column($p['list'], 'count'),      SORT_ASC,
                $p['list']);
		
		//pr($p['list']);die();
		
		foreach($p['list'] AS $prod_id=>$prod){
			echo '<tr>';
				echo '<td>'.$prod['name'].'</td>';
				echo '<td>'.$prod['count'].'</td>';
				echo '<td class="text_right">'.price($prod['price']).'</td>';
			echo '</tr>';
		}
}

			echo '<tr>';
				echo '<td>&nbsp;</td>';
				echo '<td></td>';
				echo '<td class="text_right"></td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>Celkem</td>';
				echo '<td></td>';
				echo '<td class="text_right"><strong>'.price($total).'</strong></td>';
			echo '</tr>';
		
	if (!empty($graphResult['noTransport']))	
	echo '<tr><td colspan="2"> </td></tr>';
	echo '<tr>';
		echo '<th>Není doprava</th>';
		echo '<th>Bon</th>';
	echo '</tr>';
	foreach($graphResult['noTransport'] AS $prod_id=>$prod){
		echo '<tr>';
				echo '<td>'.$prod['name'].'</td>';
				echo '<td>'.$prod['bon'].'</td>';
			echo '</tr>';
	
	}
	//pr($statsResult['noTransport']);
?>
</table>
</div>
<?php
if (isset($statsTitle)){
	echo '<strong class="title">Spotřeba skladu</strong>';
	echo '<div class="table-responsive">';
	echo '<table class="stats-table table-striped">';
		if (isset($stock_products)){
			echo '<tr>';
				echo '<th>ID</th>';
				echo '<th>Kod</th>';
				echo '<th>Produkt</th>';
				echo '<th>Počet</th>';
				echo '<th>Jednotka</th>';
			echo '</tr>';
			$nostockList = [];
			
			foreach($stock_products AS $prod_id=>$stock){
			//pr($productList[$prod_id]['unit_id']);
				if (!isset($stock_unit_list[$productList[$prod_id]->unit_id])){
					$nostockList[] = ($productList[$prod_id]);
				}
				echo '<tr>';
					echo '<td>'.$productList[$prod_id]['id'].'</td>';
					echo '<td>'.$productList[$prod_id]['code'].'</td>';
					echo '<td>'.$productList[$prod_id]['name'].'</td>';
					echo '<td>'.$stock.'</td>';
					echo '<td>'.(isset($stock_unit_list[$productList[$prod_id]->unit_id])?$stock_unit_list[$productList[$prod_id]->unit_id]:'Není přiřazeno').'</td>';
				echo '</tr>';
			}
			//echo '<tr><td>Nenalezeno:</td><td></td><td></td></tr>';
			/*
			foreach($nostockList AS $prod_id=>$stock){
				echo '<tr>';
					echo '<td>'.$productList[$prod_id]['name'].'</td>';
					echo '<td>'.$stock.'</td>';
					echo '<td>'.$stock_unit_list[$productList[$prod_id]->unit_id].'</td>';
				echo '</tr>';
			}
			*/
		} else {
			echo '<tr><td>Nenalezeny žádné produkty</td></tr>';
		}
	echo '</table>';
	echo '</div>';
	//pr('aa');
}


//pr($graphResult['products']); ?>
<?php //pr($graphResult); ?>

<div id="sale_chart_price_json" class="none"><?php echo json_encode($graphResult['sales_price']);?></div>
<div id="sale_chart_json" class="none"><?php echo json_encode($graphResult['sales']);?></div>
<div id="area_chart_json" class="none"><?php echo json_encode($graphResult['areas']);?></div>
<div id="cats_chart_json" class="none"><?php echo json_encode($graphResult['cats']);?></div>
<div id="sources_chart_json" class="none"><?php echo json_encode($graphResult['sources']);?></div>
<div id="delivery_chart_json" class="none"><?php echo json_encode($graphResult['delivery_stats']);?></div>
<div id="maps_json" class="none"><?php echo json_encode($graphResult['maps']);?></div>

<script type="text/javascript">
//<![CDATA[
window.addEvent('domready',function(){
	google.charts.load('current', {packages: ['corechart', 'bar']});
	google.charts.setOnLoadCallback(saleChart);
	google.charts.setOnLoadCallback(saleChartPrice);
	google.charts.setOnLoadCallback(areasChart);
	google.charts.setOnLoadCallback(catsChart);
	google.charts.setOnLoadCallback(sourcesChart);
	google.charts.setOnLoadCallback(deliveryChart);
	//load_heat_map();

	// prodej ks
	function saleChart() {
		var options = {
		  animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
		var jsonData = $('sale_chart_json').get('text');
		var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.ColumnChart(document.getElementById('sale_chart'));
		chart.draw(data,options);
	}
	
	// prodej cena
	function saleChartPrice() {
		var options = {
		  animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
		var jsonData = $('sale_chart_price_json').get('text');
		var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.ColumnChart(document.getElementById('sale_chart_price'));
		chart.draw(data,options);
	}
	
	// rozvozce
	function deliveryChart() {
		var options = {
		  animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
		var jsonData = $('delivery_chart_json').get('text');
		var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.ColumnChart(document.getElementById('delivery_chart'));
		chart.draw(data,options);
	}
	
	
	// prodej cena
	function areasChart() {
		var options = {
		  animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
		var jsonData = $('area_chart_json').get('text');
		var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.ColumnChart(document.getElementById('area_chart'));
		chart.draw(data,options);
	}
	
	// category
	function catsChart() {
		var options = {
		  animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
		var jsonData = $('cats_chart_json').get('text');
		var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.PieChart(document.getElementById('cats_chart'));
		chart.draw(data,options);
	}
	
	// sources
	function sourcesChart() {
		var options = {
		  animation:{
			startup: true,
			duration: 1000,
			easing: 'out',
		  },
		};
		var jsonData = $('sources_chart_json').get('text');
		var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.PieChart(document.getElementById('sources_chart'));
		chart.draw(data,options);
	}
	
	// maps
	function load_heat_map() {
		var taxiData = [];
		var data = JSON.decode($('maps_json').get('text'));
		//console.log( google.maps);
		Object.each(data, function(item){
			taxiData.push(new google.maps.LatLng(item.lat, item.lng));
		});
		//console.log(taxiData);
		
			
		var map, pointarray, heatmap;
		var geocoder;
		//var myJSON = JSON.encode("https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA");

	  //function initialize() {
		geocoder = new google.maps.Geocoder();
		 var styles = [{
			"featureType": "landscape",
			"stylers": [
				{"hue": "#FF0300"},
				{"saturation": -100},
				{"lightness": 4},
				{"gamma": 1}
			]
			},
			{
			"featureType": "road.highway",
			"stylers": [
				{"hue": "#FF0300"},
				{"saturation": -100},
				{"lightness": 21.39999999999999},
				{"gamma": 1}
			]
			},
			{
			"featureType": "road.arterial",
			"stylers": [
				{"hue": "#FBFF00"},
				{"saturation": 0},
				{"lightness": 0},
				{"gamma": 1}
			]
			},
			{
			"featureType": "road.local",
			"stylers": [
				{"hue": "#00FFFD"},
				{"saturation": 0},
				{"lightness": 0},
				{"gamma": 1}
			]
			},
			{
			"featureType": "water",
			"stylers": [
				{"hue": "#0078FF"},
				{"saturation": 0},
				{"lightness": 0},
				{"gamma": 1}
			]
			},
			{
			"featureType": "poi",
			"stylers": [
				{"hue": "#9FFF00"},
				{"saturation": -97.8},
				{"lightness": 0},
				{"gamma": 1}
			]
			}
		];
		
		var mapOptions = {
		  center: new google.maps.LatLng(data[0].lat,data[0].lng),
		  zoom: 13,
		  styles: styles
		};
		var map = new google.maps.Map($('maps_chart'),mapOptions);
		
		var pointArray = new google.maps.MVCArray(taxiData);
		
		heatmap = new google.maps.visualization.HeatmapLayer({
			data: pointArray
		  });
		
		  heatmap.setMap(map);
		  heatmap.set('radius', heatmap.get('radius') ? null : 15);
		  
		  var gradient = [
			'rgba(0, 255, 255, 0)',
			'rgba(0, 255, 255, 1)',
			'rgba(0, 191, 255, 1)',
			'rgba(0, 127, 255, 1)',
			'rgba(0, 63, 255, 1)',
			'rgba(0, 0, 255, 1)',
			'rgba(0, 0, 223, 1)',
			'rgba(0, 0, 191, 1)',
			'rgba(0, 0, 159, 1)',
			'rgba(0, 0, 127, 1)',
			'rgba(63, 0, 91, 1)',
			'rgba(127, 0, 63, 1)',
			'rgba(191, 0, 31, 1)',
			'rgba(255, 0, 0, 0)'
		  ];
		  //heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
			heatmap.setOptions({
					dissipating: true,
					maxIntensity: 10,
					radius: 10,
					opacity: 0.9,
					//dissipating: false
			});
	  //}

	  //google.maps.event.addDomListener(window, 'load', initialize);
	}
});
//]]>
</script>
<?php } else { ?>
	<div class="alert alert-warning">
		<strong>Upozornění: </strong>Žádné data pro zobrazení, upravte filtraci. (nebo pokladna neposílá data)
	</div>
<?php } ?>

</div>