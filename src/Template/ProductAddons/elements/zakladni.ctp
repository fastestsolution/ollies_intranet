    <fieldset>
        <legend><?php echo __("Základní údaje")?></legend>
            <div class="row">
                <div class="col-sm-6 col-md-6">
                        <?php echo $this->Form->input("name", ['label' => __("Název"),'class'=>'form-control']); ?>
                        <?php echo $this->Form->input("price", ['label' => __("Cena"),'class'=>'float form-control']); ?>

                </div>
                <div class="col-sm-6 col-md-6">
                        <?php echo $this->Form->input("group_id", ['label' => __("Skupina"),'class'=>'','options'=>$addon_list,'class'=>'form-control']); ?>

                </div>
            </div>
    </fieldset>

<script type="text/javascript">
//<![CDATA[

/*
$('dodavatel-id').addEvent('change',function(e){
	
	$('sklad_item').getElements('.show_dodavatel').each(function(item){
		item.addClass('none');
		if (item.hasClass('dodavatel'+$('dodavatel-id').value)){
			item.removeClass('none');
		}
		//$('sklad_item').getElement('.dodavatel'+e.target.value).removeClass('none');
	});
	
});
$('dodavatel-id').fireEvent('change');
*/
//]]>
</script>
