<?php
echo $this->Form->create($data,['id'=>'FormEdit']);
echo $this->Form->input("id");
//echo $this->Form->hidden("user_id", ['value' => $this->request->Session()->read("Auth.User.id")]);

echo $this->element('modal',['params'=>['zakladni'=>__('Základní')]]);
?>
<div class="col col-md-12 centered">
    <?php echo $this->Form->button('Uložit',['type'=>'submit','class'=>'btn btn-primary','label'=>false,'id'=>'SaveButton','data-path'=>'/product-addons/']); ?>
    <?php echo $this->Form->button('Zpět',['type'=>'button','class'=>'btn btn-danger','label'=>false,'id'=>'BackButton','data-path'=>'/product-addons/']); ?>

</div>
<!--echo $this->Form->button(__('Uložit'), ['class' => 'btn','id'=>'SaveModal']);-->
<?php
echo $this->Form->end();
?>
<script type="text/javascript">
    //<![CDATA[
    saveData();
    modalWindow();
    //]]>
</script>