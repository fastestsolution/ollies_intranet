<div id="show_menu" class="onlymobile fa fa fa-bars"></div>
<ul class="navigace" id="navigace">
	<?php 
	if($loggedUser->system_id != 1000){ 
		unset($menu[4]);
		unset($menu[5]);
		unset($menu[9]);
		unset($menu[10]); 
	}
	?>
	<?php foreach($menu AS $m){?>
		<li class="nav-item <?php echo (($m['url'] == $here)?'active':'')?>">
			<?php 
			if (isset($m['disable'])){
				echo '<span class="disable">'.$m['name'].'</span>';
			} else {
				echo $this->Html->link($m['name'],$m['url'],['title'=>$m['name'],'class'=>'nav-link pagePre']);
			}	
			?>
			<?php if (isset($m['child'])){ ?>
				<ul>
				<?php foreach($m['child'] AS $mm){?>
					<li class="nav-item dropdown-item <?php echo (($mm['url'] == $here)?'active':'')?>">
						<?php 
						if (isset($mm['disable'])){
							echo '<span class="disable">'.$mm['name'].'</span>';
						} else {
							echo $this->Html->link($mm['name'],$mm['url'],['title'=>$mm['name'],'class'=>'nav-link']);
						
						}
						?>
					</li>	
				<?php } ?>
				</ul>
			<?php } ?>
		</li>
	<?php } ?>


<!--    <li class="nohover">
        <i class="fa fa-envelope-o" aria-hidden="true"></i>
    </li>
    <li class="nohover">
        <i class="fa fa-bell-o" aria-hidden="true"></i>
    </li>
    <li class="nohover">
        <i class="fa fa-search" aria-hidden="true"></i>
    </li>-->


<?php if (isset($loggedUser)){ ?>
	<li  class="nohover " id="loggedUser">
		<?php echo $loggedUser['name']; ?>
		<?php echo $this->Html->link('Odhlásit','/logout/',['class'=>'btn btn-primary btn-sm']); ?>
	</li>
<?php } ?>

<?php
	if (isset($sendUpdate)){
		echo '<div class="lastUpdateContainer">';
		echo $this->Form->input('Odeslat aktualizace',['type'=>'button','class'=>'btn btn-warning','label'=>false,'id'=>'sendUpdate']);
		echo '</div>';
		if (!empty($last_update)){
			echo "<div class=\"lastUpdateDate\">";
				echo '<p>Poslední aktualizace:</p><p>'.date('d.m. H:i:s',strtotime($last_update)) . "</p>";
			echo "</div>";
		}
	}
?>

</ul>
<a id="logo" href="/"></a>	<div class="versionInfo"><?php echo VERSION ?></div>

<script type="text/javascript">
//<![CDATA[
   $('show_menu').addEvent('click',function(e){
		$('navigace').toggleClass('open');
   });
   if ($('pay_points')){
	$('pay_points').addEvent('change',function(e){
		e.stop();
		if (e.target.checked){
			var value = 1;
		} else {
			var value = 0;
		}
		this.req_sent = new Request.JSON({
            url:'/status-online/switch/true/',//routa v controleru
			onError: this.req_error = (function(data){
                //button_preloader(button);
            }).bind(this),

            onComplete :(function(json){
			    //button_preloader(button);
                if (json.result == false){
                    FstError(json.message);
				} else {
                    FstAlert(json.message);
				    //console.log(json.message2);

                }

            }).bind(this)
        });
        this.req_sent.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
        this.req_sent.send();
		//console.log(value);
	});
   }
//]]>
</script>
