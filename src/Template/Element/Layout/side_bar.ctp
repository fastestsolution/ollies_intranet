<div id="modal-tabs"></div>
<?php

if(isset($submenu)){
    echo "<ul class=\"side-bar-items\">";
    
    foreach ($submenu AS $s){
        echo '<li class="'.(isset($activeUrl) && $activeUrl == $s['url']?'active':'').'">';
            echo '<a class="pagePre" href="'.$s['url'].'">';
                echo $s['name'];
            echo '</a>';
        echo '</li>';
    }
    echo "</ul>";
}

?>