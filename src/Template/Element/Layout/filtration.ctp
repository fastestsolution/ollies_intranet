<div id="filtration-holder">

<?php

if (isset($params['filtrations'])) { 
echo '<div id="filtration">';

if (isset($params["view"]) && isset($params["view"]->show)) {
	echo "<div id=\"view-selected-holder\" style=\"display: none;\">";
	echo $this->Form->create("Send", ["url" => ["controller" => "Deadlines", "action" => "edit"], "type" => "post"]);
		echo $this->Form->hidden("ids", ["default" => $params["view"]->url, "id" => "view-selected-value-holder"]);
		echo $this->Form->submit("Zobrazit vybrané", ["class" => "btn btn-primary btn-small", "id" => "view-selected"]);
	echo $this->Form->end();
	echo "</div>";
}

if (isset($params["totalPriceSum"])) {
	echo "<div id=\"sum\"></div>";
}

echo '<form id="filtrations" class="row" onsubmit="return false">';
echo '<div class="row">';

	foreach ($params['filtrations'] AS $filtr) {
		
		if ($filtr != "") {
			
			$value = (isset($filtrData) && isset($filtrData[$filtr['key'].(isset($filtr['type'])?'|'.$filtr['type']:'')])?$filtrData[$filtr['key'].(isset($filtr['type'])?'|'.$filtr['type']:'')]:'');
			
			if ($filtr['type'] == 'select'){
				echo $this->Form->input($filtr['key'],['placeholder'=>$filtr['name'],'label'=>false,'class'=>'form-control fst_h','data-fparams'=>$filtr['key'],'options'=>$filtr['list'],'value'=>$value]);
			} else if ($filtr['type'] == 'date'){
				echo $this->Form->input($filtr['key'],['placeholder'=>$filtr['name'],'label'=>false,'class'=>'form-control fst_h date','data-fparams'=>$filtr['key'].(isset($filtr['type'])?'|'.$filtr['type']:''),'value'=>$value, "autocomplete" => "off"]);
			} else {
				echo $this->Form->input($filtr['key'],['placeholder'=>$filtr['name'],'label'=>false,'class'=>'form-control fst_h','data-fparams'=>$filtr['key'].(isset($filtr['type'])?'|'.$filtr['type']:''),'value'=>$value, "type" => "text", "autocomplete" => "off"]);
			}
		}
	}
	echo $this->Form->submit('Filtrovat',['class'=>'btn btn-primary btn-small']);
echo '</div>';
echo '</form>';
echo '</div>';
}
?>

</div>