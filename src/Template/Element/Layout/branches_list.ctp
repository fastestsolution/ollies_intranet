<?php

if (isset($swithBranches)){
echo '<div class="branches"></div>';
echo '<h2 class="branches">Pobočky</h2>';
echo '<span id="removeAll" style="font-size: 12px;">Odebrat vše</span>';
echo '<ul id="branches_list_foo">';
    foreach($branches_list AS $bid=>$b){
        echo '<li data-class="branch'.$bid.'">';
            $class = '';
            if (isset($params['filtrBranch'])){
                $class = ' fst_h';
            }
            echo $this->Form->input('branch'.$bid,['type'=>'checkbox','class'=>'checkbox branch'.$bid.' '.$class,'label'=>$b,'data-col'=>'branch'.$bid,'data-fparams'=>'system_id_'.$bid]);
            //echo $this->Html->link($b,'?switch_branch='.$bid,['title'=>'Přepnout provoz']);
        echo '</li>';
    }
echo '</ul>';
}
// pr($params['filtrBranch']);
?>
<script type="text/javascript">
//<![CDATA[
showBranches();
sendUpdate();
//]]>

/*
 *  Úprava checkboxů a labelů pro umožnění stylování.
 */
var list = document.getElementById("branches_list_foo"),
    child,
    child2,
    child3,
    i,
    j,
    k;
if(list && list.childNodes){
    for (i = 0; i < list.childNodes.length; i++) {
        child = list.childNodes[i];

        if (child.nodeName == "LI") {
            for (j = 0; j < child.childNodes.length; j++) {
                child2 = child.childNodes[j];

                if (child2.nodeName == "DIV") {
                    for (k = 0; k < child2.childNodes.length; k++) {
                        child3 = child2.childNodes[k];

                        if (child3.nodeName == "LABEL") {
                            child2.insertBefore(child3.childNodes[0], child3);
                            break;
                        }
                    }
                }
            }
        }
    }
}
</script>