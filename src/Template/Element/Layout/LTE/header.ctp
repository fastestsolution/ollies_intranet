 <?php /* ?>
 <header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo"></a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
           
          <li class="dropdown user user-menu slide-down ng-hide" ng-show="AuthUser">
            <a href="javascript:;" class="dropdown-toggle clearfix" data-toggle="dropdown">
              <img ng-src="{{getPath('user', AuthUser.avatar, AuthUser.id === -1 ? 'css_img' : 'avatar')}}" class="user-image">
              <span class="hidden-xs">
                  <strong ng-bind="AuthUser.first_name"></strong> <strong ng-bind="AuthUser.last_name"></strong><br/>
                  <small style="position: absolute; bottom: 6px" ng-bind="AuthUser.domain"></small>
              </span> 
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img ng-src="{{getPath('user', AuthUser.avatar,  AuthUser.id === -1 ? 'css_img' : 'avatar')}}" class="img-circle" alt="User Image">
                <p> 
                  <span>{{AuthUser.first_name}} {{AuthUser.last_name}}</span>
                  <small>Administrator</small>
                </p>
              </li>
              <?php /* <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li> */ ?>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                </div>
                <div class="pull-right">
                    <?= $this->Html->link( __('Odhlásit'), 'javascript:;', ['class'=>'btn btn-default btn-flat', 'ng-click'=>'redirect("'.$this->Url->build(['controller'=>'Pages', 'action'=>'logout','prefix'=>false]).'")']); ?>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>