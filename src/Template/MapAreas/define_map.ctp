<p class="no_mobile">Není dostupné na mobilním zařízení</p>
<div id="map_area">
	<div id="panel">
	<p class="tips">Nejprve vytvořte oblast a následně klikněte na Uložit oblasti.</p>
	<strong>Barva oblasti</strong><br />
	<div id="color-palette"></div>
	<br class="clear" />
	<br class="clear" />
	<div>
		<form action="/mapareas/" method="post" id="FormArea" class="form">
			<div class="none">
			<?php echo $this->Form->input('area_coords',[]); ?>
			<?php echo $this->Form->input('area_color',[]); ?>
			</div>
			<strong>Název oblasti:</strong><br />
			<?php echo $this->Form->input('area_name',['class'=>'form-control','placeholder'=>'Název oblasti','label'=>false]);?>
        </form>
		<div class="buttons">
			<button id="delete-button" class="btn btn-default">Smazat vybranou oblast</button>
			<button id="rename-button" class="btn btn-default">Přejmenovat oblast</button>
			<button id="gen_coords"  class="btn btn-primary">Uložit oblasti</button>
		</div>
     </div>
	
	<div id="coords" class="none">
		Nejprve vytvořte oblasti, nebo je upravte a následnì kliknìte na Uložit oblasti.
	</div>
	  
    </div>
	
	<div id="win_name" class="none">
		<div id="win_name_over"></div>
		<div id="win_name_inner">
			
        
		</div>
	</div>
	<div id="pac-container">
        <input id="pac-input" type="text" class="form-control" placeholder="Zadejte adresu">
      
	</div>
	<div id="maparea_canvas"></div>
	
	<div id="marea_map_data_load" class="none">
	<?php /* ?>
	{"ic4hb81f":{"name":"aaa","color":"#1E90FF","coords":"}_otH}|jvAjbdB~nr@xs^e~}D"},"ic4hb81g":{"name":"vvfd","color":"#32CD32","coords":"qphqHmvmeBhfeBn}nD~{aAk}uG"}}
	<?php */ ?>
	<?php 
	if (isset($load->coords) && !empty($load->coords)){ 
		echo $load->coords;
	}
	?>
	</div>

</div>

<?php echo $this->Html->script('/js/MapAreas/MapAreas.js'); ?>
<script type="text/javascript">
//<![CDATA[
   
window.addEvent('load', function () {
	loadScript();
	

});

//]]>
</script>