<?= $this->Form->create($data, ["id" => "FormEdit"]); ?>
<div class="row">
    <div class="col col-md-12 col-12">
        <div class="row">
            <div class="col col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="order-extract no-wrap">
                    <table>
                        <tr>
                            <td class="center break-up break-down" colspan="5"><img class="logo" src="/css/img/logo_cmyk_bw.jpg"></td>
                        </td>
                        
                        <?php
                        
                        /* Pokud je účtenka z Bistra Balbín } else { jiné pobočky. +++ */
                        if ($data->system_id == 12) {
                            echo
                                "<tr>" .
                                    "<td class=\"center\" colspan=\"5\">Bistro OLLIES</td>" .
                                "</tr>" .
                                "<tr>" .
                                    "<td class=\"center\" colspan=\"5\">Ollies Franchise s.r.o.</td>" .
                                "</tr>" .
                                "<tr>" .
                                    "<td class=\"center\" colspan=\"5\">Zengrova 660/33</td>" .
                                "</tr>" .
                                "<tr>" .
                                    "<td class=\"center\" colspan=\"5\">Ostrava-Vítkovice, 703 00</td>" .
                                "</tr>" .
                                "<tr>" .
                                    "<td class=\"center\" colspan=\"5\">IČ: 06889221, DIČ: CZ06889221</td>" .
                                "</tr>";
                        } else {
                            echo
                                "<tr>" .
                                    "<td class=\"center\" colspan=\"5\">Cukrárna OLLIES</td>" .
                                "</tr>" .
                                "<tr>" .
                                    "<td class=\"center\" colspan=\"5\">Ollies dorty s.r.o.</td>" .
                                "</tr>" .
                                "<tr>" .
                                    "<td class=\"center\" colspan=\"5\">Výstavní 2968/108</td>" .
                                "</tr>" .
                                "<tr>" .
                                    "<td class=\"center\" colspan=\"5\">Ostrava-Vítkovice, 703 00</td>" .
                                "</tr>" .
                                "<tr>" .
                                    "<td class=\"center\" colspan=\"5\">IČ: 28596030, DIČ: CZ28596030</td>" .
                                "</tr>";
                        }

                        ?>
                        <tr>
                            <td class="center" colspan="5">------------------------------------</td>
                        </tr>
                        <tr>
                            <td class="center" colspan="5">OBJEDNÁVKOVÝ TELEFON: 603 424 268</td>
                        </tr>
                        <tr>
                            <td class="center" colspan="5">OBJEDNÁVKY ONLINE: www.ollies.cz</td>
                        </tr>
                        <tr>
                            <td class="left big bold break-up break-down" colspan="5">Název: <?= (isset($data->table_name)) ? $data->table_name : "uloženo v lokální pokladně" ?></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="5">Daňový doklad: <?= $data->order_id ?></td>
                        </tr>
                        <tr>
                            <td class="right" colspan="5"><?= $data->created ?></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="5">Tisk provedl: <?= (isset($data->user_name)) ? $data->user_name : "nedefinováno" ?></td>
                        </tr>
                        <tr>
                            <td class="center" colspan="5">===========================================</td>
                        </tr>
                        <tr>
                            <td class="left bold">Položka</td>
                            <td class="center bold">Mn.</td>
                            <td class="center bold">DPH</td>
                            <td class="right bold">Kč/jed.</td>
                            <td class="right bold">Cena</td>
                        </tr>
                        <tr>
                            <td class="center" colspan="5">-------------------------------------------</td>
                        </tr>
                        <?php
                        
                        /*
                         * 0 = 15%
                         * 1 = 21%
                         * 2 = 0%
                         * 3 = 10%
                         */
                        $taxes = [0, 0, 0, 0];
                        $taxesRoot = [0, 0, 0, 0];

                        foreach ($data->order_items as $item) {
                            $count =  ($item->count);
                            $showItem = false;
                            if(isset($item->repair_count) && $item->repair_count > 0){
                                $showItem = true;
                                $count .= ' <strong class="purple" title="Oprava">(-'.$item->repair_count.')</strong>';
                            }
                            if(isset($item->soft_storno_count) && $item->soft_storno_count > 0){
                                $showItem = true;
                                $count .= ' <strong class="red"  title="Mekke storno">(-'.$item->soft_storno_count.')</strong>';
                            }
                            if (!$showItem && $count < 1){
                                continue;
                            }
                            
                            
                            switch ($item->tax_id) {
                                case 1:
                                    $taxes[0] += ($item->price - $item->price_without_tax) * $item->count;
                                    $taxesRoot[0] += $item->price_without_tax * $item->count;
                                break;
                                case 2:
                                    $taxes[1] += ($item->price - $item->price_without_tax) * $item->count;
                                    $taxesRoot[1] += $item->price_without_tax * $item->count;
                                break;
                                case 3:
                                    $taxes[2] += ($item->price - $item->price_without_tax) * $item->count;
                                    $taxesRoot[2] += $item->price_without_tax * $item->count;
                                break;
                                case 4:
                                    $taxes[3] += ($item->price - $item->price_without_tax) * $item->count;
                                    $taxesRoot[3] += $item->price_without_tax * $item->count;
                                break;
                            }

                            echo
                                "<tr>".
                                    "<td class=\"left\">" . $item->name . "</td>" .
                                    "<td class=\"center\">" . $count . "</td>" .
                                    "<td class=\"center\">" . $price_tax_list[$item->tax_id] . "</td>" .
                                    "<td class=\"right\">" . number_format($item->price, 2, '.', ' ') . "</td>" .
                                    "<td class=\"right\">" . number_format(($item->price * $item->count), 2, '.', ' ') . "</td>" .
                                "</tr>";
                            
                        }

                        ?>
                        <tr>
                            <td class="left big bold break-down break-up" colspan="3">Celkem s DPH:</td>
                            <td class="right big bold break-down break-up" colspan="2"><?= number_format($data->total_price, 2, '.', ' ') ?></td>
                        </tr>
                        <?php

                        if (isset($data->pay_casch) && $data->pay_casch > 0) {
                            echo
                                "<tr>" .
                                    "<td class=\"left break-down\" colspan=\"5\">Platba: Hotově</td>" .
                                "</tr>";
                        }

                        if (isset($data->pay_card) && $data->pay_card > 0) {
                            echo
                                "<tr>" .
                                    "<td class=\"left break-down\" colspan=\"5\">Platba: Kartou</td>" .
                                "</tr>";
                        }

                        if (isset($data->pay_gastro_ticket) && $data->pay_gastro_ticket > 0) {
                            echo
                                "<tr>" .
                                    "<td class=\"left break-down\" colspan=\"5\">Platba: Stravenkami</td>" .
                                "</tr>";
                        }

                        if (isset($data->pay_gastro_card) && $data->pay_gastro_card > 0) {
                            echo
                                "<tr>" .
                                    "<td class=\"left break-down\" colspan=\"5\">Platba: E-stravenkami</td>" .
                                "</tr>";
                        }

                        if (isset($data->voucher) && $data->voucher > 0) {
                            echo
                                "<tr>" .
                                    "<td class=\"left break-down\" colspan=\"5\">Platba: Voucher</td>" .
                                "</tr>";
                        }

                        if (isset($data->discount) && $data->discount > 0) {
                            echo
                                "<tr>" .
                                    "<td class=\"left break-down\" colspan=\"5\">Sleva: " . $data->discount . "</td>" .
                                "</tr>";
                        }

                        if (isset($data->pay_employee) && $data->pay_employee > 0) {
                            echo
                                "<tr>" .
                                    "<td class=\"left break-down\" colspan=\"5\">Zaměstnanecká sleva: " . $data->pay_employee . "</td>" .
                                "</tr>";
                        }

                        if ($taxes[0] > 0 && $taxesRoot[0] > 0) {
                            echo
                                "<tr>" .
                                    "<td class=\"left\" colspan=\"3\">DPH 15%</td>" .
                                    "<td class=\"right\" colspan=\"2\">" . number_format($taxes[0], 2, '.', ' ') . "</td>" .
                                "</tr>" .
                                "<tr>" .
                                    "<td class=\"left\" colspan=\"3\">Základ DPH 15%</td>" .
                                    "<td class=\"right\" colspan=\"2\">" . number_format($taxesRoot[0], 2, '.', ' ') . "</td>" .
                                "</tr>";
                        }

                        if ($taxes[1] > 0 && $taxesRoot[1] > 0) {
                            echo
                                "<tr>" .
                                    "<td class=\"left\" colspan=\"3\">DPH 21%</td>" .
                                    "<td class=\"right\" colspan=\"2\">" . number_format($taxes[1], 2, '.', ' ') . "</td>" .
                                "</tr>" .
                                "<tr>" .
                                    "<td class=\"left\" colspan=\"3\">Základ DPH 21%</td>" .
                                    "<td class=\"right\" colspan=\"2\">" . number_format($taxesRoot[1], 2, '.', ' ') . "</td>" .
                                "</tr>";
                        }

                        if ($taxes[2] > 0 && $taxesRoot[2] > 0) {
                            echo
                                "<tr>" .
                                    "<td class=\"left\" colspan=\"3\">DPH 0%</td>" .
                                    "<td class=\"right\" colspan=\"2\">" . number_format($taxes[2], 2, '.', ' ') . "</td>" .
                                "</tr>" .
                                "<tr>" .
                                    "<td class=\"left\" colspan=\"3\">Základ DPH 0%</td>" .
                                    "<td class=\"right\" colspan=\"2\">" . number_format($taxesRoot[2], 2, '.', ' ') . "</td>" .
                                "</tr>";
                        }

                        if ($taxes[3] > 0 && $taxesRoot[3] > 0) {
                            echo
                                "<tr>" .
                                    "<td class=\"left\" colspan=\"3\">DPH 10%</td>" .
                                    "<td class=\"right\" colspan=\"2\">" . number_format($taxes[3], 2, '.', ' ') . "</td>" .
                                "</tr>" .
                                "<tr>" .
                                    "<td class=\"left\" colspan=\"3\">Základ DPH 10%</td>" .
                                    "<td class=\"right\" colspan=\"2\">" . number_format($taxesRoot[3], 2, '.', ' ') . "</td>" .
                                "</tr>";
                        }

                        ?>
                        <tr>
                            <td class="center break-up" colspan="5">FIK: <?= ($data->fik != null) ? $data->fik : "uloženo v lokální pokladně" ?></td>
                        </tr>
                        <tr>
                            <td class="center" colspan="5">BKP: <?= ($data->bkp != null) ? $data->bkp : "uloženo v lokální pokladně" ?></td>
                        </tr>
                        <tr>
                            <td class="center break-up" colspan="5">Děkujeme za návštěvu</td>
                        </tr>
                        <tr>
                            <td class="center break-up break-down" colspan="5">a</td>
                        </tr>
                        <tr>
                            <td class="center break-down" colspan="5">Mějte sladký život</td>
                        </tr>
                        <tr>
                            <td class="center" colspan="5">Všechny nepečené výrobky jsou určeny k okamžité spotřebě,</td>
                        </tr>
                        <tr>
                            <td class="center break-down" colspan="5">uchovejte při teplotě +4°C do +8°C.</td>
                        </tr>
                        <tr>
                            <td class="center" colspan="5">Všechny pečené výrobky (Záviny, Muffiny, Koláče, Ricotty, Jahelníky, Sacher, Brownies) jsou určeny k okamžité spotřebě, uchovejte při teplotě +4°C do +15°C.</td>
                        </tr>
                        <tr>
                            <td class="center break-down" colspan="5">Zmrzlina je určena k okamžité spotřebě, uchovávejte při teplotě -17°C až -19°C.</td>
                        </tr>
                        <tr>
                            <td class="center" colspan="5">*Tímto symbolem jsou označeny pečené výrobky.</td>
                        </tr>
                        <tr>
                            <td class="center big bold break-up break-down" colspan="5">JAK SE VÁM LÍBILO?</td>
                        </tr>
                        <tr>
                            <td class="center" colspan="5">Podělte se s ostatními a napište nám recenzi na Google.</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="order-extract wrap">
                    <table>
                        <tr>
                            <td class="center break-down" colspan="5">Informace k objednávce:</td>
                        </tr>
                        <tr>
                            <td class="left" colspan="2">Storno objednávky:</td>
                            <td class="right" colspan="3"><span class="<?= strtolower($yes_no[$data->storno]) ?>"><?= $yes_no[$data->storno] ?></span></td>
                        </tr>
                        <?php

                        if (isset($data->storno_reason) && $data->storno_reason) {
                            echo
                                "<tr>
                                    <td class=\"left\" colspan=\"2\">Důvod storna:</td>
                                    <td class=\"left\" colspan=\"3\">", $data->storno_reason, "</td>
                                </tr>";
                        }

                        ?>
                        <tr>
                            <td class="left" colspan="2">Rozvoz:</td>
                            <td class="right" colspan="3">
                           
                            <?php if(isset($yes_no[(int) $data->delivery])): ?>
                                <span class="delivery <?= strtolower($yes_no[(int) $data->delivery]) ?>"><?= $yes_no[ (int) $data->delivery] ?></span>
                            <?php endif; ?>
                            </td> 
                        </tr>
                    </table>
                </div>
                <div style="text-align: right; margin-bottom: 10px;">
                    <?= $this->Form->button("Zpět", ["type" => "button", "class" => "btn btn-danger", "label" => false, "id" => "BackButton", "onclick" => "window.location = document.referrer;"]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<script type="text/javascript">
    //saveData();
</script>