<?= $this->Form->create($data, ['id' => 'FormEdit']); ?>
<?php

// phone number format 111222333 to 111 222 333
$matches = [];
$formattedPhoneNumber = $data->web_client->telefon;

if (preg_match('/^(\d{3})(\d{3})(\d{3})$/', $data->web_client->telefon, $matches))
{
    $formattedPhoneNumber = $matches[1] . ' ' .$matches[2] . ' ' . $matches[3];
}

?>
<div class="row">
    <div class="col-lg-2">

    </div>
    <div class="col-lg-8">
        <div class="row custom-web-order">
            <div class="col-lg-6">
                <?= $this->Form->input('id', ['class' => 'form-control', 'label' => 'ID:']); ?>
                
                <div class="row">
                    <div class="col-lg-6">
                        <?= $this->Form->input('WebClients_jmeno', ['class' => 'form-control', 'label' => 'Jméno:', 'value' => $data->web_client->jmeno]); ?>
                        <?= $this->Form->input('WebClients_id', ['class' => 'form-control none', 'label' => false, 'type' => 'text', 'value' => $data->web_client->id]); ?>
                        <?= $this->Form->input('WebClients_email', ['class' => 'form-control', 'label' => 'E-Mail:', 'type' => 'text', 'value' => $data->web_client->email]); ?>
                        <?= $this->Form->input('WebClients_ulice', ['class' => 'form-control', 'label' => 'Ulice:', 'type' => 'text', 'value' => $data->web_client->ulice]); ?>
                        <?= $this->Form->input('WebClients_psc', ['class' => 'form-control', 'label' => 'PSČ:', 'type' => 'text', 'maxlength' => '5', 'value' => $data->web_client->psc]); ?>
                    </div>
                    <div class="col-lg-6">
                        <?= $this->Form->input('WebClients_prijmeni', ['class' => 'form-control', 'label' => 'Příjmení:', 'value' => $data->web_client->prijmeni]); ?>
                        <?= $this->Form->input('WebClients_telefon', ['class' => 'form-control', 'label' => 'Telefon:', 'type' => 'text', 'maxlength' => '9', 'value' => $formattedPhoneNumber]); ?>
                        <?= $this->Form->input('WebClients_mesto', ['class' => 'form-control', 'label' => 'Město:', 'type' => 'text', 'value' => $data->web_client->mesto]); ?>
                    </div>
                </div>
                <?= $this->Form->input('created', ['class' => 'form-control', 'label' => 'Datum vytvoření:', 'readonly' => 'readonly', 'type' => 'text']); ?>
                <?= $this->Form->input('order_date', ['class' => 'form-control', 'label' => 'Datum vyzvednutí:', 'type' => 'text', 'value' => $data->order_date]); ?>
                <?= $this->Form->input('pickup_time', ['class' => 'form-control', 'label' => 'Čas vyzvednutí:', 'type' => 'text', 'value' => $data->pickup_time]); ?>
                <?= $this->Form->input('shop_doprava_id', ['class' => 'form-control', 'label' => 'Pobočka:', 'onchange' => 'checkBranch();', 'value' => $data->shop_doprava_id, 'options' => $webBranchesList]); ?>
                <?= $this->Form->input('delivery_fake', ['class' => 'form-control', 'label' => 'Rozvoz:', 'disabled' => 'disabled', 'value' => $data->delivery, 'options' => $yes_no]); ?>
                <?= $this->Form->input('delivery', ['class' => 'form-control none', 'label' => false, 'value' => $data->delivery, 'options' => $yes_no]); ?>
                <?= $this->Form->input('price_with_tax', ['class' => 'form-control', 'label' => 'Cena:']); ?>
                <div class="row">
                    <div class="col-lg-6">
                        <?= $this->Form->input('status', ['class' => 'form-control', 'label' => 'Status:', 'value' => $data->status, 'options' => $yes_no]); ?>
                    </div>
                    <div class="col-lg-6">
                        <?= $this->Form->input('kos', ['class' => 'form-control', 'label' => 'Koš:', 'value' => $data->kos, 'options' => $yes_no]); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <span class="header">Produkty:</span>
                <div class="inventory">
                    <?php

                    foreach ($data->web_order_items as $item) {
                        echo
                            '<div class="item">
                                <span>', $item->name, '</span><span>x', $item->ks, '</span>
                            </div>';
                    }

                    ?>
                </div>
                <span class="header">Historie:</span>
                <div class="history">
                    <?= $this->Form->input('history', ['class' => 'form-control', 'type' => 'textarea', 'readonly' => 'readonly', 'value' => str_replace('&lt;br&gt;', PHP_EOL, $data->history), 'label' => false]); ?>
                </div>
            </div>
        </div>
        <div class="row custom-web-order" style="margin-top: 20px;">
            <div class="col-md-1">
                <?= $this->Form->button('Zpět', ['type' => 'button', 'class' => 'btn btn-danger', 'label' => false, 'id' => 'BackButton', 'data-path' => '/orders-web/']); ?>
            </div>
            <div class="col-md-10">
                
            </div>
            <div class="col-md-1" style="text-align: right;">
                <?= $this->Form->button('Uložit', ['type' => 'submit', 'class' => 'btn btn-primary', 'label' => false, 'id' => 'SaveButton', 'data-path' => '/orders-web/']); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?= $this->Form->end(); ?>
<script type="text/javascript">
    saveData();

    function checkBranch() {
        
        'use strict';

        var branch = $('shop-doprava-id'),
            deliveryFake = $('delivery-fake'),
            delivery = $('delivery');
        
        if (branch.value === '1' || branch.value === '6') {
            deliveryFake.value = 1;
            delivery.value = 1;
        } else {
            deliveryFake.value = 0;
            delivery.value = 0;
        }

    }
</script>