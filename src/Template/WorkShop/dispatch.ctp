
<?php if($branches): ?>
    <ul class="nav navbar-dark bg-dark justify-content-center">
        <?php foreach($branches as $id => $branch): ?>
            <li class="nav-item">
                <a href="/work-shop/dispatch/<?= $typeId; ?>/<?= $id; ?>" class="nav-link <?= $id == $branchId ? 'active' : '' ?>"><?= $branch; ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<?php if($types): ?>
    <ul class="nav navbar-light justify-content-center" style="background-color: #2aabd2;">
        <?php foreach($types as $id => $type): ?>
            <li class="nav-item">
                <a href="/work-shop/dispatch/<?= $id ; ?>/<?= $branchId; ?>" class="nav-link <?= $id == $typeId ? 'active' : '' ?>"><?= $type; ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<div class="container">

    <strong><?= $this->Flash->render() ?></strong>

    <strong><?= (isset($types[$typeId]) ? $types[$typeId] . ' - ' : '') . ( isset($branches[$branchId]) ? $branches[$branchId] . ' '  : '') . $date->format('d.m.Y'); ?></strong>
    
    <?php if($branchId): ?>
        <?php if($data): ?>
            <?php echo $this->Form->create(null); 
                $allowSendForm = false;
            ?>
            <?php if($typeId == 1): ?>
                    <?= $this->Form->input('system_id_to' ,['type'=>'hidden', 'value'=> $branchId,  'label'=> false ]); ?>
                    <table class="table table-strip">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Produkt</th>
                                <th>Objednáno</th>
                                <th>Dnes převedeno</th>
                                <th>Převést</th>
                                <th>Poznamka</th>
                            </tr>
                        </thead>
                        <?php foreach($data as $i=>$item):
                            $allCakeCount = ceil( $item->count_item / 12 );
                        ?>
                            <tr>
                                <td><?= $item->stock_item_id; ?></td>
                                <td><?= $item->name; ?></td>
                                <td><?= $allCakeCount ?></td>
                                <td><?= (isset($stockMoves[$item->stock_item_id]) ? round($stockMoves[$item->stock_item_id] / 12, 2) : 0) ?></td>
                               
                                <?php if(!isset($stockMoves[$item->stock_item_id]) || $item->count_item > $stockMoves[$item->stock_item_id]): 
                                    $allowSendForm = true;
                                    if(isset($stockMoves[$item->stock_item_id]) && $stockMoves[$item->stock_item_id] > 0){
                                        $allCakeCount = ceil(($item->count_item - $stockMoves[$item->stock_item_id]) / 12);
                                    }
                                ?>
                                    <td>
                                        <?= $this->Form->input('items.'. $i . '.count' ,['type'=>'number', 'data-target'=> 'items-'.$i .'-count-items' , 'value'=>  $allCakeCount, 'label'=>false, 'class'=>'text-center']); ?>
                                        <?= $this->Form->input('items.' . $i . '.stock_item_id' ,['type'=>'hidden', 'value'=> $item->stock_item_id, 'label'=>false ]); ?>
                                    </td>
                                    <td>
                                         <?= $this->Form->input('items.'. $i . '.note' ,['type'=>'text', 'label'=>false, 'placeholder'=>'Poznamka...']); ?>
                                    </td>
                                <?php else: ?>
                                    <td colspan="2" class="text-center">Již nelze převest</td>
                                <?php endif; ?>
                               
                                
                            </tr>
                        <?php endforeach; ?>
                    </table>

                    <?php if($allowSendForm): ?>
                        <?= $this->Form->submit('Odeslat převodku (na jmeno)', ['class'=>'btn btn-primary pull-right']); ?>
                    <?php else: ?>
                        <strong class="pull-right">Převodka pro tuto pobočku již byla odeslána</strong>
                    <?php endif; ?>
            <?php elseif($typeId == 2): ?>
                    <?= $this->Form->input('system_id_to' ,['type'=>'hidden', 'value'=> $branchId,  'label'=> false ]); ?>
                    <table class="table table-strip">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Produkt</th>
                                <th>Objednáno</th>
                                <th>Dnes převedeno</th>
                                <th>Převést</th>
                                <th>Poznamka</th>
                            </tr>
                        </thead>
                        <?php foreach($data as $i=>$item):
                            $allCakeCount = ceil( $item->count_item / 12 );
                        ?>
                            <tr>
                                <td><?= $item->stock_item_id; ?></td>
                                <td><?= $item->name; ?></td>
                                <td><?= $allCakeCount ?></td>
                                <td><?= (isset($stockMoves[$item->stock_item_id]) ? round($stockMoves[$item->stock_item_id] / 12, 2) : 0) ?></td>
                                <?php 
                                    if(isset($stockMoves[$item->stock_item_id]) && $stockMoves[$item->stock_item_id] > 0){
                                        $allCakeCount = ceil(($item->count_item - $stockMoves[$item->stock_item_id]) / 12);
                                        if($allCakeCount <= 0){
                                            $allCakeCount = 0;
                                        }
                                    }
                                ?>
                                    <td>
                                        <?= $this->Form->input('items.'. $i . '.count' ,['type'=>'number', 'data-target'=> 'items-'.$i .'-count-items' , 'value'=>  $allCakeCount, 'label'=>false, 'class'=>'text-center']); ?>
                                        <?= $this->Form->input('items.' . $i . '.stock_item_id' ,['type'=>'hidden', 'value'=> $item->stock_item_id, 'label'=>false ]); ?>
                                    </td>
                                    <td>
                                            <?= $this->Form->input('items.'. $i . '.note' ,['type'=>'text', 'label'=>false, 'placeholder'=>'Poznamka...']); ?>
                                    </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>

                    <?= $this->Form->submit('Odeslat převodku (volny prodej)', ['class'=>'btn btn-primary pull-right']); ?>
            <?php endif; ?>

       
            <?php echo $this->Form->end(); ?>

            <script>
                /*$$('.sync-fields').addEvent('change', function(){
                    var value = this.value;
                    var target = this.get('data-target');
                    if(this.get('min') && this.get('min') > 0){
                        if(value < this.get('min')){
                            this.value = value = this.get('min');
                        }
                    }
                    if(target.substr(-5) == 'items'){
                        $(target).value = value * 12;
                    }else{
                        $(target).value = value / 12;
                    }
                });*/
            </script>
    <?php else: ?>
        <div class="strong">Pobočka pro dnešek nemá žádné objednávky na jmeno</div>
    <?php endif; ?>
    <?php else: ?>
        <div class="strong">Není zvolena provozovna, zvolte ji v horním menu</div>
    <?php endif; ?>
</div>

