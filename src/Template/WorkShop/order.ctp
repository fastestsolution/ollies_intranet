<div class="row">
    <?= $this->Form->create($data, ['url' => ['controller' => 'WorkShop', 'action' => 'orderSend'], 'id' => 'FormWorkshopOrder', 'style' => 'width: 100%']); ?>
        <div class="row workshop-filtration">
            <div class="col-lg-3">
                <?php
                
                $id = null;
                
                foreach ($dataToday as $dt) {
                    $id = $dt->web_order->id;
                    break;
                }

                ?>
                <?= $this->Form->input('id', ['class' => 'form-control', 'label' => false, 'type' => 'hidden', 'value' => $id != null ? $id : null]); ?>
                <?= $this->Form->input('system_id', ['class' => 'form-control', 'label' => false, 'options' => $branches_list, 'onchange' => 'selectSystemID(this);', 'value' => $system_id]); ?>
            </div>
            <div class="col-lg-6">
                
            </div>
            <div class="col-lg-3">
                <?= $this->Form->input('Uložit', ['type' => 'button', 'class' => 'btn', 'label' => false, 'id' => 'ButtonSet']); ?>
            </div>
        </div>
        <div class="workshop-order-header">
            <table>
                <tr>
                    <td style="width: 30%;"></td>
                    <td style="width: 8%;"></td>
                    <td class="eshop" style="width: 24%;">Objednávky e-shop na další den</td>
                    <td style="width: 8%;"></td>
                    <td class="employee" style="width: 20%;">Objednávky obsluha</td>
                    <td style="width: 10%;"></td>
                </tr>
            </table>
        </div>
        <div class="workshop-order">
            <table>
                <tr>
                    <th style="width: 30%;">Název</th>
                    <th class="center" style="width: 8%;">Na skladě</th>
                    <th class="center eshop" style="width: 8%;">Celé</th>
                    <th class="center eshop" style="width: 8%;">Kusy</th>
                    <th class="center eshop" style="width: 8%;">Celkem</th>
                    <th class="center" style="width: 8%;">Zbytky</th>
                    <th class="center employee" style="width: 20%;">Volný prodej</th>
                    <th class="center" style="width: 10%;">Celkem vše</th>
                </tr>
                <?php

                $iterator = 0;

                // $data = WebOrderItems
                foreach ($data as $d) {
                    echo
                        '<tr>
                            <td>', $d->name .' ('.$d->code.')', '</td>
                            <td class="center">', isset($d->stock) ? $d->stock : '', '</td>
                            <td class="center eshop">', isset($d->ks) ? $d->ks : '', '</td>
                            <td class="center eshop">', isset($d->ks_ks) ? $d->ks_ks : '', '</td>
                            <td class="center eshop">', isset($d->total) ? $d->total : '', '</td>
                            <td class="center">', isset($d->left) ? $d->left : '', '</td>
                            <td class="center employee">';

                            $valueAmount = null;
                            $is_new = true;

                            foreach ($dataToday as $dt) {
                                if ($d->code == $dt->code || $d->code == ($dt->code . '*')) {
                                    $valueAmount = $dt->ks;
                                    $is_new = false;
                                }
                            }

                            echo
                                $this->Form->input('items.' . $iterator . '.ks', ['class' => 'form-control', 'placeholder' => 'Počet celých kusů', 'label' => false, 'type' => 'number', 'min' => '1', 'value' => $valueAmount != null ? $valueAmount : null, 'onchange' => 'checkTotalFinal(this);', 'onkeyup' => 'checkTotalFinal(this);', 'onpaste' => 'checkTotalFinal(this);']),
                                $this->Form->input('items.' . $iterator . '.id', ['class' => 'form-control', 'label' => false, 'type' => 'hidden', 'value' => $d->id]),
                                $this->Form->input('items.' . $iterator . '.name', ['class' => 'form-control', 'label' => false, 'type' => 'hidden', 'value' => $d->name]),
                                $this->Form->input('items.' . $iterator . '.code', ['class' => 'form-control', 'label' => false, 'type' => 'hidden', 'value' => $d->code]),
                                $this->Form->input('items.' . $iterator . '.ean', ['class' => 'form-control', 'label' => false, 'type' => 'hidden', 'value' => isset($d->ean) ? $d->ean : null]),
                                $this->Form->input('items.' . $iterator . '.is_new', ['class' => 'form-control', 'label' => false, 'type' => 'hidden', 'value' => $is_new == true ? 1 : 0]),
                            '</td>
                            <td class="center"><span id="items-' . $iterator . '-total-final-base" class="none">', isset($d->total_final) ? $d->total_final : '', '</span><span id="items-' . $iterator . '-total-final">', isset($d->total_final) ? $d->total_final : 0, '</span></td>
                        </tr>';
                    
                    $iterator++;
                }

                ?>
            </table>
        </div>
    <?= $this->Form->end(); ?>
</div>
<script type="text/javascript">

function selectSystemID(el) {

    'use strict';

    window.location.href = '/work-shop/order/' + el.value;

}

function checkTotalFinal(el) {

    'use strict';

    var totalEl,
        totalElBase,
        totalElBaseValue    = 0,
        elValue             = 0,
        args                = el.id.split('-');

    totalElBase             = document.getElementById('items-' + args[1] + '-total-final-base');
    totalEl                 = document.getElementById('items-' + args[1] + '-total-final');
    totalElBaseValue        = !isNaN(parseInt(totalElBase.innerHTML)) ? parseInt(totalElBase.innerHTML) : 0;
    elValue                 = !isNaN(parseInt(el.value)) ? parseInt(el.value) : 0;

    if (totalElBaseValue < 0) {
        totalElBaseValue    = 0;
    }

    if (elValue < 0) {
        elValue             = 0;
    }

    totalEl.innerHTML = totalElBaseValue + elValue;

}

window.onload = evt => {

    var elCount = <?= count((array)$data); ?>,
        i;

    for (i = 0; i < elCount; i++) {
        checkTotalFinal(document.getElementById('items-' + i + '-ks'));
    }
    $('ButtonSet').addEvent('click', function(e){
        e.stop();
        $('FormWorkshopOrder').submit();
        FstAlert('Data byly uloženy');
    });

};

</script>