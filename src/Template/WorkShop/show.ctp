
        <div class="row workshop-filtration">
            <div class="col-lg-9">
                <div class="workshop-filtration-input">
                    <?= $this->Form->input('dateBeginning', ['class' => 'form-control date', 'label' => false, 'id' => 'DateBeginning', 'placeholder' => 'Datum zobrazení', 'value' => (($dateBeginning != null) ? $dateBeginning : '')]); ?>
                </div>
            </div>
            <div class="col-lg-3">
                <?= $this->Form->input('Nastavit', ['type' => 'button', 'class' => 'btn', 'label' => false, 'id' => 'ButtonSet', 'onclick' => 'setDate(event);']); ?>
            </div>
        </div>
        <div class="workshop">
            <?= $this->Form->create($data, ['id' => 'FormWorkshopOrder']); ?>
                <table>
                    <?php

                    $names = [];

                    echo
                    '<tr>
                        <th>Produkty k výrobě</th>';

                    foreach ($period as $day) {
                        echo
                            '<th class="center">', $day->format('j.n. Y'), '</th>';
                    }

                    echo
                        '</tr>';

                    foreach ($data as $product) {
                        if (!in_array($product->code, $names)) {
                            if (isset($result[$product->code])) {
                                echo
                                    '<tr>
                                        <td>', $product->name, '</td>';
                                
                                foreach ($result[$product->code] as $r) {
                                    echo
                                        '<td class="center">', $r, '</td>';
                                }

                                echo
                                    '</tr>';

                                array_push($names, $product->code);
                            }
                        }
                    }

                    ?>
                </table>
            <?= $this->Form->end(); ?>
        </div>
    </div>
   
<script type="text/javascript">

function setDate(e) {

    "use strict";

    var input = document.getElementById('DateBeginning');

    e.preventDefault();

    if (input.value !== '' && input.value.match(/((0[1-9]|[12]\d|3[01]).(0[1-9]|1[0-2]).[12]\d{3})$/)) {
        window.location = input.value;
    } else {
        window.location = '/work-shop/show/';
    }
}

</script>