<?php echo $this->Form->create($data,['id'=>'FormEdit']);?>
<?php //pr($data); ?>	
<div class="row">
	<div class="col col-md-6 col-12">
		<?php echo $this->Form->input('id',['class'=>'form-control','label'=>'id']); ?>
		<?php echo $this->Form->input('name',['class'=>'form-control','label'=>'Název']); ?>
		<?php echo $this->Form->input('email',['class'=>'form-control','label'=>'Email pro uzávěrky']); ?>
	</div>
	<div class="col col-md-6 col-12">
		<?php 
		// if (empty($data->id)){
			echo $this->Form->input('branches_from',['class'=>'form-control','label'=>'Zkopírovat ceny z','options'=>$branches_list,'empty'=>true]); 
		// }
		?>
		
	</div>
	<div class="col col-md-12 centered">
		<?php echo $this->Form->button('Uložit',['type'=>'submit','class'=>'btn btn-primary','label'=>false,'id'=>'SaveButton','data-path'=>'/branches/']); ?>
		<?php echo $this->Form->button('Zpět',['type'=>'button','class'=>'btn btn-danger','label'=>false,'id'=>'BackButton','data-path'=>'/branches/']); ?>
	
	</div>
	
</div>	
<?php echo $this->Form->end();?>
<script type="text/javascript">
//<![CDATA[
saveData();
//]]>
</script>
