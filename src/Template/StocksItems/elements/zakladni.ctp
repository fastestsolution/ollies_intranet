<fieldset id="sklad_item">
    <legend><?php echo __("Základní údaje")?></legend>
        <div class="row">
            <div class="col-sm-6 col-md-6">
                    <?php echo $this->Form->input("dodavatel_id", ['label' => __("Dodavatel"),'options'=>$supplier_list,'empty'=>true,'class'=>'fst_select form-control','data-url'=>'/skladItems/dodavatelAdd/']); ?>
                    <?php echo $this->Form->input("name", ['label' => __("Název"),'class'=>'form-control']); ?>
                    <div class="show_dodavatel dodavatel1">
                    <?php echo $this->Form->input("ean", ['label' => __("Ean"),'class'=>'form-control']); ?>
                    </div>
                    <div class="show_dodavatel dodavatel2">
                    <?php echo $this->Form->input("bidfood_id", ['label' => __("BidFood ID"),'type'=>'text','class'=>'form-control']); ?>
                    </div>
                    <?php echo $this->Form->input("jednotka_prevod", ['label' => __("Jednotka převod"),'type'=>'text','class'=>'form-control']); ?>
            </div>
            <div class="col-sm-6 col-md-6">
                    <?php echo $this->Form->input("jednotka_id", ['label' => __("Jednotka"),'options'=>$stock_unit_list,'class'=>'form-control']); ?>
                    <?php echo $this->Form->input("jednotka_id_dodavatel", ['label' => __("Jednotka dodavatel"),'class'=>'form-control']); ?>
                    <?php //echo $this->Form->input("group_id", ['label' => __("Skupina"),'options'=>$sklad_group_list,'empty'=>true,'class'=>'fst_select','data-url'=>'/skladItems/skladGroupAdd/']); ?>
                    <?php echo $this->Form->input("sklad_item_global_id", ['label' => __("Hlavní položka"),'options'=>$stock_global_list,'empty'=>true,'class'=>'search_select form-control','data-url'=>'/StocksItems/StocksGroupAdd/']); ?>
            </div>
      </div>
</fieldset>

<fieldset>
    <legend><?php echo __("Nákupní údaje")?></legend>
        <div class="row">
            <div class="col-sm-6 col-md-6">
                    <?php echo $this->Form->input("min", ['label' => __("Min množství"),'class'=>'float form-control']); ?>
                    <?php echo $this->Form->input("max", ['label' => __("Max množství"),'class'=>'float form-control']); ?>
            </div>
            <div class="col-sm-6 col-md-6">
                    <?php echo $this->Form->input("tax_id", ['label' => __("DPH"),'class'=>'form-control','options'=>$price_tax_list]); ?>
            </div>
        </div>
</fieldset>
<script type="text/javascript">
//<![CDATA[
$('dodavatel-id').addEvent('change',function(e){
	
	$('sklad_item').getElements('.show_dodavatel').each(function(item){
		item.addClass('none');
		if (item.hasClass('dodavatel'+$('dodavatel-id').value)){
			item.removeClass('none');
		}
		//$('sklad_item').getElement('.dodavatel'+e.target.value).removeClass('none');
	});
	
});
$('dodavatel-id').fireEvent('change');
//]]>
</script>
