

<?php 
/*
echo 'Přihlášen: '.$loggedUser['name']; ?>
<br />
<br />
<?php echo $this->Html->link('Odhlásit','/logout/',['class'=>'btn btn-primary']); 
*/
?>

<p class='h2 text-center text-muted'>Dashboard | Statistiky</p>


<!-- <p>dodělat coly ,  kde budou statisiky prodeje a grafiky, možná rozložit grafy</p>
<p>Brát z tabulky Stock a tam je stock_item_product_id || jinak je to na staré pokladně statistiky</p>
<p>udělat si ješte field, jenom potrebne id ktere potrebuji a potrebuje sumaci toho vseho </p>
<p>Pomoct si group by a sum ||| cake 3 php example</p>
<p>nachystat inputy dle staré poklady a layout</p> -->

<div class='container-fluid'>
    <div class='row'>
        <div class='col-sm-6 col-md-3 top-statistics'>
            <div class='media'>
                <div>
                    <i class="fa fa-usd green" aria-hidden="true"></i>
                </div>
                <div class='right-media'>
                    <h2>899 tisíc kč</h2>
                    <p class='text-muted'>Čistého zisku</p>
                </div>
            </div>
        </div>
        <div class='col-sm-6 col-md-3 top-statistics'>
            <div class='media'>
                <div>
                    <i class="fa fa-shopping-cart yellow" aria-hidden="true"></i>
                </div>
                <div class='right-media'>
                    <h2>1678</h2>
                    <p class='text-muted'>Prodaných produktů</p>
                </div>
            </div>
        </div>
        <div class='col-sm-6 col-md-3 top-statistics'>
            <div class='media'>
                <div>
                    <i class="fa fa-users red" aria-hidden="true"></i>
                </div>
                <div class='right-media'>
                    <h2>822</h2>
                    <p class='text-muted'>Zákazníků</p>
                </div>
            </div>
        </div>
        <div class='col-sm-6 col-md-3 top-statistics'>
            <div class='media'>
                <div class=''>
                    <i class="fa fa-home blue" aria-hidden="true"></i>
                </div>
                <div class='right-media'>
                    <h2>4</h2>
                    <p class='text-muted'>pobočky</p>
                </div>
                
            </div>
        </div>
    </div>

    <div class='row'>
        <div class='col-sm-12 col-md-9 middle-statistics'>
            <h3 class='text-center text-muted'>Statistiky prodeje produktů</h3>    
                <div class='col-sm-6 col-md-3 pull-left'>
                    <?php //echo $this->Form->input("tax_id", ['label' => __("DPH"),'class'=>' save_item pull-right input-own form-control','options'=>$price_tax_list]); ?>
                    <?php //echo $this->Form->input("value", ['label' => __("Množství"),'class'=>'float price_ks save_item clear_save pull-right input-own form-control']); ?>
                    <?php echo $this->Form->input("created_date", ['placeholder' => __("Datum od"),'label'=>false,'class'=>'float price save_item clear_save pull-right input-own form-control form-control-sm date','data-type'=>'created_date']); ?>
                    <?php echo $this->Form->input("created_date2", ['placeholder' => __("Datum do"),'label'=>false,'class'=>'float price  save_item clear_save pull-right input-own form-control form-control-sm date','data-type'=>'created_date2']); ?>
                    <?php echo $this->Form->input("product_group_id", ['placeholder' => __("Skupina"),'options'=>[0=>'Skupina']+ $groupList,'label'=>false,'class'=>'float price  save_item clear_save pull-right input-own form-control form-control-sm','data-type'=>'product_group_id']); ?>
                    <?php echo $this->Form->input("user_id", ['placeholder' => __("Pracovník"),'options'=>[0=>'Pracovník'],'label'=>false,'class'=>'float price save_item clear_save pull-right input-own form-control form-control-sm','data-type'=>'user_id']); ?>
                    <?php echo $this->Form->input("price_tax_id", ['placeholder' => __("DPH"),'label'=>false,'options'=>[0=>'DPH']+ $price_tax_list,'class'=>'float price save_item clear_save pull-right input-own form-control form-control-sm','data-type'=>'price_tax_id']); ?>
                    <?php echo $this->Form->input("storno", ['placeholder' => __("Storno"),'label'=>false,'options'=>[2=>'Storno']+ $yes_no,'class'=>'float price save_item clear_save pull-right input-own form-control form-control-sm','data-type'=>'storno']); ?>
                </div>

                <div class='col-sm-6 col-md-9 pull-right'>
                    <div id='stats-items'>
                        <?php 
                            echo $this->element('stats/table');
                        ?>
                    </div>
                </div>

        </div>

        <div class='col-sm-12 col-md-3 middle-statistics'>                        
            <div class="card-box">
                <h3 class="header-title m-t-0 text-center text-muted">Výdělky poboček</h3>
                    <div id="morris-bar-example" style="height: 280px; position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);" class="m-t-10"><svg height="280" version="1.1" width="491" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; left: -0.328125px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="32.515625" y="241" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#eeeeee" d="M45.015625,241H466" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.515625" y="187" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">25</tspan></text><path fill="none" stroke="#eeeeee" d="M45.015625,187H466" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.515625" y="133" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">50</tspan></text><path fill="none" stroke="#eeeeee" d="M45.015625,133H466" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.515625" y="79" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">75</tspan></text><path fill="none" stroke="#eeeeee" d="M45.015625,79H466" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.515625" y="25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">100</tspan></text><path fill="none" stroke="#eeeeee" d="M45.015625,25H466" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="435.9296875" y="253.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2016</tspan></text><text x="315.6484375" y="253.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2014</tspan></text><text x="195.3671875" y="253.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2012</tspan></text><text x="75.0859375" y="253.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2010</tspan></text><rect x="69.071875" y="79" width="12.028125000000001" height="162" rx="0" ry="0" fill="#42a5f5" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="129.2125" y="150.28" width="12.028125000000001" height="90.72" rx="0" ry="0" fill="#42a5f5" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="189.353125" y="79" width="12.028125000000001" height="162" rx="0" ry="0" fill="#42a5f5" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="249.49375" y="158.92" width="12.028125000000001" height="82.08000000000001" rx="0" ry="0" fill="#42a5f5" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="309.634375" y="199.95999999999998" width="12.028125000000001" height="41.04000000000002" rx="0" ry="0" fill="#42a5f5" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="369.775" y="40.119999999999976" width="12.028125000000001" height="200.88000000000002" rx="0" ry="0" fill="#42a5f5" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="429.915625" y="133" width="12.028125000000001" height="108" rx="0" ry="0" fill="#42a5f5" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect></svg><div class="morris-hover morris-default-style" style="left: 89.2266px; top: 109px; display: none;"><div class="morris-hover-row-label">2011</div><div class="morris-hover-point" style="color: #42a5f5">Statistics:42</div>
            </div>
        </div>
    </div>
</div>


 