<?php 
if (isset($onlineStatus)){ 
	if ($onlineStatus['result'] == false){
		echo '<div class="alert alert-danger">'.$onlineStatus['message'].'</div>';
	} else {
		echo '<div class="alert '.(($onlineStatus['status_online'] == 0)?'alert-danger':'alert-success').'">'.$onlineStatus['message'].'</div>';
	
	}
	//echo $this->Form->button('Změnit stav',['id'=>'switchOnline','class'=>'btn']);
	echo $this->Html->link('Změnit stav','/status-online/switch/',['title'=>'Změnit stav','class'=>'btn btn-primary','id'=>'changeStav']);
}
?>
<script type="text/javascript">
//<![CDATA[
window.addEvent('domready',function(){
	$('changeStav').addEvent('click',function(){
		 button_preloader($('changeStav'));
	});
});
//]]>
</script>