<?php echo $this->Form->create('',['id'=>'FormEdit']);?>
<?php //pr($data); ?>	
<script src="/js/fst_uploader/fst_uploader.js" ></script>
<div class="row">
		<?php //pr($intranet_type_list); ?>
	<div class="col col-md-6 col-12">
		<?php echo $this->Form->input('phone_list',['class'=>'form-control','label'=>'Telefon','options'=>$phone_list,'multiple'=>true,'style'=>'height:500px;']); ?>
		<br />
		<?php echo $this->Form->button('Vybrat vše',['type'=>'button','class'=>'btn btn-warning','label'=>false,'id'=>'SelectAll']); ?>
	</div>
	<div class="col col-md-6 col-12">
		<?php echo $this->Form->input('text',['class'=>'form-control','type'=>'textarea','label'=>'SMS']); ?>
		<br />
		<?php echo $this->Form->button('Poslat',['type'=>'submit','class'=>'btn btn-primary','label'=>false,'id'=>'SaveButton','data-path'=>'/intranetEdit/']); ?>
		
	</div>
	<div class="col col-md-12 centered">
		<?php //echo $this->Form->button('Poslat',['type'=>'submit','class'=>'btn btn-primary','label'=>false,'id'=>'SaveButton','data-path'=>'/intranetEdit/']); ?>
		<?php //echo $this->Form->button('Zpet',['type'=>'button','class'=>'btn btn-danger','label'=>false,'id'=>'BackButton','data-path'=>'/intranetEdit/']); ?>
	
	</div>
</div>	
<?php echo $this->Form->end();?>
<script type="text/javascript">
//<![CDATA[
window.addEvent('domready',function(){
	$('SelectAll').addEvent('click',function(e){
			if (!$('SelectAll').hasClass('all')){
				$('SelectAll').addClass('all');
				$('phone-list').getElements('option').each(function(item){
					item.setProperty('selected','selected');
				});
			} else {
				$('SelectAll').removeClass('all');
				$('phone-list').getElements('option').each(function(item){
					item.removeProperty('selected','selected');
				});			
			}
	});
	$('SaveButton').addEvent('click',function(e){
			e.stop();
			button_preloader($('SaveButton'));
						
				var saveReg = new Request.JSON({
					url:$('FormEdit').action,
					onComplete: function(json){
					
						button_preloader($('SaveButton'));
						if (json.result == true){
							FstAlert(json.message);
							$('text').value = '';
							$('phone-list').value = '';
							
						} else {
							FstError(json.message);
						}
					}
				});
				saveReg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				saveReg.post($('FormEdit'));

		});
});	

//]]>
</script>
