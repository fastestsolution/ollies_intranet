<?php
echo $this->Form->create($data,['id'=>'FormEdit']);
echo $this->Form->input("id");
//echo $this->Form->hidden("user_id", ['value' => $this->request->Session()->read("Auth.User.id")]);
//echo 'a';
echo '<div class="maxOverflowa">';
echo "<div class=\"row\">";
// echo $this->element('modal',['params'=>['zakladni'=>__('Základní'),'import'=>__('Import')]]);
echo "<div class=\"col-md-12 col-lg-4 col-xl-4\">";
echo $this->element('modal',['params'=>['zakladni'=>__('Základní')]]);
echo "</div>";
echo "<div class=\"col-md-12 col-lg-8 col-xl-8\">";
echo "<div class=\"stocks-buttons\">";
echo $this->Form->button('Tisk příjemky',['type'=>'button','class'=>'btn','label'=>false,'id'=>'PrintButton','data-path'=>'/#/']);
echo $this->Form->button('Uložit',['type'=>'submit','class'=>'btn btn-primary','label'=>false,'id'=>'SaveButton','data-path'=>'/stocks/']);
echo $this->Form->button('Zpět',['type'=>'button','class'=>'btn btn-danger','label'=>false,'id'=>'BackButton','data-path'=>'/stocks/']);
echo "</div>";
echo $this->element('../Stocks/elements/table');
echo "</div>";
echo "</div>";
echo '</div>';
?>
<!--echo $this->Form->button(__('Uložit'), ['class' => 'btn','id'=>'SaveModal']);-->
<?php
echo $this->Form->end();
?>
<script type="text/javascript">
    //<![CDATA[
    saveData();
    modalWindow();

    var list = document.getElementsByClassName("modal_tabs nav nav-pills tabs-ul")[0];
        //target = document.getElementById("modal-tabs");

    //list.addClass("stocks");
    //target.appendChild(list);

    list.style.display = "none";

    //]]>
</script>