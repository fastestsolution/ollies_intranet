<?php echo $this->Form->create($data,['id'=>'FormEdit']);?>
<?php //pr($data); ?>
<div class="row">

    <div class="col col-md-6">
        <?php echo $this->Form->input('id',['class'=>'form-control','label'=>'id']); ?>
        <?php echo $this->Form->input('name',['type'=>'text','class'=>'form-control','label'=>'Název produktu']); ?>
        <?php echo $this->Form->input('code',['type'=>'text', 'class'=>'form-control','label'=>'Kód']); ?>
        <?php echo $this->Form->input('group_id',['class'=>'form-control','label'=>'Skupina', 'options'=>$stock_type_list]); ?>

    </div>
    <div class="col col-md-6">
        <?php echo $this->Form->input('unit_id',['class'=>'form-control','label'=>'Jednotka', 'options'=>$stock_unit_list]); ?>
        <?php echo $this->Form->input('loss',['type'=>'text','class'=>'form-control','label'=>'Ztráta v %']); ?>
    </div>
    <div class="col col-md-12 centered">
        <?php echo $this->Form->button('Uložit',['type'=>'submit','class'=>'btn btn-primary','label'=>false,'id'=>'SaveButton','data-path'=>'/stocks/products/']); ?>
        <?php echo $this->Form->button('Zpět',['type'=>'button','class'=>'btn btn-danger','label'=>false,'id'=>'BackButton','data-path'=>'/stocks/products/']); ?>

    </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
    //<![CDATA[
    saveData();
    //]]>
</script>
