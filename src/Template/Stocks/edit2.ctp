<?php echo $this->Form->create($data,['id'=>'FormEdit']);?>
<?php //pr($data); ?>
<div class="row">

    <div class="col col-md-6">
        <?php echo $this->Form->input('id',['class'=>'form-control','label'=>'id']); ?>
        <?php echo $this->Form->input('name',['type'=>'text','class'=>'form-control','label'=>'Název produktu']); ?>
        <?php echo $this->Form->input('select_products',['type'=>'select','class'=>'form-control','label'=>'Produkty','style'=>'height:500px;','multiple'=>true,'options'=>$stockProductsList]); ?>
		<div id="products" data-prod='<?php echo json_encode($stockProducts);?>'></div>
		<div id="connected" data-prod='<?php echo (!empty($data->connected)?$data->connected:'');?>'></div>
    </div>
    <div class="col col-md-6">
        <?php echo $this->Form->input('code',['type'=>'text', 'class'=>'form-control','label'=>'Kód']); ?>
		<br />
		<strong>Receptura</strong>
		<ul id="product_connected">
			
		</ul>
  </div>
    <div class="col col-md-12 centered">
        <?php echo $this->Form->button('Uložit',['type'=>'submit','class'=>'btn btn-primary','label'=>false,'id'=>'SaveButton','data-path'=>'/stocks/']); ?>
        <?php echo $this->Form->button('Zpět',['type'=>'button','class'=>'btn btn-danger','label'=>false,'id'=>'BackButton','data-path'=>'/stocks/']); ?>

    </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
    //<![CDATA[
    saveData();
	
window.addEvent('domready',function(){ 
	window.products = JSON.decode($('products').get('data-prod'));
	window.dataList = JSON.decode($('connected').get('data-prod'));
	if (window.dataList == null){
		window.dataList = [];
	}
	//console.log(window.dataList);
	createList(window.dataList);
	function createList(data){
		$('product_connected').empty();
		data.each(function(item,count){
		
		var li = new Element('li').set('html','<span>'+window.products[item.id].name+'</span>').inject($('product_connected'));
		var in_id = new Element('input',{'name':'connected['+count+'][id]','value':item.id,'type':'hidden'}).inject(li);
		var in_loss = new Element('input',{'name':'connected['+count+'][loss]','value':window.products[item.id].loss,'type':'hidden'}).inject(li);
		var in_ks = new Element('input',{'name':'connected['+count+'][count]','placeholder':'Počet','class':'form-control ks float','value':((item.count)?item.count:'')}).inject(li);
		var del = new Element('input',{'type':'button','class':'btn btn-danger','value':'Smazat','data-key':count}).inject(li);
		del.addEvent('click',function(e){
			if (confirm('Opravdu smazat?')){
				window.dataList.splice(e.target.get('data-key'),1);
				createList(window.dataList);
				//e.target.getParent('li').destroy();
			}
		});
		$('product_connected').getElements('.float, .integer').inputLimit();
		});
	}
	
	//window.dataList = [];
	
	$('select-products').getElements('option').addEvent('click',function(e){
		//console.log(e.target);
		var id = e.target.value;
		var count = $('product_connected').getElements('li').length;
		window.dataList.push({
			'id':id,
			'count':'',
		});
		createList(window.dataList);
	});
});	
    //]]>
</script>
