<style>

.price_right  {
	text-align: right;
}

.clearFix {
	clear: both;
}

</style>
<div class="stocks-add">
	<?= $this->Form->input("Přidat", ['label' => false,'class'=>'btn btn-success','type'=>'button','id'=>'AddProduct']); ?>
</div>
<fieldset class="stocks">
	<legend><?= __("Základní údaje")?></legend>
	<div class="row">
		<div class="col-sm-6 col-md-6 col-lg-8">
			<div id="prevodka_show_from">
				<?= $this->Form->input("system_id",                     ['label' => __("Provozovna"),'options'=>$branches_list,'class'=>'search_selecta save_item clear_savea form-control', "empty" => true]); ?>
			</div>
			<div class="prevodka_show none">
				<?= $this->Form->input("system_id_to",                  ['label' => __("Provozovna kam"),'options'=>$branches_list,'class'=>'search_selecta save_item clear_savea form-control','empty'=>true]); ?>
			</div>
			<?= $this->Form->input("group_id",                          ['label' => __("Skupina globální pol."),'options'=>$stock_group_list,'class'=>'search_selecta clear_save form-control']); ?>
			<div class="clear" ></div>
			<label>Globální skladová položka</label>
			<?= $this->Form->input("stock_item_id",                     ['label' => __("Položka"),'options'=>$stockProductsList,'empty'=>true,'class'=>'search_selecta save_item clear_save bigheight form-control','multiple'=>true,'label'=>false]); ?>
			<!-- <label>Produkt</label> 
			<?= $this->Form->input("stock_item_product_id",             ['label' => __("Produkt"),'options'=>$product_list,'empty'=>false,'class'=>'search_selecta save_item clear_save smallheight form-control','multiple'=>true,'label'=>false]); ?> --> 
			<div class="none">
				<?= $this->Form->input("stock_type_id",                 ['label' => __("Typ"),'options'=>$stock_type_list,'empty'=>true,'class'=>' save_item','value'=>(isset($this->request->query['type_id'])?$this->request->query['type_id']:$data->sklad_type_id)]); ?>
			</div>
			<div id="tax_items" class="none"><?php echo json_encode($price_tax_list);?></div>
			<div id="jednotka_items" class="none"><?php echo json_encode($stock_unit_list);?></div>
			<div id="product_items" class="none"><?php echo json_encode($product_list);?></div>
			<div id="sklad_items" class="none"><?php echo json_encode($stock_items);?></div>
		</div>
		<div class="col-sm-6 col-md-6 col-lg-4">
			<?php echo $this->Form->input("value",                      ['label' => __("Množství"),'class'=>'float price_ks save_item clear_save pull-right input-own form-control']); ?>
			<?php echo $this->Form->input("tax_id",                     ['label' => __("DPH"),'class'=>' save_item pull-right input-own form-control','options'=>$price_tax_list]); ?>    
			<div id="hide_price">
				<?php echo $this->Form->input("nakup_price",            ['label' => __("Jed. bez DPH"),'class'=>'float price save_item clear_save pull-right input-own form-control','data-type'=>'nakup_price']); ?>
				<?php echo $this->Form->input("nakup_price_vat",        ['label' => __("Jed. s DPH"),'class'=>'float price  save_item clear_save pull-right input-own form-control','data-type'=>'nakup_price_vat']); ?>
				<?php echo $this->Form->input("nakup_price_total",      ['label' => __("Cel. bez DPH"),'class'=>'float price  save_item clear_save pull-right input-own form-control','data-type'=>'nakup_price_total']); ?>
				<?php echo $this->Form->input("nakup_price_total_vat",  ['label' => __("Cel. s DPH"),'class'=>'float price save_item clear_save pull-right input-own form-control','data-type'=>'nakup_price_total_vat']); ?>
				<?php echo $this->Form->input("jednotka_id",            ['label' => __("Jednotka"),'class'=>' save_item pull-right input-own form-control','options'=>$stock_unit_list]); ?>
				<?php echo $this->Form->input("faktura",                ['label' => __("Faktura"),'class'=>' save_item pull-right input-own form-control']); ?>
				<?php echo $this->Form->input("note",                   ['label' => __("Poznámka"),'class'=>' save_item clear_save pull-right input-own form-control','type'=>'textarea','rows'=>'3']); ?>
			</div>
			<br />
		</div>
	</div>
</fieldset>
<script type="text/javascript">
//<![CDATA[
	if ($('group-id'))
	$('group-id').addEvent('change',function(e){
		new Request.JSON({
			url:'/stocks/getStockItems/'+$('group-id').value,
			onComplete:function(json){
				if (json.data){
					$('stock-item-id').empty();
					Object.each(json.data,function(item,k){
						new Element('option',{'value':k}).set('text',item).inject($('stock-item-id'));
					});
				}
			}
		}).send();

   });

	if ($('stock-item-id'))
	$('stock-item-id').addEvent('change',function(e){
		new Request.JSON({
			url:'/stocks/getStockItemsPrice/1/'+e.target.value,
			onComplete:function(json){
				if (json.data){
					$('nakup-price').value = json.data.nakup_price;
				}
			}
		}).send();

   });
   if ($('stock-item-product-id'))
   $('stock-item-product-id').addEvent('change',function(e){
		new Request.JSON({
			url:'/stocks/getStockItemsPrice/2/'+e.target.value,
			onComplete:function(json){
				if (json.data){
					$('nakup-price').value = json.data.nakup_price;
				}
			}
		}).send();

   });
   
   
   $('group-id').fireEvent('change');
//]]>
</script>
