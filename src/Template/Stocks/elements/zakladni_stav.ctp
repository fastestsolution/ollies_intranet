<style>
.table_sklad tr td{
	padding:3px;
}
.stav_alert {
	color:#ff0000;
}
</style>
<fieldset>
	<legend><?php echo __("Skladové údaje")?></legend>
	<?php 
	//pr($sklad_values);
	if (isset($sklad_values)){ 
		echo '<table class="table_sklad"> ';
		echo '<tr>';
			echo '<th>Produkt</th>';
			echo '<th>Stav před (pokladna)</th>';
			echo '<th>Stav po (reálný)</th>';
			echo '<th>Stav rozdíl</th>';
		echo '</tr>';
		
		foreach($sklad_values AS $k=>$sv){
			//pr($sv);
			$row_id = $sv['sklad_item_id'];
			echo '<tr>';
				if (isset($sklad_items_list[$sv['sklad_item_id']])){
				echo '<td>'.(isset($sklad_items_list[$sv['sklad_item_id']])?$sklad_items_list[$sv['sklad_item_id']]:'Neni přiřazeno');
					echo $this->Form->input("sklad_stav_items.".$row_id.".sklad_item_id", ['placeholder' => __("Stav"),'class'=>'float none','label'=>false,'value'=>$sv['sklad_item_id'],'type'=>'text']);
				echo '</td>';
				echo '<td>'. $this->Form->input("sklad_stav_items.".$row_id.".stav", ['placeholder' => __("Stav"),'label'=>false,'class'=>'float stav','value'=>$sv['sum'],'readonly'=>true]).'</td>';
				echo '<td>'. $this->Form->input("sklad_stav_items.".$row_id.".stav_real", ['placeholder' => __("Stav"),'label'=>false,'class'=>' stav_real']).'</td>';
				echo '<td>'. $this->Form->input("sklad_stav_items.".$row_id.".stav_count", ['placeholder' => __("Stav"),'label'=>false,'class'=>' stav_count '.((isset($skladStavs->sklad_stav_items[$row_id]) && $skladStavs->sklad_stav_items[$row_id]->stav_count<0)?'stav_alert':''),'readonly'=>true]);
					echo $this->Form->input("sklad_stav_items.".$k.".id", ['placeholder' => __("Stav"),'label'=>false]);
				echo '</td>';
				}
			echo '</tr>';
		}
		echo '</table>';
	}
	//pr($skladStavs);
	echo $this->Form->input("from_sklad_id", ['label'=>false,'class'=>'none','type'=>'text']);
	?>
	<div class="sll"> 
		<?php //echo $this->Form->input("nakup_price", ['label' => __("Nákupní cena"),'class'=>'float']); ?>
		<?php //echo $this->Form->input("tax_id", ['label' => __("DPH"),'class'=>'','options'=>$price_tax_list]); ?>
		
	</div>
	<div class="slr"> 
		<?php //echo $this->Form->input("nakup_prumer", ['label' => __("Nákupní průměr"),'class'=>'float']); ?>
		
	</div>
</fieldset>

<script type="text/javascript">
//<![CDATA[
$$('.stav_real').addEvent('change',function(e){
	tr = e.target.getParent('tr');
	stav_count =  e.target.value.toFloat() - tr.getElement('.stav').value.toFloat();
	if (stav_count != 0){
		tr.getElement('.stav_count').addClass('stav_alert');
		//stav_count = tr.getElement('.stav').value.toFloat();
	} else {
	
		tr.getElement('.stav_count').removeClass('stav_alert');
	}
	tr.getElement('.stav_count').value = stav_count;
});
//]]>
</script>