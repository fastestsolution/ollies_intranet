

<div class="row">
    <div class="col-md-12">
    <?php if($data): ?>
        <?= $this->Form->create($data); ?>
            <input type="submit" class="btn btn-primary pull-right" value="Uložit" />
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Kategorie</th>
                        <th>Tiskárna</th>
                    </tr>
                </thead>
            <?php foreach($data as $i=> $item): ?>
                    <tr>
                        <td><?= $item->id; ?></td>
                        <td><?= $item->parent_group->name; ?> -> <?= $item->name; ?></td>
                        <td>
                            <?= $this->Form->input( $i .'.id', ['label'=>false ,'type'=>'hidden']); ?>
                            <?= $this->Form->input( $i .'.printer_id', ['label'=>false ,'options'=> $printerTypes, 'type'=>'select', 'class'=>'form-control', 'empty'=> [''=>'']]); ?>
                        </td>
                    </tr>
            <?php endforeach; ?>
        <?= $this->Form->end(); ?>
    <?php endif; ?>
    </div>
</div>


