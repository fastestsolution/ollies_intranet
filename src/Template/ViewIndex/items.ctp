<?php if (isset($params['data'])){ ?>
<?php if (!isset($params["noPaginationTop"])) { ?>
<div class="topPagination hideMobile">
<?php echo $this->element('pagination',['top'=>true]); ?>
</div>
<?php } ?>
<?php

if (isset($params["totalPriceSum"])) {
    if ($params["totalPriceSum"] != false) {
        echo "<span id=\"total_price_sum\" style=\"display: none;\"><p class=\"sec\">Celková cena celkem:</p><p class=\"pri\">" . 
			( $params["totalPriceSum"] == -1 ? 'Zvolte období <br/> <small>max 6 měsíců</small>' : number_format($params["totalPriceSum"], 2, '.', ' ') . ",- Kč" )
		."</p></span>";
    }
}

if (isset($params["heading"])) {
	echo "<h2>" . $params["heading"] . "</h2>";
}

if (isset($params["view"]) && isset($params["view"]->show)) {
	echo "<div id=\"view-selected-bool\" style=\"display: none;\">" . $params["view"]->show . "</div>";
	echo "<div id=\"view-selected-value\" style=\"display: none;\">" . $params["view"]->url . "</div>";
}

?>
<div class="table-responsive">
<table class="table table-striped table-hover" id="tableItems">
	<colgroup>
		<?php foreach($params['cols'] AS $k=>$col){ ?>
		<col style="<?php echo (isset($col['color']) ? 'background-color:'. $col['color'] : '') ?>">
		<?php } ?>
	</colgroup>
	<?php  
		// pr($sumData);
		if (isset($sumData)){
			echo '<tr>';
			foreach($params['cols'] AS $col_key=>$col){ 
				if (!isset($sumData[$col_key])){
					echo '<td></td>';
				} else {

					echo '<td '.(isset($col['col_class']) ? 'class="'.$col['col_class'].'" ' : '').'>';
						if ($sumList[$col_key] == 'text'){
							echo $sumData[$col_key];
						} 
						if ($sumList[$col_key] == 'price'){
							echo $this->Fastest->price($sumData[$col_key]);
						} 
					echo '</td>';
				}
			}	
			echo '</tr>';
		}
			
		?>
	<tr>
		<?php foreach($params['cols'] AS $k=>$col){ ?>
			<th class="<?php echo $k .  (isset($col['col_class']) ? ' '.$col['col_class'] : '') . (isset($col['th_class']) ? ' '.$col['th_class'] : '') ?>" style="<?= isset($col["style"]) ? $col["style"] : "" ?>"><?php echo $col['name'];?></th>
		<?php } ?>
		<?php 
		if (isset($params['posibility']) && !empty($params['posibility'])){
			echo '<th>Možnosti</th>';
		} ?>
	</tr>
		
		<?php foreach($params['data'] AS $data){ ?>
			<?php 
				//pr($params["data"]);
				$trClass = [];
				if(isset($params['rowConditions'])){
					foreach($params['rowConditions'] as $class => $cond){
						if (strpos($cond, '>=') !== false) {
							list($atr, $val) = explode('>=', $cond);

							if (isset($data[$atr]) && $data[$atr] >= $val) {
								$trClass[] = $class;
							}
						} elseif (strpos($cond, '<=') !== false) {
							list($atr, $val) = explode('<=', $cond);

							if (isset($data[$atr]) && $data[$atr] <= $val) {
								$trClass[] = $class;
							}
						} else {
							list($atr, $val) = explode('=', $cond);

							if (isset($data[$atr]) && $data[$atr] == $val) {
								$trClass[] = $class;
							}
						}
					}
				}
				echo '<tr'. (!empty($trClass) ? ' class="'. implode(' ', $trClass).'"' : '') .'>';
			?>
	
			<?php 
			// pr($params['cols']);die();
			foreach($params['cols'] AS $col_key=>$col){ ?>
				
				<?php 
				
				if (isset($params['sumList']) && isset($params['sumList'][$col_key])){
					$params['sumList'][$col_key] += $data->$col_key;
				}
				?>
				<td class="<?php echo $col_key .' '. (isset($col['trClass'])? $col['trClass'][$data->$col_key] : '' ) . (isset($col['col_class']) ? ' '.$col['col_class'] : ''); ?>" style="<?= isset($col["style"]) ? $col["style"] : "" ?>">
				<?php 
				if (is_object($data->$col_key)){
					echo $data->$col_key->format('d.m.Y H:i:s');
				} else {
					if (isset($col['list'])){
						echo (isset($col['list'][$data->$col_key])?$col['list'][$data->$col_key]:'Nevybráno');
					} else {
						echo $data->$col_key;
					}
				}
				?></td>
			<?php } ?>
			
				<?php 
				if (isset($params['posibility']) && !empty($params['posibility'])){
					echo '<td class="posibility">';
					$icons = [
						'edit'=>'fa-edit',
						'trash'=>'fa-trash',
						'copy'=>'fa-copy',
					];
					foreach($params['posibility'] AS $pos_key=>$pos){
						$url = explode('/',$pos['url']);
						//pr($url);
						//pr();

						if (isset($icons[$url[2]])){
							
							echo $this->Html->link('',$pos['url'].$data->id,['title'=>$pos['name'],'class'=>'fa '.$icons[$url[2]].' '.(($pos_key == 'trash')?'btn-danger':''),'data-type'=>(isset($pos['type'])?$pos['type']:'')]);

						} else {
							echo $this->Html->link($pos['name'],$pos['url'].$data->id,['title'=>$pos['name'],'class'=>'btn '.(($pos_key == 'trash')?'btn-danger':'btn-primary').' btn-sm']);

						}
					}
					echo '</td>';
				} ?>
			</tr>
		<?php } ?>
		<?php 
		// pr($params);die();
		if (isset($params['sumLista'])){
			echo '<tr class="summary-row">';
			$minusColCount = 0;
			foreach($params['cols'] AS $col_key=>$col){
				$colspan = '';
				$col_class = '';
				if($minusColCount > 0){
					$minusColCount--;
					continue;
				}
				if(isset($col['sum_colspan'])){
					$minusColCount = $col['sum_colspan'] - 1;
					$colspan = ' colspan="'.$col['sum_colspan'].'"';
				}
				if(isset($col['sum_col_class'])){
					$col_class = ' class="'.$col['sum_col_class'].'"';
				}
				echo '<td '.$colspan.' '.$col_class.'>';
					if (isset($params['sumLista'][$col_key])){
						echo $params['sumLista'][$col_key];
					}
					// echo $col_key;
				echo '</td>';
				//pr($col_key);
			}
			echo '</tr>';
		}
		?>
		
</table>
</div>
<?php echo $this->element('pagination'); ?>
<?php } else { ?>
	<p class="alert alert-warning">Nenalezeny žádné záznamy</p>
<?php } ?>

<script type="text/javascript">
//<![CDATA[
   
//]]>

<?php

if (isset($params["totalPriceSum"])) {

    echo
        "function watchTotalPrice() {" .
            "var value = document.getElementById(\"total_price_sum\").innerHTML;" .

            "if (value !== sync) {" .
                "document.getElementById(\"sum\").innerHTML = document.getElementById(\"total_price_sum\").innerHTML;" .
                "sync = value;" .
            "}" .
        "}" .
        
        "setInterval(watchTotalPrice, 500); ";

}

if (isset($params["view"]) && isset($params["view"]->show) && $params["view"]->show == true) {

	echo
        "function watchViewSelected() {" .
			"var boolValue = document.getElementById(\"view-selected-bool\").innerHTML," .
				"value = document.getElementById(\"view-selected-value\").innerHTML;" .

            "if (parseInt(boolValue) === 1) {" .
                "document.getElementById(\"view-selected-holder\").style.display = \"block\";" .
			"} else {" .
				"document.getElementById(\"view-selected-holder\").style.display = \"none\";" .
			"}" .

			"if (value !== sync) {" .
				"document.getElementById(\"view-selected-value-holder\").value = value;" .
				"sync = value;" .
			"}" .
        "}" .
        
        "setInterval(watchViewSelected, 500); ";

}

?>

</script>