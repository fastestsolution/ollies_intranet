<?php 
if (isset($params['topActions'])){ 
echo '<div id="topActions">';
	foreach($params['topActions'] AS $top){
	
		echo $this->Html->link($top['name'],$top['url'],['title'=>$top['name'],'class'=>'btn btn-primary','data-type'=>(isset($top['type'])?$top['type']:'')]);
	}
echo '</div>';
}

?>
<div id="fst_history">
<?php echo $this->element('../ViewIndex/items'); ?>
</div>
<div id="page_url" class="none"><?php echo $here;?></div>
<script type="text/javascript">
//<![CDATA[

var options = {'complete_fce': historyComplete};
   window.fsthistory = new FstHistory(options);
   viewIndex();
   dblClickTable();
//]]>

var sync = "";

</script>