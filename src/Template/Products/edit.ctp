<?php
echo $this->Form->create($data,['id'=>'FormEdit']);
echo $this->Form->input("id");
//echo $this->Form->hidden("user_id", ['value' => $this->request->Session()->read("Auth.User.id")]);

echo $this->element('modal',['params'=>['zakladni'=>__('Základní'),'recipes'=>__('Receptury'),'menu_items'=>__('Kategorie')]]);


//echo $this->Form->button(__('Uložit'), ['class' => 'btn','id'=>'SaveModal']);
?>
<div class="col col-md-12 centered buttons">
    <?php echo $this->Form->button('Uložit',['type'=>'submit','class'=>'btn btn-primary','label'=>false,'id'=>'SaveButton','data-path'=>'/products/', 'disabled' => 'disabled']); ?>
    <?php echo $this->Form->button('Zpět',['type'=>'button','class'=>'btn btn-danger','label'=>false,'id'=>'BackButton','data-path'=>'/products/']); ?>
</div>
<?php
echo $this->Form->end();
?>
<div id="formIsOk">
    <input type="hidden" id="recipeIsOk" name="recipeIsOk" value="0">
    <input type="hidden" id="categoryIsOk" name="categoryIsOk" value="0">
</div>
<script type="text/javascript">
    //<![CDATA[
    saveData();
    modalWindow();
    //]]>

    var recipe = document.getElementById('recipeIsOk'),
        category = document.getElementById('categoryIsOk'),
        button = document.getElementById('SaveButton'),
        data = <?= $productGroups ?>;

    setInterval(function () {
        if (parseInt(recipe.value) === 1 && parseInt(category.value) === 1) {
            button.removeAttribute('disabled');
        } else {
            button.setAttribute('disabled', 'disabled');
        }
    }, 1000);
</script>