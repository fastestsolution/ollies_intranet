
<script type="text/javascript" src="/js/fst_treeMenu/Tree.js"></script>
<script type="text/javascript" src="/js/fst_treeMenu/Collapse.js"></script>
<script type="text/javascript" src="/js/fst_treeMenu/Collapse.Cookie.js"></script>
<script type="text/javascript" src="/js/fst_treeMenu/fst_tree.js"></script>
<div class="header_top"></div>


<ul id="tree_menu" class="tree"></ul>
<div class="cats_buttons">
<?php echo $this->Form->button('Nová položka',['class'=>'button','id'=>'newCats']); ?>
<?php echo $this->Form->button('Uložit',['class'=>'button','id'=>'saveCats']); ?>
</div>
<div id="menu_data" class="none"><?php echo json_encode($menus);?></div>
<?php //pr($menus); ?>
<script type="text/javascript">
//<![CDATA[

window.addEvent('domready', function(){
	var fstmenu = new FstTreeMenu({
		'data':'<?php echo json_encode($menus);?>'
	});
    //new Collapse('tree_menu');
	//console.log('run');
});

//]]>
</script>
