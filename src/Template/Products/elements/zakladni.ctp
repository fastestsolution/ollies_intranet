    <fieldset>
        <legend><?php echo __("Základní údaje")?></legend>
        <div class="row">
        <div class="col-sm-6 col-md-6">
    
                <?php echo $this->Form->input("name", ['label' => __("Název"),'class'=>'form-control']); ?>

                <?php //echo $this->Form->input("price", ['label' => __("Cena s DPH")]); ?>
                <?php //echo $this->Form->input("price_zam", ['label' => __("Zam. Cena s DPH ")]); ?>
                <?php echo $this->Form->input("tax_id",['label'=>__('DPH'),'class'=>'form-control','options'=>$price_tax_list,'empty'=>false]); ?>
                <?php echo $this->Form->input("is_mirror",['label'=>__('Povolit do zrcadla'),'class'=>'form-control','options'=>$yes_no,'empty'=>false]); ?>
                <?php echo $this->Form->input("reserve",['label'=>__('Rezerva'),'class'=>'form-control','empty'=>false]); ?>
                <?php echo $this->Form->input("view_api",['label'=>__('Povolit zobrazení na webu'),'class'=>'form-control','options'=>$yes_no,'empty'=>false]); ?>

        </div>
        
        <div class="col-sm-6 col-md-6">
                <?php echo $this->Form->input("num", ['label' => __("Číslo"),'class'=>'form-control']); ?>
                <?php echo $this->Form->input("code", ['label' => __("Kód"),'class'=>'form-control']); ?>
                <?php echo $this->Form->input("group_trzba_id",['label'=>__('Skupina tržby'),'class'=>'form-control','options'=>$group_trzba_list,'empty'=>true]); ?>
                <?php echo $this->Form->input("is_dashboard",['label'=>__('Povolit na dashboard'),'class'=>'form-control','options'=>$yes_no,'empty'=>false]); ?>
                <?php //echo $this->Form->input("Kopírovat", ['label' => false,'class'=>'button form-control','id'=>'CloneButton','type'=>'button']); ?>
                <?php echo $this->Form->input("view_workshop",['label'=>__('Povolit zobrazení ve výrobně'),'class'=>'form-control','options'=>$yes_no,'empty'=>false]); ?>

        </div>
</fieldset>
<fieldset>
        <legend>Ceny</legend>
        <div class="col col-12">
                <div id="prices">
                <?php 
                //pr($data->product_prices);
                foreach($branches_list AS $bid=>$branch){
                        echo '<div class="price_line">';
                        
                        echo $this->Form->input("product_prices.".$bid.".system_id", ['label' => $branch,'class'=>'checkbox form-control show_price_list','type'=>'checkbox','value'=>$bid,'id'=>'branch'.$bid,'checked'=>(isset($product_prices_selected[$bid]) && $product_prices_selected[$bid] == 1?true:false)]); 
                        echo '<div class="price_list row '.(isset($product_prices_selected[$bid]) && $product_prices_selected[$bid]==1?'':'none').'">';
                                echo '<div class="col col-6 col-md-3">'.$this->Form->input('product_prices.'.$bid.'.price',['type'=>'text','class'=>'price form-control','label'=>false,'placeholder'=>'Cena']).'</div>';
                                echo '<div class="col col-6 col-md-3">'.$this->Form->input('product_prices.'.$bid.'.price2',['type'=>'text','class'=>'price form-control','label'=>false,'placeholder'=>'Cena zam',]).'</div>';
                                echo '<div class="col col-6 col-md-3">'.$this->Form->input('product_prices.'.$bid.'.price3',['type'=>'text','class'=>'price form-control','label'=>false,'placeholder'=>'Cena 3']).'</div>';
                                echo '<div class="col col-6 col-md-3">'.$this->Form->input('product_prices.'.$bid.'.price4',['type'=>'text','class'=>'price form-control','label'=>false,'placeholder'=>'Cena 4']).'</div>';
                        echo '</div>';        
                        echo '</div>';        
                
                }
                
                ?>
                </div>
        </div>
        

</fieldset>

<div id="group_list_level" class="none"><?php echo json_encode($group_list_level)?></div>
<?php echo $this->element('../Products/elements/product_addons'); ?>

<script type="text/javascript">
//<![CDATA[
window.addEvent('domready',function(){
    $$('.show_price_list').addEvent('click',function(e){
        var price_list = e.target.getParent('.price_line').getElement('.price_list');    
        if (e.target.checked){
                price_list.removeClass('none');
        } else {
                price_list.addClass('none');
                
        }
    });

    if ($('CloneButton'))
    $('CloneButton').addEvent('click',function(e){
		if (confirm('Opravdu kopírovat?')){
			//console.log(window.parent.document.getElementById('modalWindow'));
			//return false
			VarsModal.load_table = new Request.JSON({
				url:'/products/cloneEl/'+$('id').value,
				onComplete: VarsModal.compl_fce = (function(json){
					if (json.result == true){
						window.parent.document.getElementById('modalWindow').src = '/products/edit/'+json.id;
						
						
					} else {
					}
				}.bind(this))
			});
			VarsModal.load_table.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.load_table.send();
			
		}
	});

}); 
//]]>
</script>