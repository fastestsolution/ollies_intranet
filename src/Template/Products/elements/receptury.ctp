<fieldset>
	<legend><?php echo __("Receptury")?></legend>
	<div class="sll"> 
		<?php //pr($sklad_item_list); ?>
		<?php echo $this->Form->input("sklad_group_id", ['label' => __("Skupina"),'options'=>$sklad_group_list,'class'=>'search_selecta clear_save']); ?>
		<?php echo $this->Form->input("product_id", ['label' => __("Produkt"),'options'=>$sklad_item_list,'empty'=>true,'class'=>'search_select clear_receptura',]); ?>
		<?php echo $this->Form->input("product_value", ['label' => __("Množství"),'class'=>'clear_receptura float']); ?>
		<?php echo $this->Form->input("product_ztrata", ['label' => __("Ztráta"),'class'=>'clear_receptura float']); ?>
		<?php echo $this->Form->input('Přidat',['type'=>'button','id'=>'addReceptura','label'=>false]); ?>
	</div>
	<div class="slr"> 
		<table>
			<thead>
				<tr>
					<th>Produkt</th>
					<th>Množství</th>
					<th>Ztráta</th>
					<th>Smazat</th>
				</tr>
			</thead>
			<tbody id="recepturas_list">
				<?php 
				if (isset($data->product_recipes)){
					foreach($data->product_recipes  AS $k=>$pr){
						echo '<tr>';
                       // pr($pr);
                        //die();
							echo '<td>'.$sklad_item_list[$pr->stock_item_global_id];
								echo '<div class="none">';
									echo $this->Form->input('product_recipes.'.$k.'.sklad_item_global_id',['value'=>$pr->sklad_item_id,'label'=>false,'type'=>'text']);
									echo $this->Form->input('product_recipes.'.$k.'.value',['value'=>$pr->value,'label'=>false]);
									echo $this->Form->input('product_recipes.'.$k.'.ztrata',['value'=>$pr->ztrata,'label'=>false]);
									echo $this->Form->input('product_recipes.'.$k.'.id',['value'=>$pr->id,'label'=>false,'type'=>'text']);
								echo '</div>';
							echo '</td>';
							echo '<td>'.$pr->value.'</td>';
							echo '<td>'.$pr->ztrata.'</td>';
							echo '<td>'. $this->Html->link('Smazat','#',['title'=>'Smazat','class'=>'delete_link']) .'</td>';
						
						echo '</tr>';
					}
				}
				?>
			</tbody>
		</table>
	</div>
	<?php //pr($sklad_item_list); ?>
</fieldset>
<script type="text/javascript">
//<![CDATA[
window.addEvent('domready',function(){
    /*$('sklad-group-id').addEvent('change',function(e){
        new Request.JSON({
            url:'/products/getStocksItems/'+$('sklad-group-id').value,
            onComplete:function(json){
                if (json.data){
                    $('product-id').empty();
                    Object.each(json.data,function(item,k){
                        new Element('option',{'value':k}).set('text',item).inject($('product-id'));
                    });
                }
            }
        }).send();

   });*/
   /*$('sklad-group-id').fireEvent('change');
   
	function delete_links(){
		$$('.delete_link').removeEvents('click');
		
		$$('.delete_link').addEvent('click',function(e){
			e.stop();
			if (confirm('Opravdu smazat?')){
				e.target.getParent('tr').destroy();
			}
		});
			
	}
	delete_links();*/
	
	/*$('addReceptura').addEvent('click',function(e){
		if ($('product-id').value == '') { FstError('Vyberte produkt');}
		else if ($('product-value').value == '') { FstError('Zadejte množství');}
		else {
			var tr = new Element('tr').inject($('recepturas_list'));
			var td1 = new Element('td').inject(tr);
			var td2 = new Element('td').inject(tr);
			var td3 = new Element('td').inject(tr);
			var td4 = new Element('td').inject(tr);
			
			td1.set('text',$('product-id').getOptionText());
			td2.set('text',$('product-value').value);
			td3.set('text',$('product-ztrata').value);
			
			count = $('recepturas_list').getElements('tr').length - 1;
			
			delete_link = new Element('a',{'href':'#','class':'delete_link'}).set('text','Smazat').inject(td4);
			delete_links();
			
			new Element('input',{'type':'hidden','name':'product_recepturas['+count+'][sklad_item_id]','value':$('product-id').value}).inject(td1);
			new Element('input',{'type':'hidden','name':'product_recepturas['+count+'][value]','value':$('product-value').value}).inject(td1);
			new Element('input',{'type':'hidden','name':'product_recepturas['+count+'][ztrata]','value':$('product-ztrata').value}).inject(td1);
			
			$$('.clear_receptura').each(function(item){
				item.value = '';
			});
		}
	});*/
});
//]]>
</script>