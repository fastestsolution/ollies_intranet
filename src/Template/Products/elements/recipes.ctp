<fieldset>
	<legend><?php echo __("Receptury")?></legend>
	<div class="row">
	<div class="col col-6"> 
		<?php //pr($sklad_item_list); ?>
		<?php echo $this->Form->input("stock_group_id", ['label' => __("Skupina"),'options'=>$stock_group_list,'class'=>'search_selecta clear_save form-control']); ?>
		<?php echo $this->Form->input("product_id", ['label' => __("Produkt"),'options'=>$sklad_item_list,'empty'=>true,'class'=>'search_select clear_receptura  form-control',]); ?>
		<?php echo $this->Form->input("product_value", ['label' => __("Množství"),'class'=>'clear_receptura float  form-control']); ?>
		<?php echo $this->Form->input("product_loss", ['label' => __("Ztráta"),'class'=>'clear_receptura float  form-control']); ?>
		<br />
		<?php echo $this->Form->input('Přidat',['type'=>'button','id'=>'addReceptura','label'=>false,'class'=>'btn btn-primary']); ?>
	</div>
	<div id="unit_list" class="none"><?php echo json_encode($unit_list);?></div>
	<div class="col col-6"> 
		<table id="recipeList" class="table table-striped">
			<thead>
				<tr>
					<th>Produkt</th>
					<th>Množství</th>
					<th>Ztráta</th>
					<th>Smazat</th>
				</tr>
			</thead>
			<tbody id="recepturas_list">
				<?php 
				//pr($data);
				if (isset($data->product_recipes)){
					foreach($data->product_recipes  AS $k=>$pr){
						echo '<tr>';
                    //    pr($pr);
                        //die();
							echo '<td>'.$sklad_item_list[$pr->stock_item_global_id];
								echo '<div class="none">';
								//pr($pr);
									echo $this->Form->input('product_recipes.'.$k.'.stock_item_global_id',['value'=>$pr->sklad_item_id,'label'=>false,'type'=>'text']);
									echo $this->Form->input('product_recipes.'.$k.'.value',['value'=>$pr->value,'label'=>false]);
									echo $this->Form->input('product_recipes.'.$k.'.loss',['value'=>$pr->ztrata,'label'=>false]);
									echo $this->Form->input('product_recipes.'.$k.'.unit_id',['value'=>$pr->unit_id,'label'=>false]);
									echo $this->Form->input('product_recipes.'.$k.'.id',['value'=>$pr->id,'label'=>false,'type'=>'text']);
								echo '</div>';
							echo '</td>';
							echo '<td>'.$pr->value.'</td>';
							echo '<td>'.$pr->ztrata.'</td>';
							echo '<td>'. $this->Html->link('Smazat','#',['title'=>'Smazat','class'=>'delete_link']) .'</td>';
						
						echo '</tr>';
					}
				}
				?>
			</tbody>
		</table>
	</div>
	</div>
	<?php //pr($sklad_item_list); ?>
</fieldset>



<script type="text/javascript">
//<![CDATA[
window.addEvent('domready',function(){
    $('stock-group-id').addEvent('change',function(e){
        new Request.JSON({
            url:'/products/getStocksItems/'+$('stock-group-id').value,
            onComplete:function(json){
				if (json.data){
                    $('product-id').empty();
                    Object.each(json.data,function(item,k){
                        new Element('option',{'value':k}).set('text',item).inject($('product-id'));
                    });
                }
            }
        }).send();

   });
   $('stock-group-id').fireEvent('change');
   
	function delete_links(){
		$$('.delete_link').removeEvents('click');
		
		$$('.delete_link').addEvent('click',function(e){
			
			e.stop();
			if (confirm('Opravdu smazat?')){
				e.target.getParent('tr').destroy();
			}
		});
			
	}
	delete_links();
	
	var unit_list = JSON.decode($('unit_list').get('text'));

	$('addReceptura').addEvent('click',function(e){
		
		if ($('product-id').value == '') { FstError('Vyberte produkt');}
		else if ($('product-value').value == '') { FstError('Zadejte množství');}
		else {
			var tr = new Element('tr').inject($('recepturas_list'));
			var td1 = new Element('td').inject(tr);
			var td2 = new Element('td').inject(tr);
			var td3 = new Element('td').inject(tr);
			var td4 = new Element('td').inject(tr);
			
			td1.set('text',$('product-id').getOptionText());
			td2.set('text',$('product-value').value);
			td3.set('text',$('product-loss').value);
			
			count = $('recepturas_list').getElements('tr').length - 1;
			delete_link = new Element('a',{'href':'#','class':'delete_link'}).set('text','Smazat').inject(td4);
			delete_links();
			
			new Element('input',{'type':'hidden','name':'product_recipes['+count+'][stock_item_global_id]','value':$('product-id').value}).inject(td1);
			new Element('input',{'type':'hidden','name':'product_recipes['+count+'][value]','value':$('product-value').value}).inject(td1);
			new Element('input',{'type':'hidden','name':'product_recipes['+count+'][loss]','value':$('product-loss').value}).inject(td1);
			new Element('input',{'type':'hidden','name':'product_recipes['+count+'][unit_id]','value':unit_list[$('product-id').value]}).inject(td1);
			
			$$('.clear_receptura').each(function(item){
				item.value = '';
			});
		}
		
	});
});
//]]>

var recipe = document.getElementById('recipeIsOk'),
	table = document.getElementById('recipeList');

setInterval(function () {
	if (table.rows.length > 1) {
		recipe.value = 1;
	} else {
		recipe.value = 0;
	}
}, 1000);
</script>