<div class="row fullHeight">
    <div class="col-sm-12 col-md-12">
        <ul id="menu_items">

            <?php
            //pr($menu_list);
            foreach($menu_list AS $pid=>$m){
                $class= '';
                if (strpos($m,'--') !== false && strpos($m,'---') === false){
                    $class='group';
                }
                echo '<li class="'.$class.'d-inline">';
                    echo $this->Form->input('product_connects.'.$pid.'.checked',['type'=>'checkbox','class'=>'checkbox','label'=>$m,'checked'=>(isset($products_group) && in_array($pid,$products_group)?'checked':false), 'onclick' => 'selectParent(this, event);']);
                    /*
                    echo $this->Form->input('product_connects.'.$pid.'.price',['type'=>'text','class'=>'price form-control','label'=>false,'placeholder'=>'Cena']);
                    echo $this->Form->input('product_connects.'.$pid.'.price2',['type'=>'text','class'=>'price form-control','label'=>false,'placeholder'=>'Cena zam',]);
                    echo $this->Form->input('product_connects.'.$pid.'.price3',['type'=>'text','class'=>'price form-control','label'=>false,'placeholder'=>'Cena 3']);
                    echo $this->Form->input('product_connects.'.$pid.'.price4',['type'=>'text','class'=>'price form-control','label'=>false,'placeholder'=>'Cena 4']);
                    */
                    echo $this->Form->input('product_connects.'.$pid.'.pref',['type'=>'text','class'=>'pref form-control','label'=>false,'placeholder'=>'Prefix']);
                echo '</li>';
            }?>
        </ul>
    </div>
</div>

<script type="text/javascript">
//<![CDATA[
    window.addEvent('domready',function(){
        function setActive(){
            $('menu_items').getElements('.checkbox').each(function(item){
                
                //item.addEvent('click',setActive());
                if (item.checked){
                    item.getParent('li').addClass('active');
                } else {

                    item.getParent('li').removeClass('active');
                }
            });
        };
        setActive();

        $('menu_items').getElements('.checkbox').each(function(item){
            item.addEvent('click',setActive);
        });
    });

//]]>

var category = document.getElementById('categoryIsOk');

setInterval(function () {
    var checked = 0;

	$('menu_items').getElements('.checkbox').each(function(item) {
        if (item.checked) {
            checked = 1;
        }
    });

    category.value = checked;
}, 1000);

function selectParent(selected, event) {

    "use strict";

    var ids,
        checked,
        parentFirst,
        parentSecond;

    checked = 0;

    $('menu_items').getElements('.checkbox').each(function(item) {
        if ((item.checked && item.name !== selected.name) || (!item.checked && item.name === selected.name)) {
            checked = 1;
        }
    });

    if (checked === 0) {
        ids = selected.name.match(/\[([^\]]+)\]/g);
        ids[0] = ids[0].replace('[', '');
        ids[0] = ids[0].replace(']', '');

        for (var key in data) {
            if (parseInt(data[key].id) === parseInt(ids[0])) {
                if (parseInt(data[key].level) === 2) {
                    document.getElementById('product-connects-' + data[key].parent_id + '-checked').checked = true;
                } else if (parseInt(data[key].level) === 3) {
                    document.getElementById('product-connects-' + data[key].parent_id + '-checked').checked = true;
                    document.getElementById('product-connects-' + getParent(data[key].parent_id) + '-checked').checked = true;
                }
            }
        }
    }    
}

function getParent(id) {
    for (var key in data) {
        if (parseInt(data[key].id) === id) {
            return data[key].parent_id;
        }
    }
}
</script>