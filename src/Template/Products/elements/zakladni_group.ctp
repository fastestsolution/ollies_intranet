<fieldset>
	<legend><?php echo __("Základní údaje")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("name", ['label' => __("Název")]); ?>
		<?php echo $this->Form->input("color", ['label' => __("Barva")]); ?>
		<?php //echo $this->Form->input("printer", ['label' => __("Tiskárna"),'class'=>'show_printer']); ?>

	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("parent_id", ['label' => __("Nadřazená skupina"),'options'=>$group_list,'empty'=>true]); ?>
		<?php echo $this->Form->input("level", ['label' => __("Level"),'options'=>$level_list,'empty'=>true]); ?>
	
	</div>
</fieldset>
