<fieldset>
	<legend><?php echo __("Základní údaje")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("name", ['label' => __("Název")]); ?>
		<?php echo $this->Form->input("price", ['label' => __("Cena")]); ?>
	
	</div>
	<div class="slr"> 
		<?php echo $this->Form->input("group_id", ['label' => __("Skupina"),'options'=>$addon_list,'empty'=>true]); ?>
	
	</div>
</fieldset>
