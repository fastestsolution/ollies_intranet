<?php if (empty($data->step_id)){
        $data->step_id = 1;
} ?>

    <fieldset>
        <legend><?php echo __("Zrcadlo skladu")?></legend>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-8">
                        <?php echo $this->Form->input("name", ['label' => __("Název zrcadla"),'class'=>'form-control','value'=>(!empty($data->name)?$data->name:(isset($branchesList[$system_id])?$branchesList[$system_id]:''))]); ?>
                        <div class="none">
                            <?php echo $this->Form->input("step_id", ['label' => false,'type'=>'text']); ?>
                            <?php pr($this->request->data); ?>
                            <?php echo $this->Form->input("data_step1", ['label' => false]); ?>
                            <?php echo $this->Form->input("data_step2", ['label' => false]); ?>
                            <?php echo $this->Form->input("data_step_change", ['label' => false]); ?>
                            <?php echo $this->Form->input("data_diff", ['label' => false]); ?>
                            <?php echo $this->Form->input("date_step1", ['label' => false,'type'=>'text']); ?>
                            <?php echo $this->Form->input("date_step2", ['label' => false,'type'=>'text']); ?>
                            <?php echo $this->Form->input("date_step_change", ['label' => false,'type'=>'text']); ?>
                        </div>
                        <?php echo $this->Form->input("system_id", ['label' => __("Pobočka"),'class'=>'form-control','options'=>$branchesList,'empty'=>true]); ?>
                        
                        <br />
                        <div id="loadDataInfo" class="none alert alert-warning">
                            <strong>Info!</strong> Vyberte si co chcete nahrát za data.
                        </div>
                        <div id="loadDataAlert" class="none alert alert-danger">
                            <strong>Pozor!</strong> Nahrané data z <em id="alertDateMsg"></em> <strong id="alertDate"></strong>, nebudou uloženy.
                        </div>
                        <table class="table-striped" id="stocksMirrorTableHeader">
                            <tr class="head">
                                <th style="width: 17%;">Sklad položka</th>
                                <th style="width: 70px;">Jednotka</th>
                                <!-- <th>Cena</th> -->
                                <th style="width: 44px;">Stav systém</th>
                                <th style="width: 60px;">Stav reálný</th>
                                <th style="width: 60px;">Rozdíl</th>
                                <th style="width: 60px;" class="stockRepair none">Oprava</th>
                                <th style="width: 30%;">Poznámka</th>
                            </tr>
                        </table>
                        <div id="mirroOverTable">
                        <div class="table-responsive">
                        <table class="table-striped" id="stocksMirrorTable">
                            <tr data-template class="trow">
                                <td style="width: 20%; white-: wrap;">{{stock_global_item.name}}</td>
                                <td style="width: 30px;">{{unit}}</td>
                                <td style="width: 60px;" class="text_right sum">{{sum}}</td>
                                <td style="width: 60px;"><input type="text" class="form-control change_real  check_disable disable" data-index="{{_tempo.index}}" value="{{real}}"/></td>
                                <td style="width: 60px;"><input type="text" class="form-control change_rozdil  check_disable disable_force text_right" data-index="{{_tempo.index}}" value="{{rozdil}}"/></td>
                                <td style="width: 60px;" class="stockRepair none"><input type="text" class="form-control check_disable  disable  change_repair" data-index="{{_tempo.index}}" value="{{repair}}"/></td>
                                <td style="width: 30%;"><input type="text" class="form-control change_note disable" data-index="{{_tempo.index}}"  value="{{note}}"/></td>
                            </tr>
                            <?php 
                            /*
                            foreach($mirrorList AS $k=>$m){ 
                                echo '<tr>';
                                    echo '<td>'.$m->stocks_item->name.'</td>';
                                    echo '<td>'.(isset($stock_unit_list[$m->stocks_item->jednotka_id])?$stock_unit_list[$m->stocks_item->jednotka_id]:'Nevyplněno').'</td>';
                                    echo '<td>'.$m->sum.'</td>';
                                    echo '<td>'. $this->Form->input("value",['class'=>'form-control','type'=>'text','label'=>false]) .'</td>';

                                echo '</tr>';
                            } 
                            // pr($mirrorList);
                            */
                            ?>
                        </table>
                        </div>
                        </div>
                        
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                        <ul id="stocksMirrorFunction">
                            <li class="row">
                                <span class="col-12 col-md-12 num">1. zrcadlo před kontrolou</span>
                                <div class="col-12 col-md-12">
                                    <?php 
                                    if (empty($data->id)){
                                        echo $this->Form->button("Uložit zrcadlo", ['type'=>'button','label' =>false,'class'=>'first btn '.(($data->step_id == 1)?'btn-primary':'btn-default'),'id'=>'SaveMirror1']); 
                                    }    
                                    ?><br />
                                </div>
                                <div class="col-12 col-md-12">
                                    <?php echo $this->Html->link('Nahrát data','#',['title'=>'Nahrát data','class'=>'loadData  btn btn-warning','data-el'=>'data-step1','data-date'=>'loadDataDate1','data-msg'=>'zrcadlo před kontrolou']);?><br />
                                    <span class="loadDataDate" id="loadDataDate1"></span>
                                </div>
                            </li>
                            
                            <li class="row">
                            <span class="col-12 col-md-12  num">2. zrcadlo po kontrole</span>
                            <div class="col-12 col-md-12">
                                <?php 
                                if (empty($data->id)){
                                    echo $this->Form->button("Upravit reálné hodnoty", ['type'=>'button','label' =>false,'class'=>'second btn '.(($data->step_id == 2)?'btn-primary':'btn-default'),'id'=>'SaveMirror2']); 
                                }
                                ?>
                            </div>
                            
                            <div class="col-12 col-md-12">
                                <?php echo $this->Html->link('Nahrát data','#',['title'=>'Nahrát data','class'=>'loadData btn btn-warning','data-el'=>'data-step2','data-date'=>'loadDataDate2','data-msg'=>'zrcadlo po kontrole']);?><br />
                                <span class="loadDataDate" id="loadDataDate2"></span>
                                <?php echo $this->Form->button("Zobrazit rozdíl", ['type'=>'button','label' =>false,'class'=>'diff btn '.(($data->step_id == 2)?'btn-primary':'btn-default'),'id'=>'showDiff']); ?>
                                <?php 
                                if ($data->is_confirm != 1)
                                echo $this->Form->button("Opravit hodnoty", ['type'=>'button','label' =>false,'class'=>'btn btn-default','id'=>'changeDiff']); ?>
                                <?php 
                                //pr($data);
                                if (!empty($data->data_step_change))
                                echo $this->Form->button("Zobrazit opravy", ['type'=>'button','label' =>false,'class'=>'btn '.(($data->step_id == 2)?'btn-primary':'btn-default'),'id'=>'showRepair']); ?>
                            </div>    
                            <div class="col-12 col-md-12">
                            </div>
                            </li>    
                            
                            <li class="row">
                                <span class="col-12 col-md-12 num">3. uložení zrcadla</span>
                                <div class="col-12 col-md-12">
                                    <?php echo $this->Form->button("Uložit celé zrcadlo", ['type'=>'button','label' =>false,'class'=>'btn '.(($data->step_id == 3)?'btn-primary':'btn-default'),'id'=>'SaveButton','data-path'=>'/stock-mirrors/']); ?>
                                    <div id="SaveButtonDisable" class="none alert alert-danger">
                                        Nelze uložit již uloženo
                                    </div>
                                </div>
                                <?php //echo $this->Html->link('Nahrát data','#',['title'=>'Nahrát data','class'=>'loadData loadData1']);?>
                                <!-- <span class="loadDataDate" id="loadDataDate3"></span> -->
                            </li>
                            <?php if (in_array($loggedUser->group_id,[1,2]) && $data->id > 0){ ?>
                            <li  class="row">
                                <span class="col-12 col-md-12 num">4. potvrzení zrcadla provozním</span>
                                <div class="col-12 col-md-12">
                                <?php 
                                if ($data->is_confirm == 0){
                                    echo $this->Form->button("Potvrdit zrcadlo", ['label' =>false,'class'=>'btn '.(($data->step_id == 3)?'btn-primary':'btn-default'),'id'=>'ConfirmMirror','data-path'=>'/stock-mirrors/']); 
                                
                                } else {
                                    echo '<div class="alert alert-danger">Již potvrzeno</div>';
                                    
                                }
                                ?>
                                </div>
                                <?php //echo $this->Html->link('Nahrát data','#',['title'=>'Nahrát data','class'=>'loadData loadData1']);?>
                                <!-- <span class="loadDataDate" id="loadDataDate3"></span> -->
                            </li>
                            <?php } ?>
                            
                        </ul>
                        <?php //pr($loggedUser->group_id == 1); ?>
                </div>
            </div>
    </fieldset>

    <script type="text/javascript">
    //<![CDATA[
        window.addEvent('domready',function(){
        window.tempoStockMirror = Tempo.prepare("stocksMirrorTable");
        window.data_diff = {};
        window.data_repair = {};

        $$('.stockRepair').addClass('none');

        function changeReal(){
            $('stocksMirrorTable').getElements('.float, .integer').inputLimit();
            $$('.change_real').addEvent('click',function(e){
                if (!e.target) e.target = e;
                e.target.select();
            });
            $$('.change_real').removeEvents('keydown');
            $$('.change_real').addEvent('keydown',function(e){
                if (e.key == 'down'){
                    e.stop();
                    if (e.target.getParent('tr').getNext('tr')){
                        next = e.target.getParent('tr').getNext('tr').getElement('.change_real');
                        next.select();
                        $('mirroOverTable').scrollTop = next.get('data-index').toInt() * /*20*/ next.parentElement.offsetHeight;
                        // next.fireEvent('click',next);
                    }
                }
                if (e.key == 'up'){
                    e.stop();
                    if (e.target.getParent('tr').getPrevious('tr')){
                        prev = e.target.getParent('tr').getPrevious('tr').getElement('.change_real');
                        prev.select();
                        $('mirroOverTable').scrollTop = prev.get('data-index').toInt()*20;
                        // next.fireEvent('click',next);
                    }
                }
            });

            $$('.change_real').removeEvents('change');
            $$('.change_real').addEvent('change',function(e){
                var index = e.target.get('data-index');

                window.data_diff[window.jsonData[index].stock_item_id] = e.target.value.toFloat() - window.jsonData[index].sum ;
                
                window.jsonData[index].real = e.target.value.toFloat();
                highlightDiff();
                $('data-diff').value = JSON.encode(window.data_diff);
                console.log(window.data_diff);
            });

            
            $$('.change_repair').removeEvents('change');
            $$('.change_repair').addEvent('change',function(e){
                var index = e.target.get('data-index');

                window.data_repair[window.jsonData[index].stock_item_id] = e.target.value.toFloat() - window.jsonData[index].real ;
                window.jsonData[index].repair = e.target.value.toFloat();

                $('data-step-change').value = JSON.encode(window.data_repair);
                console.log(window.data_repair);
            });

            
            $$('.change_note').removeEvents('change');
            $$('.change_note').addEvent('change',function(e){
                var index = e.target.get('data-index');

                
                window.jsonData[index].note = e.target.value;
                $('data-diff').value = JSON.encode(window.data_diff);
                console.log(window.data_diff);
            });
        }

        function checkDisable(){
            $('stocksMirrorTable').getElements('.check_disable').each(function(item){
                if (item.hasClass('disable')){
                    
                    if ($('step-id').value != 2){
                        item.setProperty('disabled','disabled');
                    } else {
                        item.removeProperty('disabled','disabled');
                        
                    }
                }
                if (item.hasClass('disable_force')){

                    item.setProperty('readonly','readonly');
                    console.log(item);
                }
            });
            $('stocksMirrorTable').getElements('.change_note').each(function(item){
                if (item.hasClass('disable')){
                    if ($('step-id').value != 2){
                        item.setProperty('disabled','disabled');
                    } else {
                        item.removeProperty('disabled','disabled');
                        
                    }
                }
            });
        }

        function loadDate(){
            if ($('date-step1').value != ''){
                //$('date-step1').value)
                var date = $('date-step1').value;
                $('loadDataDate1').set('text',date)
            }
            if ($('date-step2').value != ''){
                var date2 = $('date-step2').value;
                $('loadDataDate2').set('text',date2)
            }
        }

        function loadData(){

            if ($('showRepair')){
                $('showRepair').addEvent('click',function(e){
                    data = JSON.decode($('data-step-change').value);
                    // console.log(data);
                    window.jsonData.each(function(item,k){
                        if (data[item.stock_item_id]){
                            window.jsonData[k].repair = window.jsonData[k].real + data[item.stock_item_id]; 
                        }
                    });
                    $$('.stockRepair').removeClass('none');
                    // console.log(window.jsonData);
                    
                    
                    window.tempoStockMirror.render(window.jsonData);
                    checkDisable();
                });
            }


            $$('.loadData').addEvent('click',function(e){
                if (confirm('Opravdu nahrát data?')){
                    if ($(e.target.get('data-el')).value == ''){
                        FstError('Nejsou uloženy data ');
                    } else {
                        data = JSON.decode($(e.target.get('data-el')).value);
                        window.jsonData = data;                        

                        $('alertDateMsg').set('text',e.target.get('data-msg'));
                        $('alertDate').set('text',$(e.target.get('data-date')).get('text'));
                        //console.log(data);
                        window.tempoStockMirror.render(data);
                        //$$('.stockRepair').addClass('none');

                        $('loadDataAlert').removeClass('none');
                        checkDisable();
                        highlightDiff();
                        $('showDiff').removeClass('btn-default');
                        $('showDiff').addClass('btn-primary');
                        

                        if ($('showRepair')){
                            $('showRepair').removeClass('btn-default');
                            $('showRepair').addClass('btn-primary');
                        }

                        if ($('changeDiff')){

                        $('changeDiff').removeClass('btn-default');
                        $('changeDiff').addClass('btn-danger');
                        $('changeDiff').addEvent('click',function(e){
                            var index = e.target.get('data-index');
                            $$('.stockRepair').removeClass('none');
                            $('date-step-change').value = new Date().format('%d.%m.%Y %H:%M:%S');
                            changeReal();
                            $('mirroOverTable').getElement('.change_repair').focus();      
                            $('mirroOverTable').getElements('.change_repair').removeProperty('disabled');      
                            // window.data_repair[window.jsonData[index].stock_item_id] = e.target.value.toFloat() - window.jsonData[index].sum ;

                            // window.jsonData[index].real = e.target.value.toFloat();
                            // //highlightDiff();
                            // $('data-step-change').value = JSON.encode(window.data_repair);

                        });
                        }
                    }
                }
            });
        }

        loadDate();
        
        loadData();
        
        
        function confirmMirror(){
            if ($('ConfirmMirror')){
                $('ConfirmMirror').addEvent('click',function(e){
                    if (confirm('Opravdu potvrdit, opravili jste případné chyby?')){
                        e.stop();
                        redirect = e.target.get('data-path');
                        pagePreloader();
                        
                        var saveReg = new Request.JSON({
                            url:$('FormEdit').action+'?confirm',
                            onError: function(json){
                                pagePreloader(true);
                                //button_preloader(button);
                            },
                            onComplete: function(json){
                                pagePreloader(true);
                                //button_preloader(button);
                                if (json.result == true){
                                    // console.log(json.data)
                                    window.location = redirect;
                                    //FstAlert(json.message);
                                    
                                } else {
                                    FstError(json.message);
                                }
                            }
                        });
                        saveReg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
                        saveReg.post($('FormEdit'));
                    }
                    
                });
            }
        }
        confirmMirror();



        function highlightDiff(){
            $('stocksMirrorTable').getElements('tr').each(function(tr){
                
                // console.log(tr.getElement('.sum'));
                // console.log(tr.getElement('.change_real'));
                if (tr.getElement('.change_real')){
                if (tr.getElement('.change_real').value.toFloat() != tr.getElement('.sum').get('text').toFloat()){
                    var sum = tr.getElement('.sum').get('text').toFloat();
                    var real = tr.getElement('.change_real').value.toFloat();
                    console.log(real);
                    console.log(sum);
                    tr.getElement('.change_rozdil').value =  real - sum;
                    tr.addClass('diff');
                } else {
                    tr.removeClass('diff');
                }
                }
            }); 
        }

        if ($('id').value != ''){
            $('SaveButton').addClass('none');
            $('SaveButtonDisable').removeClass('none');
        }

        $('system-id').addEvent('change',function(e){
            pagePreloader();
                    
            var saveReg = new Request.JSON({
                url:'/stock-mirrors/loadData/'+$('system-id').value,
                onError: function(json){
                    pagePreloader(true);
                    //button_preloader(button);
                },
                onComplete: function(json){
                    pagePreloader(true);
                    //button_preloader(button);
                    if (json.result == true){
                        // console.log(json.data)
                        window.jsonData = json.data;

                        window.tempoStockMirror.render(window.jsonData)
                        changeReal();
                        checkDisable();
                        //FstAlert(json.message);
                        
                    } else {
                        FstError(json.message);
                    }
                }
            });
            saveReg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
            saveReg.send();
        });

        if ($('system-id').value > 0){
            $('name').setProperty('disabled','disabled');
            $('loadDataInfo').removeClass('none');
            //$('system-id').fireEvent('change',$('system-id'));
        }


        $('stocksMirrorFunction').getElements('.btn').addEvent('click',function(e){
            if (e.target.hasClass('btn-default')){
                FstError('Nelze použít');
            } else {
               if (e.target.hasClass('first')){
                    $('data-step1').value = JSON.encode(window.jsonData);
                    $('date-step1').value = new Date().format('%d.%m.%Y %H:%M:%S');
                    $('step-id').value = 2;
                    $('stocksMirrorFunction').getElements('.btn').addClass('btn-default');
                    $('stocksMirrorFunction').getElements('.btn').removeClass('btn-primary');
                    $('SaveMirror2').removeClass('btn-default');
                    $('SaveMirror2').addClass('btn-primary');
                    $('showDiff').removeClass('btn-default');
                    $('showDiff').addClass('btn-primary');
                    
                    checkDisable();
                    loadDate();

                    $('stocksMirrorTable').getElement('.change_real').select();
                    $('stocksMirrorTable').getElement('.change_real').focus();
               }

               if (e.target.hasClass('second')){
                    $('data-step2').value = JSON.encode(window.jsonData);
                    $('date-step2').value = new Date().format('%d.%m.%Y %H:%M:%S');
                    $('step-id').value = 3;
                    $('stocksMirrorFunction').getElements('.btn').addClass('btn-default');
                    $('stocksMirrorFunction').getElements('.btn').removeClass('btn-primary');
                    $('SaveButton').removeClass('btn-default');
                    $('SaveButton').addClass('btn-primary');
                    $('showDiff').removeClass('btn-default');
                    $('showDiff').addClass('btn-primary');
                    
                    checkDisable();
                    loadDate();
               }

               if (e.target.hasClass('diff')){
                    if (e.target.hasClass('hide')){
                        e.target.removeClass('hide');
                        $('stocksMirrorTable').getElements('tr.trow').each(function(tr){
                            if (!tr.hasClass('diff')){
                                tr.removeClass('none');
                            }
                        });
                    } else {
                        e.target.addClass('hide');
                        $('stocksMirrorTable').getElements('tr.trow').each(function(tr){
                            if (!tr.hasClass('diff')){
                                tr.addClass('none');
                            }
                        });
                    }
                    
               }
            }
        });


        


        });
    //]]>
    </script>
