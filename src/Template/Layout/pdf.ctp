<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?= $this->fetch('scripts') ?>
    <?= $this->fetch('css') ?>
</head>
<body>
   		<?php echo $this->fetch('content'); ?>
   
</body>
</html>
