<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>   
	<?php echo (isset($title)?$title.' - Ollies Intranet':'Ollies Intranet'). ' '.VERSION;?>
    </title>
   <?php // $this->Html->meta('icon') ?>
	<link rel="icon" type="image/png" href="/css/img/favicon.png" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" >
	<link rel="stylesheet" href="/js/mbox/assets/mBoxNotice.css" >
	<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous"> -->
	<!--
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	-->
    <?php  
	$styles = [
		'/js/calendar/datepicker_vista/datepicker_vista.css',
		'/css/custom-switch.css',
		'/css/default.css',
		'/css/style2.css',
	];
	foreach($styles AS $style){
		echo '<link rel="stylesheet/less" href="'.$style.'?ver=' . VERSION . '" >'."\n";
	}
	$scripts = [
		'https://scripts.fastesthost.cz/js/mootools1.4/core1.6.js',
		'https://scripts.fastesthost.cz/js/mootools1.4/c_more1.6.js',
		'/js/fst_history/fst_history.js',
		'/js/mbox/min/mBox.All.min.js',
		'/js/calendar/Picker.js',
		'/js/calendar/Picker.Attach.js',
		'/js/calendar/Picker.Date.js',
		'/js/calendar/Picker.Date.Range.js',
		'/js/lang/cz.js',
		'/js/main.js',
		'/js/less.js',
		'/js/tempo.js'
	];
	foreach($scripts AS $scr){
		echo '<script src="'.$scr.'?ver=' . VERSION . '"></script>'."\n";
	}
	
	
	?>

    <?= $this->fetch('scripts') ?>
    <?= $this->fetch('css') ?>
	<?php echo $this->element('monitoring'); ?>
</head>
<body <?= $_SERVER['HTTP_HOST'] == 'test-intr.fastestdev.cz' ? 'class="test-app"': '' ?>>
    
    <?= $this->Flash->render() ?>
    <div class="container-fluid margin_top fullHeight">
        
        <?php
        if (isset($loggedUser)){
			echo '<div class="row">';
			
			echo $this->element('Layout/top_menu'); 
			echo '</div>';
		}	
		?>


        <div class="row fullHeight2">
			<?php if (!isset($simpleLayout)) {?>
			<div class="col-sm-2 col-md-2 margin-top side-bar" id="side-bar">
                <?php
				echo $this->element('Layout/filtration');
                echo $this->element('Layout/side_bar');
				echo $this->element('Layout/branches_list');
                ?>
			</div>
			<?php } ?>
            <div class="<?php echo (isset($simpleLayout))?'col-sm-12 col-md-12 margin-top':'col-sm-10 col-md-10 margin-top'?>">
                <div class="container-fluid">
						<?php
							echo $this->element('Layout/breadcums');
						?>
					<?php
                    //if (isset($loggedUser))
                     echo $this->fetch('content'); ?>
                </div>
            </div>
        </div>
    </div>
	<div id="page_preloader" class="none"></div>
    <?= $this->fetch('script') ?>
</body>
</html>
