<form action="/login/" method="post" id="FormLogin" class="form">
	
<div class="row">
	<div class="col col-md-6 col-12 offset-md-3">
			
		<div class="col col-md-12 col-12">
			<div id="login_logo"><span>Intranet</div>
	
		</div>
		<div class="col col-md-12 col-12">
			<?php echo $this->Form->input('username',['class'=>'form-control','label'=>'Uživatelské jméno']); ?>
		</div>
		<div class="col col-md-12 col-12">
			<?php echo $this->Form->input('password',['type'=>'password','class'=>'form-control','label'=>'Uživatelské heslo']); ?>
		</div>
		<div class="col col-md-12  col-12 centered">
			<?php echo $this->Form->button('Přihlásit se',['type'=>'submit','class'=>'btn btn-primary','label'=>false,'id'=>'LoginButton']); ?>
		
		</div>

	</div>
</div>
<?php

echo
	$this->Form->hidden("requestedURL", ["default" => ($this->request->session()->check("requestedURL")) ? $this->request->session()->read("requestedURL") : "", "id" => "requestedURL"]);

if ($this->request->session()->check("requestedURL")) { 
	$this->request->session()->delete("requestedURL"); 
}

?>	
</form>
<script type="text/javascript">
//<![CDATA[
login();
//]]>
</script>
