<?php echo $this->Form->create($data,['id'=>'FormEdit']);?>
<?php //pr($data); ?>	
<div class="row">
		
	<div class="col col-md-6 col-12">
		<?php echo $this->Form->input('id',['class'=>'form-control','label'=>'id']); ?>
		<?php echo $this->Form->input('name',['class'=>'form-control','label'=>'Jméno a příjmení']); ?>
		<?php echo $this->Form->input('username',['class'=>'form-control','label'=>'Uživatelské jméno']); ?>
		<?php echo $this->Form->input('password',['type'=>'password','class'=>'form-control','label'=>'Uživatelské heslo']); ?>
		<?php echo $this->Form->input('password2',['type'=>'password','class'=>'form-control','label'=>'Uživatelské heslo znovu']); ?>
		<?php echo $this->Form->input('password_tmp',['type'=>'hidden','class'=>'form-control','label'=>'Uživatelské heslo znovu']); ?>
		<?php echo $this->Form->input('email',['type'=>'text','class'=>'form-control','label'=>'Email']); ?>
		
	</div>
	<div class="col col-md-6 col-12">
		<?php echo $this->Form->input('branch_id',['class'=>'form-control','label'=>'Provoz','options'=>$branches_list]); ?>
		<?php echo $this->Form->input('group_id',['class'=>'form-control','label'=>'Skupina','options'=>$users_group_list]); ?>
		<?php echo $this->Form->input('phone',['class'=>'form-control integer','label'=>'Telefon','maxlength'=>9]); ?>
		<?php echo $this->Form->input('url',['class'=>'form-control','label'=>'URL']); ?>
		
	</div>
	<div class="col col-md-12 centered">
		<?php echo $this->Form->button('Uložit',['type'=>'submit','class'=>'btn btn-primary','label'=>false,'id'=>'SaveButton','data-path'=>'/users/']); ?>
		<?php echo $this->Form->button('Zpět',['type'=>'button','class'=>'btn btn-danger','label'=>false,'id'=>'BackButton','data-path'=>'/users/']); ?>
	
	</div>
</div>	
<?php echo $this->Form->end();?>
<script type="text/javascript">
//<![CDATA[
saveData();
//]]>
</script>
