<?php echo $this->Form->create($data,['id'=>'FormEdit']);?>
<?php //pr($data); ?>	
<div class="row">
		
	<div class="col col-md-6 col-12">
		<?php echo $this->Form->input('id',['class'=>'form-control','label'=>'id']); ?>
		<?php echo $this->Form->input('branch_id',['class'=>'form-control','label'=>'Provoz','options'=>$branches_list]); ?>
	</div>
	<div class="col col-md-6 col-12">
		
        <?php echo $this->Form->input('date',['class'=>'form-control date','label'=>'Datum','type'=>'text']); ?>
        <?php echo $this->Form->input('value',['class'=>'form-control','label'=>'Předpoklad']); ?>
	</div>
	<div class="col col-md-12 centered">
		<?php echo $this->Form->button('Uložit',['type'=>'submit','class'=>'btn btn-primary','label'=>false,'id'=>'SaveButton','data-path'=>'/branches-stats/']); ?>
		<?php echo $this->Form->button('Zpět',['type'=>'button','class'=>'btn btn-danger','label'=>false,'id'=>'BackButton','data-path'=>'/branches-stats/']); ?>
	
	</div>
</div>	
<?php echo $this->Form->end();?>
<script type="text/javascript">
//<![CDATA[
saveData();
//]]>
</script>
