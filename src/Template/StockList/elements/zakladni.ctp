<fieldset>
	<legend><?php echo __("Základní údaje")?></legend>
	<div class="sll"> 
		<?php echo $this->Form->input("dodavatel_jednotka", ['label' => __("Jednotka dodavatel"),'class'=>'form-control']); ?>
		<?php echo $this->Form->input("jednotka_id",['label'=>__('Jednotka'),'options'=>$stock_unit_list,'empty'=>false,'class'=>'form-control']); ?>
        <?php echo $this->Form->input("prevod", ['label' => __("Převod"),'class'=>'form-control']); ?>
	</div>
<!--	<div class="slr">

		
	</div>-->
</fieldset>