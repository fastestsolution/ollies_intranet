	<link rel="stylesheet" href="/js/mootols_three/Css/tree.css" >
	<link rel="stylesheet" href="/js/mootols_three/Css/Collapse.css" >
<?php

$scripts = [
	'/mootols_three/Tree.js',
	'/mootols_three/fst_tree.js',
	'/mootols_three/Collapse.js',
	'/mootols_three/Collapse.Cookie.js',
	//'/mootols_three/Delegation.js',

	];

	foreach($scripts as $s){
        echo '<script src="/js'.$s.'"></script>'."\n";
	}
?>
    <script type="text/javascript">

        window.addEvent('domready', function(){

            var fstmenu = new FstTreeMenu({
                'data':'<?php echo json_encode($menus);?>'
            });

        });


    </script>
<?php
    if(isset($data)){
    }
    else{
        echo 'Nenalezeny žádné data';
    }
?>
<?php
// $out = '';
//     function genMenu($data,$level = 1){

//         $clasesul = [
//             1 => 'first-l',
//             2 => 'second-l none',
//             3 => 'third-l none',
//         ];
//         $out = '<ul class="tree list-group '.($level > 1?'collapsessss':'').' '.(isset($clasesul[$level])?$clasesul[$level]:'').'" id="'.(($level == 1)?'tree_menu':'').'">';

//         $clases = [
//                 1 => 'list-group-item-own',
//                 2 => 'list-group-item',
//                 3 => 'list-group-flush',
//         ];
//         foreach ($data as $d) {
//             $out .= '<li id="item'.$d->id.'" class="list-group-item  '.(isset($clases[$level])?$clases[$level]:'').'"><span class="colaps">+</span><span>';
//                 $out .= '<a class="expand">'.$d->name.'</a>';
//                 if(isset($d->children) && !empty($d->children)){
//                     $out .= genMenu($d->children,$level+1);
//                 } else {
//                     $out .= '</span></li>';
//                 }

//         }
//         $out .= '</ul>';
//         return $out;
//     }
//     $out .= genMenu($data);
//         ?>
 <?php
//     echo($out);
//pr($data);
?>
<ul id="tree_menu" class="tree"></ul> 
<div class="col col-md-12 centered buttons">
    <?php echo $this->Form->button('Nová položka',['class'=>'btn btn-primary','id'=>'newCats']); ?>
    <?php echo $this->Form->button('Uložit',['class'=>'btn btn-danger','id'=>'saveCats']); ?>
</div>

    <div id="menu_data" class="none"><?php echo json_encode($menus);?></div>
    <div id="tree_scroll"></div> 

<script type="text/javascript">
//<![CDATA[
window.addEvent('load',function(){
    // $('tree_menu').setStyle('height',window.getSize().y - 150);
    var height = window.getScrollSize().y; 
		//console.log(height);
	if ($('side-bar'))
		$('side-bar').setStyle('height',height - 60);
});
//]]>
</script>
