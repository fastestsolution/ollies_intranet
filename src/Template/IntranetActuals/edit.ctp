<?php echo $this->Form->create($data,['id'=>'FormEdit']);?>
<?php //pr($data); ?>	
<script src="/js/fst_uploader/fst_uploader.js" ></script>
<div class="row">
		<?php //pr($intranet_type_list); ?>
	<div class="col col-md-6">
		<?php echo $this->Form->input('id',['class'=>'form-control','label'=>'id']); ?>
		<?php echo $this->Form->input('name',['class'=>'form-control','label'=>'Název']); ?>
		<?php echo $this->Form->input('type',['class'=>'form-control','label'=>'Typ','options'=>$intranet_type_list]); ?>
		<?php echo $this->Form->input('text',['class'=>'form-control','type'=>'textarea','label'=>'Popis']); ?>
	</div>
	<div class="col col-md-6">
		<div class="fileSelectOver"> 
			<?php echo $this->Form->input('file',['class'=>'form-control fileSelect','type'=>'file','multiple'=>'multiple', 'label'=>'Soubor']); ?>
		</div>
		
		<?php echo $this->Form->input('files',['class'=>'form-control','type'=>'hidden','label'=>false]); ?>
		<?php echo $this->Form->input('files_delete',['class'=>'form-control','type'=>'hidden','label'=>false]); ?>
		
	</div>
	<div class="col col-md-12 centered">
		<?php echo $this->Form->button('Uložit',['type'=>'submit','class'=>'btn btn-primary','label'=>false,'id'=>'SaveButton','data-path'=>'/intranetEdit/']); ?>
		<?php echo $this->Form->button('Zpět',['type'=>'button','class'=>'btn btn-danger','label'=>false,'id'=>'BackButton','data-path'=>'/intranetEdit/']); ?>
	
	</div>
</div>	
<?php echo $this->Form->end();?>
<script type="text/javascript">
//<![CDATA[
saveData();
var opt = {
	fileName: true,
	fileList: true,
	filesData: $('files'),
	filesDataDelete: $('files-delete')
}
fstUploader = new FstUploader($('file'),opt);
//]]>
</script>
