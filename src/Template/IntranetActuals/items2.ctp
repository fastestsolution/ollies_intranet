<div id="messages">
	<?php 
	foreach($data AS $key=>$d){
		echo '<div class="message '. (($key++ % 2) ? "even" : "odd").'">';
			echo '<h2>'.$d->name.'</h2>';
			echo '<p>'.$d->created->format('d.m.Y').' - '.$intranet_type_list[$d->type].'</p>';
			echo '<p>'.$d->text.'</p>';
				$files = json_decode($d->files);
				//pr($files);
				if (!empty($files)){
					echo '<ul class="files">';
					foreach($files AS $f){
						echo '<li>';
							echo $this->Html->link('Stáhnout přílohu ',$f->path.$f->file,['title'=>'Stáhnout','target'=>'_blank']);
						echo '</li>';
						//pr($f);
					}
					
					echo '</ul>';
				}
			
		echo '</div>';
	}
	?>
</div>
<?php //pr($data); ?>
<?php echo $this->element('pagination'); ?>