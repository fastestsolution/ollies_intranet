<?php
if (isset($filtrations)){ 
echo '<div class="container">';
echo '<form id="filtrations" class="row" onsubmit="return false">';
	foreach($filtrations AS $filtr){
		echo '<div class="col col-md-3">';
			if ($filtr['type'] == 'select'){
				echo $this->Form->input($filtr['key'],['placeholder'=>$filtr['name'],'label'=>false,'class'=>'form-control fst_h','data-fparams'=>$filtr['key'],'options'=>$filtr['options']]);
			
			} else {
				echo $this->Form->input($filtr['key'],['placeholder'=>$filtr['name'],'label'=>false,'class'=>'form-control fst_h','data-fparams'=>$filtr['key'].(isset($filtr['type'])?'|'.$filtr['type']:'')]);
			}
		echo '</div>';
	}
	echo $this->Form->submit('Filtrovat',['class'=>'btn btn-primary']);
	
echo '</form>';
echo '</div>';
}
?>
<div id="fst_history">
<?php echo $this->element('../IntranetActuals/items2'); ?>
</div>
<div id="page_url" class="none"><?php echo $here;?></div>
<script type="text/javascript">
//<![CDATA[
   window.fsthistory = new FstHistory();
//]]>
</script>