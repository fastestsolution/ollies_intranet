<?php

/**
 * Change $value to currency format.
 * 
 * @since 1.54
 * @param mixed $value
 * @return float
 */
function n_format($value)
{
    return number_format((float) $value, 2, ".", " ");
}

/**
 * Fill the product list.
 * 
 * @since 1.54
 * @param string $name
 * @return array
 */
function productList($name, $js)
{
    $data = (object) [];

    if (property_exists($js, $name)) {
        foreach ($js->{$name} as $list) {
            if (!isset($data->{$list->name})) {
                $data->{$list->name} = (object) [
                    "name"          => $list->name,
                    "units"         => $list->total_units,
                    "price"         => $list->total_price,
                    "items"         => (object) []
                ];
            } else {
                if (!empty((array) $list->items)) {
                    $data->{$list->name}->units += $list->total_units;
                    $data->{$list->name}->price += $list->total_price;
                }
            }
            
            foreach ($list->items as $item) {
                if (!isset($data->{$list->name}->items->{$item->name})) {
                    $data->{$list->name}->items->{$item->name} = (object) [
                        "name"          => $item->name,
                        "units"         => $item->units,
                        "price"         => $item->price
                    ];
                } else {
                    $data->{$list->name}->items->{$item->name}->units += $item->units;
                    $data->{$list->name}->items->{$item->name}->price += $item->price;
                }
            }
        }
    }

    return $data;
}

$big                = "---------------------------------------";
$small              = "-------------------";
$undefined          = "nedefinováno";

$data = (object) [
    "title"                 => "",
    "dateFrom"              => date("Y-m-d 00:00:00"),
    "dateTo"                => date("Y-m-d 00:00:00"),
    "branches"              => (object) [],
    "sum_total_complete"    => (object) [
        "price"                 => 0,
        "orders"                => 0,
        "items"                 => 0
    ],
    "sum_total_storno"      => (object) [
        "price"                 => 0,
        "orders"                => 0,
        "items"                 => 0
    ],
    "sum_groups"            => (object) [],
    "soft_storno"           => (object) [
        "price"                 => 0,
        "count"                 => 0,
        "items"                 => (object) []
    ],
    "payed_cards"           => (object) [
        "price"                 => 0,
        "items"                 => (object) []
    ],
    "payed_gastro"           => (object) [
        "price"                 => 0,
        "items"                 => (object) []
    ],
    "product_list"          => (object) [],
    "product_list_delivery" => (object) [],
    "product_list_employee" => (object) [],
    "product_list_company"  => (object) []
];

$data->sum_groups->{'Zaměstnanci a Firma'} =  (object)[
    'name' => 'Zaměstnanci a Firma',
    'price' => 0,
    'price_without_tax' => 0,
    /*'taxs' => (object)[
            '15%' => 0,
            '21%' => 0,
            '0%' => 0,
            '10%' => 0
    ]*/
];

$iterator = 1;
$count = 0;

foreach ($json as $row) {
    if (is_object($row) && property_exists($row, "json")) {
        $count++;
    }
}

foreach ($json as $row) {
    if (is_object($row) && property_exists($row, "json")) {
        $js = $row->json;
        if ($count > 1) {
            if ($iterator == $count) {
                $data->dateFrom = $js->header->titleDate;
            } else if ($iterator == 1) {
                $data->title = $js->header->title;
                $data->dateTo = $js->header->titleDate;
            }
        } else {
            $data->title = $js->header->title;
            $data->dateFrom = $js->header->titleDate;
        }

        if (!in_array($branches_list[$row->system_id], (array) $data->branches)) {
            $data->branches->{$iterator} = $branches_list[$row->system_id];
        }

        $data->sum_total_complete->price += $js->sum_total_complete->total_price;
        $data->sum_total_complete->orders += $js->sum_total_complete->orders;
        $data->sum_total_complete->items += $js->sum_total_complete->units;

        $data->sum_total_storno->price += $js->sum_total_storno->total_price;
        $data->sum_total_storno->orders += $js->sum_total_storno->orders;
        $data->sum_total_storno->items += $js->sum_total_storno->items;

       
        
        foreach ($js->sum_group as $group) {  
            if (!isset($data->sum_groups->{$group->name})) {
                $data->sum_groups->{$group->name} = (object) [
                    "name"              => $group->name,
                    "price"             => $group->total_with_tax,
                    "price_without_tax" => $group->total_without_tax,
                    "taxs"              => (object) [
                        "15%"               => $group->taxs->{"1"},
                        "21%"               => $group->taxs->{"2"},
                        "0%"                => $group->taxs->{"3"},
                        "10%"               => $group->taxs->{"4"}
                    ]
                ];
            } else {
              
                if ($group->total_with_tax > 0) {
                    $data->sum_groups->{$group->name}->price += $group->total_with_tax;
                    $data->sum_groups->{$group->name}->price_without_tax += $group->total_without_tax;
                    $data->sum_groups->{$group->name}->taxs->{"15%"} += $group->taxs->{"1"};
                    $data->sum_groups->{$group->name}->taxs->{"21%"} += $group->taxs->{"2"};
                    $data->sum_groups->{$group->name}->taxs->{"0%"} += $group->taxs->{"3"};
                    $data->sum_groups->{$group->name}->taxs->{"10%"} += $group->taxs->{"4"};
                }
            }
        }


        /*foreach( $data['product_list_employee'] as $employee){
            $data['sum_groups'][''] = [
                
            ];
        }
        foreach( $data['product_list_company'] as $company){
            
        }*/

        $data->soft_storno->price += $js->soft_storno->price;
        $data->soft_storno->count += $js->soft_storno->count;

        foreach ($js->soft_storno->items as $item) {
            if (!isset($data->soft_storno->items->{$item->name})) {
                $data->soft_storno->items->{$item->name} = (object) [
                    "name"      => $item->name,
                    "price"     => $item->price,
                    "units"     => $item->units
                ];
            } else {
                $data->soft_storno->items->{$item->name}->price += $item->price;
                $data->soft_storno->items->{$item->name}->units += $item->units;
            }
        }

        $data->payed_cards->price += $js->payed_cards->total_price;
        $data->payed_cards->total_units = 0;

        $data->payed_gastro->total_price = $js->payed_gastro;

        foreach ($js->payed_cards->order_items as $item) {
            /*$data->payed_cards->items->{$item->id} = (object) [
                "id"        => $item->id,
                "price"     => $item->total_price
            ];*/
            $data->payed_cards->total_units += 1;
        }

        $data->product_list             = productList('products_list', $js);
        $data->product_list_delivery    = productList('products_list_delivery', $js);
        $data->product_list_employee    = productList('products_list_employee', $js);
        $data->product_list_company     = productList('products_list_company', $js);

        $iterator++;
    }
}


foreach($data->product_list_employee as $item){ 
    $data->sum_groups->{'Zaměstnanci a Firma'}->price += $item->price;
    $data->sum_groups->{'Zaměstnanci a Firma'}->price_without_tax += $item->price;
    //$data->sum_groups->{'Zaměstnanci a Firmy'}->count += $item->units;
}
foreach($data->product_list_company as $item){
    $data->sum_groups->{'Zaměstnanci a Firma'}->price += $item->price;
    $data->sum_groups->{'Zaměstnanci a Firma'}->price_without_tax += $item->price;
    //$data->sum_groups->{'Zaměstnanci a Firmy'}->count += $item->units;
}

?>
<div class="row">
    <div class="col col-md-12 col-12">
        <div class="row">
            <div class="col col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="order-extract no-wrap">
                    <table>
                        <tr>
                            <td class="center break-up break-down" colspan="3"><img class="logo" src="/css/img/logo_cmyk_bw.jpg"></td>
                        </tr>
                        <tr>
                            <td class="center" colspan="3"><?= (isset($data->title)) ? $data->title : $undefined ?></td>
                        </tr>
                        <?php

                        if ($count > 1) {
                            echo
                                "<tr>
                                    <td class=\"center\" colspan=\"3\">od ", $data->dateFrom, "</td>
                                </tr>
                                <tr>
                                    <td class=\"center break-down\" colspan=\"3\">do ", $data->dateTo, "</td>
                                </tr>
                                <tr>
                                    <td class=\"left break-down\" colspan=\"3\">Z poboček:</td>
                                </tr>";

                            $countBranches      = count((array) $data->branches);
                            $iterator   = 1;
                            foreach ($data->branches as $branch) {
                                if ($iterator == $countBranches) {
                                    echo
                                        "<tr>
                                            <td class=\"left break-down\" colspan=\"3\">", $branch, "</td>
                                        </tr>";
                                } else {
                                    echo
                                        "<tr>
                                            <td class=\"left\" colspan=\"3\">", $branch, "</td>
                                        </tr>";
                                }
                                
                                $iterator++;
                            }
                        } else {
                            echo
                                "<tr>
                                    <td class=\"center break-down\" colspan=\"3\">", $data->dateFrom, "</td>
                                </tr>
                                <tr>
                                    <td class=\"left break-down\" colspan=\"3\">Z pobočky:</td>
                                </tr>";

                            foreach ($data->branches as $branch) {
                                echo
                                    "<tr>
                                        <td class=\"left break-down\" colspan=\"3\">", $branch, "</td>
                                    </tr>";
                            }
                        }

                        ?>
                        <tr>
                            <td class="left">Produkt</td>
                            <td class="center">Ks</td>
                            <td class="right">Cena</td>
                        </tr>
                        <tr>
                            <td class="center" colspan="3"><?= $big ?></td>
                        </tr>
                        <tr>
                            <td class="left bold" colspan="2">Tržba celkem</td>
                            <td class="right bold"><?= (isset($data->sum_total_complete->price)) ? n_format($data->sum_total_complete->price) : $undefined ?></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="3"><?= $small ?></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="2">Objednávek</td>
                            <td class="right"><?= (isset($data->sum_total_complete->orders)) ? $data->sum_total_complete->orders : $undefined ?></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="2">Položek</td>
                            <td class="right"><?= (isset($data->sum_total_complete->items)) ? $data->sum_total_complete->items : $undefined ?></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="3"><?= $small ?></td>
                        </tr>
                        <tr>
                            <td class="left bold" colspan="2">Storna</td>
                            <td class="right bold"><?= (isset($data->sum_total_storno->price)) ? n_format($data->sum_total_storno->price) : $undefined ?></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="2">Celkem</td>
                            <td class="right"><?= (isset($data->sum_total_storno->price)) ? n_format($data->sum_total_storno->price) : $undefined ?></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="2">Objednávek</td>
                            <td class="right"><?= (isset($data->sum_total_storno->orders)) ? $data->sum_total_storno->orders : $undefined ?></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="2">Položek</td>
                            <td class="right"><?= (isset($data->sum_total_storno->items)) ? $data->sum_total_storno->items : $undefined ?></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="3"><?= $small ?></td>
                        </tr>
                        <tr>
                            <td class="left bold" colspan="2">Rozpis skupin</td>
                            <td class="right bold">Cena</td>
                        </tr>
                        <?php
    
                        foreach ($data->sum_groups as $group) {
                            echo
                                "<tr>
                                    <td class=\"left\" colspan=\"2\">", $group->name, "</td>
                                    <td class=\"right\">", n_format($group->price_without_tax), "</td>
                                </tr>";
                            if(isset($group->taxs->{"15%"})){
                                echo 
                                    "<tr>
                                        <td class=\"left\" colspan=\"2\">15%</td>
                                        <td class=\"right\">", n_format($group->taxs->{"15%"}), "</td>
                                    </tr>
                                    <tr>
                                        <td class=\"left\" colspan=\"2\">21%</td>
                                        <td class=\"right\">", n_format($group->taxs->{"21%"}), "</td>
                                    </tr>
                                    <tr>
                                        <td class=\"left\" colspan=\"2\">0%</td>
                                        <td class=\"right\">", n_format($group->taxs->{"0%"}), "</td>
                                    </tr>
                                    <tr>
                                        <td class=\"left\" colspan=\"2\">10%</td>
                                        <td class=\"right\">", n_format($group->taxs->{"10%"}), "</td>
                                    </tr>
                                    <tr>
                                        <td class=\"left\" colspan=\"2\">Celkem s DPH</td>
                                        <td class=\"right\">", n_format($group->price), "</td>
                                    </tr>";
                            }
                            echo "<tr>
                                    <td class=\"left\" colspan=\"3\">", $small, "</td>
                                </tr>";
                        }

                        

                        if (!empty($data->soft_storno->items)) {
                            echo
                                "<tr>
                                    <td class=\"left bold\">Měkké storno</td>
                                    <td class=\"center bold\">", $data->soft_storno->count, "</td>
                                    <td class=\"right bold\">", n_format($data->soft_storno->price), "</td>
                                </tr>";

                            foreach ($data->soft_storno->items as $item) {
                                echo
                                    "<tr>
                                        <td class=\"left\">", $item->name, "</td>
                                        <td class=\"center\">", $item->units, "</td>
                                        <td class=\"right\">", n_format($item->price), "</td>
                                    </tr>";
                            }

                            echo
                                "<tr>
                                    <td class=\"left\" colspan=\"3\">", $small, "</td>
                                </tr>";
                        }

                        if (!empty($data->payed_cards->items)) {
                            echo
                                "<tr>
                                    <td class=\"left bold\">Platba kartou</td>
                                    <td class=\"center bold\">", $data->payed_cards->total_units,"</td>
                                    <td class=\"right bold\">", n_format($data->payed_cards->price), "</td>
                                </tr>";
                            /*
                            foreach ($data->payed_cards->items as $item) {
                                echo
                                    "<tr>
                                        <td class=\"left\" colspan=\"2\">", $item->id, "</td>
                                        <td class=\"right\">", n_format($item->price), "</td>
                                    </tr>";
                            }

                            echo
                                "<tr>
                                    <td class=\"left\" colspan=\"2\">Celkem</td>
                                    <td class=\"right\">", n_format($data->payed_cards->price), "</td>
                                </tr>";*/
                        }

                        if(isset($data->payed_gastro->total_price) && !empty($data->payed_gastro->total_price)){

                            echo "<tr>
                                    <td class=\"left\" colspan=\"3\">", $big, "</td>
                                </tr>
                                <tr>
                                    <td class=\"left bold\">Platba stravenkami</td>
                                    <td class=\"center bold\"></td>
                                    <td class=\"right bold\">", n_format($data->payed_gastro->total_price), "</td>
                                </tr>";
                        }

                        if (!empty($data->product_list)) {
                            foreach ($data->product_list as $list) {
                                if (!empty($list->items) && $list->units > 0) {
                                    echo
                                        "<tr>
                                            <td class=\"left\" colspan=\"3\">", $big, "</td>
                                        </tr>
                                        <tr>
                                            <td class=\"left bold\">", $list->name, "</td>
                                            <td class=\"center bold\">", $list->units, "</td>
                                            <td class=\"right bold\">", n_format($list->price), "</td>
                                        </tr>";

                                    foreach ($list->items as $item) {
                                        echo
                                            "<tr>
                                                <td class=\"left\">", $item->name, "</td>
                                                <td class=\"center\">", $item->units, "</td>
                                                <td class=\"right\">", n_format($item->price), "</td>
                                            </tr>";
                                    }
                                }
                            }
                        }

                        if (!empty($data->product_list_delivery)) {
                            echo
                                "<tr>
                                    <td class=\"left\" colspan=\"3\">", $big, "</td>
                                </tr>
                                <tr>
                                    <td class=\"left bold\">Rozvoz</td>
                                    <td class=\"center bold\">Ks</td>
                                    <td class=\"right bold\">Cena</td>
                                </tr>";

                            foreach ($data->product_list_delivery as $list) {
                                if (!empty($list->items) && $list->units > 0) {
                                    echo
                                        "<tr>
                                            <td class=\"left\" colspan=\"3\">", $big, "</td>
                                        </tr>
                                        <tr>
                                            <td class=\"left bold\">", $list->name, "</td>
                                            <td class=\"center bold\">", $list->units, "</td>
                                            <td class=\"right bold\">", n_format($list->price), "</td>
                                        </tr>";

                                    foreach ($list->items as $item) {
                                        echo
                                            "<tr>
                                                <td class=\"left\">", $item->name, "</td>
                                                <td class=\"center\">", $item->units, "</td>
                                                <td class=\"right\">", n_format($item->price), "</td>
                                            </tr>";
                                    }
                                }
                            }
                        }

                        if (!empty($data->product_list_employee)) {
                            echo
                                "<tr>
                                    <td class=\"left\" colspan=\"3\">", $big, "</td>
                                </tr>
                                <tr>
                                    <td class=\"left bold\">Zaměstnanci</td>
                                    <td class=\"center bold\">Ks</td>
                                    <td class=\"right bold\">Cena</td>
                                </tr>";

                            foreach ($data->product_list_employee as $list) {
                                if (!empty($list->items) && $list->units > 0) {
                                    echo
                                        "<tr>
                                            <td class=\"left\" colspan=\"3\">", $big, "</td>
                                        </tr>
                                        <tr>
                                            <td class=\"left bold\">", $list->name, "</td>
                                            <td class=\"center bold\">", $list->units, "</td>
                                            <td class=\"right bold\">", n_format($list->price), "</td>
                                        </tr>";

                                    foreach ($list->items as $item) {
                                        echo
                                            "<tr>
                                                <td class=\"left\">", $item->name, "</td>
                                                <td class=\"center\">", $item->units, "</td>
                                                <td class=\"right\">", n_format($item->price), "</td>
                                            </tr>";
                                    }
                                }
                            }
                        }

                        if (!empty($data->product_list_company)) {
                            echo
                                "<tr>
                                    <td class=\"left\" colspan=\"3\">", $big, "</td>
                                </tr>
                                <tr>
                                    <td class=\"left bold\">Firma</td>
                                    <td class=\"center bold\">Ks</td>
                                    <td class=\"right bold\">Cena</td>
                                </tr>";

                            foreach ($data->product_list_company as $list) {
                                if (!empty($list->items) && $list->units > 0) {
                                    echo
                                        "<tr>
                                            <td class=\"left\" colspan=\"3\">", $big, "</td>
                                        </tr>
                                        <tr>
                                            <td class=\"left bold\">", $list->name, "</td>
                                            <td class=\"center bold\">", $list->units, "</td>
                                            <td class=\"right bold\">", n_format($list->price), "</td>
                                        </tr>";

                                    foreach ($list->items as $item) {
                                        echo
                                            "<tr>
                                                <td class=\"left\">", $item->name, "</td>
                                                <td class=\"center\">", $item->units, "</td>
                                                <td class=\"right\">", n_format($item->price), "</td>
                                            </tr>";
                                    }
                                }
                            }
                        }

                        ?>
                    </table>
                </div>
            </div>
            <div class="col col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="order-extract wrap">
                    <table>
                        <tr>
                            <td class="center break-down" colspan="5">Informace k uzávěrce:</td>
                        </tr>
                        <?php

                        if (isset($debug)) {
                            echo
                                "<tr>
                                    <td class=\"center break-down\" colspan=\"5\"><a href=\"/statistics/show/", $json->url, "?debug\">Debug [obsluha]</a></td>
                                </tr>
                                <tr>
                                    <td class=\"center break-down\" colspan=\"5\"><a href=\"/statistics/show/", $json->url_privileged, "?debug\">Debug [celé]</a></td>
                                </tr>";
                        }

                        ?>
                    </table>
                    <?= $this->Form->create("Send", ["url" => ["controller" => "Deadlines", "action" => "send"], "type" => "post"]); ?>
                        <?= $this->Form->hidden("url", ["default" => $json->url]) ?>
                        <?= $this->Form->submit("Vytisknout [obsluha]", ["class" => "btn btn-primary btn-small print"]) ?>
                    <?= $this->Form->end(); ?>
                    <?= $this->Form->create("SendPrivileged", ["url" => ["controller" => "Deadlines", "action" => "send"], "type" => "post"]); ?>
                        <?= $this->Form->hidden("url", ["default" => $json->url_privileged]) ?>
                        <?= $this->Form->submit("Vytisknout [celé]", ["class" => "btn btn-primary btn-small print"]) ?>
                    <?= $this->Form->end(); ?>
                </div>
                <div style="text-align: right; margin-bottom: 10px;">
                    <?= $this->Form->button("Zpět", ["type" => "button", "class" => "btn btn-danger", "label" => false, "id" => "BackButton", "onclick" => "window.location = document.referrer;"]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    //saveData();
</script>