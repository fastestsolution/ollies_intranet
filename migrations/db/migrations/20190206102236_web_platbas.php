<?php


use Phinx\Migration\AbstractMigration;

class WebPlatbas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table("web_platbas", [
            'id' => false,
            'primary_key' => ['id'],
            'engine' => 'MyISAM',
            'encoding' => 'utf8',
            'collation' => 'utf8_czech_ci',
            'comment' => '',
            'row_format' => 'Dynamic',
        ])
        ->addColumn('id', 'integer', [
            'null' => false,
            'limit' => MysqlAdapter::INT_REGULAR,
            'precision' => '10',
            'signed' => false,
            'identity' => 'enable',
        ])
        ->addColumn('name', 'string', [
            'null' => false,
            'limit' => 50,
            'collation' => 'utf8_czech_ci',
            'encoding' => 'utf8',
            'after' => 'id',
        ])
        ->addColumn('cms_user_id', 'integer', [
            'null' => true,
            'limit' => MysqlAdapter::INT_REGULAR,
            'precision' => '10',
            'after' => 'name',
        ])
        ->addColumn('created', 'datetime', [
            'null' => true,
            'after' => 'cms_user_id',
        ])
        ->addColumn('modified', 'datetime', [
            'null' => true,
            'after' => 'created',
        ])
        ->addColumn('kos', 'integer', [
            'null' => true,
            'limit' => MysqlAdapter::INT_REGULAR,
            'precision' => '1',
            'default' => '0',
            'after' => 'modified',
        ])
        ->addColumn('nd', 'integer', [
            'null' => true,
            'limit' => MysqlAdapter::INT_REGULAR,
            'precision' => '1',
            'default' => '0',
            'after' => 'modified',
        ])
        ->addColumn('price', 'float', [
            'null' => true,
            'limit' => MysqlAdapter::FLOAT_REGULAR,
            'precision' => '10',
            'scale' => '2',
            'after' => 'nd',
        ])
        ->addColumn('price_with_tax', 'float', [
            'null' => true,
            'limit' => MysqlAdapter::FLOAT_REGULAR,
            'precision' => '10',
            'scale' => '2',
            'after' => 'price',
        ])
        ->addColumn('dph_list', 'integer', [
            'null' => true,
            'limit' => MysqlAdapter::INT_REGULAR,
            'precision' => '10',
            'after' => 'price_with_tax',
        ])
        ->addColumn('code', 'varchar', [
            'null' => true,
            'limit' => 6,
            'collation' => 'utf8_czech_ci',
            'encoding' => 'utf8',
            'after' => 'dph_list',
        ])
        ->addColumn('poradi', 'int', [
            'null' => true,
            'precision' => '10',
            'after' => 'data',
        ])
        ->addColumn('text', 'text', [
            'null' => true,
            'limit' => 65500,
            'precision' => '10',
            'after' => 'total_price',
        ])
        ->addIndex(['cms_user_id', 'local_id'], [
                'name' => 'cms_user_id',
                'unique' => false,
        ])
        ->addIndex(['kos'], [
                'name' => 'kos',
                'unique' => false,
        ])
        ->create();
    }
}
