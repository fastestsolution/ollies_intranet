<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class InitDb extends AbstractMigration
{
    public function change()
    {
        $this->execute("ALTER DATABASE CHARACTER SET 'utf8';");
        $this->execute("ALTER DATABASE COLLATE='utf8_general_ci';");
        $this->table("attendances", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('code', 'string', [
                'null' => true,
                'limit' => 10,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'system_id',
            ])
            ->addColumn('delivery_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'code',
            ])
            ->addColumn('date_from', 'datetime', [
                'null' => true,
                'after' => 'delivery_id',
            ])
            ->addColumn('date_to', 'datetime', [
                'null' => true,
                'after' => 'date_from',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 20,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'date_to',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'name',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
        ->addIndex(['system_id', 'code'], [
                'name' => 'system_id',
                'unique' => false,
            ])
        ->addIndex(['code'], [
                'name' => 'code',
                'unique' => false,
            ])
        ->addIndex(['delivery_id'], [
                'name' => 'delivery_id',
                'unique' => false,
            ])
            ->create();
        $this->table("branches", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('eet_active', 'integer', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'name',
            ])
            ->addColumn('eet_certificate', 'string', [
                'null' => true,
                'limit' => 255,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'eet_active',
            ])
            ->addColumn('eet_cert_pass', 'string', [
                'null' => true,
                'limit' => 200,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'eet_certificate',
            ])
            ->addColumn('eet_dic', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'eet_cert_pass',
            ])
            ->addColumn('eet_dic_pover', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'eet_dic',
            ])
            ->addColumn('eet_branch_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_MEDIUM,
                'precision' => '7',
                'comment' => 'ID pobocky',
                'after' => 'eet_dic_pover',
            ])
            ->addColumn('email', 'string', [
                'null' => true,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'eet_branch_id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'email',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'modified',
            ])
            ->addColumn('view_api', 'integer', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'trash',
            ])
        ->addIndex(['trash'], [
                'name' => 'kos',
                'unique' => false,
            ])
            ->create();
        $this->table("branches_stats", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('branch_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'branch_id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'modified',
            ])
            ->addColumn('value', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'trash',
            ])
            ->addColumn('date', 'date', [
                'null' => true,
                'after' => 'value',
            ])
        ->addIndex(['trash'], [
                'name' => 'kos',
                'unique' => false,
            ])
            ->create();
        $this->table("error_messages", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('message', 'string', [
                'null' => true,
                'limit' => 200,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'system_id',
            ])
            ->addColumn('phone', 'string', [
                'null' => true,
                'default' => '',
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'message',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'phone',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'created',
            ])
            ->addColumn('provoz_name', 'string', [
                'null' => true,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'name',
            ])
            ->create();
        $this->table("intranet_actuals", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => false,
                'limit' => 150,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('text', 'text', [
                'null' => false,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'name',
            ])
            ->addColumn('created', 'datetime', [
                'null' => false,
                'after' => 'text',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => false,
                'after' => 'created',
            ])
            ->addColumn('status', 'boolean', [
                'null' => false,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'modified',
            ])
            ->addColumn('kos', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'status',
            ])
            ->addColumn('nd', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'kos',
            ])
            ->addColumn('type', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'nd',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'type',
            ])
            ->addColumn('files', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'trash',
            ])
        ->addIndex(['kos'], [
                'name' => 'kos',
                'unique' => false,
            ])
        ->addIndex(['name'], [
                'name' => 'name',
                'unique' => false,
            ])
        ->addIndex(['status'], [
                'name' => 'status',
                'unique' => false,
            ])
        ->addIndex(['type'], [
                'name' => 'type',
                'unique' => false,
            ])
            ->create();
        $this->table("intranet_files", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => false,
                'limit' => 150,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('text', 'text', [
                'null' => false,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'name',
            ])
            ->addColumn('created', 'datetime', [
                'null' => false,
                'after' => 'text',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => false,
                'after' => 'created',
            ])
            ->addColumn('status', 'boolean', [
                'null' => false,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'modified',
            ])
            ->addColumn('kos', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'status',
            ])
            ->addColumn('nd', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'kos',
            ])
            ->addColumn('file', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'nd',
            ])
            ->addColumn('type', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'file',
            ])
            ->addColumn('intranet_actual_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'type',
            ])
        ->addIndex(['kos'], [
                'name' => 'kos',
                'unique' => false,
            ])
        ->addIndex(['name'], [
                'name' => 'name',
                'unique' => false,
            ])
        ->addIndex(['status'], [
                'name' => 'status',
                'unique' => false,
            ])
            ->create();
        $this->table("intranet_logs", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => false,
                'limit' => 150,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => false,
                'after' => 'name',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => false,
                'default' => '0000-00-00 00:00:00',
                'after' => 'created',
            ])
            ->addColumn('status', 'boolean', [
                'null' => false,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'modified',
            ])
            ->addColumn('kos', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'status',
            ])
            ->addColumn('nd', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'kos',
            ])
            ->addColumn('browser', 'string', [
                'null' => true,
                'limit' => 255,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'nd',
            ])
            ->addColumn('ip', 'string', [
                'null' => true,
                'limit' => 255,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'browser',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'ip',
            ])
            ->addColumn('type', 'string', [
                'null' => true,
                'limit' => 255,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'system_id',
            ])
            ->addColumn('read_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'type',
            ])
        ->addIndex(['kos'], [
                'name' => 'kos',
                'unique' => false,
            ])
        ->addIndex(['name'], [
                'name' => 'name',
                'unique' => false,
            ])
        ->addIndex(['status'], [
                'name' => 'status',
                'unique' => false,
            ])
            ->create();
        $this->table("intranet_zpravas", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => false,
                'limit' => 150,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('text', 'text', [
                'null' => false,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'name',
            ])
            ->addColumn('created', 'datetime', [
                'null' => false,
                'after' => 'text',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => false,
                'default' => '0000-00-00 00:00:00',
                'after' => 'created',
            ])
            ->addColumn('status', 'boolean', [
                'null' => false,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'modified',
            ])
            ->addColumn('kos', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'status',
            ])
            ->addColumn('nd', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'kos',
            ])
        ->addIndex(['kos'], [
                'name' => 'kos',
                'unique' => false,
            ])
        ->addIndex(['name'], [
                'name' => 'name',
                'unique' => false,
            ])
        ->addIndex(['status'], [
                'name' => 'status',
                'unique' => false,
            ])
            ->create();
        $this->table("logs", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('message', 'string', [
                'null' => true,
                'limit' => 200,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'system_id',
            ])
            ->addColumn('error', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'message',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'error',
            ])
            ->create();
        $this->table("map_areas", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'InnoDB',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Compact',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('coords', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'modified',
            ])
            ->create();
        $this->table("printer_types", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => false,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('setting_name', 'string', [
                'null' => false,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'comment' => 'nazev atributu tiskarny v kase',
                'after' => 'name',
            ])
            ->create();
        $this->table("product_conn_addons", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'InnoDB',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Compact',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('product_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'modified',
            ])
            ->addColumn('product_addon_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'product_id',
            ])
        ->addIndex(['product_id', 'product_addon_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
            ->create();
        $this->table("product_connects", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('product_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'modified',
            ])
            ->addColumn('product_group_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'product_id',
            ])
            ->addColumn('price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'product_group_id',
            ])
            ->addColumn('price2', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price',
            ])
            ->addColumn('price3', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price2',
            ])
            ->addColumn('price4', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price3',
            ])
            ->addColumn('pref', 'string', [
                'null' => true,
                'limit' => 5,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'price4',
            ])
        ->addIndex(['product_group_id'], [
                'name' => 'product_group_id',
                'unique' => false,
            ])
        ->addIndex(['product_id'], [
                'name' => 'product_id',
                'unique' => false,
            ])
            ->create();
        $this->table("product_connects_old", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('product_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'modified',
            ])
            ->addColumn('product_group_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'product_id',
            ])
            ->addColumn('price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'product_group_id',
            ])
            ->addColumn('price2', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price',
            ])
            ->addColumn('price3', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price2',
            ])
            ->addColumn('price4', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price3',
            ])
            ->addColumn('pref', 'string', [
                'null' => true,
                'limit' => 5,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'price4',
            ])
        ->addIndex(['product_group_id'], [
                'name' => 'product_group_id',
                'unique' => false,
            ])
        ->addIndex(['product_id'], [
                'name' => 'product_id',
                'unique' => false,
            ])
            ->create();
        $this->table("product_groups", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('parent_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'id',
            ])
            ->addColumn('level', 'integer', [
                'null' => false,
                'default' => '1',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'parent_id',
            ])
            ->addColumn('lft', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'level',
            ])
            ->addColumn('rght', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'lft',
            ])
            ->addColumn('printer_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_SMALL,
                'precision' => '5',
                'signed' => false,
                'after' => 'rght',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'default' => '',
                'limit' => 80,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'printer_id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'name',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('web_id', 'string', [
                'null' => true,
                'limit' => 255,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'modified',
            ])
            ->addColumn('order_num', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'web_id',
            ])
        ->addIndex(['parent_id'], [
                'name' => 'parent_id',
                'unique' => false,
            ])
            ->create();
        $this->table("product_groups_old", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('parent_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'id',
            ])
            ->addColumn('level', 'integer', [
                'null' => false,
                'default' => '1',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'parent_id',
            ])
            ->addColumn('lft', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'level',
            ])
            ->addColumn('rght', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'lft',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'default' => '',
                'limit' => 80,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'rght',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'name',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('web_id', 'string', [
                'null' => true,
                'limit' => 255,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'modified',
            ])
            ->addColumn('order_num', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'web_id',
            ])
        ->addIndex(['parent_id'], [
                'name' => 'parent_id',
                'unique' => false,
            ])
            ->create();
        $this->table("product_prices", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Fixed',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('checked', 'boolean', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'checked',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('product_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'modified',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'product_id',
            ])
            ->addColumn('price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'system_id',
            ])
            ->addColumn('price2', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price',
            ])
            ->addColumn('price3', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price2',
            ])
            ->addColumn('price4', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price3',
            ])
        ->addIndex(['system_id'], [
                'name' => 'product_group_id',
                'unique' => false,
            ])
        ->addIndex(['product_id'], [
                'name' => 'product_id',
                'unique' => false,
            ])
        ->addIndex(['system_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
            ->create();
        $this->table("product_recipes", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'InnoDB',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Compact',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('product_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'modified',
            ])
            ->addColumn('stock_item_global_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'product_id',
            ])
            ->addColumn('value', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '4',
                'after' => 'stock_item_global_id',
            ])
            ->addColumn('loss', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'value',
            ])
            ->addColumn('unit_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'loss',
            ])
        ->addIndex(['product_id', 'stock_item_global_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
        ->addIndex(['product_id'], [
                'name' => 'product_id',
                'unique' => false,
            ])
        ->addIndex(['stock_item_global_id'], [
                'name' => 'stock_item_global_id',
                'unique' => false,
            ])
            ->create();
        $this->table("sklad_ciselniks", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'name',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('kos', 'boolean', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'modified',
            ])
            ->addColumn('type', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'kos',
            ])
        ->addIndex(['kos', 'type'], [
                'name' => 'kos',
                'unique' => false,
            ])
        ->addIndex(['type'], [
                'name' => 'type',
                'unique' => false,
            ])
            ->create();
        $this->table("sklad_stav_items", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('sklad_stav_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'sklad_stav_id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('kos', 'boolean', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'modified',
            ])
            ->addColumn('sklad_item_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'kos',
            ])
            ->addColumn('stav', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'sklad_item_id',
            ])
            ->addColumn('stav_real', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'stav',
            ])
            ->addColumn('stav_count', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'stav_real',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'stav_count',
            ])
        ->addIndex(['sklad_item_id'], [
                'name' => 'sklad_type_id',
                'unique' => false,
            ])
            ->create();
        $this->table("sklad_stavs", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('date', 'date', [
                'null' => true,
                'after' => 'id',
            ])
            ->addColumn('from_sklad_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'date',
            ])
            ->addColumn('to_sklad_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'from_sklad_id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'to_sklad_id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('kos', 'boolean', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'modified',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'kos',
            ])
            ->create();
        $this->table("stock_global_items", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 200,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('jednotka_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'name',
            ])
            ->addColumn('jednotka_id_dodavatel', 'string', [
                'null' => true,
                'limit' => 5,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'jednotka_id',
            ])
            ->addColumn('group_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'jednotka_id_dodavatel',
            ])
            ->addColumn('min', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'group_id',
            ])
            ->addColumn('max', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'min',
            ])
            ->addColumn('dodavatel_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'max',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'dodavatel_id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('tax_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'modified',
            ])
            ->addColumn('ean', 'string', [
                'null' => true,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'tax_id',
            ])
            ->addColumn('bidfood_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'ean',
            ])
            ->addColumn('jednotka_prevod', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'bidfood_id',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'jednotka_prevod',
            ])
        ->addIndex(['group_id', 'dodavatel_id', 'ean', 'name'], [
                'name' => 'group_id',
                'unique' => false,
            ])
            ->create();
        $this->table("stock_list", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('jednotka_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('dodavatel_jednotka', 'string', [
                'null' => true,
                'limit' => 5,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'jednotka_id',
            ])
            ->addColumn('prevod', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'dodavatel_jednotka',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'prevod',
            ])
        ->addIndex(['jednotka_id', 'dodavatel_jednotka'], [
                'name' => 'jednotka_id',
                'unique' => false,
            ])
        ->addIndex(['trash'], [
                'name' => 'trash',
                'unique' => false,
            ])
            ->create();
        $this->table("stock_mirrors", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'InnoDB',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Compact',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'modified',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'system_id',
            ])
            ->addColumn('data_step1', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'name',
            ])
            ->addColumn('step_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'data_step1',
            ])
            ->addColumn('data_step2', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'step_id',
            ])
            ->addColumn('date_step1', 'datetime', [
                'null' => true,
                'after' => 'data_step2',
            ])
            ->addColumn('date_step2', 'datetime', [
                'null' => true,
                'after' => 'date_step1',
            ])
            ->addColumn('data_diff', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'date_step2',
            ])
            ->addColumn('is_confirm', 'boolean', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'data_diff',
            ])
            ->addColumn('data_step_change', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'is_confirm',
            ])
            ->addColumn('date_step_change', 'datetime', [
                'null' => true,
                'after' => 'data_step_change',
            ])
        ->addIndex(['is_confirm'], [
                'name' => 'is_confirm',
                'unique' => false,
            ])
        ->addIndex(['system_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
            ->create();
        $this->table("stock_products", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'name',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('code', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'modified',
            ])
            ->addColumn('group_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'code',
            ])
            ->addColumn('unit_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'group_id',
            ])
            ->addColumn('loss', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'unit_id',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'default' => '1000',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'loss',
            ])
            ->addColumn('status', 'boolean', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'system_id',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'status',
            ])
        ->addIndex(['group_id', 'system_id'], [
                'name' => 'group_id',
                'unique' => false,
            ])
        ->addIndex(['system_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
            ->create();
        $this->table("stock_queues", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('confirm', 'boolean', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'id',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'confirm',
            ])
            ->addColumn('system_id_from', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'system_id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'system_id_from',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'modified',
            ])
            ->addColumn('data', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'trash',
            ])
            ->addColumn('system_name_from', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'data',
            ])
        ->addIndex(['trash'], [
                'name' => 'kos',
                'unique' => false,
            ])
            ->create();
        $this->table("stocks", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('local_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'system_id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'local_id',
            ])
            ->addColumn('createdTime', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'created',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'createdTime',
            ])
            ->addColumn('invoice', 'string', [
                'null' => true,
                'limit' => 20,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'modified',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'invoice',
            ])
            ->addColumn('order_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'trash',
            ])
            ->addColumn('stock_item_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'order_id',
            ])
            ->addColumn('stock_item_product_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'stock_item_id',
            ])
            ->addColumn('value', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '4',
                'after' => 'stock_item_product_id',
            ])
            ->addColumn('stock_type_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'value',
            ])
            ->addColumn('system_id_to', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'stock_type_id',
            ])
            ->addColumn('nakup_price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'system_id_to',
            ])
            ->addColumn('nakup_price_vat', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'nakup_price',
            ])
            ->addColumn('nakup_price_total', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'nakup_price_vat',
            ])
            ->addColumn('nakup_price_total_vat', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'nakup_price_total',
            ])
            ->addColumn('tax_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'nakup_price_total_vat',
            ])
            ->addColumn('unit_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'tax_id',
            ])
            ->addColumn('note', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'unit_id',
            ])
            ->addColumn('user_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'note',
            ])
            ->addColumn('user_name', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'user_id',
            ])
            ->addColumn('terminal_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'user_name',
            ])
            ->addColumn('storno_date', 'datetime', [
                'null' => true,
                'after' => 'terminal_id',
            ])
            ->addColumn('product_group_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'storno_date',
            ])
        ->addIndex(['created', 'stock_type_id'], [
                'name' => 'created',
                'unique' => false,
            ])
        ->addIndex(['stock_item_id', 'stock_item_product_id', 'system_id', 'invoice'], [
                'name' => 'sklad_item_id',
                'unique' => false,
            ])
        ->addIndex(['stock_type_id', 'stock_item_id'], [
                'name' => 'sklad_type_id',
                'unique' => false,
            ])
        ->addIndex(['stock_item_product_id'], [
                'name' => 'stock_item_product_id',
                'unique' => false,
            ])
        ->addIndex(['product_group_id'], [
                'name' => 'product_group_id',
                'unique' => false,
            ])
        ->addIndex(['stock_type_id'], [
                'name' => 'stock_type_id',
                'unique' => false,
            ])
        ->addIndex(['system_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
        ->addIndex(['user_id'], [
                'name' => 'user_id',
                'unique' => false,
            ])
        ->addIndex(['terminal_id'], [
                'name' => 'terminal_id',
                'unique' => false,
            ])
        ->addIndex(['local_id'], [
                'name' => 'local_id',
                'unique' => false,
            ])
        ->addIndex(['storno_date'], [
                'name' => 'storno_date',
                'unique' => false,
            ])
        ->addIndex(['stock_item_id'], [
                'name' => 'stock_item_id',
                'unique' => false,
            ])
            ->create();
        $this->table("stocks_items", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('stock_global_item_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('product_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'stock_global_item_id',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 200,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'product_id',
            ])
            ->addColumn('jednotka_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'name',
            ])
            ->addColumn('jednotka_id_dodavatel', 'string', [
                'null' => true,
                'limit' => 5,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'jednotka_id',
            ])
            ->addColumn('group_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'jednotka_id_dodavatel',
            ])
            ->addColumn('min', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'group_id',
            ])
            ->addColumn('max', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'min',
            ])
            ->addColumn('dodavatel_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'max',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'dodavatel_id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('kos', 'boolean', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'modified',
            ])
            ->addColumn('tax_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'kos',
            ])
            ->addColumn('ean', 'string', [
                'null' => true,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'tax_id',
            ])
            ->addColumn('bidfood_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'ean',
            ])
            ->addColumn('jednotka_prevod', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'bidfood_id',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'jednotka_prevod',
            ])
        ->addIndex(['group_id', 'dodavatel_id', 'ean', 'name'], [
                'name' => 'group_id',
                'unique' => false,
            ])
        ->addIndex(['stock_global_item_id'], [
                'name' => 'sklad_item_global_id',
                'unique' => false,
            ])
            ->create();
        $this->table("system_logs", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('user_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'user_id',
            ])
            ->addColumn('message', 'string', [
                'null' => true,
                'limit' => 200,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'system_id',
            ])
            ->addColumn('error', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'message',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'error',
            ])
            ->addColumn('controller', 'string', [
                'null' => true,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'created',
            ])
            ->addColumn('action', 'string', [
                'null' => true,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'controller',
            ])
            ->addColumn('url', 'string', [
                'null' => true,
                'limit' => 150,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'action',
            ])
            ->addColumn('data', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'url',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'data',
            ])
        ->addIndex(['system_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
        ->addIndex(['user_id'], [
                'name' => 'user_id',
                'unique' => false,
            ])
        ->addIndex(['controller'], [
                'name' => 'controller',
                'unique' => false,
            ])
            ->create();
        $this->table("users", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('name', 'string', [
                'null' => false,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'modified',
            ])
            ->addColumn('username', 'string', [
                'null' => true,
                'limit' => 255,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'name',
            ])
            ->addColumn('password', 'string', [
                'null' => true,
                'limit' => 255,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'username',
            ])
            ->addColumn('status', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'password',
            ])
            ->addColumn('kos', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'status',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'kos',
            ])
            ->addColumn('email', 'string', [
                'null' => true,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'system_id',
            ])
            ->addColumn('url', 'string', [
                'null' => true,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'email',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'url',
            ])
            ->addColumn('phone', 'string', [
                'null' => true,
                'limit' => 9,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'trash',
            ])
            ->addColumn('branch_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'phone',
            ])
            ->addColumn('group_id', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'branch_id',
            ])
        ->addIndex(['status', 'kos', 'system_id'], [
                'name' => 'status',
                'unique' => false,
            ])
        ->addIndex(['name'], [
                'name' => 'name',
                'unique' => false,
            ])
        ->addIndex(['username'], [
                'name' => 'username',
                'unique' => false,
            ])
        ->addIndex(['branch_id'], [
                'name' => 'branch_id',
                'unique' => false,
            ])
        ->addIndex(['system_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
        ->addIndex(['group_id'], [
                'name' => 'group_id',
                'unique' => false,
            ])
            ->create();
        $this->table("web_clients", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_SMALL,
                'precision' => '5',
                'signed' => false,
                'after' => 'id',
            ])
            ->addColumn('eshop_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'system_id',
            ])
            ->addColumn('web_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'eshop_id',
            ])
            ->addColumn('local_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'web_id',
            ])
            ->addColumn('jmeno', 'string', [
                'null' => false,
                'limit' => 30,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'local_id',
            ])
            ->addColumn('prijmeni', 'string', [
                'null' => false,
                'limit' => 30,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'jmeno',
            ])
            ->addColumn('ulice', 'string', [
                'null' => false,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'prijmeni',
            ])
            ->addColumn('mesto', 'string', [
                'null' => false,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'ulice',
            ])
            ->addColumn('psc', 'string', [
                'null' => false,
                'limit' => 10,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'mesto',
            ])
            ->addColumn('telefon', 'string', [
                'null' => false,
                'limit' => 20,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'psc',
            ])
            ->addColumn('email', 'string', [
                'null' => false,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'telefon',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'email',
            ])
            ->addColumn('updated', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
        ->addIndex(['system_id', 'local_id'], [
                'name' => 'system_id_2',
                'unique' => true,
            ])
        ->addIndex(['eshop_id', 'web_id'], [
                'name' => 'CLI:eshop_id_2',
                'unique' => true,
            ])
        ->addIndex(['email'], [
                'name' => 'email',
                'unique' => false,
            ])
        ->addIndex(['web_id'], [
                'name' => 'web_id',
                'unique' => false,
            ])
        ->addIndex(['system_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
        ->addIndex(['eshop_id'], [
                'name' => 'eshop_id',
                'unique' => false,
            ])
        ->addIndex(['local_id'], [
                'name' => 'local_id',
                'unique' => false,
            ])
            ->create();
        $this->table("web_order_items", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_SMALL,
                'precision' => '5',
                'signed' => false,
                'after' => 'id',
            ])
            ->addColumn('eshop_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_SMALL,
                'precision' => '5',
                'signed' => false,
                'after' => 'system_id',
            ])
            ->addColumn('web_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'eshop_id',
            ])
            ->addColumn('local_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'web_id',
            ])
            ->addColumn('shop_order_id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'local_id',
            ])
            ->addColumn('shop_product_id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'shop_order_id',
            ])
            ->addColumn('pokl_product_id', 'integer', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'shop_product_id',
            ])
            ->addColumn('code', 'string', [
                'null' => true,
                'limit' => 10,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'pokl_product_id',
            ])
            ->addColumn('ean', 'string', [
                'null' => false,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'code',
            ])
            ->addColumn('name', 'string', [
                'null' => false,
                'limit' => 150,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'ean',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'name',
            ])
            ->addColumn('updated', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('status', 'boolean', [
                'null' => false,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'updated',
            ])
            ->addColumn('kos', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'status',
            ])
            ->addColumn('nd', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'kos',
            ])
            ->addColumn('price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'nd',
            ])
            ->addColumn('price_with_tax', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price',
            ])
            ->addColumn('row_price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price_with_tax',
            ])
            ->addColumn('row_price_with_tax', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'row_price',
            ])
            ->addColumn('ks', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'row_price_with_tax',
            ])
            ->addColumn('dph', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'ks',
            ])
            ->addColumn('shop_dodavatel_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'dph',
            ])
            ->addColumn('bezlepkovy', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_SMALL,
                'precision' => '5',
                'after' => 'shop_dodavatel_id',
            ])
            ->addColumn('darkove_baleni', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_SMALL,
                'precision' => '5',
                'after' => 'bezlepkovy',
            ])
            ->addColumn('cislovka', 'string', [
                'null' => true,
                'limit' => 20,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'darkove_baleni',
            ])
            ->addColumn('napis', 'string', [
                'null' => true,
                'limit' => 20,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'cislovka',
            ])
        ->addIndex(['system_id', 'local_id'], [
                'name' => 'system_id_2',
                'unique' => true,
            ])
        ->addIndex(['eshop_id', 'web_id'], [
                'name' => 'OITEM:eshop_id_2',
                'unique' => true,
            ])
        ->addIndex(['kos'], [
                'name' => 'kos',
                'unique' => false,
            ])
        ->addIndex(['name'], [
                'name' => 'name',
                'unique' => false,
            ])
        ->addIndex(['status'], [
                'name' => 'status',
                'unique' => false,
            ])
        ->addIndex(['web_id'], [
                'name' => 'web_id',
                'unique' => false,
            ])
        ->addIndex(['system_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
        ->addIndex(['eshop_id'], [
                'name' => 'eshop_id',
                'unique' => false,
            ])
        ->addIndex(['local_id'], [
                'name' => 'local_id',
                'unique' => false,
            ])
        ->addIndex(['code'], [
                'name' => 'code',
                'unique' => false,
            ])
            ->create();
        $this->table("web_orders", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('type_id', 'integer', [
                'null' => false,
                'default' => '1',
                'limit' => MysqlAdapter::INT_SMALL,
                'precision' => '5',
                'signed' => false,
                'comment' => 'na objednavku, volny prodej',
                'after' => 'id',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'type_id',
            ])
            ->addColumn('eshop_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_SMALL,
                'precision' => '5',
                'signed' => false,
                'after' => 'system_id',
            ])
            ->addColumn('web_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'eshop_id',
            ])
            ->addColumn('local_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'web_id',
            ])
            ->addColumn('shop_client_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'local_id',
            ])
            ->addColumn('shop_doprava_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'shop_client_id',
            ])
            ->addColumn('shop_platba_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'shop_doprava_id',
            ])
            ->addColumn('name', 'string', [
                'null' => false,
                'limit' => 150,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'shop_platba_id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'name',
            ])
            ->addColumn('updated', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('status', 'boolean', [
                'null' => false,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'updated',
            ])
            ->addColumn('kos', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'status',
            ])
            ->addColumn('nd', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'kos',
            ])
            ->addColumn('cms_user_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'nd',
            ])
            ->addColumn('price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'cms_user_id',
            ])
            ->addColumn('price_with_tax', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price',
            ])
            ->addColumn('doprava_price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price_with_tax',
            ])
            ->addColumn('doprava_price_with_tax', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'doprava_price',
            ])
            ->addColumn('platba_price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'doprava_price_with_tax',
            ])
            ->addColumn('platba_price_with_tax', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'platba_price',
            ])
            ->addColumn('doprava_zdarma', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'platba_price_with_tax',
            ])
            ->addColumn('paymentSessionId', 'string', [
                'null' => false,
                'limit' => 50,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'doprava_zdarma',
            ])
            ->addColumn('poznamka', 'text', [
                'null' => false,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'paymentSessionId',
            ])
            ->addColumn('poznamka_interni', 'text', [
                'null' => false,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'poznamka',
            ])
            ->addColumn('pay', 'integer', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'poznamka_interni',
            ])
            ->addColumn('vs', 'string', [
                'null' => false,
                'limit' => 10,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'pay',
            ])
            ->addColumn('vs2', 'string', [
                'null' => false,
                'limit' => 10,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'vs',
            ])
            ->addColumn('balik_code', 'string', [
                'null' => false,
                'limit' => 25,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'vs2',
            ])
            ->addColumn('stav', 'integer', [
                'null' => false,
                'default' => '1',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'balik_code',
            ])
            ->addColumn('odeslane_email', 'text', [
                'null' => false,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'stav',
            ])
            ->addColumn('odeslane_sms', 'text', [
                'null' => false,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'odeslane_email',
            ])
            ->addColumn('historie', 'text', [
                'null' => false,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'odeslane_sms',
            ])
            ->addColumn('fa_code', 'string', [
                'null' => true,
                'limit' => 11,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'historie',
            ])
            ->addColumn('fa_code2', 'string', [
                'null' => true,
                'limit' => 11,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'fa_code',
            ])
            ->addColumn('dopropis_date', 'date', [
                'null' => true,
                'after' => 'fa_code2',
            ])
            ->addColumn('dobropis_code', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'dopropis_date',
            ])
            ->addColumn('fa_date', 'date', [
                'null' => true,
                'after' => 'dobropis_code',
            ])
            ->addColumn('order_date', 'date', [
                'null' => true,
                'after' => 'fa_date',
            ])
            ->addColumn('pickup_time', 'string', [
                'null' => true,
                'limit' => 20,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'order_date',
            ])
            ->addColumn('gifted_name', 'string', [
                'null' => true,
                'limit' => 100,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'pickup_time',
            ])
            ->addColumn('gifted_phone', 'string', [
                'null' => true,
                'limit' => 15,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'gifted_name',
            ])
            ->addColumn('gdpr', 'string', [
                'null' => true,
                'limit' => 15,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'gifted_phone',
            ])
            ->addColumn('delivery', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'gdpr',
            ])
            ->addColumn('synchronized', 'datetime', [
                'null' => true,
                'after' => 'delivery',
            ])
        ->addIndex(['system_id', 'local_id'], [
                'name' => 'system_id_2',
                'unique' => true,
            ])
        ->addIndex(['eshop_id', 'web_id'], [
                'name' => 'eshop_id_2',
                'unique' => true,
            ])
        ->addIndex(['cms_user_id'], [
                'name' => 'cms_user_id',
                'unique' => false,
            ])
        ->addIndex(['kos'], [
                'name' => 'kos',
                'unique' => false,
            ])
        ->addIndex(['name'], [
                'name' => 'name',
                'unique' => false,
            ])
        ->addIndex(['status'], [
                'name' => 'status',
                'unique' => false,
            ])
        ->addIndex(['web_id'], [
                'name' => 'web_id',
                'unique' => false,
            ])
        ->addIndex(['eshop_id'], [
                'name' => 'eshop_id',
                'unique' => false,
            ])
        ->addIndex(['order_date'], [
                'name' => 'order_date',
                'unique' => false,
            ])
        ->addIndex(['type_id'], [
                'name' => 'type_id',
                'unique' => false,
            ])
            ->create();
        $this->table("web_platbas", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_general_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => false,
                'limit' => 150,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => false,
                'after' => 'name',
            ])
            ->addColumn('updated', 'datetime', [
                'null' => false,
                'after' => 'created',
            ])
            ->addColumn('status', 'boolean', [
                'null' => false,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'updated',
            ])
            ->addColumn('kos', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'status',
            ])
            ->addColumn('nd', 'boolean', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'kos',
            ])
            ->addColumn('cms_user_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'nd',
            ])
            ->addColumn('price', 'float', [
                'null' => false,
                'precision' => '10',
                'scale' => '2',
                'after' => 'cms_user_id',
            ])
            ->addColumn('price_with_tax', 'float', [
                'null' => false,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price',
            ])
            ->addColumn('dph_list', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'price_with_tax',
            ])
            ->addColumn('code', 'string', [
                'null' => false,
                'limit' => 6,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'dph_list',
            ])
            ->addColumn('poradi', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'code',
            ])
            ->addColumn('text', 'text', [
                'null' => false,
                'limit' => 65535,
                'collation' => 'utf8_general_ci',
                'encoding' => 'utf8',
                'after' => 'poradi',
            ])
        ->addIndex(['cms_user_id'], [
                'name' => 'cms_user_id',
                'unique' => false,
            ])
        ->addIndex(['kos'], [
                'name' => 'kos',
                'unique' => false,
            ])
        ->addIndex(['name'], [
                'name' => 'name',
                'unique' => false,
            ])
        ->addIndex(['poradi'], [
                'name' => 'poradi',
                'unique' => false,
            ])
        ->addIndex(['status'], [
                'name' => 'status',
                'unique' => false,
            ])
            ->create();
        $this->table("deadlines", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_czech_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('pokladna_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('terminal_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'pokladna_id',
            ])
            ->addColumn('user_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'terminal_id',
            ])
            ->addColumn('branch_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'user_id',
            ])
            ->addColumn('order_id_from', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'branch_id',
            ])
            ->addColumn('order_id_to', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'order_id_from',
            ])
            ->addColumn('type_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'order_id_to',
            ])
            ->addColumn('price_total', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'type_id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'price_total',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('status', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'modified',
            ])
            ->addColumn('nodelete', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'status',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'nodelete',
            ])
            ->addColumn('createdTime', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'trash',
            ])
            ->addColumn('data', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'createdTime',
            ])
        ->addIndex(['created'], [
                'name' => 'created',
                'unique' => false,
            ])
        ->addIndex(['createdTime'], [
                'name' => 'createdTime',
                'unique' => false,
            ])
        ->addIndex(['order_id_from'], [
                'name' => 'order_id_from',
                'unique' => false,
            ])
        ->addIndex(['order_id_to'], [
                'name' => 'order_id_to',
                'unique' => false,
            ])
        ->addIndex(['status'], [
                'name' => 'status',
                'unique' => false,
            ])
        ->addIndex(['trash'], [
                'name' => 'trash',
                'unique' => false,
            ])
        ->addIndex(['type_id'], [
                'name' => 'type_id',
                'unique' => false,
            ])
        ->addIndex(['pokladna_id'], [
                'name' => 'pokladna_id',
                'unique' => false,
            ])
        ->addIndex(['branch_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
        ->addIndex(['pokladna_id'], [
                'name' => 'branch_id',
                'unique' => false,
            ])
        ->addIndex(['user_id'], [
                'name' => 'user_id',
                'unique' => false,
            ])
        ->addIndex(['terminal_id'], [
                'name' => 'terminal_id',
                'unique' => false,
            ])
            ->create();
        $this->table("order_items", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_czech_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('order_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'id',
            ])
            ->addColumn('storno', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'order_id',
            ])
            ->addColumn('storno_user_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'storno',
            ])
            ->addColumn('storno_type', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'storno_user_id',
            ])
            ->addColumn('soft_storno_count', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_SMALL,
                'precision' => '5',
                'after' => 'storno_type',
            ])
            ->addColumn('repair_count', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_SMALL,
                'precision' => '5',
                'after' => 'soft_storno_count',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'repair_count',
            ])
            ->addColumn('amount', 'string', [
                'null' => true,
                'limit' => 11,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'name',
            ])
            ->addColumn('code', 'string', [
                'null' => true,
                'limit' => 11,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'amount',
            ])
            ->addColumn('num', 'string', [
                'null' => true,
                'limit' => 11,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'code',
            ])
            ->addColumn('price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'num',
            ])
            ->addColumn('price_without_tax', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price',
            ])
            ->addColumn('price_total', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price_without_tax',
            ])
            ->addColumn('price_total_without_tax', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price_total',
            ])
            ->addColumn('count', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'price_total_without_tax',
            ])
            ->addColumn('product_group_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'count',
            ])
            ->addColumn('web_group_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'product_group_id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'web_group_id',
            ])
            ->addColumn('createdTime', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'created',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'createdTime',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'modified',
            ])
            ->addColumn('product_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'trash',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'product_id',
            ])
            ->addColumn('price_default', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'system_id',
            ])
            ->addColumn('free', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'price_default',
            ])
            ->addColumn('price_addon', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'free',
            ])
            ->addColumn('addon', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'price_addon',
            ])
            ->addColumn('addonAdd', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'addon',
            ])
            ->addColumn('tax_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'addonAdd',
            ])
            ->addColumn('user_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'tax_id',
            ])
            ->addColumn('queue', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_SMALL,
                'precision' => '5',
                'after' => 'user_id',
            ])
        ->addIndex(['free'], [
                'name' => 'free',
                'unique' => false,
            ])
        ->addIndex(['order_id'], [
                'name' => 'order_id',
                'unique' => false,
            ])
        ->addIndex(['product_group_id'], [
                'name' => 'product_group_id',
                'unique' => false,
            ])
        ->addIndex(['product_id'], [
                'name' => 'product_id',
                'unique' => false,
            ])
        ->addIndex(['system_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
        ->addIndex(['trash'], [
                'name' => 'trash',
                'unique' => false,
            ])
        ->addIndex(['web_group_id'], [
                'name' => 'web_group_id',
                'unique' => false,
            ])
            ->create();
        $this->table("orders", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_czech_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('order_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('terminal_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'order_id',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'terminal_id',
            ])
            ->addColumn('local_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'system_id',
            ])
            ->addColumn('user_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'local_id',
            ])
            ->addColumn('user_name', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'user_id',
            ])
            ->addColumn('table_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'user_name',
            ])
            ->addColumn('table_type_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'table_id',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'table_type_id',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'name',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'modified',
            ])
            ->addColumn('status', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'trash',
            ])
            ->addColumn('note', 'string', [
                'null' => true,
                'limit' => 100,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'status',
            ])
            ->addColumn('close_order', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'note',
            ])
            ->addColumn('total_price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'close_order',
            ])
            ->addColumn('payment_id', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'total_price',
            ])
            ->addColumn('createdTime', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'payment_id',
            ])
            ->addColumn('storno', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'createdTime',
            ])
            ->addColumn('storno_type', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'storno',
            ])
            ->addColumn('fik', 'string', [
                'null' => true,
                'limit' => 250,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'storno_type',
            ])
            ->addColumn('bkp', 'string', [
                'null' => true,
                'limit' => 250,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'fik',
            ])
            ->addColumn('table_open', 'datetime', [
                'null' => true,
                'after' => 'bkp',
            ])
            ->addColumn('table_close', 'datetime', [
                'null' => true,
                'after' => 'table_open',
            ])
            ->addColumn('table_paid', 'datetime', [
                'null' => true,
                'after' => 'table_close',
            ])
            ->addColumn('table_close_history', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'table_paid',
            ])
            ->addColumn('pay_casch', 'decimal', [
                'null' => true,
                'default' => '0.00',
                'precision' => '10',
                'scale' => '2',
                'after' => 'table_close_history',
            ])
            ->addColumn('pay_card', 'decimal', [
                'null' => true,
                'default' => '0.00',
                'precision' => '10',
                'scale' => '2',
                'after' => 'pay_casch',
            ])
            ->addColumn('pay_voucher', 'decimal', [
                'null' => true,
                'default' => '0.00',
                'precision' => '10',
                'scale' => '2',
                'after' => 'pay_card',
            ])
            ->addColumn('pay_discount', 'decimal', [
                'null' => true,
                'default' => '0.00',
                'precision' => '10',
                'scale' => '2',
                'after' => 'pay_voucher',
            ])
            ->addColumn('pay_employee', 'decimal', [
                'null' => true,
                'default' => '0.00',
                'precision' => '10',
                'scale' => '2',
                'after' => 'pay_discount',
            ])
            ->addColumn('pay_company', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'pay_employee',
            ])
            ->addColumn('pay_gastro_ticket', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'pay_company',
            ])
            ->addColumn('pay_gastro_card', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'pay_gastro_ticket',
            ])
            ->addColumn('print_user_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'pay_gastro_card',
            ])
            ->addColumn('storno_user_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'print_user_id',
            ])
            ->addColumn('storno_reason', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'storno_user_id',
            ])
            ->addColumn('table_name', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'storno_reason',
            ])
            ->addColumn('delivery', 'boolean', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'table_name',
            ])
        ->addIndex(['close_order'], [
                'name' => 'close_order',
                'unique' => false,
            ])
        ->addIndex(['created'], [
                'name' => 'created',
                'unique' => false,
            ])
        ->addIndex(['createdTime'], [
                'name' => 'createdTime',
                'unique' => false,
            ])
        ->addIndex(['name'], [
                'name' => 'name',
                'unique' => false,
            ])
        ->addIndex(['payment_id'], [
                'name' => 'payment_id',
                'unique' => false,
            ])
        ->addIndex(['print_user_id'], [
                'name' => 'print_user_id',
                'unique' => false,
            ])
        ->addIndex(['status'], [
                'name' => 'status',
                'unique' => false,
            ])
        ->addIndex(['storno'], [
                'name' => 'storno',
                'unique' => false,
            ])
        ->addIndex(['system_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
        ->addIndex(['table_id'], [
                'name' => 'table_id',
                'unique' => false,
            ])
        ->addIndex(['table_type_id'], [
                'name' => 'table_type_id',
                'unique' => false,
            ])
        ->addIndex(['trash'], [
                'name' => 'trash',
                'unique' => false,
            ])
        ->addIndex(['local_id'], [
                'name' => 'local_id',
                'unique' => false,
            ])
        ->addIndex(['storno_type'], [
                'name' => 'storno_type',
                'unique' => false,
            ])
            ->create();
        $this->table("orders2", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_czech_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('table_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'table_id',
            ])
            ->addColumn('source_id', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'name',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'source_id',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'modified',
            ])
            ->addColumn('status', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'trash',
            ])
            ->addColumn('note', 'string', [
                'null' => true,
                'limit' => 100,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'status',
            ])
            ->addColumn('close_order', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'note',
            ])
            ->addColumn('total_price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'close_order',
            ])
            ->addColumn('payment_id', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'total_price',
            ])
            ->addColumn('createdTime', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'payment_id',
            ])
            ->addColumn('storno', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'createdTime',
            ])
            ->addColumn('fik', 'string', [
                'null' => true,
                'limit' => 250,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'storno',
            ])
            ->addColumn('bkp', 'string', [
                'null' => true,
                'limit' => 250,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'fik',
            ])
            ->addColumn('table_open', 'datetime', [
                'null' => true,
                'after' => 'bkp',
            ])
            ->addColumn('table_close', 'datetime', [
                'null' => true,
                'after' => 'table_open',
            ])
            ->addColumn('pay_casch', 'decimal', [
                'null' => true,
                'default' => '0.00',
                'precision' => '10',
                'scale' => '2',
                'after' => 'table_close',
            ])
            ->addColumn('pay_card', 'decimal', [
                'null' => true,
                'default' => '0.00',
                'precision' => '10',
                'scale' => '2',
                'after' => 'pay_casch',
            ])
            ->addColumn('pay_bonus', 'decimal', [
                'null' => true,
                'default' => '0.00',
                'precision' => '10',
                'scale' => '2',
                'after' => 'pay_card',
            ])
            ->addColumn('pay_voucher', 'decimal', [
                'null' => true,
                'default' => '0.00',
                'precision' => '10',
                'scale' => '2',
                'after' => 'pay_bonus',
            ])
            ->addColumn('pay_discount', 'decimal', [
                'null' => true,
                'default' => '0.00',
                'precision' => '10',
                'scale' => '2',
                'after' => 'pay_voucher',
            ])
            ->addColumn('pay_employee', 'decimal', [
                'null' => true,
                'default' => '0.00',
                'precision' => '10',
                'scale' => '2',
                'after' => 'pay_discount',
            ])
            ->addColumn('print_user_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'pay_employee',
            ])
        ->addIndex(['close_order'], [
                'name' => 'close_order',
                'unique' => false,
            ])
        ->addIndex(['created'], [
                'name' => 'created',
                'unique' => false,
            ])
        ->addIndex(['createdTime'], [
                'name' => 'createdTime',
                'unique' => false,
            ])
        ->addIndex(['name'], [
                'name' => 'name',
                'unique' => false,
            ])
        ->addIndex(['payment_id'], [
                'name' => 'payment_id',
                'unique' => false,
            ])
        ->addIndex(['source_id'], [
                'name' => 'source_id',
                'unique' => false,
            ])
        ->addIndex(['status'], [
                'name' => 'status',
                'unique' => false,
            ])
        ->addIndex(['storno'], [
                'name' => 'storno',
                'unique' => false,
            ])
        ->addIndex(['trash'], [
                'name' => 'trash',
                'unique' => false,
            ])
            ->create();
        $this->table("product_addons", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'InnoDB',
                'encoding' => 'utf8',
                'collation' => 'utf8_czech_ci',
                'comment' => '',
                'row_format' => 'Compact',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => false,
                'limit' => 50,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('created', 'timestamp', [
                'null' => false,
                'default' => 'CURRENT_TIMESTAMP',
                'after' => 'name',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'modified',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'trash',
            ])
            ->addColumn('group_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'system_id',
            ])
            ->addColumn('price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'group_id',
            ])
        ->addIndex(['system_id', 'trash'], [
                'name' => 'system_id',
                'unique' => false,
            ])
            ->create();
        $this->table("products", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_czech_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('code', 'string', [
                'null' => true,
                'limit' => 10,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'name',
            ])
            ->addColumn('web_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'code',
            ])
            ->addColumn('product_group_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'web_id',
            ])
            ->addColumn('web_group_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'product_group_id',
            ])
            ->addColumn('num', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'web_group_id',
            ])
            ->addColumn('price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'num',
            ])
            ->addColumn('price_without_tax', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'price',
            ])
            ->addColumn('amount', 'string', [
                'null' => true,
                'limit' => 10,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'price_without_tax',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'amount',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('status', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'modified',
            ])
            ->addColumn('nodelete', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'status',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'nodelete',
            ])
            ->addColumn('tax_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'trash',
            ])
            ->addColumn('addons', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'tax_id',
            ])
            ->addColumn('group_trzba_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'addons',
            ])
            ->addColumn('is_mirror', 'boolean', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'group_trzba_id',
            ])
            ->addColumn('is_dashboard', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'is_mirror',
            ])
            ->addColumn('view_workshop', 'integer', [
                'null' => false,
                'default' => '0',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'is_dashboard',
            ])
            ->addColumn('reserve', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'view_workshop',
            ])
            ->addColumn('view_api', 'boolean', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'reserve',
            ])
        ->addIndex(['name'], [
                'name' => 'name',
                'unique' => false,
            ])
        ->addIndex(['trash'], [
                'name' => 'trash',
                'unique' => false,
            ])
        ->addIndex(['status'], [
                'name' => 'status',
                'unique' => false,
            ])
        ->addIndex(['product_group_id'], [
                'name' => 'product_group_id',
                'unique' => false,
            ])
        ->addIndex(['web_group_id'], [
                'name' => 'web_group_id',
                'unique' => false,
            ])
        ->addIndex(['code'], [
                'name' => 'code',
                'unique' => false,
            ])
        ->addIndex(['web_id'], [
                'name' => 'web_id',
                'unique' => false,
            ])
        ->addIndex(['is_mirror'], [
                'name' => 'is_mirror',
                'unique' => false,
            ])
        ->addIndex(['is_dashboard'], [
                'name' => 'is_dashboard',
                'unique' => false,
            ])
        ->addIndex(['view_api'], [
                'name' => 'view_api',
                'unique' => false,
            ])
            ->create();
        $this->table("settings", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_czech_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'identity' => 'enable',
            ])
            ->addColumn('name', 'string', [
                'null' => true,
                'limit' => 50,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'id',
            ])
            ->addColumn('data', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'name',
            ])
            ->addColumn('created', 'datetime', [
                'null' => true,
                'after' => 'data',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('status', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'modified',
            ])
            ->addColumn('nodelete', 'integer', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'signed' => false,
                'after' => 'status',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'nodelete',
            ])
            ->addColumn('logo', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'trash',
            ])
            ->addColumn('printer', 'string', [
                'null' => true,
                'limit' => 200,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'logo',
            ])
            ->addColumn('operations', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'printer',
            ])
            ->create();
        $this->table("tables", [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'MyISAM',
                'encoding' => 'utf8',
                'collation' => 'utf8_czech_ci',
                'comment' => '',
                'row_format' => 'Dynamic',
            ])
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'identity' => 'enable',
            ])
            ->addColumn('system_id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'id',
            ])
            ->addColumn('local_id', 'integer', [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'signed' => false,
                'after' => 'system_id',
            ])
            ->addColumn('name', 'string', [
                'null' => false,
                'limit' => 50,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'local_id',
            ])
            ->addColumn('table_type_id', 'integer', [
                'null' => true,
                'default' => '1',
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'name',
            ])
            ->addColumn('date_open', 'datetime', [
                'null' => true,
                'after' => 'table_type_id',
            ])
            ->addColumn('created', 'timestamp', [
                'null' => false,
                'default' => 'CURRENT_TIMESTAMP',
                'after' => 'date_open',
            ])
            ->addColumn('modified', 'datetime', [
                'null' => true,
                'after' => 'created',
            ])
            ->addColumn('num', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'modified',
            ])
            ->addColumn('group_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'num',
            ])
            ->addColumn('on_map', 'boolean', [
                'null' => true,
                'default' => '0',
                'limit' => MysqlAdapter::INT_TINY,
                'precision' => '3',
                'after' => 'group_id',
            ])
            ->addColumn('data', 'text', [
                'null' => true,
                'limit' => 65535,
                'collation' => 'utf8_czech_ci',
                'encoding' => 'utf8',
                'after' => 'on_map',
            ])
            ->addColumn('total_price', 'decimal', [
                'null' => true,
                'precision' => '10',
                'scale' => '2',
                'after' => 'data',
            ])
            ->addColumn('terminal_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'total_price',
            ])
            ->addColumn('user_id', 'integer', [
                'null' => true,
                'limit' => MysqlAdapter::INT_REGULAR,
                'precision' => '10',
                'after' => 'terminal_id',
            ])
            ->addColumn('trash', 'datetime', [
                'null' => true,
                'after' => 'user_id',
            ])
        ->addIndex(['system_id', 'local_id'], [
                'name' => 'system_id_2',
                'unique' => true,
            ])
        ->addIndex(['terminal_id'], [
                'name' => 'terminal_id',
                'unique' => false,
            ])
        ->addIndex(['system_id'], [
                'name' => 'system_id',
                'unique' => false,
            ])
        ->addIndex(['table_type_id'], [
                'name' => 'table_type_id',
                'unique' => false,
            ])
        ->addIndex(['local_id'], [
                'name' => 'local_id',
                'unique' => false,
            ])
            ->create();
    }
}
