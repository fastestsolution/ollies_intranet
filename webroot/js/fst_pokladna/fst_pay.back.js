var FstPay = this.FstPay = new Class({
	Implements:[Options,Events],

	//Implements:[Options,Events,FstUzaverka],	
	// init fce
	initialize:function(options){
		window.addEvent('domready',function(){
			if (window.debug) console.log('init pay');
			
			this.pays_events();
		}.bind(this));
	},
	
	// show pay modal
	showModal:function(options){
		$('basket_pay').removeClass('none');
		$$('.product_addons').addClass('none');
		this.create_line_to_bill();
		//$('vybrat-vse').fireEvent('click');
	
	},
	
	// kontrola zda je neco vybrano k zaplaceni
	check_bill_products: function(){
		if (Object.getLength(this.bill_products_data) == 0){
			FstError('Musíte vybrat produkty');
			return false;
		} else {
			this.bill_back_button();
		}
	},
	
	// save order data
	saveOrder: function(platba_id){
		window.platba_id = platba_id;
		this.ukoncit_order();
		
		
	},
	
	saveOrderReq: function(){
		//console.log(window.table_id);
		//console.log(this.bill_products_data);
		$('CheckUkoncit').value = 0;
		VarsModal.load_data = new Request.JSON({
			url:'/orders/save_order_data/'+window.table_id+'/'+window.platba_id+'/',
			data: this.bill_products_data,
			onComplete: VarsModal.compl_fce = (function(json){
				if (json && json.r == true){
					// print data
					fstPrint.print_data(json.print_data);	
					
					// clear pay products
					this.clear_payed_products(json);
					
					//$('ukoncit').fireEvent('click',$('ukoncit'));
					
				} else {
					FstError(json.m);
				}
				json = null;
					
				VarsModal.load_data.cancel();
				json = null;
			}.bind(this))
		});
		VarsModal.load_data.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		VarsModal.load_data.post();// load orders request	
	},
	
	// ukoncit order
	ukoncit_order: function(delete_function){
		if (delete_function){
			this.delete_function = true;
		} else {
			this.delete_function = false;
		}
		//console.log(VarsModal.local_table_data);
		if (VarsModal.local_table_data)
		Object.each(VarsModal.local_table_data,VarsModal.data_each = function(item,table_id){
			if (typeof VarsModal.local_table_data[table_id]['items'] != 'undefined')
			Object.each(VarsModal.local_table_data[table_id]['items'],VarsModal.data_each2 = function(item_p,line_id){
				VarsModal.local_table_data[table_id]['items'][line_id].done = 1;
			});
		});
		if (window.debug) console.log('save table data',VarsModal.local_table_data[window.table_id]);
		
		VarsModal.save_data = new Request.JSON({
			url:'/orders/save_table_data/',
			data:VarsModal.local_table_data,
			onComplete: VarsModal.compl_fce = (function(json){
						
				if (json && json.r == true){
					//this.reset_default_status();		
					this.saveOrderReq();
					//fstPrint.print_data();
		
				} else {
				}
				VarsModal.save_data.cancel();
				json = null;
			}.bind(this))
		});
		VarsModal.save_data.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		VarsModal.save_data.send();// load orders request
	},
	
	
	// vycisteni zaplacenych produktu
	clear_payed_products: function(json){
		$('CheckUkoncit').value = 0;
		console.log(this.bill_products_data);
		Object.each(this.bill_products_data,VarsModal.each = function(item){
			$(item.elId).removeEvents('click');
			$(item.elId).destroy();
			//console.log(item);
		});
		//console.log(json);
		//fstvars.clearEl($('bill_products'));
		/*
		$('pay_products_list').getElements('.active').each(function(item){
			item.removeEvents('click');
			item.destroy();
		});
		*/
		this.bill_products_data = {}
		fstvars.clearEl($('bill_products'));
		$('pay_bill').addClass('empty');
		$('TableId'+json.table_id).click();
		json = null;
		this.bill_recount_total_price();
	},
	
	// pay ucet
	pay_ucet: function(el){
		// hotove
		if (el.get('data-type') == 'hotove'){
			
			// save order
			this.saveOrder(el.get('data-platba_type_id'));
			
		}
		
		// kartou
		if (el.get('data-type') == 'kartou'){
			
			// save order
			this.saveOrder(el.get('data-platba_type_id'));
		}
	},
	
	// pay events
	pays_events: function(){
		// posunuti klavesnici
		$('body').addEvent('keydown',function(e){
			if ($('pay_products_list').getElement('.active')){
				active_li = $('pay_products_list').getElement('.active');
				if (e.key == 'up'){
					if (active_li.getPrevious('div:not(.none)')){
						active_li.removeClass('active');
						active_li.getPrevious('div').addClass('active');
					}
				}
				if (e.key == 'down'){
					if (active_li.getNext('div:not(.none)')){
						active_li.removeClass('active');
						active_li.getNext('div').addClass('active');
					}
				}
				if (e.key == 'enter'){
					active_li.fireEvent('click',active_li);
				}
			}
		});
		
		
		// bill funkce
		$('basket_pay').getElements('.bill_fce').addEvent('click',VarsModal.bill_fce = function(e){
			// zaplatit
			if (e.target.get('data-type') == 'done'){
				this.check_bill_products();
				
			}
		}.bind(this));
		
		// platebni funkce
		$('basket_pay').getElements('.pay_fce').addEvent('click',VarsModal.pay_fce = function(e){
			this.pay_ucet(e.target);
		
			
		}.bind(this));
		
		
		// ucet zpet pri vyberu platby
		if ($('bill_back'))
		$('bill_back').addEvent('click',this.bill_back_button);
		
		// schovani placeni uctu
		$('billHide').addEvent('click',this.hideModal);
		
		// vybrat vse pri otevreni
		$('vybrat-vse').addEvent('click',function(e){
			$('vybrat-vse').toggleClass('active');
			
			$('pay_products_list').getElements('div.product').each(VarsModal.data_each = function(item){
				this.data_product = JSON.decode(item.get('data-product'));
				this.data_product.elId = item.get('id');
				//console.log(item);
				if ($('vybrat-vse').hasClass('active')){
					item.addClass('active');
					this.create_new_line_bill(this.data_product.count);
					this.prepocet_ks_ucet_vyber(item.get('id'));
			
				} else {
					item.removeClass('active');
					this.create_new_line_bill(this.data_product.count,true);
					this.prepocet_ks_ucet_vyber(item.get('id'));
			
				}
				
			}.bind(this));
		}.bind(this));
	},
	
	
	// bill back button
	bill_back_button: function(){
		
		if ($('bill_back') && $('bill_back').hasClass('none')){
			$('bill_back').removeClass('none');
			$('pay_fce1').addClass('none');
			$('pay_fce2').removeClass('none');
			
		} else {
			if ($('bill_back'))
			$('bill_back').addClass('none');
			$('pay_fce1').removeClass('none');
			$('pay_fce2').addClass('none');
				
		}
		
		//console.log(VarsModal.order_data[this.table_id]['done_items']);
	},
	
	// hide pay modal
	hideModal:function(options){
		$('basket_pay').addClass('none');
	},
	
	// create line from data
	create_line_to_bill: function(){
		data = VarsModal.local_table_data[window.table_id];
		window.payData = data;
		//console.log(window.table_id);
		//console.log(VarsModal.local_table_data);
		fstvars.clearEl($('pay_products_list'));
		fstvars.clearEl($('bill_products'));
		//console.log(data);
		VarsModal.select_ids = {};
		VarsModal.select_ids.id = [];
		VarsModal.select_ids.data = {};
		
		this.bill_products_data = {};
		
		
		//console.log(data.items);
		if (Object.getLength(data.items)>0){
			
			$('pay_bill').addClass('empty');
			Object.each(data.items,VarsModal.load_each = (function(item,key_done){
				//console.log(item);
				//console.log(key_done);
				/*
				if (item.addon){
					console.log(item);
					console.log(product);
					
				} else {
				*/
				//for (i = 1; i <= item.count; i++){
					if (!item.parse){
						//console.log(JSON.encode(item));
						product = new Element('div',{'type':'button','class':'button product','data-id':item.id,'id':'SelectPay'+item.id,'data-ks':item.count,'data-key_done':key_done,'data-product':JSON.encode(item)}).set('html','<span>'+item.count+'</span>ks '+item.name).inject($('pay_products_list'));
					
						product.addEvent('click',this.click_bill_product.bind(this));
					}
				//}
				//console.log(item.ks);
				if (item.count < 1){
					if (!item.parse){
						
						product = new Element('input',{'type':'button','class':'button product','data-id':item.id,'value':item.name,'id':'SelectPay'+item.id,'data-ks':item.count,'data-key_done':key_done}).inject($('pay_products_list'));
					
						product.addEvent('click',this.click_bill_product.bind(this));
					}
				}
				//}
				
				if (item.addon){
					data.items[key_done_parent].addonList.push(key_done);
					//console.log(data.items[key_done_parent]);
					//product.addClass('none');
					product.addClass('parent_id parent_id_'+key_done_parent);
					key_done_parent = false;
				} else {
					
					data.items[key_done].addonList = [];
					key_done_parent = key_done;
					
				}
				window.payData = data;
				/**/
				
				
			}).bind(this));
			console.log(window.payData);
		} else {
			$('pay_bill').addClass('empty');
		}
		
		
		
	},
	
	
	// prepocet cena v uctu
	bill_recount_total_price: function(){
		var total_price = 0;
		$('bill_products').getElements('li').each(VarsModal.data_each = function(item){
			if (!item.hasClass('none')){
				
				//console.log(item.getElement('.price').get('text').toInt() * item.getElement('.ks').get('text').toInt());
				total_price += item.getElement('.price').get('text').toInt() * item.getElement('.count').get('text').toInt();
				//console.log(total_price);
			}
		});
		$('bill_total_price').set('text',price(total_price));
	},
	
	// vytvoreni noveho radku do uctu na zaplaceni
	create_new_line_bill: function(ks,minus,parent_el){
		$('pay_bill').removeClass('empty');
			
		this.data_product.count = this.data_product.count.toInt();
		ks = ks.toInt();
		data = Object.clone(this.data_product);
		data.count = ks;
		console.log('ks',ks);
		// pokud na uctu jiz existuje
		if (this.bill_products_data[data.line_id]){
			// minus
			if (minus){
				this.bill_products_data[data.line_id].count = this.bill_products_data[data.line_id].count.toInt() - ks;
				this.data_product.count = this.data_product.count + ks;
				
			} else {
				this.bill_products_data[data.line_id].count = this.bill_products_data[data.line_id].count.toInt() + ks;	
				this.data_product.count = this.data_product.count - ks;
			}	
			
		// pokud je novy produkt	
		} else {
			this.bill_products_data[data.line_id] = data;
			this.data_product.count = this.data_product.count - ks;
			
		}
		
		// pokud je mensi nez 0 smazat
		if (this.bill_products_data[data.line_id].count < 1){
			delete(this.bill_products_data[data.line_id]);
		}
		
		// prepocet row_price
		if (this.bill_products_data[data.line_id])
		this.bill_products_data[data.line_id].row_price = this.bill_products_data[data.line_id].count * this.bill_products_data[data.line_id].price;
		
		// pokud je addon
		if (data.addon){
			data.count = 1;
			this.bill_products_data[data.line_id] = data;
		
		}
		if (this.bill_products_data)
		console.log('bill_data',this.bill_products_data);
		console.log('product_data',this.data_product);
		this.vytvoreni_polozek_uctu();
		
		/*
		console.log(this.bill_products_data[data.line_id].count);
		console.log(this.bill_products_data[data.line_id]);
		return false;
		*/
		/*
		
		*/
	},
	
	// vytvoreni polozek uctu
	vytvoreni_polozek_uctu: function(){
		// vymazani polozek na uctu
		fstvars.clearEl($('bill_products'));
		
		// vytvoreni polozek na uctu
		if (Object.getLength(this.bill_products_data)>0){
			$('pay_bill').removeClass('empty');
			
			Object.each(this.bill_products_data,VarsModal.data_each = function(data){
				//console.log(data.count);
				if (data.count > 0){
					// pokud neni addon
					if (!data.addon){
						product_li = new Element('li',{'id':'BillLine_'+data.line_id,'data-id':data.id,'data-elId':data.elId,'data-row_key':data.line_id,'data-price':data.price,'data-ks':data.count,'class':(data.addon?'addon':'')}).set('html','<span class="count">'+data.count+'x</span><span class="name">'+data.name+'</span><span class="price">'+price(data.row_price)+'</span>').inject($('bill_products'));
						product_li.addEvent('click',this.minus_bill_fce.bind(this,Object.clone(data)));
					// pokud je addon
					} else {
						if ($('BillLine_'+parent_el)){
							product_li = new Element('li',{'id':'BillLine_'+data.line_id,'data-id':data.id,'data-row_key':data.line_id,'data-price':data.price,'data-ks':data.count,'class':(data.addon?'addon':'')}).set('html','<span class="count">'+data.count+'x</span><span class="name">'+data.name+'</span><span class="price">'+price(data.row_price)+'</span>').inject($('BillLine_'+parent_el));
							
						}
					}
					
				}
			}.bind(this)); 
			
		// pokud je ucet prazdny
		} else {
			$('pay_bill').addClass('empty');
		}
		
		// prepocet celkem na uctu
		this.bill_recount_total_price();
	},
	
	
	// change ks modal
	change_ks_modal2: function(ks,completeFce,minus){
		$('basket_change_ks').removeClass('none');
		
		$('ChangeKs').value = ks;
		$('ChangeKs').focus();
		$('ChangeKs').select();
		
		
		$('ChangeKsButton').removeEvents('click');
		$('ChangeKsButton').addEvent('click',VarsModal.click_change_ks = (function(e){
			$('ChangeKs').fireEvent('keydown','fire_click');
			
		}).bind(this));
		$('ChangeKs').removeEvents('keydown');
		$('ChangeKs').addEvent('keydown',VarsModal.keydown_fce = function(e){
			/*
			if ($('ChangeKs').value < 1){
				$('basket_change_ks').addClass('none');
				completeFce.call(this, $('ChangeKs').value,minus);
			}
			*/
			if (e.key && e.key == 'enter'){
				e.stop();
				$('ChangeKs').fireEvent('keydown','fire_click');
				return false;
			}
			if (e == 'fire_click'){
				if ($('ChangeKs').value > ks){
					FstError('Neplatný pocet kusu');
					$('ChangeKs').focus();
					return false;
				}
				
				$('basket_change_ks').addClass('none');
				completeFce.call(this, $('ChangeKs').value,minus);
				
			}
			
		}.bind(this));
				
	},
	
	change_pay_products: function(ks,minus){
		ks = ks.toInt();
		this.data_product.count = this.data_product.count.toInt();
		//console.log(ks);
		//console.log(this.data_product.count);
		
		if (minus){
			console.log('minus',ks);
			// odebrani z uctu
			this.create_new_line_bill(ks,true);
			
			// prepocet vyber
			this.prepocet_ks_ucet_vyber(this.data_product.elId);
			console.log('aaa',this.data_product);
			//this.data_product.count += ks;
			$(this.data_product.elId).set('data-product',JSON.encode(this.data_product));
			return false;
		
		} else {
			if (ks > 0){
				console.log('plus');
				//this.data_product.count -= ks;
				this.curEl.set('data-product',JSON.encode(this.data_product));
				this.create_new_line_bill(ks);
				// prepocet vyber
				this.prepocet_ks_ucet_vyber();
			}
		/*
		//console.log(ks);
		//if (!this.curEl.hasClass('active')){
		if (this.data_product.count > 0){
			console.log('plus');
			new_count = this.data_product.count - ks;
				
			this.data_product.count = ks.toInt();
			if (new_count == this.data_product.count){
				this.curEl.addClass('active');
					
			}
				console.log('cnt plus',ks);
				//console.log(data_product);
				//console.log('cnt2',data_product.count);
				//console.log(curEl.get('data-key_done'));
			this.create_new_line_bill(ks,this.data_product);
				
				if (this.curEl.get('data-key_done')){
					
					$('pay_products_list').getElements('.parent_id_'+this.curEl.get('data-key_done')).each(function(addon){
						data_product_addon = JSON.decode(addon.get('data-product'));
						this.create_new_line_bill(1,data_product_addon,false,curEl.get('data-key_done'));
					
						//console.log(addon);
					}.bind(this));
				}
				
				if (new_count == 0){
					this.curEl.addClass('active');
					//curEl.removeEvents('click');
					//curEl.destroy();
				}
				
				
				this.prepocet_ks_ucet_vyber(new_count);
				
			} else {
				/*
				console.log('minus');
				console.log(this.bill_products_data[this.data_product.line_id].count);
				if (this.bill_products_data[this.data_product.line_id].count > 1){
					this.change_ks_modal2(this.bill_products_data[this.data_product.line_id].count,this.change_pay_products,true);
				} else {
					this.curEl.removeClass('active');
					this.change_pay_products(this.data_product.count,true);
					//this.create_new_line_bill(1,this.data_product,true);
					
				}
				*/
				//console.log(this.bill_products_data[data_product.line_id]);
				
			//}
		}
	},
	
	// odebrani z uctu pri zaplaceni
	minus_bill_fce: function(data){
		console.log('data minus',data);
		this.data_product = JSON.decode($(data.elId).get('data-product'));
		line_id = data.line_id;
		$('pay_products_list').getElements('.active').removeClass('active');
		
		if (this.bill_products_data[line_id] && this.bill_products_data[line_id].count > 1){
			this.change_ks_modal2(data.count,this.change_pay_products,true);
		} else {
			this.change_pay_products(data.count,true);
					
		}
	},
	
	// prepocet ks vyber ucet
	prepocet_ks_ucet_vyber: function(elId){
		//console.log('prepocet data',this.data_product);
		if (elId){
			this.curEl = $(elId);
		}
		this.curEl.set('data-product',JSON.encode(this.data_product));
		this.curEl.getElement('span').set('text',this.data_product.count);
		
		if (this.data_product.count == 0){
			this.curEl.addClass('none');
			if ($('pay_products_list').getElement('.button:not(.none)'))
				$('pay_products_list').getElement('.button:not(.none)').addClass('active');
		} else {
			this.curEl.removeClass('none');
			
		}
	},
	
	// click bill product left side
	click_bill_product: function(e){
		if (!e.target) e.target = e;
		this.curEl = e.target;
		console.log(this.curEl.get('data-key_done'));
		//this.data_product = JSON.decode(this.curEl.get('data-product'));
		this.data_product = window.payData.items[this.curEl.get('data-key_done')];
		//console.log('data product',this.data_product);
		this.data_product.elId = this.curEl.get('id');
		if (this.data_product.count > 1){
			//console.log(this.bill_products_data);
			this.change_ks_modal2(this.data_product.count,this.change_pay_products);
		} else {
			this.change_pay_products(this.data_product.count);
		}
		//console.log(data_product);return false;
		//this.change_ks_modal(VarsModal.order_data[this.table_id]['items'][line_id].ks,line_id.toInt(),e.target.getParent('tr'));	
		
		return false;
	},
	
});
/*
window.addEvent('domready',function(){
	fstPay = new FstPay();
});
*/