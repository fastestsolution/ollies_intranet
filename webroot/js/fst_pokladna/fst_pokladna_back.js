/*
Fst Context Menu
created Jakub Tyson Fastest Solution 
copyright 2016
*/
var tax_list = {
	1:15,
	2:21,
};
var platba_list = {
	1:'Hotově',
	2:'Kartou',
};
var FstPokladna = this.FstPokladna = new Class({
	Implements:[Options,Events,FstUzaverka],
	debug : true,
	debug_storage : true,
	tab_index : 0,
	options: {
		user_group_admin: [2],
	},
	connection_table: {},
	history_data: [],
	timeout_save: 5,
	is_online : true,
	
	// init fce
	initialize:function(options){
		this.openSmena = new Hash.Cookie('openSmena', {duration: 36000});
					
		//console.log(window.fstClient);
		//localStorage.clear();
		//localStorage.removeItem("local_orders_data");
		this.setOptions(options);
		if (this.debug) console.log('start fst pokladna');
		if ($('stul_rozdelit')){
			this.fm = new FstPokladnaModal();
			//this.is_online = navigator.onLine;
		
			this.click_events();
			this.close_detail_product();
			this.show_map_table();
		
			this.map_table_events();
			this.current_time();
			this.product_parser();
			this.rozdelit_stul();
			
			//this.keyboard();
			//this.setting_printers();
			this.orders_modal();
			
			this.save_local_orders_data();
			(function(){
				this.save_local_orders_data();
			}).periodical(1000*60,this);
			
			this.clear_data();
			
			this.table_prices();
			this.app_cache();
			this.layout_resize();
			this.open_smena();
		}
		
		this.cur_date = new Date().format('%Y-%m-%d');
		
		if ($('p_ucet_user')){
			this.logged_user = $('p_ucet_user').get('text');
			this.logged_user_id = $('p_ucet_user_id').get('text').toInt();
		}
		
	},
	
	// open smena
	open_smena: function(){
		$('OpenSmenaButton').addEvent('click',function(e){
			e.stop();
			var type = e.target.get('data-type');
			if (type == 'open'){
				this.openSmena.set('open_time', new Date().format('db'));
				//console.log(this.openSmena.get('open_time'));
			}
			
			$('open_smena_over').addClass('none');
			
		}.bind(this));
	},
	
	// layout resize
	layout_resize: function(){
		$('OrderForm').setStyle('height',$('body').getSize().y - 30);
		$('table_list').setStyle('height',$('body').getSize().y - $('table_list').getCoordinates().top -45);
	},
	
	// app cache 
	app_cache: function(){
		var appCache = window.applicationCache;
		//console.log(appCache);
   
		// Check if a new cache is available on page load.
		window.addEventListener('load', function(e) {
		  window.applicationCache.addEventListener('updateready', function(e) {
			
			if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
			  // Browser downloaded a new app cache.
			  if (confirm('A new version of this site is available. Load it?')) {
				window.location.reload();
			  }
			} else {
			  // Manifest didn't changed. Nothing new to server.
			}
		  }, false);

		}, false);
	},
	
	// orders Modal 
	clear_data: function(){
		$('clear_data').addEvent('click',function(e){
			e.stop();
			localStorage.clear();
		}.bind(this));
	},
	
	
	// orders Modal 
	orders_modal: function(){
		$('OrdersModalOpen').addEvent('click',function(e){
			e.stop();
			$('modal_orders').removeClass('none');
			this.load_orders_request();
			
		}.bind(this));
		
		$('TrzbaDate').addEvent('change',function(e){
			this.load_orders_request(e.target.value);
			
		}.bind(this));
		
		if ($('UzaverkasModalOpen')){
			$('UzaverkasModalOpen').addEvent('click',function(e){
				e.stop();
				$('modal_uzaverkas').removeClass('none');
				
				this.uzaverkas_events();
				this.load_uzaverka_list();
			
				
			}.bind(this));
		}
		if ($('CreateUzaverkas')){
			$('CreateUzaverkas').addEvent('click',function(e){
				e.stop();
				if (confirm('Opravdu uzavřít směnu?')){
					e = {};
					e.target = $('UzavritSmenuButton');
					this.gen_uzaverka(e);
				}
				
			}.bind(this));
		}
	},
	
	// save_local_orders_data
	save_local_orders_data: function(){
		VarsModal.local_orders_data = JSON.decode(localStorage.getItem("local_orders_data"));
			
		//console.log(VarsModal.local_orders_data);
		if (this.is_online){
			VarsModal.save_local_data = new Request.JSON({
				timeout: 1000*this.timeout_save,
				url:'/orders/save_local_orders/',
				data: VarsModal.local_orders_data,
				onComplete: VarsModal.compl_fce = (function(json){
					
					if (json && json.r == true){
						//console.log(json.return_ids);		
						Object.each(json.return_ids, VarsModal.data_each = function(item_ids,date){
							item_ids.each(function(item_id){
								Object.each(VarsModal.local_orders_data[date],VarsModal.data_each2 = function(item,k){
									VarsModal.local_orders_data[date][k].id = item_id;
								});							
							});
							
						});
						
						//console.log(VarsModal.local_orders_data);	

						localStorage.setItem(
							"local_orders_data", 
							JSON.stringify(VarsModal.local_orders_data)
						);
		
						
					} else {
					}
					VarsModal.save_local_data.cancel();
					json = null;
				}.bind(this))
			});
			VarsModal.save_local_data.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.save_local_data.post();// load orders request
			
		}
	},
	
	
	load_orders_request: function(date){
		var el_inject = $('modal_orders').getElement('.content_in');
		var el_inject_template = $('modal_orders').getElement('.content_in_template');
		el_inject.addClass('preloader');
		
		if (this.is_online){
			VarsModal.load_table = new Request.JSON({
				url:'/orders/load_orders/'+((date)?date:''),
				onComplete: VarsModal.compl_fce = (function(json){
					el_inject.removeClass('preloader');
					if (json.r == true){
						this.create_orders_lines(json.data);
						
						// trzba date list
						if (json.trzba_date_list && Object.getLength(json.trzba_date_list) >0){
							Object.each(json.trzba_date_list,VarsModal.data_each = function(item,k){
								
								new Element('option',{'text':item,'value':k}).inject($('TrzbaDate'));
								
							});
						}
						$$('.adopt_total').each(VarsModal.data_each = function(item){
							if (item.get('data-type') == 'datum'){
								datum  =  Date.parse(json.total[item.get('data-type')]);
								item.set('text',datum.format('%d.%m. %Y'));
								
							} else {
								item.set('text',price(json.total[item.get('data-type')]));
							}
						});

						
					} else {
					}
					el_inject = null;
				}.bind(this))
			});
			VarsModal.load_table.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.load_table.send();
		} else {
			el_inject.removeClass('preloader');
			//localStorage.removeItem("local_orders_data");
			VarsModal.local_orders_data = JSON.decode(localStorage.getItem("local_orders_data"));
			//console.log(VarsModal.local_orders_data);
			if (Object.getLength(VarsModal.local_orders_data)>0){
				if (!VarsModal.local_orders_data[this.cur_date]){
					Object.each(VarsModal.local_orders_data,VarsModal.data_each = function(item){
						
						data = item;
					});
				} else {
					data = VarsModal.local_orders_data[this.cur_date];
				}
				
				var user_list = JSON.decode($('user_list').get('text'));
				Object.each(data,VarsModal.data_each = function(item,k){
					//console.log(user_list[data[k].print_user_id]);
					data[k]['print_username'] = user_list[data[k].print_user_id];
					data[k].order_items.each(function(pitem,kitem){
						data[k]['order_items'][kitem]['username'] = user_list[pitem.user_id];
					
						//console.log(pitem);
					});
					//console.log(data[k]);
				});
				//console.log(data);
				
				this.create_orders_lines(data.reverse());	
			}
			console.log(VarsModal.local_orders_data[this.cur_date]);
		}
	},
	
	// create orders lines
	create_orders_lines: function(data){
		var el_inject = $('modal_orders').getElement('.content_in');
		var el_inject_template = $('modal_orders').getElement('.content_in_template');
		
			
		fstvars.clearEl(el_inject);
		el_inject_template.getElement('table').clone().set('id','orders_list').inject(el_inject);
		
		Tempo.prepare(el_inject).when(TempoEvent.Types.RENDER_COMPLETE, function (event) {
			// show detail products
			el_inject.getElements('.show_items').addEvent('click',VarsModal.click_fce = function(e){
				e.stop();
				e.target.getParent('tr').getElement('table').toggleClass('none');
				tr = e.target.getParent('tr');
				tr.setStyle('height',tr.getSize().y);
				tr.getElement('table').setStyle('position','absolute');
				
				if (e.target.hasClass('open')){
					e.target.value = 'Položky';
					e.target.removeClass('open');
					tr.setStyle('height','');
					tr.getElement('table').setStyle('position','');
				
				} else {
					e.target.addClass('open'); 
					e.target.value = 'Skrýt';
				}
				
			}.bind(this));
			// storno orders
			el_inject.getElements('.storno_items').addEvent('click',VarsModal.click_fce = function(e){
				e.stop();
				FstAlert('Stornovano');
			}.bind(this));
			
			
			if (data == ''){
				el_inject.getElements('.orders_empty').removeProperty('style');
			}
			
		}).render(data);
		
		
	},
	
	
	// current time on top
	rozdelit_stul: function(){
		$('stul_rozdelit').getElements('.select_all').addEvent('click',VarsModal.click_all = function(e){
			e.stop();
			
			this.check_select_table();
			
			e.target.getPrevious('.itms').getElements('.product').each(VarsModal.imts_each = function(item){
				result = this.click_stul_product(item);
				if (result == false) return false;
			}.bind(this));
		}.bind(this));
			
		if ($('RozdelitStul')){
			
			$('RozdelitStul').addEvent('click',VarsModal.click_rozdelit = function(e){
				e.stop();
				$('stul_rozdelit').toggleClass('none');
				clone_els_or = $('table_list').getElements('.tg');
				
				
				if (!$('table_left').getElement('.tg')){
					Object.each(clone_els_or,VarsModal.cl_each = function(item){
						if (typeof item == 'object'){
							//item.removeClass('none');
							item.clone().inject($('table_left'));
							item.clone().inject($('table_right'));
							$('table_left').getElements('.tg').removeClass('none');
							$('table_right').getElements('.tg').removeClass('none');
						}
					});
				}
				
				$('stul_rozdelit').getElements('.tg').addEvent('click',VarsModal.click_table_sel = function(e){
					e.stop();
					
					var parent = e.target.getParent('.table_select');
					parent.getElements('.tg').removeClass('active');
					e.target.addClass('active');
					//console.log(VarsModal.order_data);
					table_id = e.target.get('data-id');
					
					var itms = $(parent.get('data-type')).getElement('.itms');
					
					fstvars.clearEl(itms);
					new Element('input',{'value':table_id,'class':'table_id','type':'hidden'}).inject(itms);
						
					if (VarsModal.order_data[e.target.get('data-id')]){		
						if (VarsModal.order_data[e.target.get('data-id')]['done_items']){
							
							Object.each(VarsModal.order_data[e.target.get('data-id')]['done_items'],VarsModal.data_each = function(item){
								product = new Element('input',{'type':'button','class':'button product','data-id':item.id,'value':item.name,'id':'SelectStulProduct'+item.id,'data-ks':item.ks}).inject($(parent.get('data-type')).getElement('.itms'));
										
								product.addEvent('click',this.click_stul_product.bind(this));
							}.bind(this));
						}
					}
					
					/*
					VarsModal.load_table = new Request.JSON({
						url:'/orders/load_table/'+e.target.get('data-id'),	
						
						onComplete: VarsModal.load_table_complete = (function(json){
							var itms = $(parent.get('data-type')).getElement('.itms');
							
							fstvars.clearEl(itms);
							
							new Element('input',{'value':json.table_id,'class':'table_id','type':'hidden'}).inject(itms);
									
							if (json.r == true){
								
								//new Element('input',{'value':json.data.id,'class':'order_id','type':'hidden'}).inject($(parent.get('data-type')).getElement('.itms'));
								
								
								Object.each(json.data,VarsModal.data_each = function(item){
									product = new Element('input',{'type':'button','class':'button product','data-id':item.id,'value':item.name,'id':'SelectStulProduct'+item.id,'data-ks':item.ks}).inject($(parent.get('data-type')).getElement('.itms'));
									
									product.addEvent('click',this.click_stul_product.bind(this));
								}.bind(this));
							} else {
							}
						}.bind(this))
					});
					VarsModal.load_table.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
					VarsModal.load_table.send();
					*/
				}.bind(this));
				
				
				
				/*
				VarsModal.load_table = new Request.JSON({
					url:'/orders/load_table/'+$('TableId').value,	
					
					onComplete: VarsModal.load_table_complete = (function(json){
						if (json.r == true){
							Object.each(json.data.order_items,VarsModal.stul_products_items_each = function(item){
								product = new Element('input',{'type':'button','class':'button product','data-id':item.id,'value':item.name,'id':'SelectStulProduct'+item.id,'data-ks':item.ks}).inject($('stul_products_list'));
								
								product.addEvent('click',this.click_stul_product.bind(this));
								//new Element('li',{'class':'product','data-id':item.id}).set('text',item.name).inject($('stul_products_list'));
							}.bind(this));
						}
					}.bind(this))
				});
				VarsModal.load_table.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				VarsModal.load_table.send();
				*/
			}.bind(this));
			$('stul_back').addEvent('click',VarsModal.click_rozdelit_back = function(e){
				e.stop();
				$('stul_rozdelit').toggleClass('none');
			});
		}
	},
	
	// click stul product
	click_stul_product: function(e){
		if (!e.target) e.target = e;
		if (this.check_select_table()){
			
			var next_table = $(e.target.getParent('.stul_col').get('data-adopt'));
			var curr_table = $(e.target.getParent('.stul_col'));
			//console.log(e.target.getParent('.stul_col').get('data-adopt'));
			//console.log(next_table.getElement('.imts'));
			next_table.getElement('.itms').adopt(e.target);
			curr_id = curr_table.getElement('.table_id').value;
			next_id = next_table.getElement('.table_id').value;
			
			order_id = e.target.get('data-id');
			/*
			console.log(order_id);
			console.log(curr_id);
			console.log(next_id);
			*/
			if (!VarsModal.order_data[next_id]) VarsModal.order_data[next_id] = {
				'created':new Date().format('db'),
				'done_items':[],
				'items':[],
				
			}
			Object.each(VarsModal.order_data[curr_id]['done_items'],VarsModal.data_each = function(item,key){
				if (item.id = order_id){
					order = item;
					order_key = key;
				}
			});
			//console.log(VarsModal.order_data[curr_id]['done_items'][order_key]);
			VarsModal.order_data[next_id]['done_items'].push(order);
			VarsModal.order_data[curr_id]['done_items'].erase(order);
			//console.log(VarsModal.order_data);
			
			this.saveStorage();
			this.table_prices();
			
			/*
			var link = '/orders/change_table/'+e.target.get('data-id')+'/'+((curr_table.getElement('.order_id'))?curr_table.getElement('.order_id').value:'0')+'/'+((next_table.getElement('.order_id'))?next_table.getElement('.order_id').value:'0') +'/'+ ((curr_table.getElement('.table_id'))?curr_table.getElement('.table_id').value:'0')+'/'+ ((next_table.getElement('.table_id'))?next_table.getElement('.table_id').value:'0');
			console.log(link);
			
			VarsModal.change_table = new Request.JSON({
				url:link,	
					
				onComplete: VarsModal.change_table_compl = (function(json){
					if (json.result == true){
						
					} else {
						FstError('Chyba uložení');
					}
				}.bind(this))
			});
			VarsModal.change_table.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.change_table.send();
			*/
		}
	},
	
	// overeni vybrano 2 stoly
	check_select_table: function(){
		if (!$('table_right').getElement('.active')) {
			FstError('Musíte zvolit druhý stůl');
			return false;
		}
		if (!$('table_left').getElement('.active')) {
			FstError('Musíte zvolit první stůl');
			return false;
		}
		return true;
	},
	
	// current time on top
	current_time: function(){
		if ($('current_time')){
		VarsModal.run_clock = function(){
			var today = new Date();
			$('current_time').set('text',today.format('%d.%m. %H:%M'));
		}
		VarsModal.run_clock();
		VarsModal.run_clock.periodical(1000*60);
		}
	},
	
	
	
	
	// init event to element right click
	init_events: function(){
	},
	
	
	
	// click events
	show_map_table: function(){
		if ($('show_map_table'))
		$('show_map_table').addEvent('click',function(e){
			e.stop();
			$('map_table').toggleClass('none');
		});
	},
	
	// click events
	click_events: function(){
		
	
		$$('.open_modal_pokl').addEvent('click',(function(e){
			e.stop();
			this.fm.open_modal(e.target);
		}).bind(this));
	
	},
	
	// create product parse line
	product_parser: function(){
		if ($('product_parser')){
			$('product_parser').getElements('li').addEvent('click',VarsModal.click_parser = function(e){
				this.create_line_basket({
					'parse':true,
					'price':0,
					'id':e.target.get('data-id'),
					'name':e.target.get('text'),
				});
			}.bind(this));
		}
	},
	
	
	// init events after modal load
	init_after_modal_load: function(){
		this.switch_product_groups();
		this.product_to_basket();
		this.load_edit_order();
	},
	
	// product to basket events
	product_to_basket: function(){
		$('product_list').getElements('.back_product').addEvent('click',this.product_back_click.bind(this));
		//$('product_list').getElements('.without_addon').addEvent('click',this.without_addon_click.bind(this));
		$('product_list').getElements('.product').addEvent('click',this.product_to_basket_click.bind(this));
		
	},
	
	// zpet pri vyberu addons 
	product_back_click: function(e){
		if (!e.target)  e.target = e;
		this.add_addon = false;
		e.target.getParent('.product_addons').addClass('none');
	},
	
	// bez addon 
	without_addon_click: function(e){
		if (!e.target)  e.target = e;
		this.add_addon = false;
		return false
		//e.target.getParent('.product_addons').addClass('none');
	},
	
	// product to basket event click
	product_to_basket_click: function(e){
		if (e.stop) e.stop();
		if (e.target.hasClass('back_product')){
			return false;
		}
		if (e.target.hasClass('without_addon')){
			e.target = e.target.getParent('.product');
			//console.log(e.target);
			without_addon = true;
			e.target.getElement('.product_addons').addClass('none');
			//return false;
		} else {
			without_addon = false;
		}
		
		if (!$('type_rozvoz').hasClass('active') && $('TableId').value == ''){
			FstError('Musíte zvolit stůl');
			return false;
		}
		
		if (e.target.getElement('.product_addons') && !without_addon){
			e.target.getElement('.product_addons').removeClass('none');
			return false;
		}
		
		if (e.target.hasClass('paddon')){
			
			// pokud je produkt
			if (!this.add_addon){
			this.create_line_basket({
				//'id':(typeof this.connection_table != 'undefined' && this.connection_table[e.target.get('data-id')]?this.connection_table[e.target.get('data-id')]:e.target.get('data-id')),
				'id':e.target.getParent('.product').get('data-id'),
				//'load':(this.connection_table[e.target.get('data-id')]?1:0),
				'product_id':e.target.getParent('.product').get('data-id'),
				'product_group_id':e.target.getParent('.product').get('data-product_group_id'),
				'num':e.target.getParent('.product').get('data-num'),
				'code':e.target.getParent('.product').get('data-code'),
				'num':e.target.getParent('.product').get('data-num'),
				'name':e.target.getParent('.product').get('data-name'),
				'addon':0,
				'price':e.target.getParent('.product').get('data-price').toInt(),
				'price_tax_id':e.target.getParent('.product').get('data-price_tax_id').toInt(),
				'product_group_id':e.target.getParent('.product').get('data-product_group_id').toInt(),
				'user_id':this.logged_user_id,
			});
			}
			
			// pokud je addon
			this.create_line_basket({
				//'id':(typeof this.connection_table != 'undefined' && this.connection_table[e.target.get('data-id')]?this.connection_table[e.target.get('data-id')]:e.target.get('data-id')),
				'id':e.target.get('data-id'),
				'parent_id':e.target.get('data-parent_id'),
				//'load':(this.connection_table[e.target.get('data-id')]?1:0),
				'product_id':e.target.get('data-id'),
				//'product_group_id':e.target.get('data-product_group_id'),
				//'num':e.target.get('data-num'),
				//'code':e.target.get('data-code'),
				'name':e.target.get('data-name'),
				'addon':1,
				'price':e.target.get('data-price').toInt(),
				'user_id':this.logged_user_id,
				//'price_tax_id':e.target.get('data-price_tax_id').toInt(),
				//'product_group_id':e.target.get('data-product_group_id').toInt(),
			});
			
			//e.target.getPrevious('.back_product').fireEvent('click',e.target.getPrevious('.back_product'));
			
		} else {
			// neni addon
			this.create_line_basket({
				//'id':(typeof this.connection_table != 'undefined' && this.connection_table[e.target.get('data-id')]?this.connection_table[e.target.get('data-id')]:e.target.get('data-id')),
				'id':e.target.get('data-id'),
				//'load':(this.connection_table[e.target.get('data-id')]?1:0),
				'product_id':e.target.get('data-id'),
				'product_group_id':e.target.get('data-product_group_id'),
				'num':e.target.get('data-num'),
				'code':e.target.get('data-code'),
				'name':e.target.get('data-name'),
				'addon':0,
				'price':e.target.get('data-price').toInt(),
				'price_tax_id':e.target.get('data-price_tax_id').toInt(),
				'product_group_id':e.target.get('data-product_group_id').toInt(),
				'user_id':this.logged_user_id,
			});
		
		}
		
		$('CheckUkoncit').value = 1;
	},
	
	// load after craete line
	after_create_line: function(){
		if (this.debug) console.log('after');
		this.count_products_basket();
	},
	
	
	// save value to object data
	save_value: function(line_id,value){
		if (typeof VarsModal.order_data[this.table_id]['items'][line_id] != 'undefined')
		VarsModal.order_data[this.table_id]['items'][line_id].note = value;
				
		if (typeof VarsModal.order_data[this.table_id]['done_items'][line_id] != 'undefined')
		VarsModal.order_data[this.table_id]['done_items'][line_id].note = value;
				
	},
	
	// load value from object data
	load_value: function(line_id,col){
		val = '';
		if (typeof VarsModal.order_data[this.table_id]['items'][line_id] != 'undefined' && typeof VarsModal.order_data[this.table_id]['items'][line_id][col] != 'undefined' && VarsModal.order_data[this.table_id]['items'][line_id][col] != ''){
			val = VarsModal.order_data[this.table_id]['items'][line_id][col];
		}
		if (typeof VarsModal.order_data[this.table_id]['done_items'][line_id] != 'undefined' && typeof VarsModal.order_data[this.table_id]['done_items'][line_id][col] != 'undefined' && VarsModal.order_data[this.table_id]['done_items'][line_id][col] != ''){
			val = VarsModal.order_data[this.table_id]['done_items'][line_id][col];
		}
		return val;
	},
	
	// basket line events
	basket_lines_events: function(opt){
		
		// delete line event
		VarsModal.button_delete.addEvent('click',VarsModal.click_fce = function(e){
			if (e.stop){
				e.stop();	
			}
			if (!e.target){
				e.target = e;
			}
			this.check_user_admin(opt);
			var line_id = e.target.get('data-line_id');
			var load_delete = e.target.get('data-load');
			//if (ret == false) return false;
			//console.log(line_id);
			
			// history
			this.history_data.push({
				'created':new Date().format('db'),
				'type':2,
				'value':1,
				'order_item_id':e.target.get('data-line_id').replace('d',''),
				'product_id':e.target.getParent('tr').get('data-product_id'),
				
			});
			
			// delete addon
			if ($('basket_products').getElement('.parent_id_'+line_id)){
				$('basket_products').getElement('.parent_id_'+line_id).getElement('.delete_button').click();
			}
			//return false;
			
			if ($('KsLine'+line_id) && $('KsLine'+line_id).value > 1){
			
				$('KsLine'+line_id).value = $('KsLine'+line_id).value - 1;
				$('KsLine'+line_id).fireEvent('change',$('KsLine'+line_id));
			
			} else {
				if (!load_delete){
					if (VarsModal.order_data[this.table_id]['items'][line_id])
						delete VarsModal.order_data[this.table_id]['items'][line_id];
					if (VarsModal.order_data[this.table_id]['done_items'][line_id])
						delete VarsModal.order_data[this.table_id]['done_items'][line_id];
				
				} else {
					if (VarsModal.order_data[this.table_id]['items'][line_id])
						delete VarsModal.order_data[this.table_id]['items'][line_id];
					if (VarsModal.order_data[this.table_id]['done_items'][line_id])
						delete VarsModal.order_data[this.table_id]['done_items'][line_id];
				}
				this.filter_done_items();
				fstvars.destroyEl(e.target.getParent('tr'));
				//console.log(VarsModal.order_data[this.table_id]);
				this.count_products_basket_price();
			}
			
			
			line_id = null;
		}.bind(this));
		
		
		// open detail product
		
		VarsModal.new_td2.addEvent('click',VarsModal.click_fce = (function(e){
			e.stop();
			if (!e.target.hasClass('detail')){
				return false;
			}
			$('basket_product_detail').removeClass('none');
			
			var line_id = e.target.get('data-order_item_id');
			$('SaveNote').set('data-line_id',line_id);
			
			
			$('OrderItemNote').value = this.load_value(line_id,'note');
			$('OrderItemNote').focus();
			
			
			// click save note to object
			$('SaveNote').removeEvents('click');
			$('SaveNote').addEvent('click',VarsModal.click_fce = function(e){
				var line_id = e.target.get('data-line_id');
				
				/*
				if (typeof VarsModal.order_data[this.table_id]['items'][line_id] != 'undefined')
				VarsModal.order_data[this.table_id]['items'][line_id].note = $('OrderItemNote').value;
				
				if (typeof VarsModal.order_data[this.table_id]['done_items'][line_id] != 'undefined')
				VarsModal.order_data[this.table_id]['done_items'][line_id].note = $('OrderItemNote').value;
				*/
				this.save_value(line_id,$('OrderItemNote').value);
				
				$('basket_product_detail_close').click();
				line_id = null;
			}.bind(this));
			
			line_id = null;
			
			
			/*
			//console.log(e.target.get('data-id'));
			VarsModal.detail_product = new Request.JSON({
				url:'/orders/productDetail/'+line_id+'/'+e.target.get('data-original_id'),	
				onComplete: VarsModal.compl_fce = (function(json){
					VarsModal.detail_product.cancel();
						
					if (json && json.r == true){
						//console.log(json);
						$('basket_product_detail').removeClass('none');
						
						fstvars.clearEl($('basket_product_detail_txt'));
						
						
						html = Elements.from(json.html);
						html.inject($('basket_product_detail_txt'));
						
						this.detail_product_function();
					}
					json = null;
				}).bind(this)
			});
			VarsModal.detail_product.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.detail_product.send();
			*/
			line_id = null;
		}).bind(this));
		
		// change ks event click
		VarsModal.ks_input.addEvent('click',VarsModal.click_fce = (function(e){
			this.check_user_admin(opt);
			var line_id = e.target.get('data-order_item_id');
			var line_count = e.target.get('data-line_count');
			//console.log(line_id);
			if (typeof VarsModal.order_data[this.table_id]['items'][line_id] != 'undefined')
			this.change_ks_modal(VarsModal.order_data[this.table_id]['items'][line_id].ks,line_id.toInt(),e.target.getParent('tr'));
			
			if (typeof VarsModal.order_data[this.table_id]['done_items'][line_count] != 'undefined')
			this.change_ks_modal(VarsModal.order_data[this.table_id]['done_items'][line_count].ks,line_count,e.target.getParent('tr'));
		
			line_id = null;
		}).bind(this));
		
		// change ks event change
		VarsModal.ks_input.addEvent('change',VarsModal.ks_input_fce = (function(e){
			if (!e.target) e.target = e;
			this.check_user_admin(opt);
			var line_id = e.target.get('data-order_item_id');
			
			// delete if 0 ks
			if (e.target.value < 1){
				delete VarsModal.order_data[this.table_id]['items'][line_id.toInt()];
				
				fstvars.destroyEl(e.target.getParent('tr'));
				
				this.count_products_basket_price();
				
				return true;
			}
			//console.log(line_id);
			//console.log(VarsModal.order_data[this.table_id]['done_items']);
			if (typeof VarsModal.order_data[this.table_id]['items'][line_id] != 'undefined')
				price_value = VarsModal.order_data[this.table_id]['items'][line_id].price*e.target.value;
		
			if (typeof VarsModal.order_data[this.table_id]['done_items'][line_id] != 'undefined'){
				price_value = VarsModal.order_data[this.table_id]['done_items'][line_id].price*e.target.value;
			}
					
			
			e.target.getParent('tr').getElement('.price_line_text').set('text',price(price_value));
		
			if (typeof VarsModal.order_data[this.table_id]['items'][line_id] != 'undefined')
			VarsModal.order_data[this.table_id]['items'][line_id].ks = e.target.value;
			
			if (typeof VarsModal.order_data[this.table_id]['done_items'][line_id] != 'undefined')
			VarsModal.order_data[this.table_id]['done_items'][line_id].ks = e.target.value;
			
			this.count_products_basket_price();
			
			
			line_id = null;
			
		}).bind(this));
		
		
	},
	
	
	// new line empty
	c_new_line_basket: function(opt){
		//console.log(opt);
		
		if (!opt.ks) opt.ks = 1;
		//console.log(opt);
		if (opt.parse){
			var tr_last = $('basket_body').getLast('tr');
			//console.log(tr_last);
			if (tr_last && tr_last.hasClass('parser')){
				console.log(tr_last);
				tr_last.getElement('.delete_button').click();
			}
		}
		
		VarsModal.new_tr = new Element('tr',{'id':'line_id_'+opt.line_id,'data-order_item_id':'line'+opt.id,'data-parent_id':opt.parent_id}).inject($('basket_body'));
		
		// pokud je produkt doplnek oznac radek
		if (opt.addon == 1){
			VarsModal.new_tr.addClass('tr_addon');
			VarsModal.new_tr.addClass('parent_id_line'+opt.parent_id);
			
			
		}
		
		if (opt.price == null){
			opt.price = 0;
		}
		
		if (!opt.parse && (opt.product_id)>=0){
			
			// create line product 
			VarsModal.new_td2 = new Element('td',{'class':'col_name detail','data-order_item_id':((opt.line_count)?opt.line_count:opt.line_id),'data-id':opt.line_id}).inject(VarsModal.new_tr).set('text',opt.name);
			VarsModal.new_td5 = new Element('td',{'class':'col_ks'}).inject(VarsModal.new_tr);
			VarsModal.new_td3 = new Element('td',{'class':'col_price price_line_text'}).set('text',price(opt.price.toInt()*opt.ks.toInt())).inject(VarsModal.new_tr);
			VarsModal.new_td4 = new Element('td',{'class':'col_pos'}).inject(VarsModal.new_tr);
			VarsModal.ks_input = new Element('input',{'class':'ks_input','value':opt.ks,'data-order_item_id':((opt.line_count)?opt.line_count:opt.line_id),'id':'KsLine'+((opt.line_count)?opt.line_count:opt.line_id),'data-line_count':((opt.line_count)?opt.line_count:'')}).inject(VarsModal.new_td5);
		
		} else {
			// create line parser between products
			VarsModal.new_tr.addClass('parser');
			VarsModal.new_td5 = new Element('td',{'class':'col_parser','colspan':3}).inject(VarsModal.new_tr);
			VarsModal.new_td4 = new Element('td',{'class':'col_pos'}).inject(VarsModal.new_tr);
			VarsModal.new_td5.set('text',opt.name);
		}
		
		VarsModal.button_delete = new Element('input',{'class':'button small delete_button','value':'X','data-line_id':((opt.line_count)?opt.line_count:opt.line_id),'data-load':opt.load}).inject(VarsModal.new_td4);
		//VarsModal.input_data = new Element('input',{'name':'products[]','type':'text','class':'data_line hide_price ','id':'data_line_'+opt.id,'value':JSON.encode(opt)}).inject(VarsModal.new_td4);
		
		//console.log(opt);
		// save order data to object
		if (!this.no_storage){
			VarsModal.order_data[this.table_id]['items'][opt.line_id] = opt;
			VarsModal.order_data[this.table_id]['items'][opt.line_id].price_default = opt.price.toInt();
			
		}
		if (!opt.load){
			this.count_products_basket_price();
		}
		
	},
	
	// new line in basket exist line
	c_exist_line_basket: function(opt){
		if (opt.parse){
			return false;
		}
		//console.log(opt);
		var ks_input = $('line_id_'+opt.line_id).getElement('.ks_input');
		
		ks_input.value = ks_input.value.toInt() + 1;
		ks_input.fireEvent('change',ks_input);
		
		// save order data to object
		if (!this.no_storage){
			VarsModal.order_data[this.table_id]['items'][opt.line_id].ks = VarsModal.order_data[this.table_id]['items'][opt.line_id].ks.toInt();
		}
		ks_input = null;
	},
	
	// create new line in basket
	create_line_basket: function(opt){
		
		if (typeof VarsModal.button_delete == 'undefined') VarsModal.button_delete = {};
		if (typeof VarsModal.button_delete_fce == 'undefined') VarsModal.button_delete_fce = [];
		if (typeof VarsModal.order_data == 'undefined') VarsModal.order_data = {};
		if (typeof VarsModal.order_data[this.table_id] == 'undefined') VarsModal.order_data[this.table_id] = {}
		if (typeof VarsModal.order_data[this.table_id]['items'] == 'undefined') VarsModal.order_data[this.table_id]['items'] = {}
		if (typeof VarsModal.order_data[this.table_id]['created'] == 'undefined') VarsModal.order_data[this.table_id]['created'] = new Date().format('db');
		if (typeof VarsModal.order_data[this.table_id]['done_items'] == 'undefined') VarsModal.order_data[this.table_id]['done_items'] = [];
		
		//console.log(opt);
		
		// pokud je jiz ulozeno zmen ID v poli
		if (opt.done){
			opt.line_id = 'd'+opt.id+'_'+opt.line_count;
			this.no_storage = true;
		} else {
			opt.line_id = 'line'+opt.id;
			this.no_storage = false;
		}
		
		if (opt.addon == 1){
			opt.line_id = opt.line_id+'_'+opt.parent_id;
		}
		
		//console.log('table id',this.table_id);
		//console.log('line_id '+opt.line_id);
		
		//console.log('aaa',VarsModal.order_data[this.table_id]['items']['line'+opt.line_id]);
		// check exist line or new line
		if (!VarsModal.order_data[this.table_id]['items'][opt.line_id] || opt.load == 1){
			this.c_new_line_basket(opt);
		
			// new line events
			this.basket_lines_events(opt,opt.line_id);
		
		} else {
			
			this.c_exist_line_basket(opt);
		}
		
		// recount product price
		if (!opt.nocount){
			//this.count_products_basket_price();
		}
		
		
		if (opt.load != 1){
		//console.log(opt);
		this.history_data.push({
			'created':new Date().format('db'),
			'type':1,
			'value':1,
			//'product_id':(typeof this.connection_table != 'undefined' && this.connection_table[opt.id]?this.connection_table[opt.id]:opt.id),
			'product_id':opt.product_id,
			'order_item_id':opt.id.replace('d',''),
			'order_id':$('OrderId').value,
		
		});
		}
		
			
		
	},
	
	// load local storage to VarsModal.order_data
	loadStorage: function(){
		
		// load order data
		var local_order_data = localStorage.getItem("order_data");
		//console.log(local_order_data);
		if (local_order_data) {
			local_order_data_json = null;	
			var local_order_data_json = JSON.parse(local_order_data);
		
		}
		
		// load orders all data
		var local_orders_data = localStorage.getItem("local_orders_data");
		//console.log(local_order_data);
		if (local_orders_data) {
			local_orders_data_json = null;	
			var local_orders_data_json = JSON.parse(local_orders_data);
		
		}
		
		// load printer setting
		var printer_setting = localStorage.getItem("printers_setting");
		if (printer_setting) {
			printer_setting_json = null;	
			var printer_setting_json = JSON.parse(printer_setting);
		
		}
		
		
		VarsModal.order_data = local_order_data_json;
		VarsModal.orders_data = local_orders_data_json;
		VarsModal.printer_setting = printer_setting_json;
		
		local_order_data = null;
		local_orders_data = null;
		local_order_data_json = null;
		printer_setting = null;
		
		if (this.debug_storage){
			console.log('load storage data',VarsModal.order_data);
			console.log('load storage orders all data',VarsModal.orders_data);
		}
	},
	
	// save data to local storage
	saveStorage: function(){
		//localStorage.clear();
		//VarsModal.order_data = {}
		/*
		localStorage.setItem(
		  "nazev-polozky", 
		  JSON.stringify(objekt)
		);
		*/
		
		//console.log(VarsModal.order_data);
		localStorage.setItem(
		  "order_data", 
		  JSON.stringify(VarsModal.order_data)
		);
		if (this.debug_storage){
			console.log('save storage data');
			//console.log(VarsModal.order_data);
		}
		
		//console.log(localStorage);
	},
	
	save_history: function(){
		console.log(this.history_data);
		$('HistoryValue').value = JSON.encode(this.history_data);
		this.history_data = [];
	},
	
	// check user admin
	check_user_admin: function(opt){
		if (opt.load && opt.load == 1){
			if (this.options.user_group_admin.contains($('user-group-id').value.toInt())){
				
				result =  false;
			} else {
				
				result = true;
			}
		} else {
			result = true;
		}
		if (result == false){
			FstError('Musíte být přihlášen jako administrátor');
		}
		return result;
	},
	
	// change ks modal
	change_ks_modal: function(ks,line_id,tr){
		$('basket_change_ks').removeClass('none');
		
		$('ChangeKs').value = ks;
		$('ChangeKs').focus();
		$('ChangeKs').select();
		
		
		$('ChangeKsButton').removeEvents('click');
		$('ChangeKsButton').addEvent('click',VarsModal.click_change_ks = (function(e){
			$('ChangeKs').fireEvent('keydown','fire_click');
			
		}).bind(this));
		
		$('ChangeKs').removeEvents('keydown');
		$('ChangeKs').addEvent('keydown',VarsModal.keydown_fce = function(e){
			tr.getElement('.ks_input').value = $('ChangeKs').value;
			
			if ($('ChangeKs').value < 1){
				$('basket_change_ks').addClass('none');
			}
		
			if (e.key && e.key == 'enter'){
				tr.getElement('.ks_input').fireEvent('change',tr.getElement('.ks_input'));
				e.stop();
				$('basket_change_ks').addClass('none');
			
			}
			if (e == 'fire_click'){
				tr.getElement('.ks_input').fireEvent('change',tr.getElement('.ks_input'));
				$('basket_change_ks').addClass('none');
				
			}
		});
				
	},
	
	// set input data line
	set_data_line: function(){
		//console.log(VarsModal.order_data);
		Object.each(VarsModal.order_data,VarsModal.order_data_each = function(item,k){
			if ($('data_line_'+k)){
				//console.log(item);
				$('data_line_'+k).value = JSON.encode(item);
			}
		});
	},
	
	// load products order data from DB
	load_edit_order: function(){
		if ($('ProductsLoad').value != ''){
			this.create_line_from_data(JSON.decode($('ProductsLoad').value));
		}
	},
	
	// create line from data
	create_line_to_bill: function(data){
		fstvars.clearEl($('pay_products_list'));
		fstvars.clearEl($('bill_products'));
		//console.log(data);
		VarsModal.select_ids = {};
		VarsModal.select_ids.id = [];
		VarsModal.select_ids.data = {};
		//console.log(data);
		if (Object.getLength(data['done_items'])>0){
			Object.each(data['done_items'],VarsModal.load_each = (function(item,key_done){
				//console.log(item);
				for (i = 1; i <= item.ks; i++){
					//product = new Element('input',{'type':'button','class':'button product','data-id':item.id,'value':'('+item.ks+') '+item.name,'id':'SelectPay'+item.id,'data-ks':item.ks}).inject($('pay_products_list'));
					if (!item.parse){
						
						product = new Element('input',{'type':'button','class':'button product','data-id':item.id,'value':item.name,'id':'SelectPay'+item.id,'data-ks':item.ks,'data-key_done':key_done}).inject($('pay_products_list'));
					
						product.addEvent('click',this.click_bill_product.bind(this));
					}
				}
				//console.log(item.ks);
				if (item.ks < 1){
					if (!item.parse){
						
						product = new Element('input',{'type':'button','class':'button product','data-id':item.id,'value':item.name,'id':'SelectPay'+item.id,'data-ks':item.ks,'data-key_done':key_done}).inject($('pay_products_list'));
					
						product.addEvent('click',this.click_bill_product.bind(this));
					}
				}
				
				if (item.addon){
					product.addClass('none');
					product.addClass('parent_id parent_id_'+key_done_parent);
					key_done_parent = false;
				} else {
					key_done_parent = key_done;
					
				}
				if (!item.parse){
					//for (i = 1; i <= item.ks; i++){
						
						this.create_new_line_bill({
							'id':item.id,
							'price':item.price,
							'ks':item.ks,
							'name':item.name,
							'key_done':key_done,
						});
						
						//product_li = new Element('li',{'data-id':item.id,'data-price':item.price,'data-ks':1,'class':(item.addon?'addon':'')}).set('html','<span class="ks">1x</span><span class="name">'+item.name+'</span><span class="price">'+price(item.price)+'</span>').inject($('bill_products'));
					//}
				}
				
			}).bind(this));
		}
		
		this.bill_recount_total_price();
		
		$('vybrat-vse').click();
		
		
		
		//console.log(data);
	},
	
	
	// prepocet cena v uctu
	bill_recount_total_price: function(){
		var total_price = 0;
		$('bill_products').getElements('li').each(VarsModal.data_each = function(item){
			if (!item.hasClass('none')){
				//console.log(item.getElement('.price').get('text').toInt() * item.getElement('.ks').get('text').toInt());
				total_price += item.getElement('.price').get('text').toInt() * item.getElement('.ks').get('text').toInt();
				//console.log(total_price);
			}
		});
		$('bill_total_price').set('text',price(total_price));
	},
	
	// vytvoreni nove rady do uctu
	create_new_line_bill: function(data){
		//console.log(data);
		product_li = new Element('li',{'id':'BillLine_'+data.key_done,'data-id':data.id,'data-row_key':data.key_done,'data-price':data.price,'data-ks':data.ks,'class':(data.addon?'addon':'')}).set('html','<span class="ks">'+data.ks+'x</span><span class="name">'+data.name+'</span><span class="price">'+price(data.price)+'</span>').inject($('bill_products'));
		//console.log(product_li);
	},
	
	
	// click bill product left side
	click_bill_product: function(e){
		if (!e.target) {
			e.target = e;
		} else {
			
			this.bill_load = false;
		}
		
		if (e.target.get){
			if (!e.target.hasClass('parent_id'))
			var row_done = e.target.get('data-key_done');
			
			//console.log(VarsModal.order_data[this.table_id]['done_items']);
			//console.log($('pay_products').getElements('.parent_id_'+row_done));
			parent_ids = {};
			Object.each($('pay_products').getElements('.parent_id_'+row_done),function(item){
				if (item.nodeName == 'INPUT'){
					if (!parent_ids[row_done]) parent_ids[row_done] = [];
					parent_ids[row_done].push(item.get('data-key_done').toInt());
				}
			});
			//console.log(parent_ids);
			if (!e.target.hasClass('active')){
				
				e.target.addClass('active');
		
				if (VarsModal.select_ids.data[row_done]){
					VarsModal.select_ids.data[row_done] += 1;
					
					if (parent_ids[row_done] && parent_ids[row_done] != ''){
						parent_ids[row_done].each(function(par_id){
							VarsModal.select_ids.data[par_id] += 1;
						});
					}
				} else {
					
					VarsModal.select_ids.data[row_done] = 1;
					
					if (parent_ids[row_done] && parent_ids[row_done] != ''){
						//console.log(parent_ids[row_done]);
						parent_ids[row_done].each(function(par_id){
							//console.log('a'+par_id);
							//console.log(VarsModal.select_ids);
							//console.log(VarsModal.select_ids.data[par_id]);
							VarsModal.select_ids.data[par_id] = 1;
						});
					}
				}
				VarsModal.select_ids.id.push(row_done);
			
				if ($('BillLine_'+row_done))
				$('BillLine_'+row_done).removeClass('none');
				if (parent_ids[row_done] && parent_ids[row_done] != ''){
					parent_ids[row_done].each(function(par_id){
						$('BillLine_'+par_id).removeClass('none');
					});
				}
				if ($('BillLine_'+row_done))
				$('BillLine_'+row_done).getElement('.ks').set('text',VarsModal.select_ids.data[row_done]+'x');
				if (parent_ids[row_done] && parent_ids[row_done] != ''){
					parent_ids[row_done].each(function(par_id){
						$('BillLine_'+par_id).getElement('.ks').set('text',VarsModal.select_ids.data[par_id]+'x');
					});
				}
				//console.log(VarsModal.select_ids);
				//return false;
			
			} else {
				//console.log('dis');
				e.target.removeClass('active');
				//$('BillLine_'+row_done).addClass('none');
		
				// pokud je jeden ks v uctu odstran jinak odecti ks
				if (VarsModal.select_ids.data[row_done] == 1){
					VarsModal.select_ids.id.erase(row_done);
					delete VarsModal.select_ids.data[row_done];
					if (parent_ids[row_done] && parent_ids[row_done] != ''){
						parent_ids[row_done].each(function(par_id){
							VarsModal.select_ids.id.erase(par_id);
							delete VarsModal.select_ids.data[par_id];
						});
					}
				} else {
					VarsModal.select_ids.data[row_done] -= 1;
					if (parent_ids[row_done] && parent_ids[row_done] != ''){
						parent_ids[row_done].each(function(par_id){
							VarsModal.select_ids.data[par_id] -= 1;
						});
					}
				}
				
				if (typeof VarsModal.select_ids.data[row_done] == 'undefined'){
					$('BillLine_'+row_done).addClass('none');
					if (parent_ids[row_done] && parent_ids[row_done] != ''){
						parent_ids[row_done].each(function(par_id){
							$('BillLine_'+par_id).addClass('none');
						});
					}
				} else {
					$('BillLine_'+row_done).getElement('.ks').set('text',VarsModal.select_ids.data[row_done]+'x');
					if (parent_ids[row_done] && parent_ids[row_done] != ''){
						parent_ids[row_done].each(function(par_id){
							$('BillLine_'+par_id).getElement('.ks').set('text',VarsModal.select_ids.data[par_id]+'x');
						});
					}
				}
				
				
		
			}
			//console.log(VarsModal.select_ids);
			
			if (!this.bill_load)
			this.bill_recount_total_price();
		}
		row_done = null;
		//console.log(VarsModal.select_ids);
		//console.log(VarsModal.order_data[this.table_id]);
	},
	
	// create line from data
	create_line_from_data: function(data){
		//console.log(data);
		if (Object.getLength(data)>0){
			Object.each(data,VarsModal.load_each = (function(item,k){
				
				if (item)
				this.create_line_basket({
					'parse':item.parse,
					'nocount':true,
					'load':1,
					'id':item.id,
					'parent_id':item.parent_id,
					'done':item.done,
					'product_id':item.product_id,
					'product_group_id':item.product_group_id,
					'num':item.num,
					'name':item.name,
					'price':item.price,
					'addon':item.addon,
					'price_tax_id':item.price_tax_id,
					'product_group_id':item.product_group_id,
					'zdarma':item.zdarma,
					'ks':item.ks,
					'user_id':item.user_id,
					'line_count':k,
				});
			}).bind(this));
			
			
		}
	},
	
	// recount price in basket
	basket_recount: function(){
		var total_price = 0;
		if (typeof VarsModal.order_data[this.table_id] != 'undefined')
		Object.each(VarsModal.order_data[this.table_id]['items'],VarsModal.data_each = function(item){
			total_price += item.price.toInt() * item.ks.toInt();
		});
		
		if (typeof VarsModal.order_data[this.table_id] != 'undefined')
		Object.each(VarsModal.order_data[this.table_id]['done_items'],VarsModal.data_each = function(item){
			if (item){
				if (!item.ks) item.ks = 1;
			
				total_price += item.price.toInt() * item.ks.toInt();
			}
		});
		$('total_price').set('text',price(total_price));
		$('TotalPrice').value = total_price;
		//console.log('total price',total_price);
		VarsModal.total_price = total_price;
		//console.log($('table_list').getElement('.active')));
		//console.log($('TableCnt'+$('table_list').getElement('.active')));
		//var active_el = $('table_list').getElement('.active');
		if ($('TableCnt'+this.table_id)){
			if (total_price > 0){
				$('TableCnt'+this.table_id).set('text',price(total_price)+',-');
			} else {
				$('TableCnt'+this.table_id).set('text','');
			}
		}
		if ($('TableCnt2'+this.table_id)){
			if (total_price > 0){
				$('TableCnt2'+this.table_id).set('text',price(total_price)+',-');
			} else {
				$('TableCnt2'+this.table_id).set('text','');
			}
		}
		active_el = null;
		
	},
	
	// show table prices
	table_prices: function(){
		this.loadStorage();
		//console.log(VarsModal);
		if (VarsModal.order_data)
		Object.each(VarsModal.order_data,VarsModal.data_each = function(data,table_id){
			total_price = 0;
				
			Object.each(VarsModal.order_data[table_id]['done_items'],VarsModal.data_each = function(item){
				if (item){
					if (!item.ks) item.ks = 1;
					
					if (!item.price) item.price = 0;
					if (!item.ks) item.ks = 1;
					
					total_price += item.price.toInt() * item.ks.toInt();
					
				}
				
			});
			if ($('TableCnt'+table_id)){
					if (total_price > 0){
						$('TableCnt'+table_id).set('text',price(total_price)+',-');
					} else {
						$('TableCnt'+table_id).set('text','');
					}
			}
			
			
		});
		
		
	},
	
	
	
	// count product in basket
	count_products_basket_price: function(nosave){
		//this.set_data_line();
		
		this.basket_recount();
		
		//if (!nosave)
		//this.save_data_stul();
		
		var pocet = $('basket_body').getElements('tr').length;
		if ($('basket_noproducts')) {
			if (pocet == 0){
				$('basket_noproducts').removeClass('none');
			} else {
				$('basket_noproducts').addClass('none');	
			}
		}
		
		// save local storage
		this.saveStorage();
		
		return pocet;
	},
	
	// switch products group
	switch_product_groups: function(){
		//this.product_groups_color();
		
		$$('.product_group_list').each((function(list){
			list.getElements('li').removeEvents('click');
			list.getElements('li').addEvent('click',this.switch_group_event.bind(this));
		}).bind(this));
		
		$$('.group_tabs').each(function(tab){
			if (!tab.hasClass('none')){
				tab.getElement('.product_group_list').getElement('li').fireEvent('click',tab.getElement('.product_group_list').getElement('li'));
			}
		});
		
		// switch table button
		$('table_list').getElements('li').addEvent('click',this.switch_table_event.bind(this));
		
		// select volny prodej
		$('TableId0').fireEvent('click',$('TableId0'));
		
		$('table_list_switch').getElements('li').addEvent('click',this.switch_table_tab.bind(this));
		
		$$('.switch_group_tab').addEvent('click',this.switch_table_group_level1.bind(this));
		
		if ($('TableId').value > 0){
			this.switch_table_event($('TableId'));
		}
		
		this.basket_function();
		
	},
	
	// swith table group level 1
	switch_table_group_level1: function(e){
		var tab_list = $$('.switch_group_tab');
		this.tab_index++;
		if (this.tab_index > tab_list.length - 1){
			this.tab_index = 0;
		}
		tab_list.addClass('none');
		tab_list.removeClass('active');
		
		tab_list[this.tab_index].removeClass('none');
		tab_list[this.tab_index].addClass('active');
		
		//console.log(tab_list);
		
		$$('.group_tabs').addClass('none');
		$(tab_list[this.tab_index].get('data-tab')).removeClass('none');
		
		$(tab_list[this.tab_index].get('data-tab')).getElement('li').fireEvent('click',$(tab_list[this.tab_index].get('data-tab')).getElement('li'));
	},
	
	// product groups color
	product_groups_color: function(){
		$$('.product_group_list').each(function(gl){
			gl.getElements('li').each(VarsModal.group_color = function(li){
				if (li.get('data-color') != '')
				li.setStyle('background-color',li.get('data-color'));
			});
		});
	},
	
	// switch table tab
	switch_table_tab: function(e){
		e.stop();
		$$('.tg').addClass('none');
		//console.log($$('.tg'+e.target.get('data-id')));
		$$('.tg'+e.target.get('data-id')).removeClass('none');
		
		
		$('table_list_switch').getElements('li').removeClass('active');
		e.target.addClass('active');
		if ($('map_table_in'))
		$('map_table_in').getElements('.canvas_group').addClass('none');
		$('map_table_in').getElement('.canvas_group'+e.target.get('data-id')).removeClass('none');
	},
	
	// map table events
	map_table_events: function(){
		if ($('map_table_in'))
		$('map_table_in').getElements('.table').addEvent('click',VarsModal.click_table = function(e){
			
			
			
			$('TableId'+e.target.get('data-id')).fireEvent('click',$('TableId'+e.target.get('data-id')));
			$('map_table').toggleClass('none');
		});
	},
	
	// switch table click
	switch_table_event: function(e){
		
		if ($('CheckUkoncit') && $('CheckUkoncit').value == 1){
			FstError('Musíte nejprve ukončit stůl');
			return false;
		}
		
		if (e.stop) e.stop();
		
		if (!e.target){
			e.target = e;
		}
		if (!e.target && e.value) {
			e.target = $('TableId'+e.value);
		}
		
		
		
		$('table_list').getElements('li').removeClass('active');
		e.target.addClass('active');
		$('TableId').value = e.target.get('data-id');
		
		$('HistoryValue').value = '';
		this.load_table_data(e.target.get('data-id'),e);
		
		$('map_table_in').getElements('.table').removeClass('active');
		if ($('MapTable'+e.target.get('data-id')))
			$('MapTable'+e.target.get('data-id')).addClass('active');
		
		
		$('tableName').set('text',e.target.getElement('.name').get('text'));
		
		this.basket_recount();
	},
	
	
	// basket function
	basket_function: function(){
		
		
		$$('.order_modal_close').addEvent('click',VarsModal.basket_modal_close = (function(){
			$$('.order_modal').addClass('none');
		}).bind(this));
		
		$('basket_function').getElements('button').addEvent('click',VarsModal.basket_fce = (function(e){
			// rozdelit
			if (e.target.get('data-fce-type') == 'rozdelit'){
				this.rozdelit_window();
			}
			if (e.target.get('data-fce-type') == 'ukoncit'){
				this.ukoncit_order();
			}
			if (e.target.get('data-fce-type') == 'zaplatit'){
				this.zaplatit_order();
			}
			if (e.target.get('data-fce-type') == 'rozdelit'){
				this.rozdelit_order();
			}
			
		}).bind(this));
		
		$('pay_products').getElements('.basket_fce').addEvent('click',VarsModal.basket_fce2 = (function(e){
			if (e.target.get('data-fce-type') == 'select_all'){
				this.bill_select_all();
			}
		}).bind(this));
		$('stul_rozdelit').getElements('.stul_fce').addEvent('click',VarsModal.basket_fce2 = (function(e){
			
			if (e.target.get('data-fce-type') == 'select_all'){
				this.stul_select_all();
			}
		}).bind(this));
		
		$('basket_products_obal').getElements('.float, .integer').inputLimit();
		
		
		
		// close modal sleva
		$$('.pay_modal_close').addEvent('click',VarsModal.pay_modal_close = function(e){
			
			e.target.getParent('.pay_modal').addClass('none');
			
		});
		
		// close big modal
		$$('.big_modal_close').addEvent('click',VarsModal.pay_modal_close = function(e){
			
			e.target.getParent('.big_modal').addClass('none');
			
		});
		
		
		// pay part save
		$('PayPartUcet').addEvent('click',VarsModal.add_button_click = function(e){
			if (!e.target) e.target = e;
			var value = $('PayPartValue').value;
			if (value < 2){
				FstError('Zadejte částku k zaplacení');
				return false;
			}
			var total_price = $('bill_total_price').get('text').toFloat();
			var ks = 0;
			
			Object.each(VarsModal.order_data[this.table_id]['done_items'], VarsModal.data_each = function(item,k){
				ks += item.ks;
			});
			
			pomer = value / (total_price);
			
			
			Object.each(VarsModal.order_data[this.table_id]['done_items'], VarsModal.data_each = function(item,row){
				ks_pomer = item.ks * pomer;
				
				//if (typeof VarsModal.order_data[this.table_id]['part_items'] == 'undefined') VarsModal.order_data[this.table_id]['part_items'] = [];
				
				VarsModal.order_data[this.table_id]['done_items'][row].ks_tmp = (pomer*item.ks);
				VarsModal.order_data[this.table_id]['done_items'][row].ks = (item.ks - pomer*item.ks);
				
			}.bind(this));
			
			this.reorder_bill();
			
			$('modal_pay_part').addClass('none');
			
		}.bind(this));
		
		// rozdelit ucet save
		$('RozdelitUcet').addEvent('click',VarsModal.add_button_click = function(e){
			if (!e.target) e.target = e;
			var value = $('RozdelitValue').value;
			if (value < 2){
				FstError('Zadejte počet částí na rozdělení');
				return false;
			}
			Object.each(VarsModal.order_data,VarsModal.order_data_each = function(item,k){
				//price_value = item.price / value;
				ks = item.ks / value;
				
				VarsModal.order_data[k].ks_rozdelit = ks;
			//	VarsModal.order_data[k].price_rozdelit = price_value;
				
				//$('BillLine_'+item.id).set('data-price',price_value);
				$('BillLine_'+item.id).set('data-ks',ks);
				$('BillLine_'+item.id).getElement('.ks').set('text',ks+'x');
				//$('BillLine_'+item.id).getElement('.price').set('text',price(price_value));
				
			}.bind(this));
			
			$('OrderRozdelit').value = value;
			
			this.reorder_bill();
			this.set_data_line();
			
			// save data stul
			this.save_data_stul();
			
			//console.log(VarsModal.order_data);
			
		}.bind(this));
		
		// save sleva
		$('AddSleva').addEvent('click',VarsModal.add_button_click = function(e){
			if (!e.target) e.target = e;
			if ($('SlevaValue').value == '' && $('SlevaPerc').value == ''){
				FstError('Musíte zadat slevu');
				return false;
			}
			if ($('SlevaValue').value != '' && $('SlevaPerc').value != ''){
				FstError('Musíte zadat jen jednu slevu');
				return false;
			}
			
			if ($('SlevaValue').value != ''){
				var sleva = $('SlevaValue').value.toInt()*-1;
				var sleva_name = 'Sleva částka';
			}
			if ($('SlevaPerc').value != ''){
				var sleva = $('bill_total_price').get('text').toInt() * ('0.'+$('SlevaPerc').value).toFloat() * -1;
				var sleva_name = 'Sleva '+$('SlevaPerc').value+'%';
			}
			
			//console.log(sleva);return false;
			
			this.create_line_basket({
				'id':'0',
				//'load':(this.connection_table[e.target.get('data-id')]?1:0),
				'product_id':0,
				'product_group_id':0,
				'num':0,
				'code':'s',
				'name':sleva_name,
				'addon':0,
				'price':sleva,
				'price_tax_id':1,
				'product_group_id':0,
				'user_id':this.logged_user_id,
			});
			
			this.create_new_line_bill({
				'id':'0',
				'price':sleva,
				'ks':1,
				'name':sleva_name,
			});
			
			this.reorder_bill();
			
			sleva = null;
			sleva_name = null;
				
			// save history
			this.save_history();
			// save data stul
			this.save_data_stul();
			// close modal
			this.close_modal_bill();
			
			
		}.bind(this));

		// pay modal enter
		$('basket_products_obal').getElements('.pay_modal').each(VarsModal.pay_m = function(modal){
			modal.getElements('.text').addEvent('keydown',VarsModal.pay_modal_enter = function(e){
				if (e.key == 'enter'){
					var el = e.target.getParent('.pay_modal').getElement('button');
					el.fireEvent('click',el);
					el = null;
				}
			}.bind(this));
		
		}.bind(this));
	
		
		$('basket_pay').getElements('.pay_fce').addEvent('click',VarsModal.pay_fce = (function(e){
			// hotove
			if (e.target.get('data-type') == 'hotove'){
				//console.log(VarsModal.order_data[this.table_id]);
		
				var hash_print = JSON.encode(VarsModal.select_ids);
				var link = '/orders/printUcet/';
				this.print_bon(link,e.target);
			}
			
			// kartou
			if (e.target.get('data-type') == 'kartou'){
				var hash_print = JSON.encode(VarsModal.select_ids);
				var link = '/orders/printUcet/'+$('OrderId').value+'/2/?params='+hash_print;
				this.print_bon(link,e.target);
			}
			
			// tisk ucet bez ulozeni
			if (e.target.get('data-type') == 'print_ucet'){
				var hash_print = JSON.encode(VarsModal.select_ids);
				var link = '/orders/printUcet/'+$('OrderId').value+'/0/?params='+hash_print;
				this.print_bon(link,e.target,true);
			}
			
			//$('basket_pay').addClass('none');
		}).bind(this));
	
		$('basket_pay').getElements('.bill_fce').addEvent('click',VarsModal.bill_fce = (function(e){
			
			if (!e.target){
				e.target = e;
			}
			var active_products = $('pay_products').getElements('.active');
			
			// hotovo
			if (e.target.get('data-type') == 'done'){
				this.bill_back_button();
			}
			
			// zpet
			if (e.target.get('data-type') == 'zpet'){
				$('basket_pay').addClass('none');
			}
			
			// sleva
			if (e.target.get('data-type') == 'sleva'){
				$('modal_sleva').removeClass('none');
				$('SlevaValue').value = '';
				$('SlevaPerc').value = '';
				$('SlevaValue').focus();
				
			}
			
			// rozdelit ucet
			if (e.target.get('data-type') == 'rozdelit'){
				$('modal_rozdelit').removeClass('none');
				$('RozdelitValue').value = '';
				$('RozdelitValue').focus();
				
			}
			
			// zaplatit vybranne
			if (e.target.get('data-type') == 'pay_select'){
				if (Object.getLength(active_products) == 0){
					FstError('Musíte vybrat produkty');
				} else {
					
					this.hide_bill_items();
					
					//console.log(VarsModal.select_ids);
					this.bill_back_button();
				}
			}
			
			// zaplatit cast
			if (e.target.get('data-type') == 'pay_part'){
				this.pay_part_order();
			}
			
		}).bind(this));
		
		if ($('bill_back'))
		$('bill_back').addEvent('click',VarsModal.bill_back = (function(e){
			this.bill_back_button();
		}).bind(this));
	},
	
	// pay_part_order
	pay_part_order: function(){
		$('modal_pay_part').removeClass('none');
		$('PayPartValue').value = '';
		$('PayPartValue').focus();
	},
	
	// close modal bill
	close_modal_bill: function(){
		$('basket_products_obal').getElement('.pay_modal').addClass('none')
	},
	
	// reorder bill
	reorder_bill: function(){
		var total_price = 0;
		//console.log(VarsModal.order_data);
		Object.each(VarsModal.order_data[this.table_id]['done_items'], VarsModal.order_data_each = function(item,row){
			if (item.ks_tmp){
				$('BillLine_'+row).getElement('.ks').set('text',item.ks_tmp.toFixed(2));
				$('BillLine_'+row).setProperty('data-ks_tmp',item.ks_tmp.toFixed(2));
			}
		});
		
		
		$('bill_products').getElements('li').each(VarsModal.each_bill_prods = function(item){
			if (!item.hasClass('none')){
				//console.log(item);
				if (item.get('data-ks_tmp')){
					total_price += item.get('data-ks_tmp').toFloat()*item.get('data-price');
				} else {
					total_price += item.get('data-ks').toFloat()*item.get('data-price');
				}
			}
		});
		$('bill_total_price').set('text',price(total_price));
	},
	
	// print bon
	print_bon: function(link,element,nahled){
		//button_preloader($(element.get('id')));
		
		VarsModal.save_ucet = {}
			
			var ks_bill = [];
			$('bill_products').getElements('li').each(VarsModal.data_each = function(item,row){
				if (item.get('data-ks_tmp'))
					ks_bill[row] = item.get('data-ks_tmp');
				else	
					ks_bill[row] = item.get('data-ks');
			});
			
			Object.each(VarsModal.order_data[this.table_id]['done_items'], VarsModal.data_each_save = function(item,row){
				if (VarsModal.select_ids.id.contains(row)){
					item.save_ks = ks_bill[row];
					VarsModal.save_ucet[row] = item;
				}
			});
			/*
		console.log(VarsModal.save_ucet);
		console.log(VarsModal.order_data[this.table_id]['done_items']);
		return false;
		*/
		
		
		// platba type
		this.platba_name = platba_list[element.get('data-platba_type_id')];
		this.platba_id = element.get('data-platba_type_id').toInt();
		
		var save_hash = {
			'created':VarsModal.order_data[this.table_id]['created'],
			'done_date':new Date().format('db'),
			'order_items':VarsModal.save_ucet,
			'user_id':this.logged_user_id,
			'user':this.logged_user,
			'table_id':this.table_id.toInt(),
			'platba_id':this.platba_id,
			'platba_name':this.platba_name,
			'printed':(nahled?0:1),
		}
		// zjisti pocet produktu po ulozeni na stole
		
		//console.log(save_hash);
		//save_hash = Base64.encode(JSON.encode(save_hash));
		save_hash = b64EncodeUnicode(JSON.encode(save_hash));
		//console.log(save_hash);
		
		this.print_bon_items_to_print();
		
		save_hash_from_offline = this.save_offline_order(save_hash);
		//console.log(save_hash_from_offline);
		//save_hash_from_offline = Base64.encode(JSON.encode(save_hash_from_offline));
		save_hash_from_offline = b64EncodeUnicode(JSON.encode(save_hash_from_offline));
		console.log(save_hash_from_offline);
		console.log(b64DecodeUnicode(save_hash_from_offline));
		if (this.is_online){
			
			VarsModal.print_bon = new Request.JSON({
				url:link+save_hash_from_offline,	
				data: {
					'data':save_hash_from_offline
				},
				timeout: 1000*this.timeout_save,
				
				
				onSuccess: VarsModal.print_bon_compl = (function(json){
					
					//button_preloader($(element.get('id')));
					if (json.r == true){
						if (json.ucet_id){
							$('p_ucet_doklad').set('text',json.ucet_id);
							this.gen_pdf();
						}
						
						this.clear_table();
					}
					
					VarsModal.print_bon.cancel();
					
					json = null;
				}).bind(this),
				
				onTimeout: VarsModal.timeout = function(){
					VarsModal.print_bon.cancel();
					this.error_eet(save_hash_from_offline);
				
				}.bind(this),	
				
				onFailure: VarsModal.se = function(data){
					console.log(data);
					
					this.error_eet(save_hash_from_offline);
					send_error(JSON.encode(data),'Chyba uložení účtu online');
				}.bind(this),
				
				onError: VarsModal.se = function(data,error){
					//console.log(data);
					this.error_eet(save_hash_from_offline);
					send_error(JSON.encode(data),'Chyba uložení účtu online');
				}.bind(this),
				
				
			});
			VarsModal.print_bon.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.print_bon.send();
			//console.log(VarsModal.print_bon);
			save_hash = null;
		
		}
	},
	
	// error eet
	error_eet: function(data){
		FstError('Není připojení k internetu nebo EET neodpovedělo');
		this.gen_pdf();
	},
	
	// save offline order
	save_offline_order: function(save_hash){
		save_hash = JSON.decode(Base64.decode(save_hash));
		//console.log(save_hash);
		VarsModal.local_orders_data = JSON.decode(localStorage.getItem("local_orders_data"));
			
		if (!VarsModal.local_orders_data || Object.getLength(VarsModal.local_orders_data)==0) VarsModal.local_orders_data = {};
		cur_date = this.cur_date;
		cur_date_minus = new Date(cur_date);
		cur_date_minus.setDate(cur_date_minus.getDate() - 2);
		cur_date_minus = cur_date_minus.format('%Y-%m-%d');
			
		//console.log(VarsModal.local_orders_data[cur_date]);
		if (!VarsModal.local_orders_data[cur_date]) VarsModal.local_orders_data[cur_date] = [];
		
		//VarsModal.local_orders_data['2016-10-15'] = {}
		
		Object.each(VarsModal.local_orders_data,VarsModal.data_each = function(item,date){
			//console.log(result);
			if (date < cur_date_minus){
				//console.log('del',date);
				delete VarsModal.local_orders_data[date];
			}
		});
		var items = [];
		Object.each(save_hash.order_items, VarsModal.data_each = function(item){
			// pokud je sleva
			if(item.code == 's'){
				save_hash.sleva = item.price;
			}
			item.price = price(item.price);
			items.push(item);
		});
		save_hash.order_items = items;
		
		save_hash.done = 1;
		save_hash.print_user_id = $('p_ucet_user_id').get('text').toInt();
		save_hash.count_items = Object.getLength(save_hash.order_items);
		save_hash.table_name = $('TableId'+this.table_id).getElement('.name').get('text');
		save_hash.total_price = price($('bill_total_price').get('text').toFloat());
		
		// hotove
		if (save_hash.platba_id == 1){
			save_hash.pay_hotove = save_hash.total_price.toFloat();
		}
		// kreditka
		if (save_hash.platba_id == 2){
			save_hash.pay_kreditka = save_hash.total_price.toFloat();
		}
		// sleva
		if (save_hash.sleva){
			save_hash.pay_sleva = save_hash.sleva.toFloat()*-1;
			
		}
		
		//console.log(save_hash);
		
		VarsModal.local_orders_data[cur_date].push(save_hash);
		
		// save to local storage
		localStorage.setItem(
			"local_orders_data", 
			JSON.stringify(VarsModal.local_orders_data)
		);
		
		if (!this.is_online)
		this.gen_pdf();
		
		
		this.clear_table();
		
			
		//console.log(VarsModal.local_orders_data);
		return save_hash;
		
	},
	
	// vycisti stul po ulozeni
	clear_table: function(){
		
		// pokud je vse zaplaceno zrusit ze storage
		if (VarsModal.save_ucet.length == VarsModal.order_data[this.table_id]['done_items'].length){
			if (this.debug) console.log('clear table');
			delete VarsModal.order_data[this.table_id];	
			fstvars.clearEl($('basket_body'));
		} else {
			
			
			Object.each(VarsModal.save_ucet,VarsModal.data_each = function(item,row){
			
				// pokud je rozdil kusu k zaplaceni
				if (VarsModal.select_ids.data[row] != VarsModal.save_ucet[row]['ks'].toInt()){
					if (typeof VarsModal.order_data[this.table_id]['done_items'][row]['save_ks'] != 'undefined'){
						VarsModal.order_data[this.table_id]['done_items'][row]['ks'] = VarsModal.order_data[this.table_id]['done_items'][row]['ks'] - VarsModal.order_data[this.table_id]['done_items'][row]['ks_tmp'];
					} else {
						VarsModal.order_data[this.table_id]['done_items'][row]['ks'] = VarsModal.order_data[this.table_id]['done_items'][row]['ks'] - VarsModal.order_data[this.table_id]['done_items'][row]['save_ks'];
					}
				} else {
					delete VarsModal.order_data[this.table_id]['done_items'][row];
					
				}
				
			}.bind(this));
		}
		this.filter_done_items();
		//console.log(VarsModal.order_data[this.table_id]['done_items']);
		
		
		this.count_products_basket_price();
		//this.saveStorage();
		
		this.reset_default_status();
		
	},
	
	// navrat na vychozi plochu
	reset_default_status: function(){
		$('CheckUkoncit').value = 0;	
				
		$('bill_back').click();
		$('zrusit').click();
		$('table_list').getElements('.tg').each(function(item){
			if (item.hasClass('active')){
				item.click();
			}
			//$('TableId0').click();
		});
	},
	
	// odstranit undefined value z done_items
	filter_done_items: function(){
		VarsModal.order_data[this.table_id]['done_items'] = VarsModal.order_data[this.table_id]['done_items'].filter(function( element ) {
		   return element !== undefined;
		});
	},
	
	// pridani polozek na uctenku
	print_bon_items_to_print: function(){
		var total_price = 0;
		Object.each(VarsModal.save_ucet,VarsModal.data_each = function(item){
			if (item.ks_tmp){
				ks = item.ks_tmp;
			} else {
				ks = item.save_ks;
			}
			
			tr = new Element('tr',{}).inject($('print_ucet_products'));
			td1 = new Element('td',{}).set('text',item.name).inject(tr);
			td2 = new Element('td',{}).set('text',item.ks).inject(tr);
			td3 = new Element('td',{}).set('text',tax_list[item.price_tax_id]+'%').inject(tr);
			td4 = new Element('td',{}).set('text',price(item.price)).inject(tr);
			td5 = new Element('td',{}).set('text',price(item.price*ks)).inject(tr);
			
			total_price += item.price*item.ks;
		});
		
		$('p_ucet_platba').set('text',this.platba_type);
		$('p_ucet_price_with_tax').set('text',price(total_price));
	},
	
	// hide bill items
	hide_bill_items: function(){
		$('bill_products').getElements('li').each(VarsModal.each_bill_prods = function(item){
			//console.log(VarsModal.select_ids);
			var row_key = item.get('data-row_key');
			if (VarsModal.select_ids.id.contains(row_key)){
				item.getElement('.ks').set('text',VarsModal.select_ids.data[row_key]+'x');
				item.set('data-ks_tmp',VarsModal.select_ids.data[row_key]);
				item.removeClass('none');
			} else {
				item.addClass('none');
			}
		});
		
		this.reorder_bill();
	},
	
	// bill select all
	bill_select_all: function(){
		if ($('vybrat-vse').hasClass('select_all')){
			$('vybrat-vse').removeClass('select_all');
			$('vybrat-vse').set('text','Vybrat vše');
			this.bill_load = true;
			$('pay_products_list').getElements('.button').each(VarsModal.each_bb = function(but){
				but.fireEvent('click',but);
			});
		} else {
			$('vybrat-vse').addClass('select_all');
			$('vybrat-vse').set('text','Odebrat vše');
			this.bill_load = true;
			$('pay_products_list').getElements('.button').each(VarsModal.each_bb = function(but){
				but.fireEvent('click',but,true);
			});
		
		}
	},
	
	// stul select all
	stul_select_all: function(){
		if ($('vybrat-vse-stul').hasClass('select_all')){
			$('vybrat-vse-stul').removeClass('select_all');
			$('vybrat-vse-stul').set('text','Vybrat vše');
			$('stul_products_list').getElements('.button').each(VarsModal.each_bb = function(but){
				but.fireEvent('click',but);
			});
		} else {
			$('vybrat-vse-stul').addClass('select_all');
			$('vybrat-vse-stul').set('text','Odebrat vše');
			$('stul_products_list').getElements('.button').each(VarsModal.each_bb = function(but){
				but.fireEvent('click',but);
			});
		
		}
	},
	
	// bill back button
	bill_back_button: function(){
		
		if ($('bill_back') && $('bill_back').hasClass('none')){
			$('bill_back').removeClass('none');
			$('pay_fce1').addClass('none');
			$('pay_fce2').removeClass('none');
			
		} else {
			if ($('bill_back'))
			$('bill_back').addClass('none');
			$('pay_fce1').removeClass('none');
			$('pay_fce2').addClass('none');
				
		}
		
		//console.log(VarsModal.order_data[this.table_id]['done_items']);
	},
	
	// zaplatit order
	zaplatit_order: function(){
		$('basket_pay').removeClass('none');
		$$('.product_addons').addClass('none');
		
		this.save_data_stul();
		//console.log('a');
		//this.ukoncit_order();
		//console.log(VarsModal.order_data);
		this.create_line_to_bill(VarsModal.order_data[this.table_id]);
		
	},
	
	// ukoncit order
	ukoncit_order: function(){
		//console.log($('OrderId').value);
		//console.log(this.history_data);
		//this.save_history();
		this.save_data_stul();
		this.print_part();
		this.reset_default_status();
		//this.gen_pdf();
		
		
	},
	// kontrola websocket
	check_websocket: function(){
		if ($('websocket_status').hasClass('failed')){
			FstError('Není připojena tiskárna');
			return false;
		} else {
			return true;
		}
	},
	
	
	// print part of order to specific printer
	
	print_part: function(){
		if (this.check_websocket()){
		
		//button_preloader($('ukoncit'));
		VarsModal.print_part = {};
		if (typeof VarsModal.printer_setting == 'undefined'){
			var printer_select = {}
			printer_select.value = $('printer_default').get('text');
		}
		//console.log(VarsModal.printer_setting);
			
		Object.each(VarsModal.order_data[this.table_id]['done_items'], VarsModal.data_each = function(item,row){
			//console.log(item);
			//if (!item.printed){
				//console.log(VarsModal.printer_setting[item.product_group_id]);
				if (!VarsModal.printer_setting[item.product_group_id]){
					//console.log(group_id);
					item.product_group_id = group_id;
					printer_name = VarsModal.printer_setting[group_id];
				
				} else {
					printer_name = VarsModal.printer_setting[item.product_group_id];
					group_id = item.product_group_id;
				
				}
				//console.log(printer_name);
				if (typeof VarsModal.print_part[printer_name] == 'undefined') VarsModal.print_part[printer_name] = [];
				
				VarsModal.print_part[VarsModal.printer_setting[item.product_group_id]].push(item);
				VarsModal.order_data[this.table_id]['done_items'][row].printed = 1;
			//}
		}.bind(this));
		
		
		// create lines to canvas and print to specific printer
		//console.log(VarsModal.print_part); 
		//return false;
		Object.each(VarsModal.print_part,VarsModal.data_each = function(items_printer,printer){
			$('print_part_data').empty();
			console.log('tisk na '+printer);
			Object.each(items_printer,VarsModal.data_each = function(item,key){
				tr = new Element('tr').inject($('print_part_data'));
				new Element('td',{'text':item.ks+'x'}).inject(tr);
				new Element('td',{'text':item.name}).inject(tr);
				
			});
			$('order_part_print_time').set('text',new Date().format('%d.%m.%Y %H:%M'));
			$('order_part_table_name').set('text',$('TableId'+this.table_id).getElement('.name').get('text'));
			
			$('print_order_part').removeClass('none');
			var opt = {
				'type':'print',
				'file_data':$('print_order_part').get('html'),
				'printer_name':printer,
			}
			//console.log(window.FstPokladnaClient);
			window.fstClient.send_data_printer(opt);
				
			
			
		}.bind(this));
		}
		//console.log(VarsModal.order_data[this.table_id]['done_items']);
		
	},
	
	
	// rozdelit window
	rozdelit_window: function(){
		$('rozdelit_window').removeClass('none');
		$('basket_products').clone().inject($('rozdelit_products_table'));
	},
	
	
	gen_pdf: function(){
		if (this.check_websocket()){
		var opt = {
			'type':'print',
			'file_data':$('print_ucet').get('html'),
			'printer_name':$('printer_default').get('text'),
		}
		//console.log(opt);						
		window.fstClient.send_data_printer(opt);
		
		}
		
	},
	
	// load table data 
	load_table_data: function(table_id,e){
		//$('basket_products').addClass('preloader');
		//this.gen_pdf();
		
		this.show_basket();
		
		if (!table_id){
			table_id = $('TableId').value;
			return false;
		}
		this.table_id = table_id;
		if (this.debug) console.log('table_id select',this.table_id);
		
		
		//this.loadStorage();
		//console.log(VarsModal.order_data[table_id]);return false;
		
		if (typeof VarsModal.order_data == 'undefined') VarsModal.order_data = {};
		//console.log(VarsModal.order_data[table_id]['items']);
		if (typeof VarsModal.order_data[table_id] != 'undefined'){
			fstvars.clearEl($('basket_body'));
			this.create_line_from_data(VarsModal.order_data[table_id]['done_items']);
			this.create_line_from_data(VarsModal.order_data[table_id]['items']);
			
		} else {
			fstvars.clearEl($('basket_body'));
			//console.log('aa');
		}
		this.count_products_basket_price();
	
		
	},
	
	// show basket / hide basket
	show_basket: function(hide){
		if (hide == true){
			$('basket_function').addClass('none');
			$('basket_products').addClass('none');
			$('notable_select').removeClass('none');
			$('table_list').getElements('.tg').removeClass('active');
			$('TableId0').fireEvent('click',$('TableId0'));
		
		} else {
			$('basket_function').removeClass('none');
			$('basket_products').removeClass('none');
			$('notable_select').addClass('none');
		
		}
		
	},
	
	// save data stul
	save_data_stul: function(){
		//$('TableId0').click();
		//console.log(VarsModal.order_data);
		if (typeof VarsModal.order_data[this.table_id]['done_items'] == 'undefined' || !VarsModal.order_data[this.table_id]['done_items']) VarsModal.order_data[this.table_id]['done_items'] = [];
		//console.log(VarsModal.order_data[this.table_id]);	
		Object.each(VarsModal.order_data[this.table_id]['items'],VarsModal.data_each = function(item,k){
			VarsModal.order_data[this.table_id]['items'][k]['done'] = 1;
			
			// prepis do done_items
			if (VarsModal.order_data[this.table_id]['items'][k] != null)
			VarsModal.order_data[this.table_id]['done_items'].push(VarsModal.order_data[this.table_id]['items'][k]);
			//delete VarsModal.order_data[this.table_id]['items'][k];
			
		
		}.bind(this));
		
		VarsModal.order_data[this.table_id]['items'] = {};
			
		this.saveStorage();
		
	},
	
	
	
	// switch group click
	switch_group_event: function(e){
		if (!e.target) e.target = e;
		if (e.stop) e.stop();
		
		
		// back button
		if (e.target.hasClass('back')){
			e.target.getParent('ul').addClass('none'); 
		}
		//console.log(e.target);
		if (e.target.getElement('ul')){
			e.target.getElement('ul').removeClass('none');
			e.target.getElement('ul').setStyle('width',$('product_list').getSize().x);
			
			
			//console.log(e.target.getElement('ul').getElement('li').getNext('li')); 
			
			e.target.getParent('ul').getElements('li').removeClass('active');
			e.target.addClass('active');
			
			e.target.getElement('ul').getElement('li').setStyle('width',e.target.getStyle('width'));
			
			// select first on level 3
			//if (e.target.getElement('ul'))
			//e.target.getElement('ul').getElement('.noselect').fireEvent('click',e.target.getElement('ul').getElement('.noselect'));
			//console.log(e.target.getElement('ul').getElement('.noselect'));
			//console.log(e.target.getElement('ul').getElement('li').getNext('li'));
			e.target.getElement('ul').getElement('li').getNext('li').fireEvent('click',e.target.getElement('ul').getElement('li').getNext('li'));
		} else {
			e.target.getParent('ul').getElements('ul').addClass('none');
			if (e.target.hasClass('active')){
				//e.target.removeClass('active');
				$$('.group_id_hide').removeClass('none');
				
			} else {
					
				$$('.group_id_hide').addClass('none');
				if ($('group_id'+e.target.get('data-id'))){
					$('group_id'+e.target.get('data-id')).removeClass('none');
					
					if ($('group_id'+e.target.get('data-id')).getElement('.product_addons'))
					$('group_id'+e.target.get('data-id')).getElements('.product_addons').setStyles({
						'width':$('group_id'+e.target.get('data-id')).getSize().x - 10,
						'height':$('group_id'+e.target.get('data-id')).getSize().y - 18,
						'position':'fixed',
						'top':$('product_list').getCoordinates().top+15,
						'left':$('product_list').getCoordinates().left,
					});
					$('noresult_product').addClass('none');
				} else {
					$('noresult_product').removeClass('none');
				}
				
			}
			e.target.getParent('ul').getElements('li').removeClass('active');
			e.target.addClass('active');
		}
	},
	
	detail_product_function: function(){
		$('OrderItemNote').removeEvents('click');
		$('OrderItemNote').addEvent('change',VarsModal.click_note = function(e){
			//console.log(VarsModal.order_data);
			//console.log($('OrderItemId').value.replace('d',''));
			VarsModal.order_data[$('OrderItemOriginalId').value].note = $('OrderItemNote').value;
			//console.log(VarsModal.order_data);
			var dat = JSON.decode($('data_line_'+$('OrderItemOriginalId').value).value);
			dat.note = $('OrderItemNote').value;
			$('data_line_'+$('OrderItemOriginalId').value).value = JSON.encode(dat);
			dat = null;
			
			this.save_data_stul(true);
			
			
		
		}.bind(this));
		
		$('SaveNote').removeEvents('click');
		$('SaveNote').addEvent('click',VarsModal.save_note = function(){
			VarsModal.order_data[this.table_id][line_id].note = $('OrderItemNote').value;
			$('basket_product_detail_close').click();
		});
		
	},
	
	close_detail_product: function(){
		
		if ($('basket_product_detail_close'))
		$('basket_product_detail_close').addEvent('click',function(e){
			e.stop();
			$('basket_product_detail').addClass('none');
		});
	},
	
	
	keyboard:function(){
		if ($('keyboard')){
			$$('.show_keyboard').removeEvents('click');
			$('keyboard').addClass('none');
			
			$$('.show_keyboard').addEvent('click',VarsModal.show_keyboard = function(e){
				this.keyboard_input = e.target;
				$('keyboard').removeClass('none');
			}.bind(this));
			
			$('keyboard_close').addEvent('click',VarsModal.close_keyboard = function(e){
				this.keyboard_input = null;
				$('keyboard').addClass('none');
			}.bind(this));
			
			$('keyboard').getElements('.key_but').addEvent('click',VarsModal.key_but = function(e){
				if (typeof this.keyboard_input == 'undefined' || this.keyboard_input == null){
					FstError('Vyberte kam chcete psát');
				} else {
					this.keyboard_input.value += e.target.get('text');
				}
			}.bind(this));
		
		}
		if ($('keyboard_num')){
			$$('.show_keyboard_num').removeEvents('click');
			$('keyboard_num').addClass('none');
			
			$$('.show_keyboard_num').addEvent('click',VarsModal.click_fce = function(e){
				this.keyboard_input = e.target;
				$('keyboard_num').removeClass('none');
			}.bind(this));
			
			$('keyboard_close_num').addEvent('click',VarsModal.click_fce = function(e){
				
				this.keyboard_input = null;
				$('keyboard_num').addClass('none');
			}.bind(this));
			
			$('keyboard_num').getElements('.key_but').addEvent('click',VarsModal.click_fce = function(e){
				
				if (typeof this.keyboard_input == 'undefined' || this.keyboard_input == null){
					FstError('Vyberte kam chcete psát');
				} else {
					if (e.target.get('text') == 'backspace'){
						this.keyboard_input.value = this.keyboard_input.value.substring(0, this.keyboard_input.value.length - 1);
						this.keyboard_input.focus();
					} else {
						this.keyboard_input.value += e.target.get('text');
						this.keyboard_input.focus();
					}
				}
			}.bind(this));
		
		}
	}   
	
});

window.addEvent('domready', function () {
	window.fstPokladna = new FstPokladna();
	//var s = $('body').getDimensions({computeSize: true});
	//console.log(s);
	//window.fstsystem.alert_fce('test');
});

function send_error(data,message){
	if (!$('senderrormessage')){
		return false;
	}
	//console.log(data);
	if (message){
	$('senderrormessage').value = message;
	
	}
	$('senderrortext').value = data;
	new Request.JSON({
		url:$('send_error_form').action,	
		onComplete:function(json){
			if (!json || json.result == false){
				FstError('Chyba odeslani chyby');
			} else {
				(function(){
					FstAlert(json.message);
					
				}).delay(1000);
			}	
		}
	}).post($('send_error_form'));
}


window.onerror = function (msg, url, lineNo, columnNo, error) {
	var error_msg = url+' Msg:'+msg + ' Line:'+lineNo+' Col:'+columnNo+''
	//console.log(error_msg);
	//send_error(error_msg,'Chyba Javascript');
			
	/*
	console.log(msg);
	console.log(url);
	console.log(lineNo);
	console.log(columnNo);
	*/
}