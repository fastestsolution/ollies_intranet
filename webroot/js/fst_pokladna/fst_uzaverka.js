var FstUzaverka = this.FstUzaverka = new Class({
	Implements:[Options,Events],
	debug : true,
	
	// init fce
	initialize:function(options){
		
	},
	
	createDump: function(){
		VarsModal.gen_uzaverka = new Request.JSON({
			url:'/schemas/createDump/',	
			onError: function(){
				FstError('Chyba generování zálohy');
				
			},
			onComplete:function(json){
				if (json.result == true){
					FstAlert(json.message);
				} else {
					FstError(json.message);
				}
				json = null;
			}.bind(this)
		});
		VarsModal.gen_uzaverka.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		VarsModal.gen_uzaverka.send();
		
	},
	
	
	// generovani uzaverka do databaze
	generovani_uzaverka_db: function(type){
		$('uzaverkas_frame').addClass('preloader_solo');
		if (type == 'nahled'){
			url = '/orders/gen_uzaverka/nahled/';
		} else {
			this.createDump();
			//return false;
			url = '/orders/gen_uzaverka/done/';
		}
		VarsModal.gen_uzaverka = new Request.JSON({
			//url:'/orders/gen_uzaverka/',	
			url:url,	
			onError: function(){
				FstError('Chyba generování uzávěrka');
				
				$('uzaverkas_frame').removeClass('preloader_solo');
			},
			onComplete:function(json){
				$('uzaverkas_frame').removeClass('preloader_solo');
			
				if (json.r == true){
					// pokud jsou tiskove data
					if (json.print_data != ''){
						fstPrint.print_data(json.print_data);
					} else {
						$('uzaverkas_frame').set('html',json.html);
						$('uzaverkas_frame').setStyle('height',window.getSize().y - 70);
					}
					//console.log(json.html);	
				} else {
					FstError(json.m);
				}
				json = null;
			}.bind(this)
		});
		VarsModal.gen_uzaverka.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		VarsModal.gen_uzaverka.send();
	},
	
	// gen uzaverka
	gen_uzaverka: function(e){
		is_clear = true;
		if (confirm(e.target.get('data-confirm'))){
			// pouze nahled
			if (e.target.get('data-type') == 'nahled'){
				this.generovani_uzaverka_db('nahled');
						
			} else {
					
				VarsModal.check_table_open = new Request.JSON({
					url:'/orders/checkOpenTables/',	
					onError: function(){
						FstError('Chyba kontrola stolu');
					},
					onComplete:function(json){
						if (json.r == true){
							this.generovani_uzaverka_db();
						} else {
							FstError('Musíte uzavřít všechny stoly: '+json.table_name);
						}
						json = null;
					}.bind(this)
				});
				VarsModal.check_table_open.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				VarsModal.check_table_open.send();
			}
			//console.log(e);
		}
		return false;
			/*	
			// check table open
			Object.each(VarsModal.order_data, VarsModal.data_each = function(item){
				if (Object.getLength(item.items)>0){
					is_clear = false;
				}
			});
			
			
			
			var type = e.target.get('data-type');
			var print = e.target.get('data-print');
			var uzaverka_id = e.target.get('data-id');
			// open print frame
			$('iframe_preloader').removeClass('none');
			
			if (!uzaverka_id && type=='done'){
				if (!is_clear){
					FstError('Musíte uzavřít všechny stoly');
					$('iframe_preloader').addClass('none');	
					return false;
				}
			}
			*/
			
			url = '/orders/gen_uzaverka/'+type+'/'+((uzaverka_id)?uzaverka_id:'');
			
			VarsModal.load_iframe = new Request.HTML({
				update:'uzaverkas_frame',	
				url:url,	
				onComplete:function(responseTree, responseElements, responseHTML, responseJavaScript){
					$('iframe_preloader').addClass('none');	
					
					if ($('uzaverkas_frame').getElement('.noresult')){
						FstError($('uzaverkas_frame').getElement('.noresult').get('text'));
					} else {
						
						// pokud je uzavreni smeny
						if (type == 'done'){
							//alert('');
							//this.load_uzaverka_list();
						}
						
						// pokud je tisk
						if (print){
							html = $('uzaverkas_frame').get('html');
							
							var opt = {
								'type':'print',
								'file_data':html,
								'printer_name':$('printer_default').get('text'),
							}
							window.fstClient.send_data_printer(opt); 
						}
					
					}
					
					html = null;
				}.bind(this)
			});
			VarsModal.load_iframe.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			VarsModal.load_iframe.send();
		
			
	},
	// uzaverkas events
	uzaverkas_events:function(){
		$$('.UzaverkaShow').removeEvents('click');
		$$('.UzaverkaShow').addEvent('click',this.gen_uzaverka.bind(this));
	},
	load_uzaverka_list: function(){
		$('uzaverka_list').addClass('preloader_solo');	
			
		VarsModal.load_iframe = new Request.HTML({
			update:'uzaverka_list',	
			url:'/orders/load_uzaverka_list/',	
			onComplete:function(){
				$('uzaverka_list').removeClass('preloader_solo');	
				this.uzaverkas_events();
			}.bind(this)
		});
		VarsModal.load_iframe.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		VarsModal.load_iframe.send();
	}
	
	/*
	
	*/
	
	
});