/*
---

name: Tree

description: Provides a way to sort and reorder a tree via drag&drop.

authors: Christoph Pojer (@cpojer)

license: MIT-style license.

requires: [Core/Events, Core/Element.Event, Core/Element.Style, Core/Element.Dimensions, Core/Fx.Tween, Core/Element.Delegation, More/Drag.Move, Class-Extras/Class.Binds, Class-Extras/Class.Singleton]

provides: Tree

...
*/

(function(){

this.Tree = new Class({

	Implements: [Options, Events, Class.Binds, Class.Singleton],

	options: {
		//onChange: this.afterDrag,
		indicatorOffset: 0,
		cloneOffset: {x: 16, y: 16},
		cloneOpacity: 0.8,
		controller: 'test',
		prefix: 'test',
		checkDrag: Function.from(true),
		checkDrop: Function.from(true)
	},

	initialize: function(element, options){
		this.setOptions(options);
		/*
		if (!$('tree_item_obal').getElement('ul')){
			var ul = new Element('ul').inject($('tree_item_obal'));
			ul.setProperty('id','tree_item');
		
		}
		*/
		element = this.element = document.id(element);
		
		return this.check(element) || this.setup();
	},

	setup: function(){
		this.indicator = new Element('div.treeIndicator');

		var self = this;
		
		this.handler = function(e){
			self.mousedown(this, e);
		};
		this.change_name();
		this.attach();
		this.create_menu();
		//console.log(this.options.controller);
		//context_menu_render(this.options.controller,this.options.path);
		//context = new ContextMenu({});
	},

	attach: function(){
		if (this.element)
		this.element.addEvent('mousedown:relay(li)', this.handler);
		document.addEvent('mouseup', this.bound('mouseup'));
		
		
		return this;
	},

	detach: function(){
		if (this.element)
		this.element.removeEvent('mousedown:relay(li)', this.handler);
		document.removeEvent('mouseup', this.bound('mouseup'));
		return this;
	},
	
	create_menu: function(){
		
		if ($('lista_add')){
			var ul = this.element;
			var _this = this;		
			$$('.add_root').addEvent("click", function(e){
					new Event(e).stop();
					
					//preloader(true);
					
					if (this.hasClass('add_root')){
						// add new item to root
						var li = new Element('li').inject(ul);
						var href = new Element('a[class=expand]').inject(li);
						href.fade('hide');
						var span = new Element('span[text=Nová položka]').inject(li);
						span.addClass('tree_name');
						
						
						
						new Request.JSON({
							url 		:	"/" + _this.options.controller + "/tree_menu/add_root/",
							onComplete	: 	function(json){
								li.setProperty('id',json.id);
								li.addClass('tree_m');
								_this.change_name();
								span.fireEvent('dblclick');						
										
							}
						}).send();		
							
					}
				});
			
			
		}
		
		
	},
	
context_menu_fce: function(context_url,action,parent_li){
	
	var ils = getUlItems(parent_li.getParent('ul'));
	var index = getIndex($(parent_li));
	var _this = this;
	new Request.JSON({
					url 		:	context_url,
					onComplete	: 	function(json){
						// move down
						if (action == 'move_down'){
							if (json && json.result === true){
								//console.log(parent_li.getParent('ul'));
								parent_li.inject(ils[index+1],'after');		
							} else {
								alert((json.message)?json.message:'Chyba aplikace');
							}
							
							
						}
						// move up
						if (action == 'move_up'){
							if (json && json.result === true){
								parent_li.inject(ils[index-1],'before');		
							} else {
								alert((json.message)?json.message:'Chyba aplikace');
							}
							
							
						}
						if (action == 'smazat') {
							parent_li.dispose();
							//_this.context_events();
							
						}
						
						if (action == 'add') {
							if (parent_li.getElement('.tree')){
								ul = parent_li.getElement('.tree');
							} else {
								ul = new Element('ul').inject(parent_li);
							}
							
							ul.removeClass('none');
							ul.getParent('ul').getElement('span').removeClass('normal').addClass('plus');
							
							if (json && json.result === true){
								
								var li = new Element('li').inject(ul);
								var href = new Element('a[class=expand]').inject(li);
								href.fade('hide');
								var span = new Element('span[text=Nová položka]').inject(li);
								span.addClass('tree_name');
								
								li.setProperty('id',json.id);
								li.addClass('tree_m');
								_this.change_name();
								span.fireEvent('dblclick');
								//context_menu_render(_this.options.controller,_this.options.path);
								
							} else {
								alert((json.message)?json.message:'Chyba aplikace');
							}
						}
							
				
					}
				}).send();

},
	
	change_name: function(){
		var _this = this;
		function hide_name_input(input,name){
			value = input.value;
			
						
			
			this.change_name_input.dispose();
			name.set('text',value);
			name.removeClass('none');
			
			sort_element_id = name.getParent('li').getProperty('id').substring(_this.options.prefix.length);
			
			
			if (value == ''){
				alert('Musíte zadat název kategorie');
				name.fireEvent('dblclick');				
						
			} else {
				
			new Request.JSON({
				url 		:	"/" + _this.options.controller + "/tree_menu/change_menu_item/" + sort_element_id  + "/" + input.value,
				onComplete	: 	function(json){
					preloader(false);
					var cntrl_length = _this.options.controller.length;
					var cntrl = _this.options.controller.substr(0,cntrl_length-1);
					context_menu_render(cntrl,_this.options.path);
							
				}
			}).send();
			}
		}
		//console.log(this.element);
		
		
		
		this.element.getElements('.tree_name').each(function(name){
			//console.log(name.get('text'));
			name.addEvent('dblclick', function(e){
				//name.removeEvents('dblclick');
				//new Event(e).stop();
				//console.log(_this.options.controller);
				value = this.get('text');
				if (value == 'Nová položka') value = '';
				this.change_name_input = change_name_input = new Element('input',{
					"type" : 'text',
					"value" : value,
					"class" : 'change_price'
				}).inject(this,'after');
				
				this.change_name_input.focus();
				
				this.change_name_input.setStyles({
					'border':'1px solid #ccc',
					'width':'200px'
				});
				this.addClass('none');
				
				change_name_tmp = this.change_name_input;
				
				this.change_name_input.addEvents({
					
					blur: function(){
						
							
							hide_name_input(this,name);
					},
					keydown: function(e){
						if (e.key == 'enter'){
								hide_name_input(this,name);
						}
					}
				});
			});
			
			
		});
	},
	
	mousedown: function(element, event){
		event.preventDefault();

		this.padding = (this.element.getElement('li ul li') || this.element.getElement('li')).getLeft() - this.element.getLeft() + this.options.indicatorOffset;
		if (this.collapse === undefined && typeof Collapse != 'undefined')
			this.collapse = this.element.getInstanceOf(Collapse);

		if(!this.options.checkDrag.call(this, element)) return;
		if (this.collapse && Slick.match(event.target, this.collapse.options.selector)) return;

		this.current = element;
		this.clone = element.clone().setStyles({
			left: event.page.x + this.options.cloneOffset.x,
			top: event.page.y + this.options.cloneOffset.y,
			opacity: this.options.cloneOpacity
		}).addClass('drag').inject(document.body);

		this.clone.makeDraggable({
			droppables: this.element.getElements('li span'),
			onLeave: this.bound('hideIndicator'),
			onDrag: this.bound('onDrag'),
			onDrop: this.bound('onDrop')
		}).start(event);
	},

	mouseup: function(){
		if (this.clone) this.clone.destroy();
	},

	onDrag: function(el, event){
		clearTimeout(this.timer);
		if (this.previous) this.previous.fade(1);
		this.previous = null;

		if (!event || !event.target) return;

		var droppable = (event.target.get('tag') == 'li') ? event.target : event.target.getParent('li');
		if (!droppable || this.element == droppable || !this.element.contains(droppable)) return;

		if (this.collapse) this.expandCollapsed(droppable);

		var coords = droppable.getCoordinates(),
			marginTop =  droppable.getStyle('marginTop').toInt(),
			center = coords.top + marginTop + (coords.height / 2),
			isSubnode = (event.page.x > coords.left + this.padding),
			position = {
				x: coords.left + (isSubnode ? this.padding : 0),
				y: coords.top
			};

		var drop;
		if ([droppable, droppable.getParent('li')].contains(this.current)){
			this.drop = {};
		} else if (event.page.y >= center){
			position.y += coords.height;
			drop = {
				target: droppable,
				where: 'after',
				isSubnode: isSubnode
			};
			if (!this.options.checkDrop.call(this, droppable, drop)) return;
			this.setDropTarget(drop);
		} else if (event.page.y < center){
			position.x = coords.left;
			drop = {
				target: droppable,
				where: 'before'
			};
			if (!this.options.checkDrop.call(this, droppable, drop)) return;
			this.setDropTarget(drop);
		}

		if (this.drop.target) this.showIndicator(position);
		else this.hideIndicator();
		
	},

	onDrop: function(el){
		el.destroy();
		this.hideIndicator();

		var drop = this.drop,
			current = this.current;
		if (!drop || !drop.target) return;

		var previous = current.getParent('li');
		if (drop.isSubnode) current.inject(drop.target.getElement('ul') || new Element('ul').inject(drop.target), 'bottom');
		else current.inject(drop.target, drop.where || 'after');

		if (this.collapse){
			if (previous) this.collapse.updateElement(previous);
			this.collapse.updateElement(drop.target);
		}
		
		//console.log(this.element);
		//console.log(drop.target);
		//console.log(drop.isSubnode);
		this.afterDrag(this.element,this.options,drop,current);
		this.fireEvent('change');
	},

	// after drag
	afterDrag:function(sort_list,options,drop,current){
		sort_element = current;
		subnode = drop.isSubnode;
		
		//console.log(sort_element);
		//console.log(sort_list.getElements('li'));
		
		
		if (sort_element.getParent('ul').getParent('ul') && sort_element.getParent('ul').getParent('ul').getParent('ul')) level_tree = 2;
		else if (sort_element.getParent('ul').getParent('ul')) level_tree = 1;
		else  level_tree = 0;
			
		//console.log(level_tree);
		if (level_tree == 2) sort_list2 = current.getParent('ul');
		if (level_tree == 1) sort_list2 = current.getParent('ul');
		else sort_list2 = sort_list;
		
		//console.log(sort_list.getElements('li'));
		//console.log(sort_list2);
		var mIndex 	= sort_list2.getChildren('li').length-1;
		sort_item = sort_list2.getChildren('li')
		
		//console.log(sort_element);
		//console.log(sort_item);
		var cIndex 	= sort_item.indexOf(sort_element);
		
		//console.log(cIndex);
		var index 	= mIndex - cIndex;
		//console.log(mIndex);
		//console.log(cIndex);
		//console.log(index);
		
		/*
		console.log(sort_list);
		console.log(sort_element.parentNode);
		*/
		var parent_id = (sort_list == sort_element.parentNode)?'root':sort_element.parentNode.parentNode.id.substring(this.options.prefix.length);
		sort_element_id = sort_element.getProperty('id').substring(this.options.prefix.length);
		
		var level = 0;
		var depth = sort_element.getParents('ul').length;
		//console.log(depth);
		//if(subnode) depth = depth+1;
		
		//level = depth -1;
		level = depth-1;
		//console.log(level);
		/*
		console.log(sort_element.getParent('ul').getParent('ul'));
		console.log(sort_element.getParent('ul').getParent('ul').getParent('ul'));
		*/
		/*
		if (sort_element.getParent('ul')) level = 0; 
		if (sort_element.getParent('ul').getParent('ul')) level = 1; 
		if (sort_element.getParent('ul').getParent('ul').getParent('ul')) level = 2; 
		
		//console.log(parent_id);
		console.log(level);
		*/
		var _this = this;
		
		new Request.JSON({
			url 		:	"/" + this.options.controller + "/tree_menu/move_item/" + sort_element_id  + "/" + parent_id + "/" + index+"/"+level,
			onComplete	: 	function(){  
			//	_this.li_hover(sort_element);
				//_this.context_events(sort_element);
				//window.location = window.location;
				location.reload();
			}
		}).send();
		
	
	
	},
	
	setDropTarget: function(drop){
		this.drop = drop;
	},

	showIndicator: function(position){
		this.indicator.setStyles({
			left: position.x + this.options.indicatorOffset,
			top: position.y
		}).inject(document.body);
	},

	hideIndicator: function(){
		this.indicator.dispose();
	},

	expandCollapsed: function(element){
		var child = element.getElement('ul');
		if (!child || !this.collapse.isCollapsed(child)) return;

		element.set('tween', {duration: 150}).fade(0.5);
		this.previous = element;
		this.timer = (function(){
			element.fade(1);
			this.collapse.expand(element);
		}).delay(300, this);
	},

	serialize: function(fn, base){
		if (!base) base = this.element;
		if (!fn) fn = function(el){
			return el.get('id');
		};

		var result = {};
		base.getChildren('li').each(function(el){
			var child = el.getElement('ul');
			result[fn(el)] = child ? this.serialize(fn, child) : true;
		}, this);
		
		//console.log(result);
		return result;
	}

});

})();

(function(){
var ContextMenu = this.ContextMenu = new Class({
	
			//implements
			Implements: [Options,Events],
		
			//options
			options: {
				actions: {},
				menu: 'contextmenu',
				stopEvent: true,
				targets: 'body',
				trigger: 'contextmenu',
				offsets: { x:0, y:0 },
				onShow: $empty,
				onHide: $empty,
				onClick: $empty,
				fadeSpeed: 200
			},
			
			//initialization
			initialize: function(options) {
				
				//set options
				this.setOptions(options)
				//option diffs menu
				this.menu = $(this.options.menu);
				this.targets = $$(this.options.targets);
				this.controller = this.options.controller;
				
				//fx
				this.fx = new Fx.Tween(this.menu, { property: 'opacity', duration:this.options.fadeSpeed });
				
				//hide and begin the listener
				this.hide().startListener();
				
				//hide the menu
				this.menu.setStyles({ 'position':'absolute','top':'-900000px', 'display':'block' });
			},
			reinit:function(){
				//console.log(this);
				
				
				//option diffs menu
				this.menu = $(this.options.menu);
				this.targets = $$(this.options.targets);
				//console.log(this.targets);
				this.targets.each(function(el) {
					/* show the menu */
					el.removeEvent(this.options.trigger);
					
				},this);	
				
				this.menu.getElements('a').each(function(item) {
					
					item.removeEvents('click');
					
					//console.log(item);
				});
				this.hide().startListener();
				
				/*
				//fx
				this.fx = new Fx.Tween(this.menu, { property: 'opacity', duration:this.options.fadeSpeed });
				
				//hide and begin the listener
				
				//hide the menu
				this.menu.setStyles({ 'position':'absolute','top':'-900000px', 'display':'block' });
				*/
			},
			
			//get things started
			startListener: function() {
				/* all elemnts */
				this.targets.each(function(el) {
					/* show the menu */
					el.addEvent(this.options.trigger,function(e) {
						//enabled?
						if(!this.options.disabled) {
							//prevent default, if told to
							if(this.options.stopEvent) { e.stop(); }
							//record this as the trigger
							this.options.element = $(el);
							//console.log(this.options.element);
							if (this.options.element){
							this.options.element.getElement('span').addClass('active_content');
							this.options.element.getElement('.tree_name').setStyles({'color':'#ff0000'});
							}
							//position the menu
							this.menu.setStyles({
								top: (e.page.y + this.options.offsets.y),
								left: (e.page.x + this.options.offsets.x),
								position: 'absolute',
								'z-index': '2000'
							});
							//show the menu
							this.show();
						}
					}.bind(this));
				},this);
				
				/* menu items */
				
				this.menu.getElements('a').each(function(item) {
								
					item.addEvent('click',function(e) {
						if(!item.hasClass('disabled')) {
							
							if (this.options.element){
								this.execute(item.get('href').split('#')[1],$(this.options.element));
								
								this.fireEvent('click',[item,e]);
							}
							
						}
					}.bind(this));
				},this);
				
				//hide on body click
				$(document.body).addEvent('click', function() {
					this.hide();
					if (this.options.element){
						this.options.element.getElement('span').removeClass('active_content');
						this.options.element.getElement('.tree_name').setStyles({'color':''});
					}
				}.bind(this));
			},
			
			//show menu
			show: function() {
				this.fx.start(1);
				this.fireEvent('show');
				this.shown = true;
				return this;
			},
			
			//hide the menu
			hide: function() {
				if(this.shown) {
					this.fx.start(0);
					this.fireEvent('hide');
					this.shown = false;
				}
				return this;
			},
			
			//disable an item
			disableItem: function(item) {
				this.menu.getElements('a[href$=' + item + ']').addClass('disabled');
				return this;
			},
			
			//enable an item
			enableItem: function(item) {
				this.menu.getElements('a[href$=' + item + ']').removeClass('disabled');
				return this;
			},
			
			//diable the entire menu
			disable: function() {
				this.options.disabled = true;
				return this;
			},
			
			//enable the entire menu
			enable: function() {
				this.options.disabled = false;
				return this;
			},
			
			//execute an action
			execute: function(action,element) {
				//console.log(this.options.actions);
				//console.log(action);
				//console.log(element);
				//console.log(this.options.actions);
				if(this.options.actions[action]) {
					this.options.actions[action](element,this,this.options.controller);
				}
				return this;
			}
			
		});
})();



function getIndex(li){
			var items = [], list;
			if (li.nodeName != 'LI') alert('Tato metoda lze pouze na LI element');
			list = li.parentNode.getElements('li');
			list.each(function(item,key){
				if (li.parentNode == item.parentNode)
					items[items.length] = item.id
			},this);
			
			if (li.id != undefined)
				return items.indexOf(li.id);
			else 
				return items.length;
}

function getUlItems(ul){
			if (ul.nodeName != 'UL') alert('Tato metoda lze pouze na UL element');
			var items = [];
			var list = ul.getElements('li');
			list.each(function(item){
				if (ul == item.parentNode){
					items[items.length] = item;
				}
			});
			return items;
}

function context_menu_render(controller,path){
//console.log(controller);
//console.log(path);
	//$$('.tree_m').removeEvent('click');
	//$$('.tree_m').getElement('a').removeEvent('click');
	var context = new ContextMenu({
				targets: '.tree_m',
				menu: 'contextmenu',
				controller: controller,
				actions: {
					trash: function(element,ref) {
						if (confirm('Opravdu chcete smazat?')){
						
						el_id = element.getProperty('id').substring(controller.length);
						var context_url = path+'/tree_menu/delete_item/'+el_id;
						var action = 'smazat';
						parent_li = element.getParent('li');
						tree.context_menu_fce(context_url,action,element);
						}
					},
					deaktivovat: function(element,ref) {
						el_id = element.getProperty('id').substring(controller.length);
						var context_url = path+'/tree_menu/status_item/'+el_id+'/0';
						var action = 'deaktivovat';
						element.addClass('tree_inactive');
						tree.context_menu_fce(context_url,action,element);
						
						
					},
					
					aktivovat: function(element,ref) {
						el_id = element.getProperty('id').substring(controller.length);
						var context_url = path+'/tree_menu/status_item/'+el_id+'/1';
						var action = 'aktivovat';
						element.removeClass('tree_inactive');
						tree.context_menu_fce(context_url,action,element);
						
					},
					move_up: function(element,ref) {
						el_id = element.getProperty('id').substring(controller.length);
						var context_url = path+'/tree_menu/move_item_up/'+el_id;
						var action = 'move_up';
						tree.context_menu_fce(context_url,action,element);
						
					},
					move_down: function(element,ref) {
						el_id = element.getProperty('id').substring(controller.length);
						var context_url = path+'/tree_menu/move_item_down/'+el_id;
						var action = 'move_down';
						tree.context_menu_fce(context_url,action,element);
						
					},
					
					add: function(element,ref) {
						el_id = element.getProperty('id').substring(controller.length);
						//console.log(path);
						var context_url = path+'/tree_menu/add_item/'+el_id;
						var action = 'add';
						tree.context_menu_fce(context_url,action,element);
						
					},
					edit: function(element,ref,cntrl) {
						//console.log(cntrl);
						if (cntrl){
							el_id = element.getProperty('id').substring(cntrl.length);
						} else {
							el_id = element.getProperty('id').substring(controller.length);
						}
						//console.log(el_id);
						//console.log(element);
						var context_url = path+'/edit/'+el_id;
						var action = 'edit';
						var modal = new SimpleModal({"width":1000});		 
						modal.show({
						"model":"modal-ajax",
						"title":'Editovat',
						"param":{
							"url":context_url
						}
						});	
					},
					
					
					
				},
				offsets: { x: 2, y: 2 }
});
context.reinit();
}