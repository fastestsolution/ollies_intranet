(function () {

Object.append(Element.NativeEvents, {
	dragenter: 2, dragleave: 2, dragover: 2, dragend: 2, drop: 2
});

var FstUploader = this.FstUploader = new Class({
	Implements:[Options,Events],
	fileList: false,
	fileNameList: [],
	options: {
		fileList : true,
		multi : true,
		fileName: false,
		count_file : 20,
		file_ext : ['jpg','doc','pdf'],
		uploadPath: '/fst_upload/',
	},
	
	/* initialization */
	initialize: function(element,options) {
		this.formData = new FormData();
		
		this.setOptions(options);
		this.element = element;
		this.renderFilesData();
		
		element.addEvent('change',function(e){
			//if (this.checkFileName()){
				this.files = e.target.files;
				this.createUploadList();
				//this.createForm();
				//this.uploadAjax();
			//}
		}.bind(this));
	},
	
	createUploadList: function(file){
		if (this.options.fileList){
		
		if (!this.fileList)
		this.fileList = new Element('ul',{'class':'fileList'}).inject(this.element,'after');
		
		if (!this.uploadButton){
		fileItemButton = new Element('li',{'class':'fileItem'}).inject(this.fileList);
		this.uploadButton = new Element('input',{'type':'button','class':'btn btn-primary','value':'Nahrát soubory'}).inject(fileItemButton);
			
			this.uploadButton.addEvent('click',function(e){
				if (this.checkFileName()){
					this.createForm();
					
					this.uploadAjax();
				}
			}.bind(this));		
		}
		
		for (var i = 0; i < this.files.length; i++) {
				
			var reader = new FileReader();

		  // Closure to capture the file information.
		  reader.onload = (function(theFile) {
			//return function(e) {
				fileItem = new Element('li',{'class':'fileItem'}).inject(fileItemButton,'before');
				
				fileItem.set('text',escape(theFile.name));
				if (this.options.fileName){
					input = new Element('input',{'type':'text','placeholder':'Popis souboru','class':'text'});
					input.inject(fileItem);
					this.fileNameList.push(input);
				}
			//};
		  }.bind(this))(this.files[i]);

		  // Read in the image file as a data URL.
		  reader.readAsDataURL(this.files[i]);
		}
		
		}
		
	},
	
	checkFileName: function(){
		result = true;
		this.fileNameList.each(function(item){
			if (item.value == ''){
				FstError('Musíte zadat název souboru');
				result = false;
			} else {
			}
		}.bind(this));
		return result;
		/*	
		if (this.options.fileName && this.options.fileName.value == ''){
			FstError('Musíte zadat název souboru');
			this.element.value = '';
			return false;
		} else {
			return true;
		}
		*/
	},
	
	createForm: function(){
		for (var i = 0; i < this.files.length; i++) {
			var file = this.files[i];
			
			// Add the file to the request.
			//this.formData.append('files[]', file);
			
			fileName = file.name;
			this.fileNameList.each(function(item){
				//file.ownName = item.value;
				this.formData.append('filesName[]', item.value);
			}.bind(this));
			this.formData.append('files[]', file, fileName);
		}
	},
	
	uploadAjax: function(){
		var xhr = new XMLHttpRequest();
		button_preloader(this.uploadButton);
        // Open the connection.
        xhr.open('POST', this.options.uploadPath, true);


        // Set up a handler for when the request finishes.
        xhr.onload = function (result) {
          button_preloader(this.uploadButton);
		  json = JSON.decode(result.target.response);
		  console.log(json);
		  if (xhr.status === 200) {
            this.data_list = JSON.decode(this.options.filesData.value);
			if (!this.data_list){
				this.data_list = [];
			}
			console.log(json.fileList);
			
			json.fileList.each(function(item){
				this.data_list.push(item);
			}.bind(this));
			
			//console.log(this.data_list);
			this.options.filesData.value = JSON.encode(this.data_list);
			
			this.clearValues();
			
			this.renderFilesData();
			//statusDiv.innerHTML = 'The file uploaded successfully.......';
          } else {
            //statusDiv.innerHTML = 'An error occurred while uploading the file. Try again';
          }
        }.bind(this);

        // Send the Data.
        xhr.send(this.formData);
	},
	
	clearValues: function(){
		this.fileNameList = [];
		this.ulList.destroy();
		this.ulList= null;
		if (this.fileList){
			this.fileList.destroy();
			this.fileList = null;
		}
		this.element.value = '';
			
	},
	
	renderFilesData: function(){
		this.data_list = JSON.decode(this.options.filesData.value);
		if (!this.ulList){
			this.ulList = new Element('ul',{'class':'fileList'}).inject(this.options.filesData,'before');
		}
		if (this.data_list){
			this.data_list.each(function(item,k){
				li = new Element('li',{}).set('html','<a href="'+item.path+item.file+'" target="_blank">'+item.fileName+'</a>').inject(this.ulList);
				deleteIco = new Element('span',{'class':'fa fa-close'}).inject(li);
				deleteIco.addEvent('click',function(e){
					this.deleteItem(k);
				}.bind(this));
			}.bind(this));
		} else {
			this.ulList.empty();
		}
	},
	
	deleteItem: function(k){
		if (confirm('Opravdu smazat?')){
			if (!this.deleteList){
				this.deleteList = [];
			}
			this.deleteList.push(this.data_list[k]);
			this.options.filesDataDelete.value = JSON.encode(this.deleteList);
			
			this.data_list.splice(k,1);
			//console.log(this.data_list);
			if (this.data_list.length > 0){
				this.options.filesData.value = JSON.encode(this.data_list);
			} else {
				this.options.filesData.value = '';
			}
			this.clearValues();
			this.renderFilesData();
			//console.log(k);
		}
	},
	
	
});

})();	