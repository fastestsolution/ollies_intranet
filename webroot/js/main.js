
function viewIndex(){
	
	if ($('topActions')){
		$('topActions').getElements('.btn').addEvent('click',function(e){
			pagePreloader();
			if (e.target.get('data-type') == 'ajax'){
				e.stop();
				var button = e.target;
				button_preloader(button);
						
				var saveReg = new Request.JSON({
					url:e.target.href,
					onError: function(json){
						pagePreloader(true);
						button_preloader(button);
                    },
					onComplete: function(json){
						pagePreloader(true);
						button_preloader(button);
						if (json.result == true){
							FstAlert(json.message);
							
						} else {
							FstError(json.message);
						}
					}
				});
				saveReg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				saveReg.send();
			}
		}.bind(this));
	}
	
	if ($('tableItems')){
		$('tableItems').getElements('.btn,.fa').addEvent('click',function(e){
			
			if (!e.target) el = e;
			else el = e.target;
			
			if (el.get('data-type') == 'ajax'){
				pagePreloader();
				e.stop();
				var saveReg = new Request.JSON({
					url:el.href,
					onError: function(json){
						pagePreloader(true);
						//button_preloader(button);
                    },
					onComplete: function(json){
						pagePreloader(true);
						//button_preloader(button);
						if (json.result == true){
							if (json.message)
							FstAlert(json.message);
							if (json.redirect){
								if (json.redirect == 'self'){
									window.location = window.location;
								}
							}
							
						} else {
							FstError(json.message);
						}
					}
				});
				saveReg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				saveReg.send();
			} else {

				if (el.hasClass('btn-danger')){
					e.stop();
					if (confirm('Opravdu smazat?')){
						pagePreloader();
						
						window.location = el.href;
					}
				} else {
					window.location = el.href;
					
				}
			}
		});
		// dblClickTable();	
		
	}
}


function sendUpdate(){
	if ($('sendUpdate')){
		$('sendUpdate').addEvent('click',function(e){
			if (confirm('Opravdu odeslat aktualizace na pokladny?')){
				var button = $('sendUpdate'); 
				button_preloader(button);

				var saveReg = new Request.JSON({
					url:'/products/sendUpdate/',
					onError: function(json){
						button_preloader(button);
                    },
					onComplete: function(json){
						// pagePreloader(true);
						button_preloader(button);
						if (json.result == true){
							if (json.message)
							FstAlert(json.message);
							
						} else {
							FstError(json.message);
						}
					}
				});
				saveReg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				saveReg.send();
			}
		});
	}
}

function dblClickTable(){
	// console.log('aa',$('tableItems').getElements('td'));
	
	$('tableItems').getElements('td').removeEvents('dblclick');
	$('tableItems').getElements('td').addEvent('dblclick',function(e){
		// console.log(e.target.getParent('tr').getElement('.fa-edit')); 
		//alert('');
		if (e.target.getParent('tr').getElement('.fa-edit')){
			pagePreloader();
			e.target.getParent('tr').getElement('.fa-edit').fireEvent('click',e.target.getParent('tr').getElement('.fa-edit'));
		}
	});
}


function login(){	
	window.addEvent('domready',function(){
		(function(){
		$('username').focus();
		}).delay(1000);
		// alert('');
		$('LoginButton').addEvent('click',function(e){
			e.stop();
			button_preloader($('LoginButton'));
						
				var saveReg = new Request.JSON({
					url:$('FormLogin').action,
					onComplete: function(json){
					
						button_preloader($('LoginButton'));
						if (json.result == true){
							FstAlert(json.message);
							if (document.getElementById("requestedURL").value !== undefined && document.getElementById("requestedURL").value !== "") {
								window.location = document.getElementById("requestedURL").value;

								var counter = 0;
								var interval = setInterval(function() {
									counter++;
									if (counter == 3) {
										window.location = "/";
										clearInterval(interval);
									}
								}, 1000);
							} else {
								window.location = '/';
							}							
						} else {
							FstError(json.message);
						}
					}
				});
				saveReg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				saveReg.post($('FormLogin'));
	
		});
	});
	}
function historyComplete(){
	// alert('');
	// hideBranches();
	showBranches();
	pagePreloader(true);
	viewIndex();
	dblClickTable();
}	

// schovani / zobrazni pobocky
function hideBranches(){
	// console.log($('side-bar'));
	
	var branch_list_all = {};
	branch_list_all = $('branches_list_foo').getElements('li').get('data-class');
	if (localStorage.getItem('branch_list_show')){
		branch_list_show = JSON.decode(localStorage.getItem('branch_list_show'));
	} else {
		branch_list_show = {};
	}
	// .log(branch_list_show);
    branch_list_all.each(function(item){
        if (Object.getLength(branch_list_show)>0){
            $$('.'+item).addClass('none');
        } else {
        $$('.'+item).removeClass('none');
    
        }
    });

    // zobrazeni vybranne pobocky
    Object.each(branch_list_show,function(item){
        $$('.'+item).removeClass('none');
        $('branches_list_foo').getElement('input.'+item).setProperty('checked','checked');
    });
}

// prepinani pobocek
function showBranches(){
    window.addEvent('domready',function(){
		var height = window.getScrollSize().y; 
		//console.log(height);
		if ($('side-bar') && window.getSize().x > 500)
		$('side-bar').setStyle('height',height - 60);

		if ($('branches_list_foo')){
		var branch_list_show = {};
        var branch_list_all = {};
        branch_list_all = $('branches_list_foo').getElements('li').get('data-class');
        
        if (localStorage.getItem('branch_list_show')){
            branch_list_show = JSON.decode(localStorage.getItem('branch_list_show'));
        }

        

        hideBranches();
		
        if (Object.getLength(branch_list_show)==0){
            $('branches_list_foo').getElements('input.checkbox').setProperty('checked','checked');
            branch_list_all.each(function(item){
                branch_list_show[item] = item;
            });

		}
		
		$('removeAll').addEvent('click',function(){
			branch_list_show = {};
			$('branches_list_foo').getElements('input.checkbox').removeProperty('checked', 'checked');
		});
		
        $('branches_list_foo').getElements('input.checkbox').addEvent('click',function(e){
			//console.log(branch_list_all);
			//console.log('a');
			if (e.target.hasClass('fst_h')){
				this.pagePreloader();
			}

            if (e.target.checked){
                branch_list_show[e.target.get('data-col')] = e.target.get('data-col');
            } else {
                delete branch_list_show[e.target.get('data-col')];
            }
			//console.log(branch_list_show);
			 
            if (Object.getLength(branch_list_show)>0){
                localStorage.setItem('branch_list_show',JSON.encode(branch_list_show));
            } else {
                localStorage.removeItem('branch_list_show');
            }
            // schovani / zobrazeni vsech vetvi
            hideBranches();   

           
        }.bind(this));
		}
	});
	
    
}


function pagePreloader(hide=false){
	if (hide){
		$('page_preloader').addClass('none');
	} else {

		$('page_preloader').removeClass('none');
	}
}

function modalWindow(){
	console.log('run');
    $$('.tabs').each(function(tabs){
        var li_list = tabs.getElement('.modal_tabs').getElements('li');
        var tab_list = tabs.getElement('.modal_contents').getElements('.modal_tab');

        // default active first tab
        li_list[0].addClass('active');

        tab_list.each(function(tab,k){
            if (k>0){
                tab.addClass('none');
            }
        });

        // event tabs li
        li_list.addEvent('click',function(e){
            e.stop();
            li_list.removeClass('active');
            e.target.addClass('active');

            var index = li_list.indexOf(e.target);
            tab_list.addClass('none');
            tab_list[index].removeClass('none');
            //console.log($('modal_in').getSize().y);

        });
    });
}

function price(price){
	symbol_before 	= '';
	kurz			= '1';
	count			= '1';
	decimal			= 2;
	symbol_after	= ''; 
		
	price = (price/kurz)* count;		
	return symbol_before + number_format(price, decimal, '.', ' ') + symbol_after;	
}


function saveData(){
	window.addEvent('domready',function(){
		$$('.select_all_fieldset').addEvent('click',function(e){
			e.stop();
			parent = this.getParent('fieldset');
			if (!this.hasClass('check')){
				this.addClass('check');
				parent.getElements('.checkbox').setProperty('checked','checked');
			} else {
				this.removeClass('check');
				parent.getElements('.checkbox').removeProperty('checked','checked');
				
			}
		});
		
		
		$('BackButton').addEvent('click',function(e){
			button_preloader($('BackButton'));
			window.location = e.target.get('data-path');
		});
		$('FormEdit').getElements('.float, .integer').inputLimit();
		
		$('SaveButton').addEvent('click',function(e){
			e.stop();
			button_preloader($('SaveButton'));
						
				var saveReg = new Request.JSON({
					url:$('FormEdit').action,
					onError: function(json){
                        button_preloader($('SaveButton'));
                    },
					onComplete: function(json){

						button_preloader($('SaveButton'));
						if (json.result == true){
							FstAlert(json.message);
							(function(){
							window.location = e.target.get('data-path');
							}).delay(1000);
							
						} else {
							FstError(json.message);
						}
					}
				});
				saveReg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				saveReg.post($('FormEdit'));

		});
	});	
}

function FstAlert(value,modal){
	
	//console.log($(window));
	//console.log($(parent.window));
	new mBox.Notice({
		type: 'ok',
		target:((modal)?$(window.parent.document):$(window)),
		inject:((modal)?window.parent.document.body:null),
		fadeDuration: 1500,
		position: {
				x: 'center',
				y: 'center'
			},
		content: value
	});
}
function FstError(value,modal){
	new mBox.Notice({
		type: 'error',
		target:((modal)?$(window.parent.document):$(window)),
		inject:((modal)?window.parent.document.body:null),
		fadeDuration: 1500,
		position: {
				x: 'center',
				y: 'center'
			},
		content: value
	});
}
function date_picker(){
	//window.addEvent('domready', function() {
	// alert('');
	if ($$('.date_range').length>0){
	var picker = new Picker.Date.Range($$('.date_range'), {
		timePicker: false,
		columns: 3,
		months: lang.mesice, 
		months_title: lang.mesice, 
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
		shortDate: '%Y-%m-%d',
		dateOrder: ['year', 'month','date'],
		shortTime: '%H:%M:%S',
		format:'%Y-%m-%d',
		allowEmpty:true,
		positionOffset: {x: 5, y: 0}
	});
	}
	if ($$('.date').length>0){
	var picker = new Picker.Date($$('.date'), {
		timePicker: false,
		columns: 1,
		//months: lang[CURRENT_LANGUAGE].mesice, 
		//'abbr',{
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
		//},
		//shortDate: '%d-%m-%Y',
		//dateOrder: ['date', 'month','year`'],
		//shortTime: '%H:%M:%S',
		format:'%d.%m.%Y',
		allowEmpty:true,
		positionOffset: {x: 5, y: 0}
	});
	
	}
	if ($$('.date_time').length>0){
	$$('.date_time').each(function(item){
		if (item.value == '0000-00-00 00:00')
			item.value = '';
	});
	var picker = new Picker.Date($$('.date_time'), {
		
		timePicker: true,
		columns: 1,
		months: lang.mesice, 
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
			//months_title: ['ja', 'fe', 'ma', 'ap', 'me', 'jn', 'jl', 'au', 'ok', 'se', 'no', 'de'],
			//days_abbr: ['zon', 'maa', 'din', 'woe', 'don', 'vri', 'zat'],
		
		
		// shortDate: '%Y-%m-%d',
		// dateOrder: ['year', 'month','date'],
		// shortTime: '%H:%M:%S',
		format:'%d.%m.%Y %H:%m',
		allowEmpty:true,
		
		//months_abbr: ['ja', 'fe', 'ma', 'ap', 'me', 'jn', 'jl', 'au', 'ok', 'se', 'no', 'de'],
		
		positionOffset: {x: 5, y: 0}
	});
	}
	
	//});
}

function initPagePreloaderElements(){
	$$('.pagePre').addEvent('click',function(e){
		pagePreloader();
	});
	
}

window.addEvent('domready', function() {
	date_picker();
	initPagePreloaderElements();
  
});