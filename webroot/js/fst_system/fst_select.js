/*
Fst History
created Jakub Tyson Fastest Solution 
copyright 2016
*/
var FstSelect = this.FstSelect = new Class({
	Implements:[Options,Events],
	options: {
		'element_class':'fst_select'
	},
	// init fce
	initialize:function(element,options){
		this.element = element;
		this.setOptions(options);
		this.init_elements();
		this.init_events();
		//console.log(window.getDimensions());
	},
	
	init_elements: function(){
		this.new_item = new Element('option',{'value':-1}).set('text','Nová položka').inject(this.element);
		//this.init_events();	
	},
	
	init_events: function(){
		//this.new_item.addEvent('click',this.create_input.bind(this));
		this.element.addEvent('change',function(e){
			if(e.target.value == '-1')
			this.create_input();
		}.bind(this));
	},
	
	create_input: function(e){
		this.element.addClass('none');
		this.new_input = new Element('input',{'type':'text'}).inject(this.element,'after');
		this.new_input.focus();
		
		this.new_input.addEvent('change',this.input_change.bind(this));
	},
	
	input_change: function(){
		if (this.new_input.value == ''){
			this.element.removeClass('none');
			this.new_input.destroy();
			
		} else {
			this.save_db();
		}
	},
	
	save_db: function(){
		Vars.load_reg = new Request.JSON({
			url:this.element.get('data-url')+this.new_input.value,	
			onComplete: (Vars.delete_req_complete = function(json){
				if (json.r == true){
					this.create_option_after_save(json);
				} else {
					FstError(json.m);
				}		
				json = null;
			}).bind(this)
		});
		Vars.load_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		Vars.load_reg.send();
	},
	
	create_option_after_save: function(json){
		
		this.element.removeClass('none');
		this.new_input.destroy();
		this.new_item_json = new Element('option',{'value':json.id,'selected':'selected'}).set('text',json.name).inject(this.new_item,'before');
		this.element.value = json.id;
	},
	
	
	
	
	
});

//})();	
window.addEvent('domready', function () {
	$$('.fst_select').each(function(item){
		window.fstselect = new FstSelect(item);
	});
	//var s = $('body').getDimensions({computeSize: true});
	//console.log(s);
	//window.fstsystem.alert_fce('test');
});

