<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$zip = new ZipArchive;

if ($zip->open('../vendor.zip') === TRUE) {
    $zip->extractTo('../');
    $zip->close();
	echo "done unzip vendor\n"; 
} else {
	die('fail unzip vendor.zip');
}

if ($zip->open('../production.zip') === TRUE) {
    $zip->extractTo('../');
    $zip->close();
    
	echo "done unzip production\n";
} else {
	die('fail unzip production.zip');
}
die('end unzip');
?>