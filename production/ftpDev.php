<?php
$subdomain = 'ollies-intr';
$domain = 'fastestdev.cz';

$config = [
	'my site' => [
		// 'remote' => 'ftp://user:secretpassword@ftp.example.com/directory',
		// 'remote' => 'sftp://109.123.216.45tst',
		'remote' => $_ENV['FTP_HOST_DEV'].$_ENV['FTP_PATH_DEV'],
		'local' => '../',
		'test' => false,
		'ignore' => '
			/deployment.*
			/log
			tmp/bake/*
			tmp/cache/*
			tmp/cache/models/*
			tmp/cache/persistent/*
			tmp/sessions/*
			tmp/tests/*
			/vendor/*
			/production/*
			/node_modules
			/.git
			!temp/.htaccess
			*/tests
			/uploaded
			/zaloha
			composer.json
			composer.lock
        ',    
        'user'=> $_ENV['FTP_USER_DEV'],
        'password'=> $_ENV['FTP_PASSWORD_DEV'],

		// 'include' => '
        // 	/app
        // 	/app/*
        // 	/index.php
        // ',

		'allowDelete' => true,
		'before' => [
			function (Deployment\Server $server, Deployment\Logger $logger, Deployment\Deployer $deployer) {
				$logger->log('Spusteni deploing na DEV server ('.$subdomain.'.'.$domain.')!');
			},
		],
		'afterUpload' => [
			// 'http://example.com/deployment.php?afterUpload'
		],
		'after' => [
            'remote: chmod 0777 logs',
           // 'remote: chmod 0777 tmp/bake',
            'remote: chmod 0777 tmp/cache',
            //'remote: chmod 0777 tmp/cache/models',
            //'remote: chmod 0777 tmp/cache/persistent',
            'remote: chmod 0777 tmp/sessions',
			'remote: chmod 0777 tmp/',
			'remote: chmod 0777 vendor',
			'remote: chmod 0777 production',
			'http://'.$subdomain.'.'.$domain.'/unzip.php',
			'remote: chmod 0755 vendor',
		],
		'purge' => [
			 //'tmp/cache',
			 /*'tmp/cache/persistent',
			 'tmp/cache/models',*/
		],
		// 'preprocess' => ['combined.js', 'combined.css'],
	],

	'tempDir' => __DIR__ . '/temp',
	'colors' => true,
];

return $config;