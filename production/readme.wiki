= Release FTP bitbucket pipelines =

Scripty pro nahravani pres bitbucket pipelines. Jedna se o automaticky deploying na DEV/PROD server pres rozhrani bitbucket.org Do projektu je nutne zkopirovat nasledujici soubory a adresare:

<pre>bitbucket-pipelines.yml
package.json
unzip.php nakopirovat do webroot projektu
/migrations
/production
/config/db_config_default.php
/config/app.php z toho jen upravit pripojeni k databazi do sveho projektu</pre>
== Ukazka app.php ==

na zacatek souboru

<pre>$dbConfig = require_once('db_config.php');
if (!isset($dbConfig['SQL_HOST'])){
    throw new Exception('Missing DB config');
}</pre>
pripojeni k DB

<pre> 'Datasources' =&gt; [
        'default' =&gt; [
            'className' =&gt; 'Cake\Database\Connection',
            'driver' =&gt; 'Cake\Database\Driver\Mysql',
            'persistent' =&gt; false,
            'host' =&gt; $dbConfig['SQL_HOST'],
            'username' =&gt; $dbConfig['SQL_USERNAME'],
            'password' =&gt; $dbConfig['SQL_PASSWORD'],
            'database' =&gt; $dbConfig['SQL_DATABASE'],
            'encoding' =&gt; 'utf8',
            'timezone' =&gt; 'UTC',
            'cacheMetadata' =&gt; true,
            'log' =&gt; false,</pre>
== Nezapomenout! ==

do '''webroot''' projektu nakopirovat '''unzip.php'''

= Zalozeni noveho projektu =

'''!!! Pozor !!!''' Hesla na bitbucket zaskrtnout se zamkem at nejsou citelne

# Upravit package.json SLACK NOTIFIKACI zmenit nazev projektu
# Na fastestdev.cz vytvorit slozku kde bude projekt a nastavit, '''pripojit se na fastestdev.cz pres FTP'''
# Nastavit v /production/ftpDev.php a /production/ftpProd.php cesty po nahrani na chmod etc.
# Spustit '''npm install''' v root projektu
# V repositari povolit Pipelines: https://nimb.ws/nNOdos
# V repositari nutno nastavit promenne: https://nimb.ws/oXvKGn

'''Pozor''' pokud zakaznik ma omezeni na produkci na IP napr na SQL nepojede pipeline

Celkem musi byt 18 zaznamu

<pre>SQL_HOST_DEV_PIPELINE url/IP  vzdaleneho serveru
SQL_HOST_DEV localhost
SQL_NAME_DEV
SQL_DB_DEV
SQL_PASSWORD_DEV

SQL_HOST_PROD_PIPELINE url/IP vzdaleneho serveru
SQL_HOST_PROD localhost
SQL_NAME_PROD
SQL_DB_PROD
SQL_PASSWORD_PROD

FTP_USER_DEV 
FTP_PASSWORD_DEV
FTP_HOST_DEV sftp://109.123.216.45
FTP_PATH_DEV /subdomena

FTP_USER_PROD
FTP_PASSWORD_PROD
FTP_HOST_PROD ftp://ftp.domena.cz
FTP_PATH_PROD /</pre>
= Deploy na DEV server =

Pipeline posila na DEV prostredi vse co se nachazi v master branch

# Nutno jednou spustit pipeline rucne vybrat moznost: '''custom:prepareFilesDev''' / '''custom:prepareFilesProd''', at nakopiruje cakephp a potrebne zavislosti https://nimb.ws/sfAN1W , https://nimb.ws/0VFMSX
# Pri kazdem provedeni git push na '''master''' se automaticky spusti build na DEV prostredi (lze upravit v bitbucket_pipeline.yml)

= Release na produkcni server =

# Pokud chcete spustit '''release aktualni verze do produkce''' je nutno zadat nasledujici prikaz v local PC, ktery automaticky navysi cislo verze v package.json a odesle na git s tagem verze, ktery automaticky spusti pipeline pro deploy na produkcni server.

<pre>npm run release</pre>
= Uprava databaze MySQL =

'''!!! Databazi nevytvaret pres PhpMyAdmin''' Jednotlive migrace jsou ve zvlastnich souborech, kazdnou zmenu v DB je nutno vytvorit dalsi soubor s migraci.

Pokud chcete zanest novy upravu databaze je nutno pouzivat prikaz

<pre>npm run newMigration -n NazevMigrace</pre>
# nasledne v adresari '''/migrations/db/migrations/''' upravit vytvoreny soubor dle dokumentace http://docs.phinx.org/en/latest/migrations.html
# spustit prikaz, ktery aplikuje migraci do Vasi local Mysql databaze

<pre>npm run applyMigration</pre>
= Tips / Troubleshoot =

=== debugovani pipeline v local PC ===

<pre>npm install -g bbrun</pre>
=== Testovani validity pipeline ===

https://bitbucket-pipelines.prod.public.atl-paas.net/validator

