<?php
    $dbConfig = require_once('./config/db_config.php');
    // print_r($_SERVER['LOGNAME']);
    if (!isset($dbConfig['SQL_HOST'])){
        throw new Exception('Missing DB config');
    }
    
    if ($_SERVER['LOGNAME'] == 'vagrant'){
        $dbConfig['SQL_HOST'] = '10.0.2.2';
    }
    // print_r($dbConfig);die();

   
    $config = [
        "paths" => [
            "migrations" => "./migrations/db/migrations",
            "seeds" =>  "./migrations/db/seeds"
        ],
        "environments" => [
            "default_migration_table" => "phinxlog",
            "default_database" => "local",
            "local" => [
                "adapter" => "mysql",
                "host" => $dbConfig['SQL_HOST'],
                "name" => $dbConfig['SQL_DATABASE'],
                "user" => $dbConfig['SQL_USERNAME'],
                "pass" => $dbConfig['SQL_PASSWORD'],
                "port" => 3306
            ]
        ]
    ];
    // print_r($config);die();
    return $config;
